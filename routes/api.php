
<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix'=>'v1','namespace'=>'Api'],function(){
    Route::post('patient_request_radiology','PatientController@patient_request_radiology');
    Route::post('notfiy','RadiologyRequestController@notig');
    
    
    Route::post('upload_radiology_prescription','PatientController@upload_radiology_prescription');
    Route::post('patient_get_radiology_response','PatientController@get_radiology_response');
    Route::post('patient_answer_radiology_question','PatientController@answer_radiology_question');
    Route::post('patient_answer_radiology_question_v2','PatientController@answer_radiology_question_v2');
    Route::post('get_radiology_detail','PatientController@get_radiology_detail_v2');
    Route::post('confirm_radiology_request','RadiologyRequestController@confirm_radiology_request');
    Route::post('cancel_radiology_request','RadiologyRequestController@cancel_radiology_request');
    Route::post('rate_radiology_request','RadiologyRequestController@rate_radiology_request');
    
    Route::post('get_labtest_tests_types','LabTestController@get_labtest_tests_types');
    Route::post('get_labtest_centers','LabTestController@get_labtest_centers');
    Route::post('paitent_request_labtest','LabTestController@paitent_request_labtest');
    Route::post('paitent_labtest_insurance','LabTestController@paitent_labtest_insurance');
    Route::post('upload_service_image','LabTestController@upload_lab_prescription');
    Route::post('upload_labtest_prescription','LabTestController@upload_lab_prescription');
    Route::post('upload_labtest_insurance','LabTestController@upload_lab_prescription');
    Route::post('upload_labtest_national_id','LabTestController@upload_lab_prescription');
    Route::post('get_lab_response','LabTestController@get_lab_response');
    Route::post('answer_lab_question','LabTestController@answer_lab_question');
    Route::post('paitent_get_lab_response','LabTestController@get_lab_detail');
    Route::post('cancel_lab_request','LabTestController@cancel_lab_request');
    Route::post('rate_lab_request','LabTestController@rate_lab_request');
    Route::post('confirm_labtest_request','LabTestController@confirm_lab_request');
    // Pharmacy //
    
    Route::post('paitent_order_pharmacy','PharmacyController@paitent_order_pharmacy');
    Route::post('patient_order_prescription_pharmacy','PharmacyController@patient_order_prescription_pharmacy');
    Route::post('upload_pharmacy_prescription','PharmacyController@upload_pharmacy_prescription');
    
    Route::post('refund_pharmacy_request','PharmacyController@refund_pharmacy_request');
    Route::post('cancel_pharmacy_request','PharmacyController@cancel_pharmacy_request');
    Route::post('confirm_pharmacy_request','PharmacyController@confirm_pharmacy_request');
    Route::post('paitent_get_pharmacy_resonse','PharmacyController@paitent_get_pharmacy_resonse');
    Route::post('update_midicine_list','PharmacyController@update_midicine_list');
    Route::post('rate_pharmacy_service','PharmacyController@rate_pharmacy_service');
    /*Route::post('get_labtest_centers','PharmacyController@get_labtest_centers');
    Route::post('paitent_request_labtest','PharmacyController@paitent_request_labtest');
    Route::post('paitent_labtest_insurance','PharmacyController@paitent_labtest_insurance');
    Route::post('upload_service_image','PharmacyController@upload_lab_prescription');
    Route::post('upload_labtest_prescription','PharmacyController@upload_lab_prescription');
    Route::post('upload_labtest_insurance','PharmacyController@upload_lab_prescription');
    Route::post('upload_labtest_national_id','PharmacyController@upload_lab_prescription');
    Route::post('get_lab_response','PharmacyController@get_lab_response');
    Route::post('answer_lab_question','PharmacyController@answer_lab_question');
    Route::post('paitent_get_lab_response','PharmacyController@get_lab_detail');
    Route::post('cancel_lab_request','PharmacyController@cancel_lab_request');
    Route::post('rate_lab_request','PharmacyController@rate_lab_request');
    Route::post('confirm_labtest_request','PharmacyController@confirm_lab_request');
    */
    
    // Pharmacy //
    Route::post('create_room','ChatRoomController@create');
    Route::post('close_room','ChatRoomController@close');

    Route::post('doctor_joined_room','ChatRoomController@doctor_joined_room');
    Route::post('patient_joined_room','ChatRoomController@patient_joined_room');

    Route::post('doctor_left_room','ChatRoomController@doctor_left_room');
    Route::post('patient_left_room','ChatRoomController@patient_left_room');

    Route::post('doctor_is_typing','ChatRoomController@doctor_is_typing');
    Route::post('patient_is_typing','ChatRoomController@patient_is_typing');

    Route::post('doctor_stop_typing','ChatRoomController@doctor_stop_typing');
    Route::post('patient_stop_typing','ChatRoomController@patient_stop_typing');

    Route::post('get_room_doctor_status','ChatRoomController@get_doctor_status');
    Route::post('get_room_patient_status','ChatRoomController@get_patient_status');


    Route::post('doctor_send_msg','RoomMessageController@doctor_send_msg');
    Route::post('patient_send_msg','RoomMessageController@patient_send_msg');


    Route::post('get_room_details','ChatRoomController@get_room_details');

    Route::post('get_room_status','ChatRoomController@get_room_status');

    Route::post('get_patient_rooms','ChatRoomController@get_patient_rooms');
    Route::post('get_doctor_rooms','ChatRoomController@get_doctor_rooms');

    Route::post('get_patient_active_rooms','ChatRoomController@get_patient_active_rooms');
    Route::post('get_doctor_active_rooms','ChatRoomController@get_doctor_active_rooms');

    Route::post('pusher_auth','PusherController@auth');

    Route::post('/webhooks','WebhookController@webhook');

    
    
});
Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');


