<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('reportPreview', 'center_reportController@reportPreview');
Route::group(['middleware'=>'auth'],function(){
    Route::get('/', function () {
        return view('home');
    })->name('home');
//    Route::any('user/create','UserController@create')->name('user.store');
//    Route::get('user','UserController@index')->name('user.index');
    Route::resource('user', 'UserController');
    Route::resource('patientrequests', 'PatientRequestController');
    Route::any('confirm', 'PatientRequestController@confirmRequestEdit');
    Route::any('updateconfirm', 'PatientRequestController@confirmRequestUpdate');
    Route::any('result', 'PatientRequestController@resultCreate');
    Route::any('resultStore', 'PatientRequestController@resultStore');
    Route::resource('radiologytypecategories','RadiologyTypeCategoryController');
    Route::resource('radiologytypes','RadiologyTypeController');
    Route::resource('centers','CentersController');
    /* Lab test*/
    Route::resource('labrequests', 'RequestController');
    Route::any('updateRefuserequest','RequestController@updateRefuse')->name('requests.updateRefuserequest');
    Route::any('sortnamerequest','RequestController@sortname')->name('requests.sortnamerequest');
    Route::any('sortdaterequest','RequestController@sortdate')->name('requests.sortdaterequest');
    Route::any('sortlocationrequest','RequestController@sortlocation')->name('requests.sortlocationrequest');
    Route::any('sortstatusrequest','RequestController@sortstatus')->name('requests.sortstatusrequest');
    Route::any('storerequest','RequestController@sortstatus')->name('requests.storerequest');
    
    /*End Lab test*/
    Route::any('/reportreportAjax','center_reportController@reportAjax');

    Route::resource('', 'HomeController');
    Route::resource('centerresponses', 'CenterResponseController');
    Route::any('updateRefuse','PatientRequestController@updateRefuse')->name('patientrequests.updateRefuse');
    Route::any('sortname','PatientRequestController@sortname')->name('patientrequests.sortname');
    Route::any('sortdate','PatientRequestController@sortdate')->name('patientrequests.sortdate');
    Route::any('sortlocation','PatientRequestController@sortlocation')->name('patientrequests.sortlocation');
    Route::any('sortstatus','PatientRequestController@sortstatus')->name('patientrequests.sortstatus');

    Route::any('/reportAjax','PatientRequestController@reportAjax');
    Route::any('/notifiy','PatientRequestController@notifiy');
    Route::any('/notifiycenter','CenterResponseController@notifiycenter');
    Route::any('/notifiyresult','CenterResponseController@notifiyresult');

    Route::get('reports/statusgroup','ReportsController@statusReports')->name('reports.statusgroup');

Route::any('/reportuploads','PatientRequestController@reportuploads');

    Route::resource('urlgroups', 'UrlGroupController');
    Route::resource('urls', 'UrlController');
    Route::resource('usertypes', 'UserTypeController');
    Route::any('batchUpdate','UserTypeController@batchUpdate')->name('usertypes.batchUpdate');
    Route::post('language-switcher','LanguageController@switchLanguage');
    Route::post('/language/',array(
            'before' => 'csrf' ,
            'as' => 'language-switcher' ,
            'uses' => 'LanguageController@switchLanguage'
        )
    );
});


Route::get('/login', function () {
    return view('auth.login');
});

Route::group(['namespace'=>'Auth'],function(){
    Route::post('/login', ['uses'=>'LoginController@login'])->name('login.post');
    Route::get('/logout', ['uses'=>'LoginController@logout'])->name('auth.logout');
});


