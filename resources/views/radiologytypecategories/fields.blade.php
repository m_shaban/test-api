

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>Radiology Type (English)</label>
            <div class="input-group">
                <span class="input-group-addon">
                    <span class="flag-icon flag-icon-us"></span>
                </span>
                <input type="text" id="en_name" class="form-control" name="en_name" value="{{(isset($cat) ? $cat->en_name : '')}}" placeholder="Radiology Type (English)" required>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>Radiology Type (Arabic)</label>
            <div class="input-group">
                <span class="input-group-addon">
                    <span class="flag-icon flag-icon-eg"></span>
                </span>
                <input type="text" id="ar_name" class="form-control" name="ar_name" value="{{(isset($cat) ? $cat->ar_name : '')}}" placeholder="Radiology Type (Arabic)" required>
            </div>
        </div>
    </div>
</div>

