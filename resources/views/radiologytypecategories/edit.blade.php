@extends('layout')
@section('content')
    @if(session()->has('db'))
        <input type="text" value="{{session()->get('db')}}" id="db" hidden />
    @endif
    <div id="alert-success" class="myadmin-alert myadmin-alert-img alert-success myadmin-alert-top-right"> <img src="{{asset('images/logo.png')}}" class="img" alt="img"><a href="#" class="closed">&times;</a>
        <h4>You done it!</h4>
        <b>Update Radiology Type</b> Was Done Successfully.
    </div>

    <div id="alert-fail" class="myadmin-alert myadmin-alert-img alert-danger myadmin-alert-top-right"> <img src="{{asset('images/logo.png')}}" class="img" alt="img"><a href="#" class="closed">&times;</a>
        <h4>Sudden Failure, Sorry!</h4>
        <b>Radiology Type</b> Was Not Updated.
    </div>

    <div id="alert-info" class="myadmin-alert myadmin-alert-img alert-info myadmin-alert-top-right"> <img src="{{asset('images/logo.png')}}" class="img" alt="img"><a href="#" class="closed">&times;</a>
        <h4>Welcome To Edit Radiology Type!</h4>
        <b>You Could Edit </b>Radiology Type Info.
    </div>

    <div class="col-md-12">
        <div class="panel panel-success">
            <div class="panel-heading"> Edit Radiology Type </div>
            <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body form">
                            {!!Form::open(['method' => 'patch','route' => ['radiologytypecategories.update', $cat->id]])!!}
                            {{ csrf_field() }}

                        @include('radiologytypecategories.fields')

                        <div class="form-actions">
                                <button type="submit" class="btn btn-success">Submit</button>
                            <a href="{{route('radiologytypecategories.index')}}" style="color:white;" class="btn btn-default">Cancel</a>
                            </div>
                            {!!Form::close()!!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
