@extends('layout')
@section('content')
    @if(session()->has('db'))
        <input type="text" value="{{session()->get('db')}}" id="db" hidden />
    @endif
    <div id="alert-success" class="myadmin-alert myadmin-alert-img alert-success myadmin-alert-top-right"> <img src="{{asset('images/logo.png')}}" class="img" alt="img"><a href="#" class="closed">&times;</a>
        <h4>You done it!</h4>
        <b>Request Response</b> Was sent Successfully.
    </div>

    <div id="alert-fail" class="myadmin-alert myadmin-alert-img alert-danger myadmin-alert-top-right"> <img src="{{asset('images/logo.png')}}" class="img" alt="img"><a href="#" class="closed">&times;</a>
        <h4>Sudden Failure, Sorry!</h4>
        <b>Response</b> Was Not Sent.
    </div>

    <div id="alert-info" class="myadmin-alert myadmin-alert-img alert-info myadmin-alert-top-right"> <img src="{{asset('images/logo.png')}}" class="img" alt="img"><a href="#" class="closed">&times;</a>
        <h4>Welcome To Request Conformation!</h4>
        <b>You Could Confirm Request</b> for Patient Arrival.
    </div>

    @if(session()->has('error') || count($errors->all()) > 0)
        <div class="myadmin-alert myadmin-alert-img myadmin-alert-click alert-danger myadmin-alert-bottom alertbottom2" style="display: block;"> <img src="{{asset('images/logo.png')}}" class="img" alt="img"><a href="#" class="closed">×</a>
            <h4>Please Report the Following Errors</h4>
            <b></b> {{session()->get('error')}} <a href="#" class="closed">×</a> <br>
            @if(count($errors->all() > 0))
                @foreach($errors->all() as $error_val)
                    <b></b> {{$error_val}} <a href="#" class="closed">×</a> <br>
                @endforeach
            @endif
        </div>
    @endif
@section('style')
    <style>
        #result td
        {
            vertical-align:middle;
        }
    </style>
@endsection
    <h3 class="text-info"> <b>Patient Reservation Information</b> </h3>
<hr>
    <table id="confirmation" class="table display" width="100%" cellspacing="0">
    <thead>
    <tr>
        <th>Date</th>
        <th>Patient</th>
        <th>Phone</th>
        <th>Location</th>
        <th>Reservation Details</th>
        <th> Confirmed </th>
        <th> Cancelled </th>
    </tr>
    </thead>

        <tfoot>
            <tr>
                <th>Date</th>
                <th>Patient Name</th>
                <th>Patient Phone</th>
                <th>Location</th>
                <th>Reservation Details</th>
                <th> Confirmed </th>
                <th> Cancelled </th>
            </tr>
        </tfoot>

        <tbody>
        @foreach($confirms as $confirm)
        <tr>
            <td>{{$confirm->RequestDate}}</td>
            <td>{{$confirm->PatientName}}</td>
            <td>
                @if(!empty($confirm->Phones))
                    @foreach(explode(',',$confirm->Phones) as $phone)
                        {{$phone}}
                    </br>
                    @endforeach
                @else
                    {{'N/A'}}
                @endif
            </td>
            <td>
                @if($confirm->is_at_home == 0)
                    {{'At Center '.$confirm->center}}
                @else
                    {{'At Home'}}
                @endif
            </td>

            <td>
                @if(!empty($confirm->radiology_items))
                    @foreach(explode(',',$confirm->radiology_items) as $radiology_item)
                    {{$radiology_item}}
                    </br>
                    @endforeach
                @else
                    {{'None'}}
                @endif
            </td>
            @if($confirm->status == 'qa')
            <td>
                <form method="post" action="{{action('PatientRequestController@confirmRequestUpdate')}}">
                    {{ csrf_field() }}
                    <input type="text" name="requestno" value="{{$confirm->RequestNo}}" hidden>
                    <input type="text" name="status" value="pa" hidden>
                    <button class="btn btn-success">Confirmed</button>
                </form>
            </td>
            <td>
                <form method="post" action="{{action('PatientRequestController@confirmRequestUpdate')}}">
                    {{ csrf_field() }}
                    <input type="text" name="requestno" value="{{$confirm->RequestNo}}" hidden>
                    <input type="text" name="status" value="pc" hidden>
                    <button class="btn btn-danger">Cancelled</button>
                </form>
            </td>
            @else
            <td> </td>
            <td> </td>
            @endif
        </tr>
        @endforeach
    </tbody>
</table>
@stop

@section('script')
    <script>
        $(document).ready(function () {
            $('#confirmation').DataTable();
        });
    </script>
@endsection