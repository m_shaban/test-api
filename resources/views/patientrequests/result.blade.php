@extends('layout')
@section('content')
    @if(session()->has('db'))
        <input type="text" value="{{session()->get('db')}}" id="db" hidden />
    @endif
    <div id="alert-success" class="myadmin-alert myadmin-alert-img alert-success myadmin-alert-top-right"> <img src="{{asset('images/logo.png')}}" class="img" alt="img"><a href="#" class="closed">&times;</a>
        <h4>You done it!</h4>
        <b>Request Response</b> Was sent Successfully.
    </div>

    <div id="alert-fail" class="myadmin-alert myadmin-alert-img alert-danger myadmin-alert-top-right"> <img src="{{asset('images/logo.png')}}" class="img" alt="img"><a href="#" class="closed">&times;</a>
        <h4>Sudden Failure, Sorry!</h4>
        <b>Response</b> Was Not Sent.
    </div>

    <div id="alert-info" class="myadmin-alert myadmin-alert-img alert-info myadmin-alert-top-right"> <img src="{{asset('images/logo.png')}}" class="img" alt="img"><a href="#" class="closed">&times;</a>
        <h4>Welcome To Request Conformation!</h4>
        <b>You Could Confirm Request</b> for Patient Arrival.
    </div>

    @if(session()->has('error') || count($errors->all()) > 0)
        <div class="myadmin-alert myadmin-alert-img myadmin-alert-click alert-danger myadmin-alert-bottom alertbottom2" style="display: block;"> <img src="{{asset('images/logo.png')}}" class="img" alt="img"><a href="#" class="closed">�</a>
            <h4>Please Report the Following Errors</h4>
            <b></b> {{session()->get('error')}} <a href="#" class="closed">�</a> <br>
            @if(count($errors->all() > 0))
                @foreach($errors->all() as $error_val)
                    <b></b> {{$error_val}} <a href="#" class="closed">�</a> <br>
                @endforeach
            @endif
        </div>
    @endif

    <h3 class="text-info"> <b>Radiology Reports</b> </h3>
<hr>
    @section('style')
        <style>
            #result td
            {
                vertical-align:middle;
            }

            /*#modal {*/
                /*display: block !important;*/
            /*}*/
             Important part
             #modal .modal-dialog{
                overflow-y: initial !important
            }
            #modal .modal-body{
                height: 60%;
                overflow-y: auto;
            }
        </style>
    @endsection
    <table id="result" class="table display" width="100%" cellspacing="0">
    <thead>
    <tr>
        <th>Date</th>
        <th>Patient</th>
        <th>Phone</th>
        <th>Location</th>
        <th>Reservation Details</th>
        <th>Report Editor</th>
    </tr>
    </thead>

        <tfoot>
            <tr>
                <th>Date</th>
                <th>Patient Name</th>
                <th>Patient Phone</th>
                <th>Location</th>
                <th>Reservation Details</th>
                <th>Report Editor</th>
            </tr>
        </tfoot>

        <tbody>
        @foreach($confirms as $confirm)
        <tr>
            <td>{{$confirm->RequestDate}}</td>
            <td>{{$confirm->PatientName}}</td>
            <td>
                @if(!empty($confirm->Phones))
                    @foreach(explode(',',$confirm->Phones) as $phone)
                        {{$phone}}
                    </br>
                    @endforeach
                @else
                    {{'N/A'}}
                @endif
            </td>
            <td>
                @if($confirm->is_at_home == 0)
                    {{'At Center '.$confirm->center}}
                @else
                    {{'At Home'}}
                @endif
            </td>
            <td>
                @if(!empty($confirm->radiology_items))
                    @foreach(explode(',',$confirm->radiology_items) as $radiology_item)
                    {{$radiology_item}}</br>
                    @endforeach
                @else
                    {{'None'}}
                @endif
            </td>
            <td>
                @if($confirm->ReportState == 'create')
                    <button data-toggle="modal" data-target="#Modal" cid="{{$confirm->RequestNo}}" class="btn btn-block btn-danger showModal"><i class="fa fa-edit"></i>Create Report</button>
                @elseif ($confirm->ReportState == 'edit')
                    <button data-toggle="modal" data-target="#Modal" cid="{{$confirm->RequestNo}}" class="btn btn-block btn-warning showModal"><i class="fa fa-edit"></i>Edit Report</button>
                @else
                    <button data-toggle="modal" data-target="#Modal" cid="{{$confirm->RequestNo}}" class="btn btn-block btn-primary showModal"><i class="fa fa-edit"></i>View Report</button>
                @endif
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@stop
<div id="Modal" class="modal fade" role="dialog">
    <div style="width:96%;" class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Report Editor</h4>
            </div>
            <form class="form-inline" method="post" action="{{action('PatientRequestController@resultStore')}}" enctype="multipart/form-data" >
            <div align="center" class="modal-body">
                {{ csrf_field() }}
                <input type="text" id="requestno" name="requestno" hidden>
                {{--<div class="m-t-5">--}}
                    {{--<div class="form-group input-group col-sm-5">--}}
                        {{--<span class="input-group-addon" id="basic-addon1"><i class="fa fa-user-md"></i></span>--}}
                        {{--<input class="form-control" type="text" id="refby" name="refby" placeholder="Refrenced By">--}}
                    {{--</div>--}}

                    {{--<div class="form-group input-group  col-sm-5">--}}
                        {{--<span class="input-group-addon" id="basic-addon1"><i class="fa fa-user"></i></span>--}}
                        {{--<input class="form-control" type="text" id="writtenby" name="writtenby" placeholder="Written  By">--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="m-t-5">--}}
                {{--<div class="form-group input-group  col-sm-5">--}}
                        {{--<span class="input-group-addon" id="basic-addon1"><i class="fa fa-calendar"></i></span>--}}
                        {{--<input class="form-control" type="date" id="studydate" name="studydate" placeholder="Study Date">--}}
                    {{--</div>--}}

                    {{--<div class="form-group input-group  col-sm-5">--}}
                        {{--<span class="input-group-addon" id="basic-addon1"><i class="fa fa-file-text"></i></span>--}}
                        {{--<input class="form-control" type="text" id="title" name="title" placeholder="Title">--}}
                    {{--</div>--}}
                {{--</div>--}}
                <textarea id="report" name="report">
{{--&lt;h1 style="text-align:center"&gt;&lt;span style="font-family:Georgia,serif"&gt;&lt;span--}}
			{{--style="color:#006699"&gt;Recognition of Achievement&lt;/span&gt;&lt;/span&gt;&lt;/h1&gt;--}}

	{{--&lt;p style="text-align:justify"&gt;&lt;span style="font-family:Georgia,serif"&gt;This letter acknowledges the invaluable input &lt;strong&gt;you&lt;/strong&gt;, as a member--}}
		{{--of our &lt;em&gt;Innovation Team&lt;/em&gt;,&amp;nbsp;have provided in the &amp;ldquo;Implement Rich Text Editor&amp;rdquo;&amp;nbsp;project. The Management would like to hereby thank you for this great accomplishment that was delivered in a timely fashion, up to the highest company standards, and with great results:&lt;/span&gt;&lt;/p&gt;--}}

	{{--&lt;table border="1" bordercolor="#ccc" cellpadding="5" cellspacing="0" style="border-collapse:collapse;width:100%"--}}
		   {{--summary="Project Schedule"&gt;--}}
		{{--&lt;thead&gt;--}}
		{{--&lt;tr&gt;--}}
			{{--&lt;th scope="col" style="background-color:#cccccc"&gt;&lt;span style="font-family:Georgia,serif"&gt;Project Phase&lt;/span&gt;&lt;/th&gt;--}}
			{{--&lt;th scope="col" style="background-color:#cccccc"&gt;&lt;span style="font-family:Georgia,serif"&gt;Deadline&lt;/span&gt;&lt;/th&gt;--}}
			{{--&lt;th scope="col" style="background-color:#cccccc"&gt;&lt;span style="font-family:Georgia,serif"&gt;Status&lt;/span&gt;&lt;/th&gt;--}}
		{{--&lt;/tr&gt;--}}
		{{--&lt;/thead&gt;--}}
		{{--&lt;tbody&gt;--}}
		{{--&lt;tr&gt;--}}
			{{--&lt;td&gt;&lt;span style="font-family:Georgia,serif"&gt;Phase 1: Market research&lt;/span&gt;&lt;/td&gt;--}}
			{{--&lt;td style="text-align:center"&gt;&lt;span style="font-family:Georgia,serif"&gt;2016-10-15&lt;/span&gt;&lt;/td&gt;--}}
			{{--&lt;td style="text-align:center"&gt;&lt;span style="font-family:Georgia,serif"&gt;&lt;span style="color:#19b159"&gt;?&lt;/span&gt;&lt;/span&gt;&lt;/td&gt;--}}
		{{--&lt;/tr&gt;--}}
		{{--&lt;tr&gt;--}}
			{{--&lt;td style="background-color:#eeeeee"&gt;&lt;span style="font-family:Georgia,serif"&gt;Phase 2: Editor implementation&lt;/span&gt;&lt;/td&gt;--}}
			{{--&lt;td style="background-color:#eeeeee; text-align:center"&gt;&lt;span style="font-family:Georgia,serif"&gt;2016-10-20&lt;/span&gt;&lt;/td&gt;--}}
			{{--&lt;td style="background-color:#eeeeee; text-align:center"&gt;&lt;span style="font-family:Georgia,serif"&gt;&lt;span--}}
					{{--style="color:#19b159"&gt;?&lt;/span&gt;&lt;/span&gt;&lt;/td&gt;--}}
		{{--&lt;/tr&gt;--}}
		{{--&lt;tr&gt;--}}
			{{--&lt;td&gt;&lt;span style="font-family:Georgia,serif"&gt;Phase 3: Rollout to Production&lt;/span&gt;&lt;/td&gt;--}}
			{{--&lt;td style="text-align:center"&gt;&lt;span style="font-family:Georgia,serif"&gt;2016-10-22&lt;/span&gt;&lt;/td&gt;--}}
			{{--&lt;td style="text-align:center"&gt;&lt;span style="font-family:Georgia,serif"&gt;&lt;span style="color:#19b159"&gt;?&lt;/span&gt;&lt;/span&gt;&lt;/td&gt;--}}
		{{--&lt;/tr&gt;--}}
		{{--&lt;/tbody&gt;--}}
	{{--&lt;/table&gt;--}}

	{{--&lt;p style="text-align:justify"&gt;&lt;span style="font-family:Georgia,serif"&gt;The project that you participated in is of utmost importance to the future success of our platform. &amp;nbsp;We are very proud to share that&amp;nbsp;the&amp;nbsp;CKEditor implementation was a huge success and brought congratulations from both the key Stakeholders and the Customers:&lt;/span&gt;&lt;/p&gt;--}}

	{{--&lt;blockquote&gt;--}}
		{{--&lt;p style="text-align:center"&gt;This new editor has totally changed our content creation experience!&lt;/p&gt;--}}

		{{--&lt;p style="text-align:center"&gt;&amp;mdash; John F. Smith, CEO, The New Web&lt;/p&gt;--}}
	{{--&lt;/blockquote&gt;--}}

	{{--&lt;p style="text-align:justify"&gt;&lt;span style="font-family:Georgia,serif"&gt;This letter recognizes that much of our success is directly attributable to your efforts.&amp;nbsp;You deserve to be proud of your achievement. May your future efforts be equally successful and rewarding.&lt;/span&gt;&lt;/p&gt;--}}

	{{--&lt;p style="text-align:justify"&gt;&lt;span style="font-family:Georgia,serif"&gt;I am sure we will be seeing and hearing a great deal more about your accomplishments in the future. Keep up the good work!&lt;/span&gt;&lt;/p&gt;--}}

	{{--&lt;p&gt;&amp;nbsp;&lt;/p&gt;--}}

	{{--&lt;p&gt;&lt;span style="font-family:Georgia,serif"&gt;Best regards,&lt;/span&gt;&lt;/p&gt;--}}

	{{--&lt;p&gt;&lt;span style="font-family:Georgia,serif"&gt;&lt;em&gt;The Management&lt;/em&gt;&lt;/span&gt;&lt;/p&gt;--}}
	</textarea>
                <div class="white-box">
                <div class="centersdiv" id="centersdiv">
                    <input type="hidden" name="count_uploads" id="count_uploads" value="1" />
                    <div class='col-md-12 centerdivchild'>
                        <div class='col-md-6'>
                            <div class='form-group'>
                                <label class='control-label col-md-4'>
                                </label><div class='col-md-8'>
                                    <input type='file' name='report_attach0' id='report_attach0' accept=".pdf,.jpeg,.jpg,.png,.tiff" /> 
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                        <span class="input-group-btn">
                                                    <button class='add_sub btn btn-success' type="button"><i
                                                                class="fa fa-lg fa-plus"></i></button>
                                                </span> 
                        </div>
                    </div>
                    <br />
                    <br />
                    
                    
                
                          </div>
            </div>
            </div>
            {{--<div style="padding: 5%"></div>--}}
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Save</button>
                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#Message">Close</button>
            </div>
            </form>
        </div>

    </div>
</div>

<div class="modal fade message" id="Message" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 style="color: red;" class="modal-title"><i class="fa fa-exclamation-triangle"></i> Alert</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to cancel ?</p>
                <p>Press Yes to Cancel , No to Continue</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                <button id="closemodals" type="button" class="btn btn-danger">Yes</button>
            </div>
        </div>
    </div>
</div>
	<input type="hidden" name="last_notifiycount" id="last_notifiycount" value="0" />
@section('script')

<link  href="fancybox/jquery.fancybox.min.css" rel="stylesheet">
<script src="fancybox/jquery.fancybox.min.js"></script>

    <script src="https://cdn.ckeditor.com/4.6.1/full-all/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'report', {
            // Define the toolbar: http://docs.ckeditor.com/#!/guide/dev_toolbar
            // The full preset from CDN which we used as a base provides more features than we need.
            // Also by default it comes with a 3-line toolbar. Here we put all buttons in a single row.
            toolbar: [
                { name: 'document', items: [ 'Print' ] },
                { name: 'clipboard', items: [ 'Undo', 'Redo' ] },
                { name: 'styles', items: [ 'Format', 'Font', 'FontSize' ] },
                { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'RemoveFormat', 'CopyFormatting' ] },
                { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
                { name: 'align', items: [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
                { name: 'links', items: [ 'Link', 'Unlink' ] },
                { name: 'paragraph', items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote' ] },
                { name: 'insert', items: [ 'Image', 'Table' ] },
                { name: 'tools', items: [ 'Maximize' ] },
                { name: 'editing', items: [ 'Scayt' ] }
            ],
            // Since we define all configuration options here, let's instruct CKEditor to not load config.js which it does by default.
            // One HTTP request less will result in a faster startup time.
            // For more information check http://docs.ckeditor.com/#!/api/CKEDITOR.config-cfg-customConfig
            customConfig: '',
            // Sometimes applications that convert HTML to PDF prefer setting image width through attributes instead of CSS styles.
            // For more information check:
            //  - About Advanced Content Filter: http://docs.ckeditor.com/#!/guide/dev_advanced_content_filter
            //  - About Disallowed Content: http://docs.ckeditor.com/#!/guide/dev_disallowed_content
            //  - About Allowed Content: http://docs.ckeditor.com/#!/guide/dev_allowed_content_rules
            disallowedContent: 'img{width,height,float}',
            extraAllowedContent: 'img[width,height,align]',
            // Enabling extra plugins, available in the full-all preset: http://ckeditor.com/presets-all
            extraPlugins: 'tableresize,uploadimage,uploadfile',
            /*********************** File management support ***********************/
            // In order to turn on support for file uploads, CKEditor has to be configured to use some server side
            // solution with file upload/management capabilities, like for example CKFinder.
            // For more information see http://docs.ckeditor.com/#!/guide/dev_ckfinder_integration
            // Uncomment and correct these lines after you setup your local CKFinder instance.
            // filebrowserBrowseUrl: 'http://example.com/ckfinder/ckfinder.html',
            // filebrowserUploadUrl: 'http://example.com/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
            /*********************** File management support ***********************/
            // Make the editing area bigger than default.
            height: 800,
            // An array of stylesheets to style the WYSIWYG area.
            // Note: it is recommended to keep your own styles in a separate file in order to make future updates painless.
            contentsCss: [ 'https://cdn.ckeditor.com/4.6.1/full-all/contents.css', 'mystyles.css' ],
            // This is optional, but will let us define multiple different styles for multiple editors using the same CSS file.
            bodyClass: 'document-editor',
            // Reduce the list of block elements listed in the Format dropdown to the most commonly used.
            format_tags: 'p;h1;h2;h3;pre',
            // Simplify the Image and Link dialog windows. The "Advanced" tab is not needed in most cases.
            removeDialogTabs: 'image:advanced;link:advanced',
            // Define the list of styles which should be available in the Styles dropdown list.
            // If the "class" attribute is used to style an element, make sure to define the style for the class in "mystyles.css"
            // (and on your website so that it rendered in the same way).
            // Note: by default CKEditor looks for styles.js file. Defining stylesSet inline (as below) stops CKEditor from loading
            // that file, which means one HTTP request less (and a faster startup).
            // For more information see http://docs.ckeditor.com/#!/guide/dev_styles
            stylesSet: [
                /* Inline Styles */
                { name: 'Marker', element: 'span', attributes: { 'class': 'marker' } },
                { name: 'Cited Work', element: 'cite' },
                { name: 'Inline Quotation', element: 'q' },
                /* Object Styles */
                {
                    name: 'Special Container',
                    element: 'div',
                    styles: {
                        padding: '5px 10px',
                        background: '#eee',
                        border: '1px solid #ccc'
                    }
                },
                {
                    name: 'Compact table',
                    element: 'table',
                    attributes: {
                        cellpadding: '5',
                        cellspacing: '0',
                        border: '1',
                        bordercolor: '#ccc'
                    },
                    styles: {
                        'border-collapse': 'collapse'
                    }
                },
                { name: 'Borderless Table', element: 'table', styles: { 'border-style': 'hidden', 'background-color': '#E6E6FA' } },
                { name: 'Square Bulleted List', element: 'ul', styles: { 'list-style-type': 'square' } }
            ]
        });

        $(document).ready(function ()
        {
            
            $('#result').DataTable();

            $('#closemodals').on('click',function(){
                $('.modal').modal('toggle');
            });
            $('.fa-chevron-circle-down').on('click', function () {
                var id = $(this).attr('cid');
                currentPanel = '.panel' + id;
            });
            
            $('.showModal').on('click',function(){
                var request = $(this).attr('cid');
                $('#requestno').val(request);

                var promise = $.ajax({
                    type: "POST",
                    url: "/reportAjax",
                    data : {'id' : request}
                }).done(function(response) {
                    $.each(JSON.parse(response),function(index,itemObj) {
                        CKEDITOR.instances.report.setData(itemObj.report);
                    });
                })
                                var promise = $.ajax({
                    type: "POST",
                    url: "/reportuploads",
                    data : {'id' : request}
                }).done(function(response) {
                    
                      document.getElementById("count_uploads").value= JSON.parse(response)['count'];   
    var temp =  (JSON.parse(response)['uploads']).toString();
    var arr = temp.split(",");
    var id =1 ;
     for ( count = 0; count < arr.length; count++) {
    var d1 = document.getElementById('centersdiv');
    var string = arr[count];
    
    substring = "pdf";
if(string.includes(substring))
{
    var link = "<a href='uploads/"+arr[count]+"'>View file</a>";
}
else 
{
     link = "<a href='uploads/"+arr[count]+"' data-fancybox=\"gallery\" ><img src='uploads/"+arr[count]+"' style=' height: 40px;margin: 5px 0 0 18px;width: 40px;' /></a>";
}
if(string!=''){
    d1.insertAdjacentHTML('beforeend', "<div class='col-md-12 centerdivchild'><div class='col-md-6'><div class='form-group'><label class='control-label col-md-4'></label><div class='col-md-8'><input type='hidden' value='"+arr[count]+"' name='temp_image"+id+"' id='temp_image"+id+"' /><input type='file'  name='report_attach"+id+"' id='report_attach"+id+"'  accept='.pdf,.jpeg,.jpg,.png,.tiff' /> </div></div></div><div class='col-md-3'><div class='form-group'><div class='col-md-9 input-group'><span class='input-group-btn'><button class='remove_sub btn btn-danger' type='button'><i class='fa fa-lg fa-minus'></i></button></span></div></div>"+link+"</div><br /></div><br />");
id+=1;
}
}
    
               
                })
                  .fail(function() {

                 })
                 .always(function() {

                 });
            });
             $('.white-box').on('click', '.add_sub', function (e) {
                 
                 var centeri = parseInt(document.getElementById("count_uploads").value);
                 if(centeri >9 )
                 {
                     return;
                 }
                centeri+=1;
                e.preventDefault();

                var d1 = document.getElementById('centersdiv');
                        d1.insertAdjacentHTML('beforeend', "<div class='col-md-12 centerdivchild'><div class='col-md-6'><div class='form-group'><label class='control-label col-md-3'></label><div class='col-md-9'><input type='file' style=' margin: 0 0 0 29px;' name='report_attach"+centeri+"' id='report_attach"+centeri+"'  accept='.pdf,.jpeg,.jpg,.png,.tiff' /> </div></div></div><div class='col-md-4'><div class='form-group'><div class='col-md-9 input-group'><span class='input-group-btn'><button style='margin:5px 0 0 -156px' class='remove_sub btn btn-danger' type='button'><i class='fa fa-lg fa-minus'></i></button></span></div></div></div><br /></div>");
    document.getElementById("count_uploads").value = parseInt(document.getElementById("count_uploads").value)+1;
    //document.getElementById("centersdiv").appendChild("<div class='col-md-12 centerdivchild'><div class='col-md-6'><div class='form-group'><label class='control-label col-md-3'>Upload file</label><div class='col-md-9'><input type='file' name='report_attach"+centeri+"' id='report_attach"+centeri+"' /> </div></div></div><div class='col-md-4'><div class='form-group'><div class='col-md-9 input-group'><span class='input-group-btn'><button class='remove_sub btn btn-danger' type='button'><i class='fa fa-lg fa-minus'></i></button></span></div></div></div></div>");
                i++;
            });
             $('.white-box').on('click', '.remove_sub', function () {
                 document.getElementById("count_uploads").value = parseInt(document.getElementById("count_uploads").value)-1;
                $(this).closest('.centerdivchild').remove();
            });
        });
         setInterval(function(){
    
        var promise = $.ajax({
                    type: "POST",
                    url: "/notifiyresult",
                    
                }).done(function(response) {
                     var last_notifiycount = document.getElementById("last_notifiycount").value;
                       if(response > last_notifiycount)
                       {
                           document.getElementById("last_notifiycount").value = response;
                           var audio = new Audio('file.mp3');
                            audio.play();
                            
                       }
                  //  $( "#fafwPatient requests" ).html( "<p style='color:red;'>"+response+"</p>" );
                   

var myElem = document.getElementById('fafwCenter response');

document.getElementById("fafwCenter response").innerHTML = response;


                });
    
    
    }, 1000);
    
        
     
    </script>
@endsection