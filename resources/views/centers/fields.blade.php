
@section('style')

@stop

<div class="row">
    <div class="row">
        <div class="col-md-9">
            <div class="form-group">
                <label>Center Address</label>
                <div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-map"></i>
                </span>
                    <input type="text" class="form-control" id="address" name="address" placeholder="Address" value="{{(isset($center->address) ? $center->address : '')}}" required>

                    {{--<input type="text" id="latitude"  hidden/>--}}
                    {{--<input type="text" id="longitude"  hidden/>--}}
                    {{--<input type="text" name="map_location" id="map_location" hidden/>--}}
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label>Center Name</label>
                <div class="input-group">
                <span class="input-group-addon">
                    <span class="fa fa-building"></span>
                </span>
                    <input type="text" id="name" class="form-control" name="name" value="{{(isset($center->name) ? $center->name : '')}}" placeholder="Radiology Type (English)" required>
                </div>
            </div>
        </div>
    </div>
</div>

@section('script')

    {{--<script src="https://maps.googleapis.com/maps/api/js?sensor=false?libraries=places&key=AIzaSyB9bD6FNx_faqcgU9uOoD9NykTYUxsgsmw"></script>--}}

    {{--<script>--}}
        {{--var x,y;--}}
        {{--function showResult(result) {--}}
            {{--x = result.geometry.location.lat();--}}
            {{--y = result.geometry.location.lng();--}}
            {{--document.getElementById('latitude').value = x;--}}
            {{--document.getElementById('longitude').value = y;--}}
        {{--}--}}

        {{--function getLatitudeLongitude(callback, address) {--}}
            {{--// If adress is not supplied, use default value 'Ferrol, Galicia, Spain'--}}
            {{--address = address || 'Alexandria, Egypt';--}}
            {{--// Initialize the Geocoder--}}
            {{--geocoder = new google.maps.Geocoder();--}}
            {{--if (geocoder) {--}}
                {{--geocoder.geocode({--}}
                    {{--'address': address--}}
                {{--}, function (results, status) {--}}
                    {{--if (status == google.maps.GeocoderStatus.OK) {--}}
                        {{--callback(results[0]);--}}
                    {{--}--}}
                {{--});--}}
            {{--}--}}
        {{--}--}}

        {{--var inpt = document.getElementById('btn');--}}

        {{--document.getElementById('address').addEventListener("blur", function () {--}}
            {{--var address = document.getElementById('address').value;--}}
            {{--getLatitudeLongitude(showResult, address);--}}
            {{--var map = document.getElementById('latitude').value+','+document.getElementById('longitude').value;--}}
            {{--document.getElementById('map_location').value = map;--}}
        {{--});--}}

    {{--</script>--}}
    {{--<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>--}}
    {{--<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCOJfU-kvBtYWP31_WlYcSbZ_w4qI8bH8g&callback=initMap"--}}
            {{--type="text/javascript"></script>--}}
@stop