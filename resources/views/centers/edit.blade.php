@extends('layout')
@section('content')
    @if(session()->has('db'))
        <input type="text" value="{{session()->get('db')}}" id="db" hidden />
    @endif
    <div id="alert-success" class="myadmin-alert myadmin-alert-img alert-success myadmin-alert-top-right"> <img src="{{asset('images/logo.png')}}" class="img" alt="img"><a href="#" class="closed">&times;</a>
        <h4>You done it!</h4>
        <b>Update Branch Info</b> Was Done Successfully.
    </div>

    <div id="alert-fail" class="myadmin-alert myadmin-alert-img alert-danger myadmin-alert-top-right"> <img src="{{asset('images/logo.png')}}" class="img" alt="img"><a href="#" class="closed">&times;</a>
        <h4>Sudden Failure, Sorry!</h4>
        <b>Branch</b> Was Not Updated.
    </div>

    <div id="alert-info" class="myadmin-alert myadmin-alert-img alert-info myadmin-alert-top-right"> <img src="{{asset('images/logo.png')}}" class="img" alt="img"><a href="#" class="closed">&times;</a>
        <h4>Welcome To Edit Center Branch!</h4>
        <b>You Could Edit </b> Branch Info.
    </div>

    <div class="col-md-12">
        <div class="panel panel-success">
            <div class="panel-heading"> Edit Center Branch </div>
            <div class="panel-wrapper collapse in" aria-expanded="true">
                <div class="panel-body" style="background-color: whitesmoke;">
                    <div class="panel-body form">
                        {!!Form::open(['method' => 'patch','route' => ['centers.update', $center->id]])!!}
                        {{ csrf_field() }}
                            @include('centers.fields')
                            <div class="form-actions">
                                <button type="submit" class="btn btn-success">Submit</button>
                                <a href="{{route('centers.index')}}" style="color:white;" class="btn btn-default">Cancel</a>
                            </div>
                        {!!Form::close()!!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
@section('script')
    <script>

        $(document).ready(function(){
            onlyEnglish($('#en_name'));
            onlyArabic($('#ar_name'));
        });
    </script>
@stop