@extends('layout')
@section('content')
    @if(session()->has('db'))
        <input type="text" value="{{session()->get('db')}}" id="db" hidden />
    @endif

    <div id="alert-info" class="myadmin-alert myadmin-alert-img alert-info myadmin-alert-top-right"> <img src="{{asset('images/logo.png')}}" class="img" alt="img"><a href="#" class="closed">&times;</a>
        <h4>Welcome To Create Center Branch!</h4>
        <b>You Could Create New Branch</b> for the Center.
    </div>

    <a class="btn btn-success" href="{{route('centers.create')}}">
        <i class="fa fa-plus"></i> Add New Branch
    </a>
    <br>
    <hr>

    <div class="col-md-12">
        <div class="panel panel-success">
            <div class="panel-heading"> Radiology Item Details </div>
            <div class="panel-wrapper collapse in" aria-expanded="true">
                <div class="panel-body" style="background-color: whitesmoke;">
                    <div class="panel-body form">

                        @include('centers.fields')

                        <div class="form-actions">
                            <div class="form-actions">
                                <a href="{{route('centers.edit',$center->id)}}" Class="btn btn-info" style="color: white;">
                                    <i Class="fa fa-lg fa-pencil"></i> Edit
                                </a>
                                <a href="{{route('centers.index')}}" style="color:white;" class="btn btn-default">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script>
        $(document).ready(function(){
            $("input").prop('disabled', true);
            $("textarea").prop('disabled', true);
        });
    </script>
@stop
