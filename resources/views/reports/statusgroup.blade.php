@extends('layout')
@section('content')
        {{--<link href="{{asset('css/kendo.common.min.css')}}" rel="stylesheet">--}}
        {{--<link href="{{asset('css/kendo.common-bootstrap.min.css')}}" rel="stylesheet">--}}
        {{--<link href="{{asset('css/kendo.default.min.css')}}" rel="stylesheet">--}}
        {{--<link href="{{asset('css/kendo.material.min.css')}}" rel="stylesheet">--}}

        <style>html { font-size: 14px; font-family: Arial, Helvetica, sans-serif; }</style>
        <title></title>
        <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.2.621/styles/kendo.common-material.min.css" />
        <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.2.621/styles/kendo.material.min.css" />
        <link rel="stylesheet" href="https://kendo.cdn.telerik.com/2017.2.621/styles/kendo.material.mobile.min.css" />
<style>
    .k-filter:before{content:"\e129" !important}
</style>
        <script src="https://kendo.cdn.telerik.com/2017.2.621/js/jquery.min.js"></script>
        <script src="https://kendo.cdn.telerik.com/2017.2.621/js/kendo.all.min.js"></script>

    <div id="example">
        <div id="grid">
        </div>
    </div>

@stop


@section('script')
    <script src="{{asset('js/kendo.all.min.js')}}"></script>

    <script>
        $(document).ready(function() {
            $("#grid").kendoGrid({
                dataSource: {
                    data: {!!$data!!},
                    schema: {
                        model: {
                            fields: {
                                PatientName: { type: "string" },
                                PatientGender: {type: "string"},
                                Phones: { type: "string"},
                                RequestDate: { type: "string"},
                                Location: { type: "string"},
                                Follow: { type:"string" }
                            }
                        }
                    },
                    pageSize: 15
                },
                height: 550,
                scrollable: true,
                filterable: {
                    extra: false,
                    operators: {
                        string: {
                            startswith: "Starts with",
                            eq: "Is equal to",
                            neq: "Is not equal to"
                        }
                    }
                },
                pageable: true,
                columns: [
                    {
                        title: "Patient Name",
                        field: "PatientName",
                        width: 160,
                        filterable: {
                            ui: nameFilter
                        },
                    },
                    {
                        title: "Gender",
                        field: "PatientGender",
                        width: 80,
                        filterable: {
                            ui: genderFilter
                        }
                    },
                    {
                        title: "Phone",
                        field: "Phones",
                        filterable: {
                            ui: phoneFilter
                        }
                    },
                    {
                        field: "RequestDate",
                        title: "Request Date",
                        format: "{0:dd/MM/yyyy HH:mm tt}",
                        filterable: {
                            //ui: "datepicker"
                            ui: dateFilter
                        }
                    },
                    {
                        title: "Location",
                        field: "Location",
                        width: 160,
                        filterable: {
                            ui: locationFilter
                        },
                    },
                    {
                        title: "Follow",
                        field: "Follow",
                        width: 160,
                        filterable: {
                            ui: followFilter
                        },
                    }
                ]
            });
        });

        function nameFilter(element) {
            element.kendoAutoComplete({
                dataSource: PatientName
            });
        }


        function dateFilter(element) {
            element.kendoAutoComplete({
                dataSource: RequestDate
            });
        }

        function genderFilter(element) {
            element.kendoAutoComplete({
                dataSource: PatientGender
            });
        }

        function phoneFilter(element) {
            element.kendoAutoComplete({
                dataSource: Phones
                //optionLabel: "--Select Value--"
            });
        }

        function locationFilter(element) {
            element.kendoAutoComplete({
                dataSource: Location
            });
        }
            function followFilter(element) {
                element.kendoAutoComplete({
                    dataSource: Follow
                });
        }

    </script>
@stop