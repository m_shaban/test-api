<?php 
    $user = \Illuminate\Support\Facades\Auth::user();
    $urls = DB::select("SELECT G.name as url_group,U.name,U.href
                FROM url_groups AS G
                INNER JOIN urls AS U
                on(G.id = U.url_group_id)
                INNER JOIN user_type_url AS T
                on(T.url_id = U.id)
                WHERE T.user_type_id = ?

                ORDER BY G.order,U.order",[$user->user_type->id]);
    $urls = \Illuminate\Support\Collection::make($urls)->groupBy('url_group');
 ?>

<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse slimscrollsidebar">
        <ul class="nav" id="side-menu">
            <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                <!-- input-group -->
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
            <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
            </span> </div>
                <!-- /input-group -->
            </li>
            <?php $__currentLoopData = $urls; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $group=>$items): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                <li>
                    <a href="#" class="waves-effect" >
                        <i data-icon=")" class="linea-icon linea-basic fa-fw"></i>
                        <span class="hide-menu"><?php echo e($group); ?> <p id="fafw<?php echo e($group); ?>" class="badge" style="text-align: center; background-color: red; border: medium none; height: 20px; margin: -4px 0px 0px; width: 20px;"></p> &nbsp;<span class="caret"></span><span class="fa arrow "></span></span>
                    </a>
                    <ul class="nav nav-second-level">
                        <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                        <?php if($item->name == ''): ?>
                        <li><a href="<?php echo e($item->href); ?>" style="display:none;"  id="hreffafw<?php echo e($item); ?>"><?php echo e($item->name); ?></a>
                            
                            </li>
                            <?php else: ?>
                            <li><a href="<?php echo e($item->href); ?>" ><?php echo e($item->name); ?></a>
                            
                            </li>
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                    </ul>
                </li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
        </ul>
    </div>
</div>

