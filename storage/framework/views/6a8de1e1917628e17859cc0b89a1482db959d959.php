<?php $__env->startSection('content'); ?>
    <?php 
        $i = 1;
     ?>
    <?php if(session()->has('db')): ?>

        <input type="text" value="<?php echo e(session()->get('db')); ?>" id="db" hidden/>
    <?php endif; ?>
    <div id="alert-success" class="myadmin-alert myadmin-alert-img alert-success myadmin-alert-top-right"><img
                src="<?php echo e(asset('images/logo.png')); ?>" class="img" alt="img"><a href="#" class="closed">&times;</a>
        <h4>You done it!</h4>
        <b>Request Response</b> Was sent Successfully.
    </div>

    <div id="alert-fail" class="myadmin-alert myadmin-alert-img alert-danger myadmin-alert-top-right"><img
                src="<?php echo e(asset('images/logo.png')); ?>" class="img" alt="img"><a href="#" class="closed">&times;</a>
        <h4>Sudden Failure, Sorry!</h4>
        <b>Response</b> Was Not Sent.
    </div>

    <div id="alert-info" class="myadmin-alert myadmin-alert-img alert-info myadmin-alert-top-right"><img
                src="<?php echo e(asset('images/logo.png')); ?>" class="img" alt="img"><a href="#" class="closed">&times;</a>
        <h4>Welcome To Request Response!</h4>
        <b>You Could Send Response</b> for Patient Requests.
    </div>


    <?php if(session()->has('error') || count($errors->all()) > 0): ?>
        <div class="myadmin-alert myadmin-alert-img myadmin-alert-click alert-danger myadmin-alert-bottom alertbottom2"
             style="display: block;"><img src="<?php echo e(asset('images/logo.png')); ?>" class="img" alt="img"><a href="#"
                                                                                                      class="closed">×</a>
            <h4>Please Report the Following Errors</h4>
            <b></b> <?php echo e(session()->get('error')); ?> <a href="#" class="closed">×</a> <br>
            <?php if(count($errors->all() > 0)): ?>
                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error_val): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                    <b></b> <?php echo e($error_val); ?> <a href="#" class="closed">×</a> <br>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
            <?php endif; ?>
        </div>
    <?php endif; ?>
    <input type="hidden" name="last_notifiycount" id="last_notifiycount" value="0" />
    <div class="col-lg-12 col-sm-4">
        <div class="panel panel-primary panel">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-sm-1">#</div>
                    <div class="col-sm-4">
                        <form action="<?php echo e(route('requests.sortnamerequest')); ?>" method="post"><?php echo e(csrf_field()); ?><input
                                    type="hidden" value="up" id="order_name" name="order_name"/>
                            <button id="btnsortname" type="submit"
                                    class="btn btn-success" style="display:none">Submit
                            </button>
                        </form>
                        patient name
                        <div style="float: left;margin: -2% 7% 0 0;width: 5px;">
                            <a href="#" onclick="sorting(1, 'up')">
                                <i class="cli up"></i>
                            </a>
                            <div style="clear: both;"></div>
                            <a href="#" onclick="sorting(1, 'down')">
                                <i class="cli down"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <form action="<?php echo e(route('requests.sortdaterequest')); ?>" method="post"><?php echo e(csrf_field()); ?><input
                                    type="hidden" value="up" id="order_date" name="order_date"/>
                            <button id="btnsortdate" type="submit"
                                    class="btn btn-success" style="display:none">Submit
                            </button>
                        </form>
                        patient date
                        <div style="float: left;margin: -2% 7% 0 0;width: 5px;">
                            <a href="#" onclick="sorting(2, 'up')">
                                <i class="cli up"></i>
                            </a>
                            <div style="clear: both;"></div>
                            <a href="#" onclick="sorting(2, 'down')">
                                <i class="cli down"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <form action="<?php echo e(route('requests.sortlocationrequest')); ?>" method="post"><?php echo e(csrf_field()); ?><input
                                    type="hidden" value="up" id="order_location" name="order_location"/>
                            <button id="btnsortlocation" type="submit"
                                    class="btn btn-success" style="display:none">Submit
                            </button>
                        </form>
                        location
                        <div style="float: left;     margin: -2% 7% 0 0;     width: 5px;">
                            <a href="#" onclick="sorting(3, 'up')"><i class="cli up"></i></a>
                            <div style="clear: both;"></div>
                            <a href="#" onclick="sorting(3, 'down')"><i class="cli down"></i></a>
                        </div>
                    </div>
                    
                        
                                    
                            
                                    
                            
                        
                        
                        
                            
                            
                            
                        
                    
                    <div class="col-sm-1"></div>
                </div>
            </div>
        </div>
    </div>
    <?php if(!empty($requests)): ?>
        <?php $__currentLoopData = $requests; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $request): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
            <div class="col-lg-12 col-sm-4">
                <div class="panel panel-info panel<?php echo e($i); ?>">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-sm-1"><?php echo e($i); ?></div>
                            <div class="col-sm-4"><?php echo e($request->PatientName); ?></div>
                            <div class="col-sm-4"><?php echo e($request->RequestDate); ?></div>
                            <div class="col-sm-3">
                                <?php if($request->Location == 'At Home'): ?>
                                    Home <i class="fa fa-home"></i>
                                <?php else: ?>
                                    Center <i class="fa fa-hospital-o"></i>
                                <?php endif; ?>
                                    <a href="#" data-perform="panel-collapse"><i cid="<?php echo e($i); ?>" class="fa fa-chevron-circle-down fa-lg pull-right" style="font-size: 25px;"></i></a>
                            </div>
                            
                                
                                    
                                    
                                
                                    
                                    
                                
                                    
                                    
                                
                                    
                                    
                                
                                    
                                    
                                
                                    
                                
                                    
                                    
                                
                            
                            
                                
                                
                            
                        </div>
                    </div>
                    <div class="panel-wrapper collapse out" aria-expanded="true">
                        <div class="panel-body">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-sm-2"><b><i Class="fa fa-barcode"></i> Patient Code :</b>
                                        <?php echo e($request->PatientCode); ?>

                                    </div>
                                    <div class="col-sm-2"><b><i Class="fa fa-venus-mars "></i> Gender :</b>
                                        <?php echo e(($request->PatientGender) == 'M'? 'Male' : 'Female'); ?>

                                    </div>
                                    <div class="col-sm-2"><b><i Class="fa fa-calendar"></i> Age :</b>
                                        <?php echo e($request->Age); ?>

                                    </div>
                                    <div class="col-sm-2"><b><i Class="fa fa-sort-numeric-asc"></i> Weight : </b>
                                        <?php echo e($request->Weight); ?>

                                    </div>
                                    <div class="col-sm-2"><b><i Class="fa fa-bed"></i> Stable : </b>
                                        <?php echo e($request->Stable); ?>

                                    </div>
                                    <div class="col-sm-2"><b><i Class="fa fa-map-marker"></i> At Home : </b>
                                        <?php echo e($request->Location); ?>

                                    </div>
                                </div>
                                <div class="row">
                                    <hr/>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4"><b><i Class="fa fa-envelope"></i> Email : </b>
                                        <?php echo e($request->email); ?>

                                    </div>
                                    <div class="col-sm-2"><b><i class="fa fa-phone"></i> Phones :</b></br>
                                        <?php if(!empty($request->Phones)): ?>
                                            <?php $__currentLoopData = explode(',',$request->Phones); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $phone): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                                <div class="col-sm-8 pull-right"><?php echo e($phone); ?></div>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                        <?php else: ?>
                                            <?php echo e('N/A'); ?>

                                        <?php endif; ?>
                                    </div>
                                    <div class="col-sm-3"><b><i class="fa fa-home"></i>Address :</b>
                                        <br>
                                        <?php if(!empty($request->Addresses)): ?>
                                            <?php $__currentLoopData = explode(',',$request->Addresses); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $address): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                                <div class="col-sm-8 pull-right"><?php echo e($address); ?></div>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                        <?php else: ?>
                                            <div class="col-sm-8 pull-right"><?php echo e('N/A'); ?></div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <hr/>
                                </div>

                                <div class="row">
                                    <div align="center" class="col-md-12">
                                        <div class="white-box">
                                            <h3 class="box-title pull-left"><i class="fa fa-image"></i>Uploaded Images :<code
                                                        class="font-12"></code></h3>
                                            <!-- START carousel-->
                                            <div id="carousel-example-captions" data-ride="carousel"
                                                 class="carousel slide">
                                                <ol class="carousel-indicators">
                                                    <?php if(!empty($request->Uploads)): ?>
                                                        <?php $slider = 0; ?>
                                                        <?php $__currentLoopData = explode(',',$request->Uploads); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                                            <li data-target="#carousel-example-captions"
                                                                data-slide-to="<?php echo e($slider); ?>"
                                                                class="<?php echo e(($slider == 0)? 'active' : ''); ?>"></li>
                                                            <?php $slider++; ?>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                                    <?php endif; ?>
                                                </ol>
                                                <div role="listbox" class="carousel-inner">
                                                    <?php $checkfirst = 1; ?>
                                                    <?php if(!empty($request->Uploads)): ?>
                                                        <?php $__currentLoopData = explode(',',$request->Uploads); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                                            <div class="item <?php echo e(($checkfirst==1)?'active':''); ?>">
                                                                <a href="../uploads/<?php echo e($image); ?>" data-fancybox="group" target="_blank">
                                                                    <img src='../uploads/<?php echo e($image); ?>' height="100" width="100" alt="First slide image">
                                                                </a>
                                                            </div>
                                                            <?php $checkfirst++; ?>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                            <!-- END carousel-->
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <hr>
                                </div>
                                <div class="row col-md-12">
                                    <h3 class="col-md-4 box-title"><i Class="fa fa-file-phone-o"></i>Call Confirmation
                                    </h3>
                                    <div class="col-md-3">
                                        <button id='clientaccept<?php echo e($i); ?>' cid="<?php echo e($i); ?>"
                                                class="clientaccept btn btn-success">Accepted
                                        </button>
                                    </div>
                                    <div class="col-md-3">
                                        <button id='clientrefuse<?php echo e($i); ?>' cid="<?php echo e($i); ?>"
                                                class="clientrefuse btn btn-danger">Rejected
                                        </button>
                                    </div>
                                    <br><br><br>
                                    <hr class="m-t-0 m-b-40">
                                </div>
                                <hr class="m-t-0 m-b-40">

                                <div id='accept<?php echo e($i); ?>' class="accept row" hidden>
                                    <div class="row">
                                        <h3 class="box-title">Create Client Request</h3>
                                    </div>
                                    <form id='acceptform' action="<?php echo e(route('requests.storerequest')); ?>" method="post"
                                          class="form-horizontal">
                                        <?php echo e(csrf_field()); ?>

                                        <input type="text" name="status" value="rw" hidden>
                                        <input type="text" id="requestno" name="requestno"
                                               value="<?php echo e($request->RequestNo); ?>" hidden>
                                        <input type="text" id="is_at_home" name="is_at_home"
                                               value="<?php echo e($request->is_at_home); ?>" hidden/>

                                        <?php if($request->is_at_home==1): ?>
                                            <div class="row col-md-12">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Visit Date</label>
                                                        <div class="col-md-9">
                                                            <input id="reservationdate" type="datetime-local"
                                                                   name="deliverydate" class="form-control" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Result Date</label>
                                                        <div class="col-md-9">
                                                            <input id="resultdate1" type="datetime-local"
                                                                   name="estdeiverydate" class="resultdate form-control"
                                                                   required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                        <div class="centersdiv row">
                                            <?php if($request->is_at_home==0): ?>
                                                
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Result Date</label>
                                                        <div class="col-md-9">
                                                            <input id="resultdate1" type="datetime-local"
                                                                   name="estdeiverydate" class="resultdate form-control"
                                                                   required>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                        

                                        <br><br><br>
                                        <hr class="m-t-0 m-b-40">
                                        <div class="col-md-12 form-actions">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <div class="col-md-offset-3 col-md-9">
                                                            <button id="submitaccept<?php echo e($i); ?>" type="submit"
                                                                    class="submit btn btn-info">Submit
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br><br><br>
                                    </form>
                                </div>
                                <hr class="m-t-0 m-b-40">
                                <div id='refuse<?php echo e($i); ?>' class="refuse row" hidden>
                                    <div class="row">
                                        <h3 class="box-title">Create Client Refuse</h3>
                                    </div>
                                    <form id='refuseform' method="post"
                                          action="<?php echo e(route('requests.updateRefuserequest')); ?>" class="form-horizontal">
                                        <?php echo e(csrf_field()); ?>

                                        <input type="text" name="requestno" value="<?php echo e($request->RequestNo); ?>" hidden>
                                        <input type="text" name="status" value="cr" hidden>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-2">comment</label>
                                                    <div class="col-md-9">
                                                        <textarea rows="9" name="refuse_comment" class="form-control"
                                                                  required></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/row-->
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <div class="col-md-offset-3 col-md-9">
                                                            <button id="submitrefuse" type="submit"
                                                                    class="btn btn-success">Submit
                                                            </button>
                                                            <button id="clearrefuse" type="button"
                                                                    class="btn btn-default">Cancel
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php  $i++;  ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
    <?php endif; ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>

    <script>
        function sorting(id, order) {
            if (id == 1) {
                if (order == 'down') {
                    document.getElementById("order_name").value = 'up';
                } else {
                    document.getElementById("order_name").value = 'down';
                }
                document.getElementById("btnsortname").click();
            } else if (id == 2) {
                if (order == 'down') {
                    document.getElementById("order_date").value = 'up';
                } else {
                    document.getElementById("order_date").value = 'down';
                }
                document.getElementById("btnsortdate").click();
            } else if (id == 3) {
                if (order == 'down') {
                    document.getElementById("order_location").value = 'up';
                } else {
                    document.getElementById("order_location").value = 'down';
                }
                document.getElementById("btnsortlocation").click();
            } else if (id == 4) {
                if (order == 'down') {
                    document.getElementById("order_status").value = 'up';
                } else {
                    document.getElementById("order_status").value = 'down';
                }
                document.getElementById("btnsortstatus").click();
            }

        }

        $(document).ready(function () {

            var sectionStart = 1;
            var centeri = 1;
            var currentPanel;
            var radiologyTypes_data ='';

            //var options = $("#radiologytypelist > option").clone();

            $('.page-title').html('Active Patient Requests');

            /*//////////////////////////////// Start Detect current panel //////////////////////////////////////*/
            $('.fa-chevron-circle-down').on('click', function () {
                var id = $(this).attr('cid');
                currentPanel = '.panel' + id;
            });
            /*/////////////////////////////// End Detect current panel /////////////////////////////////////////*/

            /*/////////////////////////////// Start Call Confirmation JS //////////////////////////////////////*/
            $('.clientaccept').on('click', function () {

                var id = $(this).attr('cid');

                $('#accept' + id).fadeIn('slow');
                $('#accept' + id).attr('hidden', false);
                $('#refuse' + id).fadeOut('slow');
                $('#refuse' + id).attr('hidden', true);
            });

            $('.clientrefuse').on('click', function () {
                var id = $(this).attr('cid');

                $('#refuse' + id).fadeIn('slow');
                $('#refuse' + id).attr('hidden', false);
                $('#accept' + id).fadeOut('slow');
                $('#accept' + id).attr('hidden', true);
            });
            /*////////////////////////////////////// End Call Confirmation JS ////////////////////////////////*/
            $('.list').attr('disabled',true);
            /*////////////////////////////////////// Start Fill in the list //////////////////////////////////*/
            $('.white-box').on("click", '.radiologytypelist', function () {
                var target = $(this).attr('clist');
                var type = $(this).val();

                $(currentPanel).find('#' + target).empty();
                $(currentPanel).find('#' + target).attr('disabled',false);
                $(currentPanel).find('#' + target).append("<option value=''></option>");
                $.each(radiologyTypes_data, function (index, itemObj) {
                    if (itemObj.type_group_id == type) {
                        $(currentPanel).find('#' + target).append("<option value='" + itemObj.id + "'>" + itemObj.en_name + "</option>");
                    }
                });
            });
            /*////////////////////////////////////// Enf Fill in the list ///////////////////////////////////*/
            $('body').on('change','.list',function(){
                var list_val = $(this).val();
                $.each(radiologyTypes_data, function (index, itemObj) {
                    if (itemObj.id == list_val) {
                        $(currentPanel).find('.price').val(itemObj.price);
                        $(currentPanel).find('.def').val(itemObj.definition);
                        $(currentPanel).find('.prep').val(itemObj.preparation);
                        $(currentPanel).find('.notes').val(itemObj.notes);
                    }
                });

            });


            /*////////////////////////////////////// Enf Fill in the list ///////////////////////////////////*/

            /*/////////////////////////////////////// Add New Section form //////////////////////////////////*/
            $('.white-box').on('click', '.Add', function (e) {
                e.preventDefault();
                $(this).attr('disabled', true);

                var pre_sectionStart = sectionStart;
                sectionStart++;
                centeri = 1;

                var copyEl = [];
                copyEl.push("<div cid=" + sectionStart + " id='section" + sectionStart + "' class='section'>");
                copyEl.push("</br></br></br>");
                copyEl.push("<hr>");
                copyEl.push("<div class='row'>");
                copyEl.push("<div class='col-md-4'>");
                copyEl.push("<div class='form-group'>");
                copyEl.push("<label class='control-label col-md-3'>Type</label>");
                copyEl.push("<div class='col-md-9'>");
                copyEl.push("<select id='radiologytypelist" + sectionStart + "' clist='radiologycategory" + sectionStart + "' class='radiologytypelist form-control' required>");
                copyEl.push($('#radiologytypelist1').html());
                copyEl.push("</select>");
                copyEl.push("</div></div></div>");

                copyEl.push("<div class='col-md-4'>");
                copyEl.push("<div class='form-group'>");
                copyEl.push("<label class='control-label col-md-3'>Radiology</label>");
                copyEl.push("<div class='col-md-9'>");
                copyEl.push("<Select class='form-control list' name='radio[" + sectionStart + "][radiology]' id='radiologycategory" + sectionStart + "' required disabled>");
                copyEl.push("<option value=''></option>");
                copyEl.push("</Select>");
                copyEl.push("</div></div></div>");

                copyEl.push("<div class='col-md-4'>");
                copyEl.push("<div class='form-group'>");
                copyEl.push("<label class='control-label col-md-3'>Price</label>");
                copyEl.push("<div class='col-md-9'>");
                copyEl.push("<input min='0' id='price' type='number' name='radio[" + sectionStart + "][price]' class='price form-control' required>");
                copyEl.push("</div></div></div></div>");

                copyEl.push("<div class='row'>");
                copyEl.push("<div class='col-md-12'>");
                copyEl.push("<div class='form-group'>");
                copyEl.push("<label class='control-label col-md-1'>Definitions</label>");
                copyEl.push("<div class='col-md-11'>");
                copyEl.push("<textarea id='definitions' rows='1' name='radio[" + sectionStart + "][definitions]' class='def form-control'></textarea>");
                copyEl.push("</div></div></div>");

                copyEl.push("<div class='row'>");
                copyEl.push("<div class='col-md-12'>");
                copyEl.push("<div class='form-group'>");
                copyEl.push("<label class='control-label col-md-1'>Preparation</label>");
                copyEl.push("<div class='col-md-11'>");
                copyEl.push("<textarea id='preparation' rows='5' name='radio[" + sectionStart + "][preparation]' class='prep form-control'></textarea>");
                copyEl.push("</div></div></div>");

                copyEl.push("<div class='col-md-12'>");
                copyEl.push("<div class='form-group' >");
                copyEl.push("<label class='control-label col-md-1' >Notes</label>");
                copyEl.push("<div class='col-md-11'>");
                copyEl.push("<textarea id='notes'  rows='5' name='radio[" + sectionStart + "][notes]' class='notes form-control'></textarea>");
                copyEl.push("</div></div></div></div>");

//                copyEl.push("<div class='centersdiv row'>");
//                if ($(currentPanel).find('#is_at_home').val() == 0) {
//                    copyEl.push("<div class='col-md-12 centerdivchild'>");
//                    copyEl.push("<div class='col-md-6'>");
//                    copyEl.push("<div class='form-group'>");
//                    copyEl.push("<label class='control-label col-md-3'>Add Centers</label>");
//                    copyEl.push("<div class='col-md-9'>");
//                    copyEl.push("<Select class='form-control' name='radio[" + sectionStart + "][centers]["+1+"][center]' required>");
//                    copyEl.push($('#centerlist1').html());
//                    copyEl.push("</Select>");
//                    copyEl.push("</div></div></div>");
//                    copyEl.push("<div class='col-md-4'>");
//                    copyEl.push("<div class='form-group'>");
//                    copyEl.push("<div class='col-md-9 input-group'>");
//                    copyEl.push("<input type='datetime-local' class='form-control' name='radio[" + sectionStart + "][centers]["+1+"][date]' required/>");
//                    copyEl.push("<span class='input-group-btn'>");
//                    copyEl.push("<button class='add_sub btn btn-success' type='button'><i class='fa fa-lg fa-plus'></i></button>");
//                    copyEl.push("</span>");
//                    copyEl.push("</div></div></div></div>");
//                }
//                copyEl.push("</div>");

                copyEl.push("<div class='col-md-12 form-actions'>");
                copyEl.push("<div class='row'>");
                copyEl.push("<div class='col-md-6'>");
                copyEl.push("<div class='row'>");
                copyEl.push("<div class='col-md-offset-3 col-md-9'>");
                copyEl.push("<button class='Add btn btn-success'>Add</button>&nbsp;");
                copyEl.push("<button class='Remove btn btn-danger'>Remove</button>");
                copyEl.push("</div></div></div></div></div></div>");

                copyEl = $(copyEl.join(''));

                $(currentPanel).find('.section:last').after(copyEl.clone());
            });
            /*///////////////////////////////////// End copy Form Section ////////////////////////////////////*/

            /*/////////////////////////////////////// Delete section /////////////////////////////////////////*/
            $('.white-box').on("click", ".Remove", function (e) {
                e.preventDefault();
                currentsection = $(this).closest(".section").attr('cid');
                lastsection = $(currentPanel).find('.section:last').attr('cid');

                if (currentsection == lastsection) {
                    $(this).closest(".section").prev().find('.Add').attr('disabled', false);
                }

                $(this).closest(".section").remove();
                $(this).remove();
            });
            /*////////////////////////////////////// End delete section /////////////////////////////////////*/

            /*////////////////////////////////// Start Control Multi Selection //////////////////////////////*/
            $('.white-box').on('mousedown', '.centers > option', function (e) {
                e.preventDefault();
                $(this).prop('selected', $(this).prop('selected') ? false : true);
                return false;
            });
            /*////////////////////////////////// End Control Multi Selection //////////////////////////////*/

            /*////////////////////////////////// Start Control Date Selection /////////////////////////////*/
            $('.white-box').on('change', '.resultdate', function () {
                var result_element = $(this);
                var resultdate = $(this).val();
                var reservedate = $(currentPanel).find('input[id*="reservationdate"]').val();
                if (resultdate < reservedate) {
                    result_element.val(" ");
                }
            });
            /*/////////////////////////////////// End end Control Date Selection /////////////////////////////*/

            /*////////////////////////////////// Start Add Centers in Case of center /////////////////////////*/
            $('.white-box').on('click', '.add_sub', function (e) {
                centeri++;
                e.preventDefault();
                var copyEl = [];
                copyEl.push("<div class='col-md-12 centerdivchild'>");
                copyEl.push("<div class='col-md-6'>");
                copyEl.push("<div class='form-group'>");
                copyEl.push("<label class='control-label col-md-3'>Add Centers</label>");
                copyEl.push("<div class='col-md-9'>");
                copyEl.push("<Select class='form-control' name='centers[" + centeri + "][center]' required>");
                copyEl.push($('#centerlist1').html());
                copyEl.push("</Select>");
                copyEl.push("</div></div></div>");
                copyEl.push("<div class='col-md-4'>");
                copyEl.push("<div class='form-group'>");
                copyEl.push("<div class='col-md-9 input-group'>");
                copyEl.push("<input type='datetime-local' class='form-control' name='centers[" + centeri + "][date]' required/>");
                copyEl.push("<span class='input-group-btn'>");
                copyEl.push("<button class='remove_sub btn btn-danger' type='button'><i class='fa fa-lg fa-minus'></i></button>");
                copyEl.push("</span>");
                copyEl.push("</div></div></div></div>");

                copyEl = $(copyEl.join(''));

                $(currentPanel).find('.centersdiv').find('.centerdivchild:last').after(copyEl.clone());
                i++;
            });
            /*/////////////////////////////// end add centers in case of center ////////////////////////////*/

            /*/////////////////////////////// Start Remove  centers in case of center /////////////////////*/
            $('.white-box').on('click', '.remove_sub', function () {
                $(this).closest('.centerdivchild').remove();
            });
            /*/////////////////////////////// End Remove centers in case of center ////////////////////////*/
        });
        var ajax_call = function() {
        var promise = $.ajax({
                    type: "POST",
                    url: "/notifiy",
                    
                }).done(function(response) {
                     var last_notifiycount = document.getElementById("last_notifiycount").value;
                       if(response > last_notifiycount)
                       {
                           document.getElementById("last_notifiycount").value = response;
                           var audio = new Audio('file.mp3');
                            audio.play();
                            
                       }
                  //  $( "#fafwPatient requests" ).html( "<p style='color:red;'>"+response+"</p>" );
                   

var myElem = document.getElementById('fafwRequests');

document.getElementById("fafwRequests").innerHTML = response;


                })
        
};

var interval = 10 ; // where X is your every X minutes

setInterval(ajax_call, interval);

    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>