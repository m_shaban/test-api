<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="icon" type="image/png"  href="<?php echo e(asset('favicon.png')); ?>">
    <title>Radiology</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo e(asset('bootstrap/dist/css/bootstrap.min.css')); ?>" rel="stylesheet">
    <!-- Menu CSS -->
    <!-- animation CSS -->
    <link href="<?php echo e(asset('plugins/bower_components/owl.carousel/owl.carousel.min.css')); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo e(asset('plugins/bower_components/owl.carousel/owl.theme.default.css')); ?>" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/jquery.fancybox.min.css')); ?>">
    <link href="<?php echo e(asset('css/animate.css')); ?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo e(asset('css/datatable.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/style.css')); ?>" rel="stylesheet">
    
    <!-- color CSS you can use different color css from css/colors folder -->
    <link href="<?php echo e(asset('css/colors/green.css')); ?>" id="theme"  rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <link href="<?php echo e(asset('flags/assets/docs.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('flags/css/flag-icon.css')); ?>" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php echo $__env->yieldContent('style'); ?>
</head>
<body dir="ltr">
<!-- Preloader -->
<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>
<input type="text" id='dirlang' value="<?php echo e(session()->get('locale')); ?>" hidden>
<div id="wrapper">
<?php echo $__env->make('partials.topnav', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('partials.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Welcome <?php echo e(Auth::user()->name); ?></h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-lg-12 col-sm-12">
                    <div class="white-box">
                        <div class="row row-in">
                               <?php echo $__env->yieldContent('content'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer class="footer text-center"> 2017 &copy; Doc Medical Services </footer>
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="<?php echo e(asset('plugins/bower_components/jquery/dist/jquery.min.js')); ?>"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo e(asset('bootstrap/dist/js/bootstrap.min.js')); ?>"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo e(asset('plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js')); ?>"></script>
<!--slimscroll JavaScript -->
<script src="<?php echo e(asset('js/jquery.slimscroll.js')); ?>"></script>
<!--Wave Effects -->
<script src="<?php echo e(asset('js/waves.js')); ?>"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo e(asset('js/custom.min.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/owl.carousel/owl.carousel.js')); ?>"></script>
<script src="<?php echo e(asset('plugins/bower_components/owl.carousel/owl.carousel.min.js')); ?>"></script>
<!--Style Switcher -->
<script src="<?php echo e(asset('plugins/bower_components/styleswitcher/jQuery.style.switcher.js')); ?>"></script>
<!-- Yagara -->
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="<?php echo e(asset('js/jquery.fancybox.min.js')); ?>"></script>
<?php echo $__env->yieldContent('script'); ?>
</body>
<script>

    function _successMessage(t)
    {
        $("#alert-success").fadeIn();
        $("#alert-success").fadeOut(t);
    }

    function _failMessage(t)
    {
        $("#alert-fail").fadeIn();
        $("#alert-fail").fadeOut(t);
    }

    function _infoMessage(t)
    {
        $("#alert-info").fadeIn();
        $("#alert-info").fadeOut(t);
    }

    function onlyEnglish(en_element)
    {
        en_element.keypress(function(event){
            var ew = event.which;
            if(ew == 32)
                return true;
            if(48 <= ew && ew <= 57)
                return true;
            if(65 <= ew && ew <= 90)
                return true;
            if(97 <= ew && ew <= 122)
                return true;
            return false;
        });
    }

    function onlyArabic(ar_element)
    {
        ar_element.keypress(function(event)
        {

        });

    }

    $(document).ready(function () {

//        if($('#dirlang').val()=='ar')
//        {
//            $('body').attr('dir','rtl');
//        }

        if($('#db').val()==1)

        {
            _successMessage(5000);
        }else if($('#db').val()==0)
        {
            _failMessage(5000);
        }else{
            _infoMessage(5000);
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        })

        $('.lang').on('click',function(e){
            e.preventDefault();
            var locale = $(this).attr('clang');

            //console.log(locale);

            var promise = $.ajax({
                type: "POST",
                url: "/language",
                data : {'lang' : locale}
            }).done(function(response) {

            })
                .fail(function() { alert("error"); })
                .always(function() {
                    window.location.reload(true);
                    if(locale=='ar')
                    {
                        $('#wrapper').attr('dir','rtl');
                    }
                });

        });


    });
</script>
</html>

    
        
            
                
                
            
            

            
            
                
                
            
        
    


