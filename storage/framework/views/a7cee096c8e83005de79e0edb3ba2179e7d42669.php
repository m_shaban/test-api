<?php $__env->startSection('style'); ?>
    <style>
        .login-register{
            background: #00bcd4;
        }
        .checkbox label::before{
            border:1px solid #AAA;
        }
        .checkbox-primary input[type=checkbox]:checked+label::before{
            background-color: #00bcd4;
            border-color: #00bcd4;
        }
    </style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <?php echo e(\Illuminate\Support\Facades\Session::get('admin')); ?>

    <section id="wrapper" class="login-register">
    <div class="login-box">
        <div class="white-box">
            <form class="form-horizontal" id="loginform" method="post" action="<?php echo e(route('login.post')); ?>">
                <?php echo e(csrf_field()); ?>

                <center>
                    <h3 class="box-title m-b-20"><span style="color: #10385B;font-weight: bold;">Doctor</span> <span style="color: #4FD3FF;font-weight: bold;">On</span> <span style="color: #10385B;font-weight: bold;">Call</span></h3>
                    <h3 class="box-title m-b-20"><span style="color: #10385B;font-weight: bold;">Radiology Services</span></h3>
                </center>
               
                <center>
                    <img src="images/doc.png" width="200" height="200" />
                </center>
                <br />

                <div class="form-group ">
                    <div class="col-xs-12">
                        <input class="form-control" type="text" placeholder="Username" name="username">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <input class="form-control" type="password" placeholder="Password" name="password">
                    </div>
                </div>
                <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                <div class="form-group">
                    <div class="col-md-12">
                        <div class="checkbox checkbox-primary pull-left p-t-0">
                            <input id="checkbox-signup" type="checkbox" name="remember">
                            <label for="checkbox-signup"> Remember me </label>
                        </div>
                    </div>
                </div>
                <div class="form-group text-center m-t-20">
                    <div class="col-xs-12">
                        <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Log In</button>
                    </div>
                </div>
            </form>

        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>
<script src="<?php echo e(asset('plugins/bower_components/jquery/dist/jquery.min.js')); ?>"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo e(asset('bootstrap/dist/js/bootstrap.min.js')); ?>"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo e(asset('plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js')); ?>"></script>
<!--slimscroll JavaScript -->
<script src="<?php echo e(asset('js/jquery.slimscroll.js')); ?>"></script>
<!--Wave Effects -->
<script src="<?php echo e(asset('js/waves.js')); ?>"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo e(asset('js/custom.min.js')); ?>"></script>
<!--Style Switcher -->
<script src="<?php echo e(asset('plugins/bower_components/styleswitcher/jQuery.style.switcher.js')); ?>"></script>
</body>
</html>

<?php echo $__env->make('loginlayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>