<nav class="navbar navbar-default navbar-static-top m-b-0">

    <div class="navbar-header">
        <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse">
            <i class="ti-menu"></i>
        </a>
        <div class="top-left-part">
            <a style='padding-left: 4%;' class="logo" href="<?php echo e(action('HomeController@index')); ?>">
                <b>
                    <img src="<?php echo e(asset('images/logo.png')); ?>" alt="home" style="width: 55px;">
                </b>
            </a>
        </div>
        <ul class="nav navbar-top-links navbar-left hidden-xs active">
            <li><a href="javascript:void(0)" class="open-close hidden-xs waves-effect waves-light">
                    <i class="icon-arrow-left-circle ti-menu"></i></a>
            </li>
            
            
            
            
            
            
        </ul>
        <ul class="nav navbar-top-links navbar-right pull-right">

        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        <!-- /.dropdown-messages -->
            </li>
            <!-- /.dropdown -->
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        <!-- /.dropdown-tasks -->
            </li>
            <!-- /.dropdown -->

            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-globe"></i>
                    <b> Language </b>
                </a>
                <ul class="dropdown-menu dropdown-user animated flipInY">
                    
                    
                    
                    
                    
                    
                    <li><a class="lang" clang="en" href="#"><span class="flag-icon flag-icon-us"></span> English</a></li>
                    <li><a class="lang" clang="ar" href="#"><span class="flag-icon flag-icon-eg"></span> Arabic</a></li>
                </ul>
                <!-- /.dropdown-user -->
            </li>

            <li class="dropdown"> <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#">
                    <img src="<?php echo e(asset('images/profileFilled-50.png')); ?>" alt="user-img" width="36" class="img-circle">
                    <b class="hidden-xs"><?php echo e(Auth::user()->name); ?></b> </a>
                <ul class="dropdown-menu dropdown-user animated flipInY">
                    
                    
                    
                    
                    
                    
                    <li><a href="<?php echo e(route('auth.logout')); ?>"><i class="fa fa-power-off"></i> Logout</a></li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- .Megamenu -->
            
                
                
                    
                        
                            
                            
                            
                            
                            
                            
                            
                            
                            

                        
                    
                    
                        
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            

                        
                    
                    
                        
                            
                            
                            
                            
                            
                            
                            
                            
                            
                        
                    
                    
                        
                            
                            
                            
                            
                            
                            
                            
                        
                    
                    
                        
                            
                            
                            
                            
                            
                            
                        
                    
                
            
            <!-- /.Megamenu -->
            <li class="right-side-toggle"> <a class="waves-effect waves-light" href="<?php echo e(route('auth.logout')); ?>"><i class="ti-lock"></i></a></li>
            <!-- /.dropdown -->
        </ul>
    </div>
    <!-- /.navbar-header -->
    <!-- /.navbar-top-links -->
    <!-- /.navbar-static-side -->
</nav>
<div style="padding-top:2px;"></div>
