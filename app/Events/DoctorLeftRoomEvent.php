<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class DoctorLeftRoomEvent extends ChannelEvent
{
    use InteractsWithSockets, SerializesModels;

    public $doctorId;


    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($roomName,$doctorId)
    {
        parent::__construct($roomName);
        $this->doctorId = $doctorId;
    }

    public function broadcastAs()
    {
        return 'doctor.left_room';
    }
}

