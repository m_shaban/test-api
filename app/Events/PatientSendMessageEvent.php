<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class PatientSendMessageEvent extends ChannelEvent
{
    use InteractsWithSockets, SerializesModels;
    public $message;
    public $message_type;
    public $created_at;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($roomName, $msg, $type, $createdAt)
    {
        parent::__construct($roomName);
        $this->message = $msg;
        $this->message_type = $type;
        $this->created_at = $createdAt;
    }


    public function broadcastAs()
    {
        return 'patient.send_message';
    }
}