<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestQuestionVendorServiceQuestion extends Model
{
    
    
    protected  $table = 'vendorservicequestion';
    public function vendor_services(){
        return $this->belongsTo(VendorServices::class,'vendor_service_id','id');
        }
        public function question_types(){
        return $this->belongsTo(QuestionTypes::class,'question_type_id','id');
        }
        public function request_question(){
        return $this->hasMany(RequestQuestion::class,'vendor_service_question_id','id');
        }
}
