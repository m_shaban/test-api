<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [

        'App\Events\SomeEvent' => [
            'App\Listeners\EventListener',
        ],

        'App\Events\DoctorTyping' => [
            'App\Listeners\DoctorTyping',
        ],
        'App\Events\PatientTyping' => [
            'App\Listeners\PatientTyping',
        ],
        'App\Events\DoctorStopTyping' => ['App\Listeners\DoctorStopTyping'],
        'App\Events\PatientStopTyping' => ['App\Listeners\PatientStopTyping'],
        'App\Events\CreateRoomEvent' => ['App\Listeners\CreateRoomListener'],
        'App\Events\CloseRoomEvent' => ['App\Listeners\CloseRoomListener'],
        'App\Events\DoctorJoinedRoomEvent' => ['App\Listeners\DoctorJoinedRoomListener'],
        'App\Events\PatientJoinedRoomEvent' => ['App\Listeners\PatientJoinedRoomListener'],
        'App\Events\PatientLeftRoomEvent' => ['App\Listeners\PatientLeftRoomListener'],
        'App\Events\DoctorLeftRoomEvent' => ['App\Listeners\DoctorLeftRoomListener'],
        'App\Events\DoctorSendMessageEvent' => ['App\Listeners\DoctorSendMessageListener'],
        'App\Events\PatientSendMessageEvent' => ['App\Listeners\PatientSendMessageListener']

    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
