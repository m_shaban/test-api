<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VendorServicesTypes extends Model
{
    protected $table = 'vendorservicestypes';
    public function vendor_services(){
        return $this->belongsTo(VendorServices::class,'vendor_service_id','id');
        }
        
        
         public function vendor_items(){
        return $this->hasMany(VendorItems::class,'vendor_service_type_id','id');
        }
}
