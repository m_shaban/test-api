<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class request_status extends Model
{
    protected  $table = 'RequestStatus';
    public function request_header(){
        return $this->hasMany(RequestHeader::class,'request_status_id','id');
        }
        public function request_log(){
        return $this->hasMany(RequestLog::class,'request_status_id','id');
        }
}
