<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VendorItems extends Model
{
    protected $table = 'vendoritems';
    public function vendor_services_types(){
        return $this->belongsTo(VendorServicesTypes::class,'vendor_service_type_id','id');
        }
        
    public function vendor_item_branches(){
        return $this->hasMany(VendorItemBranches::class,'vendor_item_id','id');
        }    
         public function request_detail(){
        return $this->hasMany(RequestDetail::class,'vendor_item_id','id');
        }
        
}
