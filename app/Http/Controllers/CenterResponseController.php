<?php
namespace App\Http\Controllers;

use App\PatientRequest;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

use App\PatientLogin;
use Illuminate\Http\Request;


class CenterResponseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = "Select * from v_patientrequest where status = 'qw'";
        $requests = collect(DB::select($query));
        return view('centerresponses.index',compact('requests'));

    }

//    public function answersAjax(Request $request)
//    {
//        $requestNo = $request->input('id');
//
//        if ($request->ajax())
//        {
//            $query = " Select EnQusestion , QuestionAnswerStr , QuestionAnswerBool  from v_patientanswers  where RequestNo = ".$requestNo;
//
//            $answers = collect(DB::select($query))->toJson();
//            return $answers;
//        }
//    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dbSuccsess = false;
        $error = 'Unknown Error - store';

        $this->validate($request,[
            'requestno'=> 'required',
            'status'=>'bail|required',
        ]);

        DB::beginTransaction();

        try
        {
            
            $patient_request = PatientRequest::Where('id','=',$request->requestno)->first();
            $patient_request->update(['status'=>$request->status]);
            $dbSuccsess = true;
               define( 'API_ACCESS_KEY', 'AIzaSyD0Xk1xHEL9q3Qo0EaZD1K_Vs9j_fdiUik' );
     $patientrequest = PatientRequest::find($request->requestno);
     $patientlogin = PatientLogin::Where('patient_id','=',$patientrequest['patient_id'])->where('device_token', '!=', "")->where('device_id', '!=', "")->orderBy('id', 'desc')->first();
    $registrationIds = $patientlogin['device_token'];
    $message = 'Your Radiology Request has been confirmed.';
    
#prep the bundle
     $msg = array
          (
		"type"=>"1",
                "title"=>"The center confirm your radiology request",
                "message"=> $message,
                "requestId"=> $request->requestno,
                "status"=>$request->status,
          );
      
	$fields = array
			(
				'to'		=> $registrationIds,
				'data'	=> $msg
			);
	
	
	$headers = array
			(
				'Authorization: key=' . API_ACCESS_KEY,
				'Content-Type: application/json'
			);
#Send Reponse To FireBase Server	
		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch );
		curl_close( $ch );
        }catch(Exception $ex)
        {
            // log error
            $error = $ex->getMessage();
        }finally{
            if($dbSuccsess)
            {
                DB::commit();
                            #API access key from Google API's Console
 

                
                
                
                return redirect()->back()
                                 ->with( 'db' , $dbSuccsess);
            }
            else{
                DB::rollback();
                return redirect()->back()
                    ->with( 'db' , $dbSuccsess)
                    ->with( 'error' , $error);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
        public function notifiycenter(Request $request)
    {

        if ($request->ajax())
        {
            $query = " Select count(*) as cid from v_patientrequest where status = 'qw'";

            $answers = DB::select($query);
            
            foreach($answers as $r){
               $count=  $r->cid;
            }
            
            
            return $count;
        }
    }
        public function notifiyresult(Request $request)
    {

        if ($request->ajax())
        {
            $query = " Select count(*) as cid from v_patientrequest where status = 'pa' AND `report` IS Null";

            $answers = DB::select($query);
            
            foreach($answers as $r){
               $count=  $r->cid;
            }
            
            
            return $count;
        }
    }    
    

}
