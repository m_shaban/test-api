<?php
namespace App\Http\Controllers;

use App\RadiologyType;
use App\RadiologyTypeCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class RadiologyTypeController extends Controller
{
    private $radiologyCats;
    function __construct()
    {
        $this->radiologyCats = RadiologyTypeCategory::all();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $radiologyRads = RadiologyType::all();
        return view('radiologytypes.index',compact('radiologyRads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $radiologyCats = $this->radiologyCats;
        return view('radiologytypes.create',compact('radiologyCats'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->input());
        $dbSuccsess = false;
        $error = 'Unknown error - Store';
        DB::beginTransaction();
        try{
            RadiologyType::create($request->input());
            $dbSuccsess = true;
        }catch(Exception $ex)
        {
            $error = $ex->getMessage();
        }finally
        {
            if($dbSuccsess)
            {
                DB::commit();
                return redirect()->back()
                    ->with( 'db' , $dbSuccsess);
            }
            else{
                //DB::rollback();
                return redirect()->back()
                    ->with( 'db' , $dbSuccsess)
                    ->with( 'error' , $error);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $rad = RadiologyType::find($id);
        $radiologyCats = $this->radiologyCats;
        return view('radiologytypes.show',compact('rad','radiologyCats'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rad = RadiologyType::find($id);
        $radiologyCats = $this->radiologyCats;
        return view('radiologytypes.edit',compact('rad','radiologyCats'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rad = RadiologyType::find($id);

        $dbSuccsess = false;

        $error = 'Unknown error - Store';

        DB::beginTransaction();
        try {
            $rad->update($request->input());
            $dbSuccsess = true;
        }catch(Exception $ex)
        {
            $error = $ex->getMessage();
        }finally
        {
            if($dbSuccsess)
            {
                DB::commit();
                return redirect()
                    ->back()
                    ->with( 'db' , $dbSuccsess);
            }
            else{
                DB::rollback();
                return redirect()
                    ->back()
                    ->with( 'db' , $dbSuccsess)
                    ->with( 'error' , $error);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $rad = RadiologyType::find($id);

        $dbSuccsess = false;

        $error = 'Unknown error - destroy';

        DB::beginTransaction();
        try {
            $rad->delete();
            $dbSuccsess = true;
        }catch(Exception $ex)
        {
            $error = $ex->getMessage();
        }finally
        {
            if($dbSuccsess)
            {
                DB::commit();
                return redirect()
                    ->back()
                    ->with( 'db' , $dbSuccsess);
            }
            else{
                DB::rollback();
                return redirect()
                    ->back()
                    ->with( 'db' , $dbSuccsess)
                    ->with( 'error' , $error);
            }
        }
    }
}
