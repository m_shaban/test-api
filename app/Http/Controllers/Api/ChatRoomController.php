<?php

namespace App\Http\Controllers\Api;

use App\DoctorLogin;
use App\Events\CloseRoomEvent;
use App\Events\CreateRoomEvent;
use App\Events\DoctorJoinedRoomEvent;
use App\Events\DoctorLeftRoomEvent;
use App\Events\DoctorStopTyping;
use App\Events\DoctorTyping;
use App\Events\PatientJoinedRoomEvent;
use App\Events\PatientLeftRoomEvent;
use App\Events\PatientStopTyping;
use App\Events\PatientTyping;
use App\Models\Chat\ChatRoom;
use App\Patient;
use App\PatientLogin;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ChatRoomController extends Controller
{
    public function create(Request $request){
        $validator = Validator::make($request->all(),[
            'patient_id'=>'bail|required',
            'doctor_api'=>'bail|required'
        ]);
        if($validator->fails()){
            $errors = $validator->errors()->all();
            return response()->json([
                'is_successful'=>false,
                'error_message'=>reset($errors)
            ]);
        }

        $patient = Patient::find($request->input('patient_id'));
        if(is_null($patient)) {
            return response()->json([
                'is_successful' => false,
                'error_message' => 'Can\'t find Patient related to Id '. $request->input('patient_id')
            ]);
        }
        $doctor_logins = DoctorLogin::where('api_key',$request->input('doctor_api'))->first();
        if(is_null($doctor_logins)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Doctor Api Key'
            ]);
        }
        $doctor = $doctor_logins->doctor;
        if(is_null($doctor)) {
            return response()->json([
                'is_successful' => false,
                'error_message' => 'Can\'t find Doctor related to Api Key '. $request->input('doctor_api')
            ]);
        }
        $room =null;

        DB::transaction(function()use($patient,$doctor,&$room){
            $room = ChatRoom::create([
                'doctor_id'=>$doctor->id,
                'patient_id'=>$patient->id
            ]);
            $room->room_name = "room-{$doctor->id}{$patient->id}{$room->id}";
            $room->save();
            event(new CreateRoomEvent($room->room_name,$patient->id,$doctor->id));
        });
        return response()->json([
            'is_successful' => true,
            'room_name' => $room->room_name
        ]);

    }
    public function close(Request $request){
        $result = $this->validateRoom($request);
        if(! $result instanceof ChatRoom){
            return $result;
        }
        if($result->closed_at == null){
            $result->closed_at=Carbon::now('Africa/Cairo');
            $result->save();
        }
        event(new CloseRoomEvent($result->room_name));
        return response()->json([
            'is_successful' => true
        ]);
    }


    public function patient_joined_room(Request $request){
        $result = $this->validateRoom($request);
        if(! $result instanceof ChatRoom){
            return $result;
        }
        $result->is_patient_online=true;
        $result->save();
        event(new PatientJoinedRoomEvent($result->room_name,$result->patient_id));
        return response()->json([
            'is_successful' => true
        ]);
    }
    public function doctor_joined_room(Request $request){
        $result = $this->validateRoom($request);
        if(! $result instanceof ChatRoom){
            return $result;
        }
        $result->is_doctor_online=true;
        $result->save();
        event(new DoctorJoinedRoomEvent($result->room_name,$result->doctor_id));
        return response()->json([
            'is_successful' => true
        ]);
    }
    public function patient_left_room(Request $request){
        $result = $this->validateRoom($request);
        if(! $result instanceof ChatRoom){
            return $result;
        }
        $result->is_patient_online=false;
        $result->save();
        event(new PatientLeftRoomEvent($result->room_name,$result->patient_id));
        return response()->json([
            'is_successful' => true
        ]);
    }
    public function doctor_left_room(Request $request){
        $result = $this->validateRoom($request);
        if(! $result instanceof ChatRoom){
            return $result;
        }
        $result->is_doctor_online=false;
        $result->save();
        event(new DoctorLeftRoomEvent($result->room_name,$result->doctor_id));
        return response()->json([
            'is_successful' => true
        ]);
    }

    public function patient_is_typing(Request $request){
        $validator = Validator::make($request->all(), [
            'room_name' => 'bail|required'
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return response()->json([
                'is_successful' => false,
                'error_message' => reset($errors)
            ]);
        }
        event(new PatientTyping($request->input('room_name')));
        return response()->json([
            'is_successful' => true
        ]);
    }
    public function doctor_is_typing(Request $request){
        //send event
        $validator = Validator::make($request->all(), [
            'room_name' => 'bail|required'
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return response()->json([
                'is_successful' => false,
                'error_message' => reset($errors)
            ]);
        }
        event(new DoctorTyping($request->input('room_name')));
        return response()->json([
            'is_successful' => true
        ]);
    }

    public function patient_stop_typing(Request $request){
        $validator = Validator::make($request->all(), [
            'room_name' => 'bail|required'
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return response()->json([
                'is_successful' => false,
                'error_message' => reset($errors)
            ]);
        }
        event(new PatientStopTyping($request->input('room_name')));
        return response()->json([
            'is_successful' => true
        ]);
    }
    public function doctor_stop_typing(Request $request){
        $validator = Validator::make($request->all(), [
            'room_name' => 'bail|required'
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return response()->json([
                'is_successful' => false,
                'error_message' => reset($errors)
            ]);
        }
        event(new DoctorStopTyping($request->input('room_name')));
        return response()->json([
            'is_successful' => true
        ]);
    }
    public function get_doctor_status(Request $request){
        $result = $this->validateRoom($request);
        if(! $result instanceof ChatRoom){
            return $result;
        }

        return $result->is_doctor_online;
    }
    public function get_patient_status(Request $request){
        $result = $this->validateRoom($request);
        if(! $result instanceof ChatRoom){
            return $result;
        }
        return $result->is_patient_online;

    }

    public function get_room_details(Request $request){
        $result = $this->validateRoom($request);
        if(! $result instanceof ChatRoom){
            return $result;
        }
        $result->load('messages','patient','doctor');
        return response()->json([
            'is_successful' => true,
            'room'=>[
                'room_name'=>$result->room_name,
                'patient'=>$result->patient->name,
                'doctor'=>$result->doctor->name,
                'patient_status'=>$result->is_patient_online,
                'doctor_status'=>$result->is_doctor_online,
                'created_at'=>$result->created_at,
                'room_status'=>$result->closed_at !== null
            ],
            'messages'=>$result->messages->map(function($item){
              return ['message'=>$item->message,
                  'message_type'=>$item->message_type,
                  'created_by'=>$item->created_by,
                  'created_at'=>$item->created_at
              ];
            })
        ]);
    }

    public function get_room_status(Request $request){
        $result = $this->validateRoom($request);
        if(! $result instanceof ChatRoom){
            return $result;
        }
        return response()->json([
            'is_successful' => true,
            'room_status'=>$result->closed_at !== null
            ]);
    }
    public function get_patient_rooms(Request $request){
        $validator = Validator::make($request->all(),[
            'patient_api'=>'bail|required'
        ]);
        if($validator->fails()){
            $errors = $validator->errors()->all();
            return response()->json([
                'is_successful'=>false,
                'error_message'=>reset($errors)
            ]);
        }
        $patient_logins = PatientLogin::where('api_key',$request->input('patient_api'))->first();
        if(is_null($patient_logins)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Patient Api Key'
            ]);
        }
        $patient = $patient_logins->patient;
        if(is_null($patient)) {
            return response()->json([
                'is_successful' => false,
                'error_message' => 'Can\'t find Patient related to Api Key '. $request->input('patient_api')
            ]);
        }
        $rooms = DB::table('chat_rooms')
            ->where('patient_id',$patient->id)
            ->join('doctor','chat_rooms.doctor_id','doctor.id')
            ->join('patient','chat_rooms.patient_id','patient.id')
            ->orderBy('chat_rooms.created_at','ASC')
            ->select('chat_rooms.room_name',
                'doctor.name AS doctor_name',
                'patient.name AS patient_name',
                'chat_rooms.created_at as created_at',
                DB::raw( "CASE
                            WHEN chat_rooms.closed_at IS NULL THEN 1
                        ELSE 0
                        END AS room_status"),
                DB::raw('(select count(*) from room_messages WHERE room_id = chat_rooms.id)  as messages_count'))
           ->get();
        return response()->json([
            'is_successful' => true,
            'rooms'=>$rooms
        ]);
    }
    public function get_patient_active_rooms(Request $request){
        $validator = Validator::make($request->all(),[
            'patient_api'=>'bail|required'
        ]);
        if($validator->fails()){
            $errors = $validator->errors()->all();
            return response()->json([
                'is_successful'=>false,
                'error_message'=>reset($errors)
            ]);
        }
        $patient_logins = PatientLogin::where('api_key',$request->input('patient_api'))->first();
        if(is_null($patient_logins)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Patient Api Key'
            ]);
        }
        $patient = $patient_logins->patient;
        if(is_null($patient)) {
            return response()->json([
                'is_successful' => false,
                'error_message' => 'Can\'t find Patient related to Api Key '. $request->input('patient_api')
            ]);
        }
        $rooms = DB::table('chat_rooms')
            ->where('patient_id',$patient->id)
            ->whereNull('chat_rooms.closed_at')
            ->join('doctor','chat_rooms.doctor_id','doctor.id')
            ->join('patient','chat_rooms.patient_id','patient.id')
            ->orderBy('chat_rooms.created_at','ASC')
            ->select('chat_rooms.room_name',
                'doctor.name AS doctor_name',
                'patient.name AS patient_name',
                'chat_rooms.created_at as created_at',
                DB::raw('(select count(*) from room_messages WHERE room_id = chat_rooms.id)  as messages_count'))
            ->get();
        return response()->json([
            'is_successful' => true,
            'rooms'=>$rooms
        ]);
    }

    public function get_doctor_rooms(Request $request){
        $validator = Validator::make($request->all(),[
            'doctor_api'=>'bail|required'
        ]);
        if($validator->fails()){
            $errors = $validator->errors()->all();
            return response()->json([
                'is_successful'=>false,
                'error_message'=>reset($errors)
            ]);
        }
        $logins = DoctorLogin::where('api_key',$request->input('doctor_api'))->first();
        if(is_null($logins)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Doctor Api Key'
            ]);
        }
        $doctor = $logins->doctor;
        if(is_null($doctor)) {
            return response()->json([
                'is_successful' => false,
                'error_message' => 'Can\'t find Doctor related to Api Key '. $request->input('doctor_api')
            ]);
        }
        $rooms = DB::table('chat_rooms')
            ->where('doctor_id',$doctor->id)
            ->join('doctor','chat_rooms.doctor_id','doctor.id')
            ->join('patient','chat_rooms.patient_id','patient.id')
            ->orderBy('chat_rooms.created_at','ASC')
            ->select('chat_rooms.room_name',
                'doctor.name AS doctor_name',
                'patient.name AS patient_name',
                'chat_rooms.created_at as created_at',
                DB::raw( "CASE
                            WHEN chat_rooms.closed_at IS NULL THEN 1
                        ELSE 0
                        END AS room_status"),
                DB::raw('(select count(*) from room_messages WHERE room_id = chat_rooms.id)  as messages_count'))
            ->get();
        return response()->json([
            'is_successful' => true,
            'rooms'=>$rooms
        ]);
    }
    public function get_doctor_active_rooms(Request $request){
        $validator = Validator::make($request->all(),[
            'doctor_api'=>'bail|required'
        ]);
        if($validator->fails()){
            $errors = $validator->errors()->all();
            return response()->json([
                'is_successful'=>false,
                'error_message'=>reset($errors)
            ]);
        }
        $logins = DoctorLogin::where('api_key',$request->input('doctor_api'))->first();
        if(is_null($logins)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Doctor Api Key'
            ]);
        }
        $doctor = $logins->doctor;
        if(is_null($doctor)) {
            return response()->json([
                'is_successful' => false,
                'error_message' => 'Can\'t find Doctor related to Api Key '. $request->input('doctor_api')
            ]);
        }
        $rooms = DB::table('chat_rooms')
            ->where('doctor_id',$doctor->id)
            ->whereNull('chat_rooms.closed_at')
            ->join('doctor','chat_rooms.doctor_id','doctor.id')
            ->join('patient','chat_rooms.patient_id','patient.id')
            ->orderBy('chat_rooms.created_at','ASC')
            ->select('chat_rooms.room_name',
                'doctor.name AS doctor_name',
                'patient.name AS patient_name',
                'chat_rooms.created_at as created_at',
                DB::raw('(select count(*) from room_messages WHERE room_id = chat_rooms.id)  as messages_count'))
            ->get();
        return response()->json([
            'is_successful' => true,
            'rooms'=>$rooms
        ]);
    }

    private function validateRoom($request)
    {
        $validator = Validator::make($request->all(), [
            'room_name' => 'bail|required'
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return response()->json([
                'is_successful' => false,
                'error_message' => reset($errors)
            ]);
        }
        $room = ChatRoom::where('room_name', $request->input('room_name'))->first();
        if (is_null($room)) {
            return response()->json([
                'is_successful' => false,
                'error_message' => 'Can\'t find Room ' . $request->input('room_name')
            ]);
        }
        return $room;
    }


}
