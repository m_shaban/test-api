<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use App\PatientLogin;
use App\RequestHeader;
use App\RequestDetail;
use App\RequestAnswer;
use App\request_status;
use App\RequestUploads;
use App\RequestVendorBranches;
use App\request_vendor_branch_item;
use App\VendorItems;
use App\RequestLog;
use App\RequestQuestion;
use App\RequestQuestionVendorServiceQuestion;
use App\PatientRequest;
use App\PatientUpload;
use App\vendors_contacts;
use App\Pharmacy;
use App\v_vendor_items_pharmacy;
use \Exception;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class PharmacyController extends Controller
{
    public function __construct()
    {
    }
    public function update_midicine_list(Request $request)
    {
             $validator = Validator::make($request->all(),[
            'api_key'=>'bail|required',
            'medicine_version'=>'required'
        ]);
        if($validator->fails()){
            $errors = $validator->errors()->all();
            return response()->json([
                'is_successful'=>false,
                'error_message'=>reset($errors)
            ]);
        }
        $login = PatientLogin::where('api_key',$request->input('api_key'))->first();
        if(is_null($login)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Api Key'
            ]);
        }
        $max_db_ver= DB::table('Pharmacy')
    ->max('medicine_version');
        
        $VendorItems = Pharmacy::where('medicine_version','>',$request->input('medicine_version'))->get();
        $medicine_list = array();
        if(is_null($VendorItems)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'medicine list types Not found'
            ]);
        }
        
        foreach ($VendorItems as $VendorItem)
        {
            $vendor_id = VendorItems::where('pharmacy_id','=',$VendorItem->id)->get();
           
            $medicine_list_t = array(
                'medicineId'=>$vendor_id[0]->id,
                'medicineName'=>$VendorItem->name_en,
                'medicineType'=>"",
                'medicinePrice'=>$VendorItem->price,
                'medicine_version'=>$VendorItem->medicine_version,
                'action'=>$VendorItem->action
                
            );
            array_push($medicine_list, $medicine_list_t);
        }
       
        return response()->json([
                'is_successful'=>true,
                'error_message'=>'',
                'medicine_version'=>$max_db_ver,
                'medicine_list'=>$medicine_list,
            
            ]);

    }
    
    public function get_labtest_centers(Request $request)
    {
         $validator = Validator::make($request->all(),[
            'api_key'=>'bail|required'
        ]);
        if($validator->fails()){
            $errors = $validator->errors()->all();
            return response()->json([
                'is_successful'=>false,
                'error_message'=>reset($errors)
            ]);
        }
        $login = PatientLogin::where('api_key',$request->input('api_key'))->first();
        if(is_null($login)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Api Key'
            ]);
        }
         $Vendors = vendors_contacts::all();
        if(is_null($Vendors)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Centers Not found'
            ]);
        }
        
        return response()->json([
                'is_successful'=>true,
                'error_message'=>'',
                'centers_list'=>$Vendors
            ]);
    }
    
    public function paitent_order_pharmacy(Request $request){

            $validator = Validator::make($request->all(),[
                'api_key'=>'required',
                'patient_address_id'=>'required'
                
            ]);
            if($validator->fails()){
                $errors = $validator->errors()->all();
                return response()->json([
                    'is_successful'=>false,
                    'error_message'=>reset($errors)
                ]);
            }

        $login = PatientLogin::where('api_key',$request->input('api_key'))->first();
        if(is_null($login))
        {
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Api Key'
            ]);
        }
        $patient = $login->patient;
        
        if(is_null($patient)) {
            return response()->json([
                'is_successful' => false,
                'error_message' => 'Can\'t find Patient related to Api Key '. $request->input('api_key')
            ]);
        }
        $patient_request = new RequestHeader($request->only([
            'patient_address_id',
            'date'
            ]));
        
        $patient_request['is_home'] = 0 ;
        $patient_request['date'] = '' ;
        $patient_request['requester_id'] = $patient->id;
        $patient_request['requester_type'] = 1;
        $patient_request['recipient_id'] = $patient->id;
        $patient_request['request_status_id'] =1;
        $patient_request['rate'] =0;
        $patient_request['is_stable'] =0;
        $patient_request['is_emergency'] =0;
        $patient_request['comment'] =null;
        $patient_request['weight'] =null;
        $patient_request['has_insurance'] =0;
        $patient_request['type'] =9;
        
        $patient_request['request_type'] =9;
        $patient_request->save();
            
        
            
            foreach($request->input('written_medicine_list') as $written)
            {
                
                $patient_details = new RequestDetail();
                $patient_details['request_header_id']= $patient_request->id;
                $patient_details['written_type_list']= $written;
                $patient_details->save();
            
            }
            foreach($request->input('medicine_type_list') as $written)
            {
                
                $patient_details = new RequestDetail();
                $patient_details['request_header_id']= $patient_request->id;
                $patient_details['vendor_item_id']= $written['medicineId'];
                $patient_details['quantity']= $written['quantity'];
                $patient_details->save();
            
            }            
            //dd($patient_request->request_detail()->createMany(array($request->input('written_type_list'))));
        try{    
          //$patient_request->request_detail()->createMany(array($request->input('test_type_list')));
           
           
            return response()->json([
                'request_id'=>$patient_request->id,
                'is_successful' => true
            ]);
        }catch (Exception $exception){
            return response()->json([
                'is_successful' => false,
                'error_message' => 'Can not create patient request'
            ]);
        }

    }
    public function patient_order_prescription_pharmacy(Request $request){

            $validator = Validator::make($request->all(),[
                'api_key'=>'required',
                'patient_address_id'=>'required',
                'number_of_scaned_prescriptions'=>'required'
                
            ]);
            if($validator->fails()){
                $errors = $validator->errors()->all();
                return response()->json([
                    'is_successful'=>false,
                    'error_message'=>reset($errors)
                ]);
            }

        $login = PatientLogin::where('api_key',$request->input('api_key'))->first();
        if(is_null($login))
        {
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Api Key'
            ]);
        }
        $patient = $login->patient;
        
        if(is_null($patient)) {
            return response()->json([
                'is_successful' => false,
                'error_message' => 'Can\'t find Patient related to Api Key '. $request->input('api_key')
            ]);
        }
        $patient_request = new RequestHeader($request->only([
            'patient_address_id',
            
            ]));
        
        $patient_request['is_home'] = 0 ;
        $patient_request['date'] = '' ;
        $patient_request['requester_id'] = $patient->id;
        $patient_request['requester_type'] = 1;
        $patient_request['recipient_id'] = $patient->id;
        $patient_request['request_status_id'] =1;
        $patient_request['rate'] =0;
        $patient_request['is_stable'] =0;
        $patient_request['is_emergency'] =0;
        $patient_request['comment'] =null;
        $patient_request['weight'] =null;
        $patient_request['has_insurance'] =0;
        
        $patient_request['request_type'] =9;
        $patient_request->save();
            
        
                      
            //dd($patient_request->request_detail()->createMany(array($request->input('written_type_list'))));
        try{    
          //$patient_request->request_detail()->createMany(array($request->input('test_type_list')));
           
           
            return response()->json([
                'request_id'=>$patient_request->id,
                'is_successful' => true
            ]);
        }catch (Exception $exception){
            return response()->json([
                'is_successful' => false,
                'error_message' => 'Can not create patient request'
            ]);
        }

    }

    public function upload_pharmacy_prescription(Request $request){
        
        $validator = Validator::make($request->all(),[
            'api_key'=>'required',
            'image'=>'required',
            'request_id'=>'required'
        ]);
        if($validator->fails()){
            $errors = $validator->errors()->all();
            return response()->json([
                'is_successful' => false,
                'error_message' => reset($errors)
            ]);
        }
        $login = PatientLogin::where('api_key',$request->input('api_key'))->first();
        if(is_null($login)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Api Key'
            ]);
        }
        $patient_request = RequestHeader::find($request->input('request_id'));
        if(is_null($patient_request)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Request Id'
            ]);
        }
        $path = $request->file('image')->store('');
        $patient_upload = new RequestUploads(['upload'=>$path]);
        $patient_request->request_uploads()->save($patient_upload);
        return response()->json([
            'is_successful'=>true
        ]);
    }
    
    
    public function paitent_get_pharmacy_resonse_old(Request $request){
        $validator = Validator::make($request->all(),[
            'api_key'=>'bail|required',
            'pharmacy_request_id'=>'bail|required'
        ]);
        if($validator->fails()){
            $errors = $validator->errors()->all();
            return response()->json([
                'is_successful'=>false,
                'error_message'=>reset($errors)
            ]);
        }
        $login = PatientLogin::where('api_key',$request->input('api_key'))->first();
        if(is_null($login)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Api Key'
            ]);
        }
        $patientRequest = request_vendor_branch_item::where('request_header_id','=',$request->input('pharmacy_request_id'))->get();
        if(is_null($patientRequest)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Request Id '.$request->input('pharmacy_request_id') . ' Not found'
            ]);
        }
        
        return response()->json([
                'is_successful'=>true,
                'error_message'=>'',
                'request_details'=>$patientRequest
            ]);

       

    }
    
    public function paitent_get_pharmacy_resonse(Request $request){
        $validator = Validator::make($request->all(),[
            'api_key'=>'bail|required',
            'pharmacy_request_id'=>'bail|required'
        ]);
        if($validator->fails()){
            $errors = $validator->errors()->all();
            return response()->json([
                'is_successful'=>false,
                'error_message'=>reset($errors)
            ]);
        }
        $login = PatientLogin::where('api_key',$request->input('api_key'))->first();
        if(is_null($login)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Api Key'
            ]);
        }
         $patientRequestsheader = RequestHeader::where('id','=',$request->input('pharmacy_request_id'))->get();
        if(is_null($patientRequestsheader)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Request Id '.$request->input('request_id') . ' Not found'
            ]);
        }
        
         $patientRequests = RequestDetail::where('request_header_id','=',$request->input('pharmacy_request_id'))->get();
       
         if(is_null($patientRequests)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Request Id '.$request->input('pharmacy_request_id') . ' Not found'
            ]);
        }
        $test_type_list = array();

foreach ($patientRequests as $patientRequest) {
    if($patientRequest->vendor_item_id!=null)
    {
 $vendor_item_branch = VendorItems::where('id','=',$patientRequest->vendor_item_id)->get();
 $RequestVendorBranches = RequestVendorBranches::where('request_details_id','=',$patientRequest->id)->get(); 
    $test_type_list_t=array(
        'medicineId'=>$vendor_item_branch[0]->id,
        'medicineName'=>$vendor_item_branch[0]->en_name,
        'medicinePrice'=>$vendor_item_branch[0]->price,
        'quantity'=>$patientRequest->quantity
    );
$test_type_list[]=$test_type_list_t;   
 
}
else 
{
     $vendor_item_branch = VendorItems::where('id','=',$patientRequest->vendor_item_id)->get();
 $RequestVendorBranches = RequestVendorBranches::where('request_details_id','=',$patientRequest->id)->get(); 
    $test_type_list_t=array(
        'medicineId'=>$patientRequest->id,
        'medicineName'=>$patientRequest->written_type_list,
        'medicinePrice'=>0,
        'quantity'=>$patientRequest->quantity
    );
$test_type_list[]=$test_type_list_t;   
 
}
    
    
}

$delivery_estimate_dateTime = $patientRequestsheader[0]->delivery_estimate_dateTime;
if(($delivery_estimate_dateTime==NULL))
{
    $delivery_estimate_dateTime = '';
}
$notes = $patientRequestsheader[0]->notes;
if(($notes==NULL))
{
    $notes = '';
}
        return response()->json([
                'is_successful'=>true,
                'error_message'=>'',
            'status'=>$patientRequestsheader[0]->request_status_id,
                'medicine_list'=>$test_type_list,
            
            'delivery_estimate_dateTime'=>$delivery_estimate_dateTime,
            'notes'=>$notes
            ]);

    }
    
    
    public function cancel_pharmacy_request(Request $request){
        $validator = Validator::make($request->all(),[
            'pharmacy_request_id'=>'bail|required|numeric',
            'api_key'=>'required',
        ]);
        if($validator->fails()){
            $errors = $validator->errors()->all();
            return response()->json([
                'is_successful' => false,
                'error_message' => reset($errors)
            ]);
        }
        $login = PatientLogin::where('api_key',$request->input('api_key'))->first();
        if(is_null($login)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Api Key'
            ]);
        }
        $patient_request = RequestHeader::find($request->input('pharmacy_request_id'));
        if(is_null($patient_request)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Request Id'
            ]);
        }
    
                $patient_request->update(['request_status_id'=>7]);
        

        return response()->json([
            'is_successful'=>true
        ]);
    }
    
    public function rate_pharmacy_service(Request $request){
        $validator = Validator::make($request->all(),[
            'pharmacy_request_id'=>'bail|required|numeric',
            'api_key'=>'required',
            'rate'=>'bail|required|numeric',
        ]);
        if($validator->fails()){
            $errors = $validator->errors()->all();
            return response()->json([
                'is_successful' => false,
                'error_message' => reset($errors)
            ]);
        }
        $login = PatientLogin::where('api_key',$request->input('api_key'))->first();
        if(is_null($login)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Api Key'
            ]);
        }
        $patient_request = RequestHeader::find($request->input('pharmacy_request_id'));
        if(is_null($patient_request)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Request Id'
            ]);
        }
  
                
                $patient_request->update(['request_status_id'=>3,'rate'=>$request->input('rate'),'comment'=>$request->input('comment')]);
        

        return response()->json([
            'is_successful'=>true
        ]);
    }
    
     public function confirm_pharmacy_request(Request $request){
        $validator = Validator::make($request->all(),[
            'pharmacy_request_id'=>'bail|required|numeric',
            'api_key'=>'required',
        ]);
        if($validator->fails()){
            $errors = $validator->errors()->all();
            return response()->json([
                'is_successful' => false,
                'error_message' => reset($errors)
            ]);
        }
        $login = PatientLogin::where('api_key',$request->input('api_key'))->first();
        if(is_null($login)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Api Key'
            ]);
        }
        $patient_request = RequestHeader::find($request->input('pharmacy_request_id'));
        if(is_null($patient_request)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Request Id'
            ]);
        }
        if(!$patient_request->is_home){
            if(empty($request->input('medicine_list') )){
                return response()->json([
                    'is_successful'=>false,
                    'message'=>'Types can not be empty'
                ]);
            }
            
           
          //  $requestvendorbranches = new RequestVendorBranches();
           DB::transaction(function () use ($request){
                DB::table('RequestVendorBranches')
                    ->whereIn('id',$request->input('medicine_list'))
                    ->update(['is_active'=>13,'updated_at'=>Carbon::now()]);
               
            });
           DB::transaction(function () use ($request){
                DB::table('RequestHeader')
                    ->where('id',$request->input('pharmacy_request_id'))
                    ->update(['request_status_id'=>13,'updated_at'=>Carbon::now()]);
               
            });            

        }else{
                DB::transaction(function () use ($request){
                DB::table('RequestHeader')
                    ->where('id',$request->input('request_id'))
                    ->update(['request_status_id'=>11,'updated_at'=>Carbon::now()]);
               
            });
        }

        return response()->json([
            'is_successful'=>true
        ]);
    }

     public function refund_pharmacy_request(Request $request){
        $validator = Validator::make($request->all(),[
            'pharmacy_request_id'=>'bail|required|numeric',
            'api_key'=>'required',
        ]);
        if($validator->fails()){
            $errors = $validator->errors()->all();
            return response()->json([
                'is_successful' => false,
                'error_message' => reset($errors)
            ]);
        }
        $login = PatientLogin::where('api_key',$request->input('api_key'))->first();
        if(is_null($login)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Api Key'
            ]);
        }
        $patient_request = RequestHeader::find($request->input('pharmacy_request_id'));
        if(is_null($patient_request)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Request Id'
            ]);
        }
        if(!$patient_request->is_home){
            if(empty($request->input('medicine_list') )){
                return response()->json([
                    'is_successful'=>false,
                    'message'=>'Types can not be empty'
                ]);
            }
            
           
          //  $requestvendorbranches = new RequestVendorBranches();
           DB::transaction(function () use ($request){
                DB::table('RequestVendorBranches')
                    ->whereIn('id',$request->input('medicine_list'))
                    ->update(['is_active'=>true,'updated_at'=>Carbon::now()]);
               
            });
           DB::transaction(function () use ($request){
                DB::table('RequestHeader')
                    ->where('id',$request->input('request_id'))
                    ->update(['request_status_id'=>17,'updated_at'=>Carbon::now()]);
               
            });            

        }else{
                DB::transaction(function () use ($request){
                DB::table('RequestHeader')
                    ->where('id',$request->input('request_id'))
                    ->update(['request_status_id'=>17,'updated_at'=>Carbon::now()]);
               
            });
        }

        return response()->json([
            'is_successful'=>true
        ]);
    }

    
}