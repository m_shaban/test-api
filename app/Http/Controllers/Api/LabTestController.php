<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use App\PatientLogin;
use App\RequestHeader;
use App\RequestDetail;
use App\RequestAnswer;
use App\request_status;
use App\RequestUploads;
use App\RequestVendorBranches;
use App\request_vendor_branch_item;
use App\VendorItems;
use App\RequestLog;
use App\RequestQuestion;
use App\RequestQuestionVendorServiceQuestion;
use App\PatientRequest;
use App\PatientUpload;
use App\vendors_contacts;
use \Exception;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class LabTestController extends Controller
{
    public function __construct()
    {
    }
    public function get_labtest_tests_types(Request $request)
    {
             $validator = Validator::make($request->all(),[
            'api_key'=>'bail|required'
        ]);
        if($validator->fails()){
            $errors = $validator->errors()->all();
            return response()->json([
                'is_successful'=>false,
                'error_message'=>reset($errors)
            ]);
        }
        $login = PatientLogin::where('api_key',$request->input('api_key'))->first();
        if(is_null($login)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Api Key'
            ]);
        }
         $VendorItems = VendorItems::where('pharmacy_id','=', 0)
               
               ->get();

        if(is_null($VendorItems)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Lab test types Not found'
            ]);
        }
        $test_type_list = array();
        foreach ($VendorItems as $VendorItem)
        {
            
            $test_type_list[] = array(
                'typeId'=>$VendorItem->id,
                'typeName'=>$VendorItem->en_name,
                'description'=>$VendorItem->description,
                'price'=>$VendorItem->price
                
            );

        }
         $Vendors = vendors_contacts::all();
        if(is_null($Vendors)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Centers Not found'
            ]);
        }
        $vendors = array();
        
        foreach ($Vendors as $Vendor)
        {
            $vendors = array (
                "vendor_id"=> $Vendor->vendor_id,
            "vendor_name"=> $Vendor->vendor_name,
            "vendor_branch_id"=> $Vendor->vendor_branch_id,
            "vendor_branch_name"=> $Vendor->vendor_branch_name,
            "longitude"=> doubleval($Vendor->long),
            "latitude"=> doubleval($Vendor->lat),
            "map_link"=> $Vendor->map_link,
            "address"=> $Vendor->address,
            "phones"=> $Vendor->phones,
            "mobiles"=> $Vendor->mobiles
            );
                    
        }
        return response()->json([
                'is_successful'=>true,
                'error_message'=>'',
                'test_type_list'=>$test_type_list,
                'centers_list'=>array($vendors)
            ]);

    }
    
    public function get_labtest_centers(Request $request)
    {
         $validator = Validator::make($request->all(),[
            'api_key'=>'bail|required'
        ]);
        if($validator->fails()){
            $errors = $validator->errors()->all();
            return response()->json([
                'is_successful'=>false,
                'error_message'=>reset($errors)
            ]);
        }
        $login = PatientLogin::where('api_key',$request->input('api_key'))->first();
        if(is_null($login)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Api Key'
            ]);
        }
         $Vendors = vendors_contacts::all();
        if(is_null($Vendors)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Centers Not found'
            ]);
        }
        
        return response()->json([
                'is_successful'=>true,
                'error_message'=>'',
                'centers_list'=>$Vendors
            ]);
    }
    
    public function paitent_request_labtest(Request $request){

            $validator = Validator::make($request->all(),[
                'api_key'=>'required',
                'is_at_home'=>'required',
                'has_insurance'=>'required',
                'date'=>'required'
            ],[
                'patient_address_id.required_if' => 'The :attribute field is required.',
                'center_id.required_if' => 'The :attribute field is required.',
            ]);
            if($validator->fails()){
                $errors = $validator->errors()->all();
                return response()->json([
                    'is_successful'=>false,
                    'error_message'=>reset($errors)
                ]);
            }

        $login = PatientLogin::where('api_key',$request->input('api_key'))->first();
        if(is_null($login))
        {
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Api Key'
            ]);
        }
        $patient = $login->patient;
        
        if(is_null($patient)) {
            return response()->json([
                'is_successful' => false,
                'error_message' => 'Can\'t find Patient related to Api Key '. $request->input('api_key')
            ]);
        }
        $patient_request = new RequestHeader($request->only([
            'patient_address_id',
            'has_insurance',
            'date'
            ]));
        
        $patient_request['is_home'] = $request->is_at_home;
        $patient_request['requester_id'] = $patient->id;
        $patient_request['requester_type'] = 1;
        $patient_request['recipient_id'] = $patient->id;
        $patient_request['request_status_id'] =1;
        $patient_request['rate'] =0;
        $patient_request['is_stable'] =0;
        $patient_request['is_emergency'] =0;
        $patient_request['comment'] =null;
        $patient_request['weight'] =null;
        $patient_request['type'] =8;
       
        $patient_request->save();
            
        
            

            
            foreach($request->input('written_type_list') as $written)
            {
                
                $patient_details = new RequestDetail();
                $patient_details['request_header_id']= $patient_request->id;
                $patient_details['written_type_list']= $written;
                $patient_details->save();
            
            }
            foreach($request->input('test_type_list') as $written)
            {
                
                $patient_details = new RequestDetail();
                $patient_details['request_header_id']= $patient_request->id;
                $patient_details['vendor_item_id']= $written;
                $patient_details->save();
            
            }            
            //dd($patient_request->request_detail()->createMany(array($request->input('written_type_list'))));
        try{    
          //$patient_request->request_detail()->createMany(array($request->input('test_type_list')));
           
           
            return response()->json([
                'request_id'=>$patient_request->id,
                'is_successful' => true
            ]);
        }catch (Exception $exception){
            return response()->json([
                'is_successful' => false,
                'error_message' => 'Can not create patient request'
            ]);
        }

    }

    public function upload_lab_prescription(Request $request){
        
        $validator = Validator::make($request->all(),[
            'api_key'=>'required',
            'image'=>'required'
        ]);
        if($validator->fails()){
            $errors = $validator->errors()->all();
            return response()->json([
                'is_successful' => false,
                'error_message' => reset($errors)
            ]);
        }
        $login = PatientLogin::where('api_key',$request->input('api_key'))->first();
        if(is_null($login)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Api Key'
            ]);
        }
        $patient_request = RequestHeader::find($request->input('request_id'));
        if(is_null($patient_request)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Request Id'
            ]);
        }
        $path = $request->file('image')->store('');
        $patient_upload = new RequestUploads(['upload'=>$path]);
        $patient_request->request_uploads()->save($patient_upload);
        return response()->json([
            'is_successful'=>true
        ]);
    }
    
    
    public function get_lab_response(Request $request){
        $validator = Validator::make($request->all(),[
            'api_key'=>'bail|required',
            'request_id'=>'bail|required'
        ]);
        if($validator->fails()){
            $errors = $validator->errors()->all();
            return response()->json([
                'is_successful'=>false,
                'error_message'=>reset($errors)
            ]);
        }
        $login = PatientLogin::where('api_key',$request->input('api_key'))->first();
        if(is_null($login)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Api Key'
            ]);
        }
        $patientRequest = request_vendor_branch_item::where('request_header_id','=',$request->input('request_id'))->get();
        if(is_null($patientRequest)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Request Id '.$request->input('request_id') . ' Not found'
            ]);
        }
        
        return response()->json([
                'is_successful'=>true,
                'error_message'=>'',
                'request_details'=>$patientRequest
            ]);

       

    }
    
    public function answer_lab_question(Request $request){
        $validator = Validator::make($request->all(),[
            'api_key'=>'bail|required',
            'request_id'=>'bail|required'
        ]);
        if($validator->fails()){
            $errors = $validator->errors()->all();
            return response()->json([
                'is_successful'=>false,
                'error_message'=>reset($errors)
            ]);
        }
        $login = PatientLogin::where('api_key',$request->input('api_key'))->first();
        if(is_null($login)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Api Key'
            ]);
        }
        $patientRequest = RequestHeader::find($request->input('request_id'));
        if(is_null($patientRequest)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Request Id '.$request->input('request_id') . ' Not found'
            ]);
        }
        try{
            //$patientRequest->update(['status'=>'qw']);
            $patientRequest->RequestQuestion()->createMany($request->input('questions_list'));

            return response()->json(['is_successful'=>true]);
        }catch (Exception $exception){
            return response()->json(['is_successful'=>false,'error_message'=>$exception->getMessage()]);
        }
    }
    public function get_lab_detail(Request $request){
        $validator = Validator::make($request->all(),[
            'api_key'=>'bail|required',
            'request_id'=>'bail|required'
        ]);
        if($validator->fails()){
            $errors = $validator->errors()->all();
            return response()->json([
                'is_successful'=>false,
                'error_message'=>reset($errors)
            ]);
        }
        $login = PatientLogin::where('api_key',$request->input('api_key'))->first();
        if(is_null($login)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Api Key'
            ]);
        }
         $patientRequestsheader = RequestHeader::where('id','=',$request->input('request_id'))->get();
        if(is_null($patientRequestsheader)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Request Id '.$request->input('request_id') . ' Not found'
            ]);
        }
        
         $patientRequests = RequestDetail::where('request_header_id','=',$request->input('request_id'))->get();
        if(is_null($patientRequests)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Request Id '.$request->input('request_id') . ' Not found'
            ]);
        }
        $test_type_list = array();
        
foreach ($patientRequests as $patientRequest) {
 
    $vendor_item_branch = VendorItems::where('id','=',$patientRequest->vendor_item_id)->get();
    
    $test_type_list=array(
        'typeId'=>$vendor_item_branch[0]->id,
        'typeName'=>$vendor_item_branch[0]->en_name,
        'price'=>$vendor_item_branch[0]->price
    );
}
$test_date_time = $patientRequestsheader[0]->test_date_time;
if(($test_date_time==NULL))
{
    $test_date_time = '';
}
$delivery_estimate_dateTime = $patientRequestsheader[0]->delivery_estimate_dateTime;
if(($delivery_estimate_dateTime==NULL))
{
    $delivery_estimate_dateTime = '';
}
$notes = $patientRequestsheader[0]->notes;
if(($notes==NULL))
{
    $notes = '';
}
        return response()->json([
                'is_successful'=>true,
                'error_message'=>'',
                'test_type_list'=>array($test_type_list),
            'test_date_time'=>$test_date_time,
            'delivery_estimate_dateTime'=>$delivery_estimate_dateTime,
            'status'=>$patientRequestsheader[0]->request_status_id,
            'notes'=>$notes
            ]);

    }
    
    
    public function cancel_lab_request(Request $request){
        $validator = Validator::make($request->all(),[
            'request_id'=>'bail|required|numeric',
            'api_key'=>'required',
        ]);
        if($validator->fails()){
            $errors = $validator->errors()->all();
            return response()->json([
                'is_successful' => false,
                'error_message' => reset($errors)
            ]);
        }
        $login = PatientLogin::where('api_key',$request->input('api_key'))->first();
        if(is_null($login)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Api Key'
            ]);
        }
        $patient_request = RequestHeader::find($request->input('request_id'));
        if(is_null($patient_request)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Request Id'
            ]);
        }
    
                $patient_request->update(['request_status_id'=>7]);
        

        return response()->json([
            'is_successful'=>true
        ]);
    }
    
    public function rate_lab_request(Request $request){
        $validator = Validator::make($request->all(),[
            'request_id'=>'bail|required|numeric',
            'api_key'=>'required',
        ]);
        if($validator->fails()){
            $errors = $validator->errors()->all();
            return response()->json([
                'is_successful' => false,
                'error_message' => reset($errors)
            ]);
        }
        $login = PatientLogin::where('api_key',$request->input('api_key'))->first();
        if(is_null($login)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Api Key'
            ]);
        }
        $patient_request = RequestHeader::find($request->input('request_id'));
        if(is_null($patient_request)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Request Id'
            ]);
        }
  
                
                $patient_request->update(['request_status_id'=>9,'rate'=>$request->input('rate'),'comment'=>$request->input('comment')]);
        

        return response()->json([
            'is_successful'=>true
        ]);
    }
    
     public function confirm_lab_request(Request $request){
        $validator = Validator::make($request->all(),[
            'request_id'=>'bail|required|numeric',
            'api_key'=>'required',
        ]);
        if($validator->fails()){
            $errors = $validator->errors()->all();
            return response()->json([
                'is_successful' => false,
                'error_message' => reset($errors)
            ]);
        }
        $login = PatientLogin::where('api_key',$request->input('api_key'))->first();
        if(is_null($login)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Api Key'
            ]);
        }
        $patient_request = RequestHeader::find($request->input('request_id'));
        if(is_null($patient_request)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Request Id'
            ]);
        }
        if(!$patient_request->is_home){
            if(empty($request->input('test_type_list') )){
                return response()->json([
                    'is_successful'=>false,
                    'error_message'=>'Types can not be empty'
                ]);
            }
            
           
          //  $requestvendorbranches = new RequestVendorBranches();
           DB::transaction(function () use ($request){
                DB::table('RequestVendorBranches')
                    ->whereIn('id',$request->input('test_type_list'))
                    ->update(['is_active'=>true,'updated_at'=>Carbon::now()]);
               
            });
           DB::transaction(function () use ($request){
                DB::table('RequestHeader')
                    ->where('id',$request->input('request_id'))
                    ->update(['request_status_id'=>13,'updated_at'=>Carbon::now()]);
               
            });            

        }else{
                DB::transaction(function () use ($request){
                DB::table('RequestHeader')
                    ->where('id',$request->input('request_id'))
                    ->update(['request_status_id'=>13,'updated_at'=>Carbon::now()]);
               
            });
        }

        return response()->json([
            'is_successful'=>true
        ]);
    }

     public function paitent_labtest_insurance(Request $request)
     {
               $validator = Validator::make($request->all(),[
            'request_id'=>'bail|required|numeric',
            'api_key'=>'required',
                   'relation'=>'required'
        ]);
        if($validator->fails()){
            $errors = $validator->errors()->all();
            return response()->json([
                'is_successful' => false,
                'error_message' => reset($errors)
            ]);
        }
        $login = PatientLogin::where('api_key',$request->input('api_key'))->first();
        if(is_null($login)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Api Key'
            ]);
        }
        $patient_request = RequestHeader::find($request->input('request_id'));
        if(is_null($patient_request)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Request Id'
            ]);
        }
  
                
                $patient_request->update(['request_status_id'=>10,'relation'=>$request->input('relation')]);
        

        return response()->json([
            'is_successful'=>true
        ]);  
     }
    
}