<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Pusher\Pusher;
use Pusher\PusherInstance;

class PusherController extends Controller
{
    public function auth(Request $request){

        $validator = Validator::make($request->all(),[
            'socket_id'=>'required',
            'channel_name'=>'required'
        ]);
        if($validator->fails()){
            $errors = $validator->errors()->all();
            return response()->json([
                'is_successful' => false,
                'error_message' => reset($errors)
            ]);
        }
        $p = PusherInstance::get_pusher();
        $auth = $p->socket_auth($request->input('channel_name'),$request->input('socket_id'));
        return $auth;

    }
}
