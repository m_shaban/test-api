<?php
namespace App\Http\Controllers\Api;

use App\CenterResponse;
use App\PatientLogin;
use App\PatientRequest;

use Davibennun\LaravelPushNotification\Facades\PushNotification;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class RadiologyRequestController extends Controller
{
    public function confirm_radiology_request(Request $request){
        $validator = Validator::make($request->all(),[
            'radiology_request_id'=>'bail|required|numeric',
            'api_key'=>'required',
        ]);
        if($validator->fails()){
            $errors = $validator->errors()->all();
            return response()->json([
                'is_successful' => false,
                'error_message' => reset($errors)
            ]);
        }
        $login = PatientLogin::where('api_key',$request->input('api_key'))->first();
        if(is_null($login)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Api Key'
            ]);
        }
        $patient_request = PatientRequest::find($request->input('radiology_request_id'));
        if(is_null($patient_request)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Request Id'
            ]);
        }
        if(!$patient_request->is_at_home){
            if(empty($request->input('radiology_types_list') )){
                return response()->json([
                    'is_successful'=>false,
                    'message'=>'Radiology Types can not be empty'
                ]);
            }
            if(empty($request->input('center_id'))){
                return response()->json([
                    'is_successful'=>false,
                    'message'=>'Center is required'
                ]);
            }
            DB::transaction(function () use ($patient_request,$request){
                DB::table('response_centers')
                    ->where('response_id',$patient_request->center_response->id)
                    ->where('center_id',$request->input('center_id'))
                    ->update(['is_approved'=>true,'updated_at'=>Carbon::now()]);
                //Update Types
                DB::table('response_radiology_items')
                    ->where('response_id',$patient_request->center_response->id)
                    ->whereIn('type_id',$request->input('radiology_types_list'))
                    ->update(['is_approved'=>true,'updated_at'=>Carbon::now()]);
                $patient_request->update(['status'=>'qw']);
            });

        }else{
                DB::transaction(function () use ($patient_request,$request){
                // Update Types
                DB::table('response_radiology_items')
                ->where('response_id',$patient_request->center_response->id)
                ->whereIn('type_id',$request->input('radiology_types_list'))
                ->update(['is_approved'=>true,'updated_at'=>Carbon::now()]);
                $patient_request->update(['status'=>'qw']);
         });
        }

        return response()->json([
            'is_successful'=>true
        ]);
    }
    
    
    public function cancel_radiology_request(Request $request){
        $validator = Validator::make($request->all(),[
            'radiology_request_id'=>'bail|required|numeric',
            'api_key'=>'required',
        ]);
        if($validator->fails()){
            $errors = $validator->errors()->all();
            return response()->json([
                'is_successful' => false,
                'error_message' => reset($errors)
            ]);
        }
        $login = PatientLogin::where('api_key',$request->input('api_key'))->first();
        if(is_null($login)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Api Key'
            ]);
        }
        $patient_request = PatientRequest::find($request->input('radiology_request_id'));
        if(is_null($patient_request)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Request Id'
            ]);
        }
    
                $patient_request->update(['status'=>'cr']);
        

        return response()->json([
            'is_successful'=>true
        ]);
    }
    
     public function rate_radiology_request(Request $request){
        $validator = Validator::make($request->all(),[
            'radiology_request_id'=>'bail|required|numeric',
            'api_key'=>'required',
        ]);
        if($validator->fails()){
            $errors = $validator->errors()->all();
            return response()->json([
                'is_successful' => false,
                'error_message' => reset($errors)
            ]);
        }
        $login = PatientLogin::where('api_key',$request->input('api_key'))->first();
        if(is_null($login)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Api Key'
            ]);
        }
        $patient_request = PatientRequest::find($request->input('radiology_request_id'));
        if(is_null($patient_request)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Request Id'
            ]);
        }
  
                
                DB::table('patient_requests')
                ->where('id',$request->input('radiology_request_id'))
                ->update(['rate'=>$request->input('rate')]);
        

        return response()->json([
            'is_successful'=>true
        ]);
    }
    public function notig(Request $request)
    {

        $message = PushNotification::Message($request->message,array(
            
             'custom' => array('custom data' => array(
        "service_id"=>$request->type,
                "title"=>"Doc",
                "message"=> $request->message,
                "status"=>$request->status,
                "request_id"=>$request->request_id,
                "result"=>""
    ))
                
)); 

PushNotification::app('appNameIOS')
                ->to($request->input('device_token'))
                ->send($message);


    }
}
