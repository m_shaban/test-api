<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\PatientLogin;
use App\PatientRequest;
use App\PatientUpload;
use App\PatientAddress;
use App\RequestAnswer;
use \Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Contracts\Routing\TerminableMiddleware;  
use Illuminate\Support\Facades\Log;

class PatientController extends Controller
{
    public function __construct()
    {
    }

    public function patient_request_radiology(Request $request){
 $logFile = 'log.txt';
        Log::useDailyFiles(storage_path().'/logs/'.$logFile);
        Log::info('app.requests', ['request' => $request->all()]);

        $validator = Validator::make($request->all(),[
            'api_key'=>'required',
            'patient_name'=>'bail|required|alpha_spaces',
            'patient_age'=>'bail|required|numeric|between:1,150',
            'patient_weight'=>'bail|required|numeric|between:1,250',
            'patient_address_id'=>'bail|required_if:is_at_home,true',
            'is_at_home'=>'required',
            'is_able_to_be_stable'=>'required',
            'number_of_scanned_prescriptions'=>'bail|required|numeric|min:1'
        ],[
            'patient_address_id.required_if' => 'The :attribute field is required.',
        ]);
        if($validator->fails()){
            $errors = $validator->errors()->all();
            return response()->json([
                'is_successful'=>false,
                'error_message'=>reset($errors)
            ]);
        }
        if($request['patient_age']>150)
        {
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'patient age must be less than 150'
            ]);
        }
        else if($request['patient_age']<1)
        {
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'patient age must be greater than 1'
            ]);
        }
        if($request['patient_weight']>250)
        {
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'patient weight must be less than 250 kg'
            ]);
        }
        else if($request['patient_weight']<1)
        {
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'patient weight must be greater than 1 kg'
            ]);
        }
        $login = PatientLogin::where('api_key',$request->input('api_key'))->first();
        if(is_null($login))
        {
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Api Key'
            ]);
        }
        $patient = $login->patient;
        if(is_null($patient)) {
            return response()->json([
                'is_successful' => false,
                'error_message' => 'Can\'t find Patient related to Api Key '. $request->input('api_key')
            ]);
        }
        $patient_request = new PatientRequest($request->only([
            'patient_name',
            'patient_age',
            'patient_weight',
            'patient_address_id',
            'is_at_home',
            'is_able_to_be_stable',
            'number_of_scanned_prescriptions']));

        try{
            $patient->patient_requests()->save($patient_request);
            return response()->json([
                'request_id'=>$patient_request->id,
                'is_successful' => true
            ]);
        }catch (Exception $exception){
            return response()->json([
                'is_successful' => false,
                'error_message' => 'Can not create patient request'
            ]);
        }

    }

    public function upload_radiology_prescription(Request $request){
 $logFile = 'log.txt';
        Log::useDailyFiles(storage_path().'/logs/'.$logFile);
        Log::info('app.requests', ['request' => $request->all()]);

        $validator = Validator::make($request->all(),[
            'request_id'=>'required',
            'api_key'=>'required',
            'image'=>'required'
        ]);
        if($validator->fails()){
            $errors = $validator->errors()->all();
            return response()->json([
                'is_successful' => false,
                'error_message' => reset($errors)
            ]);
        }
        $login = PatientLogin::where('api_key',$request->input('api_key'))->first();
        if(is_null($login)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Api Key'
            ]);
        }
        $patient_request = PatientRequest::find($request->input('request_id'));
        if(is_null($patient_request)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Request Id'
            ]);
        }
        $path = $request->file('image')->store('');
        $patient_upload = new PatientUpload(['image_url'=>$path]);
        $patient_request->request_uploads()->save($patient_upload);
        return response()->json([
            'is_successful'=>true
        ]);
    }

    public function get_radiology_response(Request $request){
         $logFile = 'log.txt';
        Log::useDailyFiles(storage_path().'/logs/'.$logFile);
        Log::info('app.requests', ['request' => $request->all()]);

        $validator = Validator::make($request->all(),[
            'api_key'=>'bail|required',
            'radiology_request_id'=>'bail|required'
        ]);
        if($validator->fails()){
            $errors = $validator->errors()->all();
            return response()->json([
                'is_successful'=>false,
                'error_message'=>reset($errors)
            ]);
        }
        $login = PatientLogin::where('api_key',$request->input('api_key'))->first();
        if(is_null($login)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Api Key'
            ]);
        }
        $patientRequest = PatientRequest::find($request->input('radiology_request_id'));
        if(is_null($patientRequest)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Request Id '.$request->input('radiology_request_id') . ' Not found'
            ]);
        }
        $centerResponse = $patientRequest->center_response;

        $centers = $centerResponse->centers()->with('phones')->get();
        $radiology_types = $centerResponse->radiology_types;

        $response = [
            'is_successful'=>true,
            'deliveryEstimateDateTime'=>$centerResponse->delivery_est_datetime,
            'radiology_items'=>$radiology_types->map(function($item){
                return [
                    'typeName'=>$item->ar_name . '-'.$item->en_name,
                    'price'=> intval($item->pivot->price),
                    'definition'=>$item->pivot->definition,
                    'preparation'=>$item->pivot->preparation,
                    'notes'=>$item->pivot->notes
                ];
            })
        ];

        if($centerResponse->is_at_home){
            $response['radiologyDateTime'] = $centerResponse->radiology_datetime;
        }else{
            $response['centersList'] = $centers->map(function($item){
                return [
                    'centerId'=>$item->id,
                    'centerName'=>$item->name,
                    'centerAddress'=>$item->address,
                    'centerPhones'=>$item->phones->map(function($item){return $item->phone;}),
                    'centerMapLocation'=>$item->map_location,
                    'dateTime'=>$item->pivot->arrive_datetime
                ];
            });
        }

        return response()->json($response);

    }
    public function answer_radiology_question(Request $request){
 $logFile = 'log.txt';
        Log::useDailyFiles(storage_path().'/logs/'.$logFile);
        Log::info('app.requests', ['request' => $request->all()]);

        $validator = Validator::make($request->all(),[
            'api_key'=>'bail|required',
            'radiology_request_id'=>'bail|required'
        ]);
        if($validator->fails()){
            $errors = $validator->errors()->all();
            return response()->json([
                'is_successful'=>false,
                'error_message'=>reset($errors)
            ]);
        }
        $login = PatientLogin::where('api_key',$request->input('api_key'))->first();
        if(is_null($login)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Api Key'
            ]);
        }
        $patientRequest = PatientRequest::find($request->input('radiology_request_id'));
        if(is_null($patientRequest)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Request Id '.$request->input('radiology_request_id') . ' Not found'
            ]);
        }
        try{
            $patientRequest->update(['status'=>'qw']);
              $question_list = $request->input('questions_list');
            if($request->input('device') == 'apple')
                {
                $question_list =  str_replace('[', '{', $request->input('questions_list')) ;
                $question_list =  str_replace(']', '}', $question_list) ;
                $question_list =  str_replace('{{', '[{', $question_list) ;
                $question_list =  str_replace('}}', '}]', $question_list) ;
                
                }
                
            $patientRequest->answers()->createMany($question_list);

            return response()->json(['is_successful'=>true]);
        }catch (Exception $exception){
            return response()->json(['is_successful'=>false,'error_message'=>$exception->getMessage()]);
        }
    }

    private function getMIMETYPE($base64string){
        preg_match("/^data:image\/(.*);base64/",$base64string, $match);
        return $match[1];
    }

    private function decodeBase64Image($base64_str,$id){
        $imageType = $this->getMIMETYPE($base64_str);
        $base64 = substr($base64_str, strpos($base64_str, ",")+1);
        $image = base64_decode($base64);
        $url = "prescription_".$id."_".time().".".$imageType;
        $path = public_path()."/uploads/".$url;
        file_put_contents($path,$image);
        return $url;
    }
       public function answer_radiology_question_v2(Request $request){
        $validator = Validator::make($request->all(),[
            'api_key'=>'bail|required',
            'radiology_request_id'=>'bail|required'
        ]);
        if($validator->fails()){
            $errors = $validator->errors()->all();
            return response()->json([
                'is_successful'=>false,
                'error_message'=>reset($errors)
            ]);
        }
        $login = PatientLogin::where('api_key',$request->input('api_key'))->first();
        if(is_null($login)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Api Key'
            ]);
        }
        $patientRequest = PatientRequest::find($request->input('radiology_request_id'));
        if(is_null($patientRequest)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Request Id '.$request->input('radiology_request_id') . ' Not found'
            ]);
        }
        try{
            $patientRequest->update(['status'=>'qw']);
            
            $i = 0 ;
            foreach ($request->input('questions_list') as $question_id)
            {
            $patient_answer = new RequestAnswer();
            
            $patient_answer->request_id = $request->input('radiology_request_id');
            $patient_answer->question_id = $question_id;
            $patient_answer->answer = $request->input('answers_list')[$i];
            $patient_answer->save();
            $i++;
            }
            
            //$patientRequest->answers()->createMany($request->input('questions_list'));

            return response()->json(['is_successful'=>true]);
        }catch (Exception $exception){
            return response()->json(['is_successful'=>false,'error_message'=>$exception->getMessage()]);
        }
    }

    public function get_radiology_detail_v2(Request $request){
        $validator = Validator::make($request->all(),[
            'api_key'=>'bail|required',
            'radiology_request_id'=>'bail|required'
        ]);
        if($validator->fails()){
            $errors = $validator->errors()->all();
            return response()->json([
                'is_successful'=>false,
                'error_message'=>reset($errors)
            ]);
        }
        $login = PatientLogin::where('api_key',$request->input('api_key'))->first();
        if(is_null($login)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Api Key'
            ]);
        }

        $patientRequest = PatientRequest::find($request->input('radiology_request_id'));
        if(is_null($patientRequest)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Request Id '.$request->input('radiology_request_id') . ' Not found'
            ]);
        }
        $address = PatientAddress::where('id',$patientRequest->patient_address_id)->get();
            $patient_upload = PatientUpload::where('request_id',$request->input('radiology_request_id'))->get();
            $patient_uploads = array();
            foreach ($patient_upload as $pu)
            {
                $patient_uploads[]='radiology.globalhomedoc.com/uploads/'.$pu->image_url;
            }
            
        $centerResponse = $patientRequest->center_response;
        
        if($patientRequest->status != Null && $patientRequest->status != 'rw')
        {
            $centers = $centerResponse->centersconfirmed()->with('phones')->get();
            $radiology_types = $centerResponse->radiology_types_confirmed;
        }
        else 
        {
            if($centerResponse!=null)
            {
            $centers = $centerResponse->centers()->with('phones')->get();
            $radiology_types = $centerResponse->radiology_types;
            }
        }
            if($centerResponse!=null)
            {
        $response = [
            'is_successful'=>true,
            'deliveryEstimateDateTime'=>$centerResponse->delivery_est_datetime,
            'status'=>$patientRequest->status,
            'is_at_home'=>$patientRequest->is_at_home,
            'weight'=>$patientRequest->patient_weight,
            'age'=>$patientRequest->patient_age,            
            'can_be_stable'=>$patientRequest->is_able_to_be_stable,    
            'prescription_scans'=>$patient_uploads,
            'radiology_items'=>$radiology_types->map(function($item){
                
                return [
                    'typeId'=>$item->pivot->type_id,
                    'typeName'=>$item->ar_name . '-'.$item->en_name,
                    'price'=> intval($item->pivot->price),
                    'definition'=>$item->pivot->definition,
                    'preparation'=>$item->pivot->preparation,
                    'notes'=>$item->pivot->notes,
                    'is_approved'=>$item->pivot->is_approved
                ];
            })
        ];
                    if($centerResponse->is_at_home==1){
            $response['radiologyDateTime'] = $centerResponse->radiology_datetime;
            $response['address']=$address[0]->address;
        }else{
            
            $response['centersList'] = $centers->map(function($item){
            
            
            $phone = array();
            foreach($item->phones as $itemphone)
            {
                $phone[]=$itemphone->phone;
            }
                return [
                    'centerId'=>$item->id,
                    'centerName'=>$item->name,
                    'centerAddress'=>$item->address,
                    'centerPhones'=>implode(',',$phone),
                    'centerMapLocation'=>$item->map_location,
                    'dateTime'=>$item->pivot->arrive_datetime,
                    'is_approved'=>$item->pivot->is_approved
                ];
            });
        }
            }
            else 
            {
                $status =$patientRequest->status ; 
                if($patientRequest->status==null)
                {
                    $status = 'n';
                }
                 $response = [
            'is_successful'=>true,
            'deliveryEstimateDateTime'=>'',
            'status'=>$status,
            'is_at_home'=>$patientRequest->is_at_home,
            'weight'=>$patientRequest->patient_weight,
            'age'=>$patientRequest->patient_age,            
            'can_be_stable'=>$patientRequest->is_able_to_be_stable,    
            'prescription_scans'=>$patient_uploads,
            
        ];
            }


        return response()->json($response);

    }


  public function get_radiology_detail(Request $request){
        $validator = Validator::make($request->all(),[
            'api_key'=>'bail|required',
            'radiology_request_id'=>'bail|required'
        ]);
        if($validator->fails()){
            $errors = $validator->errors()->all();
            return response()->json([
                'is_successful'=>false,
                'error_message'=>reset($errors)
            ]);
        }
        $login = PatientLogin::where('api_key',$request->input('api_key'))->first();
        if(is_null($login)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Invalid Api Key'
            ]);
        }

        $patientRequest = PatientRequest::find($request->input('radiology_request_id'));
        if(is_null($patientRequest)){
            return response()->json([
                'is_successful'=>false,
                'error_message'=>'Request Id '.$request->input('radiology_request_id') . ' Not found'
            ]);
        }
        $centerResponse = $patientRequest->center_response;
        if($patientRequest->status != Null && $patientRequest->status != 'rw')
        {
            $centers = $centerResponse->centersconfirmed()->with('phones')->get();
            
            $radiology_types = $centerResponse->radiology_types_confirmed;
        }
        else 
        {
            
            $centers = $centerResponse->centers()->with('phones')->get();
            $radiology_types = $centerResponse->radiology_types;
        }
        
        
        $response = [
            'is_successful'=>true,
            'deliveryEstimateDateTime'=>$centerResponse->delivery_est_datetime,
            'status'=>$patientRequest->status,
'is_at_home'=>$patientRequest->is_at_home,
            'radiology_items'=>$radiology_types->map(function($item){
                
                return [
                    'typeId'=>$item->pivot->type_id,
                    'typeName'=>$item->ar_name . '-'.$item->en_name,
                    'price'=> intval($item->pivot->price),
                    'definition'=>$item->pivot->definition,
                    'preparation'=>$item->pivot->preparation,
                    'notes'=>$item->pivot->notes,
                    'is_approved'=>$item->pivot->is_approved
                ];
            })
        ];
        if($centerResponse->is_at_home==1){
            $response['radiologyDateTime'] = $centerResponse->radiology_datetime;
        }else{
            $response['centersList'] = $centers->map(function($item){
                return [
                    'centerId'=>$item->id,
                    'centerName'=>$item->name,
                    'centerAddress'=>$item->address,
                    'centerPhones'=>$item->phones->map(function($item){return $item->phone;}),
                    'centerMapLocation'=>$item->map_location,
                    'dateTime'=>$item->pivot->arrive_datetime,
                    'is_approved'=>$item->pivot->is_approved
                ];
            });
        }

        return response()->json($response);

    }



}

