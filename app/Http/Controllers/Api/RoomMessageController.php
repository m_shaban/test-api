<?php

namespace App\Http\Controllers\Api;

use App\Events\DoctorSendMessageEvent;
use App\Events\PatientSendMessageEvent;
use App\Models\Chat\ChatRoom;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class RoomMessageController extends Controller
{
    public function doctor_send_msg(Request $request){
        $result = $this->validateRoom($request);
        if(! $result instanceof ChatRoom){
            return $result;
        }
        $msg = $result->messages()->create([
            'message'=>$request->input('message'),
            'message_type'=>$request->input('message_type'),
            'user_type'=>'d'
        ]);
        event(new DoctorSendMessageEvent($result->room_name,$msg->message,$msg->message_type,$msg->getCreatedAt()));
        return response()->json([
            'is_successful' => true
        ]);
    }
    public function patient_send_msg(Request $request){
        $result = $this->validateRoom($request);
        if(! $result instanceof ChatRoom){
            return $result;
        }

        $msg = $result->messages()->create([
            'message'=>$request->input('message'),
            'message_type'=>$request->input('message_type'),
            'user_type'=>'p'
        ]);
        event(new PatientSendMessageEvent($result->room_name,$msg->message,$msg->message_type,$msg->getCreatedAt()));
        return response()->json([
            'is_successful' => true
        ]);
    }

    private function validateRoom($request)
    {
        $validator = Validator::make($request->all(), [
            'message_type'=>'required|numeric|between:1,3',
            'room_name' => 'bail|required',
            'message'=>'bail|required'

        ]);
        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return response()->json([
                'is_successful' => false,
                'error_message' => reset($errors)
            ]);
        }
        $room = ChatRoom::where('room_name', $request->input('room_name'))->first();
        if (is_null($room)) {
            return response()->json([
                'is_successful' => false,
                'error_message' => 'Can\'t find Room ' . $request->input('room_name')
            ]);
        }
        return $room;
    }
}
