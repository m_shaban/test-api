<?php
namespace App\Http\Controllers;

use App\Center;
use App\CenterResponse;
use App\RadiologyType;
use App\RadiologyTypeCategory;
/*Lab test*/
use App\RequestHeader;
use App\RequestDetail;

use Illuminate\Http\Request;
use App\PatientRequest;
use App\Patient;
use App\PatientLogin;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;
use Illuminate\Support\Facades\Validator;

class RequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$centers = Center::Select('id','name')->get();
        $requests = RequestHeader::all();
        
        return view('requests.index',compact('requests'));
    }

    public function sortname(Request $request)
    {
        
      $order = 'DESC';
                if($request['order_name']=="up")
                {
                    $order = 'ASC';
                }

        $centers = Center::Select('id','name')->get();
        $radiologyTypesCategory = RadiologyTypeCategory::all();
        $radiologyTypes = RadiologyType::Select('id','en_name','type_group_id')->get()->toJson();
        $query = "Select * from v_patientrequest where status is NULL ORDER BY `PatientName` ".$order.";";
        
        $requests = collect(DB::select($query));
         return (view('patientrequests.index',compact('requests' , 'centers' ,'radiologyTypesCategory' , 'radiologyTypes')));
         
    }

    public function sortdate(Request $request)
    {
            
                if($request['order_date']=='up')
                {
                    $order = 'ASC';
                }
                else 
                {
                     $order = 'DESC';
                }
        $centers = Center::Select('id','name')->get();
        $radiologyTypesCategory = RadiologyTypeCategory::all();
        $radiologyTypes = RadiologyType::Select('id','en_name','type_group_id')->get()->toJson();
        $query = "Select * from v_patientrequest where status is NULL ORDER BY `RequestDate` ".$order.";";
        $requests = collect(DB::select($query));
         return (view('patientrequests.index',compact('requests' , 'centers' ,'radiologyTypesCategory' , 'radiologyTypes')));
    }
        public function sortlocation(Request $request)
    {
             
                if($request['order_location']=='up')
                {
                    $order = 'ASC';
                }
                else 
                {
                    $order = 'DESC';
                }
        $centers = Center::Select('id','name')->get();
        $radiologyTypesCategory = RadiologyTypeCategory::all();
        $radiologyTypes = RadiologyType::Select('id','en_name','type_group_id')->get()->toJson();
        $query = "Select * from v_patientrequest where status is NULL ORDER BY `RequestDate` ".$order.";";
        $requests = collect(DB::select($query));
         return (view('patientrequests.index',compact('requests' , 'centers' ,'radiologyTypesCategory' , 'radiologyTypes')));
         
    }    
            public function sortstatus(Request $request)
    {
                
                if($request['order_status']=='up')
                {
                    $order = 'ASC';
                }
                else 
                {
                    $order = 'DESC';
                }
        $centers = Center::Select('id','name')->get();
        $radiologyTypesCategory = RadiologyTypeCategory::all();
        $radiologyTypes = RadiologyType::Select('id','en_name','type_group_id')->get()->toJson();
        $query = "Select * from v_patientrequest where status is NULL ORDER BY `RequestDate` ".$order.";";
        $requests = collect(DB::select($query));
         return (view('patientrequests.index',compact('requests' , 'centers' ,'radiologyTypesCategory' , 'radiologyTypes')));
         
    }    
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // dd($request->input());
        $this->validate($request,[
            'requestno'=> 'required',
            'is_at_home'=>'bail|required',
            'status'=>'bail|required',
            'estdeiverydate'=>'bail|required',
            'radio'=>'bail|required'
        ]);

        $dbSuccsess = false;
        $error = 'Unknown error - Store';

        $radio = $request['radio'];       //radio['centers'] center+date in response_center

        $centerResponse_data = [];
        
        DB::beginTransaction();
        
        try{
        
            if($request->is_at_home == 1)
            {
                $centerResponse_data['is_at_home'] = $request->is_at_home;
                $centerResponse_data['request_id'] = $request->requestno;
                $centerResponse_data['delivery_est_datetime'] = $request->estdeiverydate;
                $centerResponse_data['radiology_datetime'] = str_replace(' ', 'T',$request->deliverydate);
                $centerResponse = CenterResponse::Create($centerResponse_data);
            
                foreach($radio as $item)
                {
                    $radio_items['price'] = $item['price'];
                    $radio_items['definition'] = $item['definitions'];
                    $radio_items['preparation'] = $item['preparation'];
                    $radio_items['notes'] = $item['notes'];

                    $radiologyType = RadiologyType::find($item['radiology']);
                    $radiologyType->center_responses()->attach(['response_id'=>$centerResponse->id],$radio_items);
                }

                

            }else{

                $centerResponse_data['is_at_home'] = $request->is_at_home;
                $centerResponse_data['request_id'] = $request->requestno;
                $centerResponse_data['delivery_est_datetime'] = str_replace(' ', 'T',$request->estdeiverydate);
                $centerResponse_data['radiology_datetime'] = str_replace(' ', 'T',$request->radiologydatetime);
                $centerResponse = CenterResponse::Create($centerResponse_data);
               // $centerResponse = CenterResponse::Create($centerResponse_data);
          

                foreach($radio as $item)
                {
                    $radio_items['price'] = $item['price'];
                    $radio_items['definition'] = $item['definitions'];
                    $radio_items['preparation'] = $item['preparation'];
                    $radio_items['notes'] = $item['notes'];

                    $radiologyType = RadiologyType::find($item['radiology']);
                    $radiologyType->center_responses()->attach(['response_id'=>$centerResponse->id],$radio_items);
                }
                
                $centers = $request['centers'];

                foreach($centers as $centerItem)
                {
                    $center = Center::find($centerItem['center']);
                    $center->responses()->attach(['response_id'=>$centerResponse->id],['arrive_datetime'=>$request->radiologydatetime]);
                }
            }
            
            $patient_request = PatientRequest::Where('id','=',$request->requestno)->first();
            $patientlogin = PatientLogin::Where('patient_id','=',$patient_request['patient_id'])->where('device_token', '!=', "")->where('device_id', '!=', "")->orderBy('id', 'desc')->first();
            
            #API access key from Google API's Console
    define( 'API_ACCESS_KEY', 'AIzaSyD0Xk1xHEL9q3Qo0EaZD1K_Vs9j_fdiUik' );
    $registrationIds = $patientlogin['device_token'];
    $message = 'Your Radiology Request under review ';
    if($request->status == 'rw') 
        {
        $message = 'Your Radiology Request Approved ';
    }
#prep the bundle
     $msg = array
          (
		"type"=>"1",
                "title"=>"Radiology",
                "message"=> $message,
                "requestId"=> $request->requestno,
                "status"=>$request->status,
          );
      
	$fields = array
			(
				'to'		=> $registrationIds,
				'data'	=> $msg
			);
	
	
	$headers = array
			(
				'Authorization: key=' . API_ACCESS_KEY,
				'Content-Type: application/json'
			);
#Send Reponse To FireBase Server	
		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch );
		curl_close( $ch );

            $patient_request->update(['status'=>$request->status]);

            $dbSuccsess = true;
        }catch (Exception $ex)
        {
            $error = $ex->getMessage();

        }finally{
            if($dbSuccsess)
            {
                DB::commit();
                return redirect()->back()
                    ->with( 'db' , $dbSuccsess);
            }
            else{
                DB::rollback();
                return redirect()->back()
                    ->with( 'db' , $dbSuccsess)
                    ->with( 'error' , $error);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query = "select * from v_patientRequest where RequestNo = ".$id;

        $request = collect(DB::select($query));
        return view('patientrequests.edit',compact('request'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function updateRefuse(Request $request)
    {

        $dbSuccsess = false;
        $error = 'Unkown Error - UpdateRefuse';

        DB::beginTransaction();
            try{
                    $patientrequest = PatientRequest::find($request->requestno);
            $patientlogin = PatientLogin::Where('patient_id','=',$patientrequest['patient_id'])->where('device_token', '!=', "")->where('device_id', '!=', "")->orderBy('id', 'desc')->first();
            
            #API access key from Google API's Console
    define( 'API_ACCESS_KEY', 'AIzaSyD0Xk1xHEL9q3Qo0EaZD1K_Vs9j_fdiUik' );
    $registrationIds = $patientlogin['device_token'];
    $message = 'Your Radiology Request has been Rejected ';
    
#prep the bundle
     $msg = array
          (
		"type"=>"2",
                "title"=>"Radiology",
                "message"=> $message,
                "requestId"=> $request->requestno
          );
      
	$fields = array
			(
				'to'		=> $registrationIds,
				'data'	=> $msg
			);
	
	$headers = array
			(
				'Authorization: key=' . API_ACCESS_KEY,
				'Content-Type: application/json'
			);
#Send Reponse To FireBase Server	
		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch );
		curl_close( $ch );
 $patientrequest->update(['status'=>$request->status,'refuse_comment'=>$request->refuse_comment]);
                    $dbSuccsess = true;
            }catch(Exception $ex)
            {
                $error = $ex->getMessage();
            }
            finally{
                if($dbSuccsess)
                {
                    DB::commit();
                    return redirect()->back()->with( 'db' , $dbSuccsess);
                }
                else{
                    DB::rollback();
                    return redirect()->back()->with('db' , $dbSuccsess , $error);
                }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function confirmRequestEdit()
    {
        $query = "
                    Select 
                           r.RequestDate 
                          ,r.RequestNo
                          ,r.PatientName
                          ,r.Phones
                          ,r.is_at_home
                          ,c.center
                          ,r.status
                          ,GROUP_CONCAT(ai.radiology_item) radiology_items
                    from v_patientrequest r
                    left join (
                              SELECT
                                request_id,
                                c.name center,
                                cr.id center_response_id
                              FROM center_responses cr LEFT JOIN response_centers rc
                                  ON (cr.id = rc.response_id)
                                LEFT JOIN centers c
                                  ON (rc.center_id = c.id)
                              WHERE rc.is_approved = 1
                              ) c
                    on (r.RequestNo=c.request_id)
                    LEFT JOIN (
                               SELECT rs.response_id, i.radiology_item
                               FROM response_radiology_items rs
                               LEFT JOIN (
                                            SELECT
                                              rt.id type_id,
                                              Concat(rt.en_name, '-', rc.en_name) radiology_item
                                            FROM radiology_types rt LEFT JOIN radiology_type_categories rc
                                                ON (rt.type_group_id = rc.id)
                                          ) i
                               on (rs.type_id=i.type_id)
                                ) ai
                    on (c.center_response_id=ai.response_id)
                    where    r.status = 'qa'
                    Group By r.RequestDate,r.RequestNo,r.PatientName,r.Phones,r.is_at_home,c.center,r.status
                    order by r.RequestDate asc";

        $confirms = collect(DB::select($query));
        return view('patientrequests.confirm',compact('confirms' ));
    }


    public function confirmRequestUpdate(Request $request)
    {
        $requestno = $request->input('requestno');
        $status  = $request->input('status');
        $dbSuccsess = false;

        $patientRequest = PatientRequest::find($requestno);

        DB::beginTransaction();
        try {
            $patientRequest->update(['status'=>$status]);
                define( 'API_ACCESS_KEY', 'AIzaSyD0Xk1xHEL9q3Qo0EaZD1K_Vs9j_fdiUik' );
     $patientrequest = PatientRequest::find($requestno);
     $patientlogin = PatientLogin::Where('patient_id','=',$patientrequest['patient_id'])->where('device_token', '!=', "")->where('device_id', '!=', "")->orderBy('id', 'desc')->first();
    $registrationIds = $patientlogin['device_token'];
    if($status =='pa')
    {
    $message = 'Center has just confirmed your reservation.';
    }
    else 
    {
        $message = 'Center has just rejected your reservation.';
    }
#prep the bundle
     $msg = array
          (
		"type"=>"1",
                "title"=>$message,
                "message"=> $message,
                "requestId"=> $requestno,
                "status"=>$status,
          );
      
	$fields = array
			(
				'to'		=> $registrationIds,
				'data'	=> $msg
			);
	
	
	$headers = array
			(
				'Authorization: key=' . API_ACCESS_KEY,
				'Content-Type: application/json'
			);
#Send Reponse To FireBase Server	
		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch );
		curl_close( $ch );

            
            $dbSuccsess = true;
        }catch(Exception $ex)
        {
            $error = $ex->getMessage();
        }
        finally
        {
            if($dbSuccsess)
            {
                DB::commit();
                return redirect()->back()->with( 'db' , $dbSuccsess);
            }
            else{
                DB::rollback();
                return redirect()->back()->with('db' , $dbSuccsess , $error);
            }
        }
    }

    public function resultCreate()
    {
        $query = "
                    Select 
                           r.RequestDate 
                          ,r.RequestNo
                          ,r.PatientName
                          ,r.Phones
                          ,r.is_at_home
                          ,c.center
                          ,GROUP_CONCAT(ai.radiology_item) radiology_items
                          ,r.status
                          ,r.report
                          ,case when report is null and status = 'pa' then 'create' 
                                when report is not null and status = 'pa' then 'edit'
                           else 'view'
                           end ReportState     
                    from v_patientrequest r
                      left join (
                                  SELECT
                                    request_id,
                                    c.name center,
                                    cr.id center_response_id
                                  FROM center_responses cr LEFT JOIN response_centers rc
                                      ON (cr.id = rc.response_id)
                                    LEFT JOIN centers c
                                      ON (rc.center_id = c.id)
                                  WHERE rc.is_approved = 1
                                ) c
                        on (r.RequestNo=c.request_id)
                      LEFT JOIN (
                                  SELECT rs.response_id, i.radiology_item
                                  FROM response_radiology_items rs
                                    LEFT JOIN (
                                                SELECT
                                                  rt.id type_id,
                                                  Concat(rt.en_name, '-', rc.en_name) radiology_item
                                                FROM radiology_types rt LEFT JOIN radiology_type_categories rc
                                                    ON (rt.type_group_id = rc.id)
                                              ) i
                                      on (rs.type_id=i.type_id)
                                  where rs.is_approved = 1
                                ) ai
                        on (c.center_response_id=ai.response_id)
                    where r.status in ('pa')
                    Group By r.RequestDate,r.RequestNo,r.PatientName,r.Phones,r.is_at_home,c.center,r.status,r.report
                    order by r.RequestDate asc;";

        $confirms = collect(DB::select($query));
        return view('patientrequests.result',compact('confirms' ));
    }

    public function resultStore(Request $request)
    {
    
//      change status from api to close the request to 'pd'
        $requestno = $request->input('requestno');
        $report    = $request->input('report');
        
        $dbSuccsess = false;
        $patientRequest = PatientRequest::find($requestno);
       
        if($request->count_uploads>0)
        {
            
            
            
        for ($i =0 ; $i <= 9 ; $i++) {
            
            if($request->file('report_attach'.$i)!='')
            {
            $filename = $request->file('report_attach'.$i)->store('');
            $temp[]=$filename;
            }
            else if($request->input('temp_image'.$i)!='')
            {
                
                $filename = $request->input('temp_image'.$i);
            $temp[]=$filename;
            }
        }
        
        $uploads = implode(',',$temp);
        
         $patientRequest->update(['uploads'=>$uploads]);
         $patientRequest->update(['status'=>'rd']);
    
        }
        
        
        DB::beginTransaction();
        try {
            $patientRequest->update(['report'=>$report]);
                       define( 'API_ACCESS_KEY', 'AIzaSyD0Xk1xHEL9q3Qo0EaZD1K_Vs9j_fdiUik' );
     $patientrequest = PatientRequest::find($requestno);
     $patientlogin = PatientLogin::Where('patient_id','=',$patientrequest['patient_id'])->where('device_token', '!=', "")->where('device_id', '!=', "")->orderBy('id', 'desc')->first();
    $registrationIds = $patientlogin['device_token'];
    
    $message = 'Please Check the results for your request.';
    
#prep the bundle
     $msg = array
          (
		"type"=>"1",
                "title"=>$message,
                "message"=> $message,
                "requestId"=> $requestno,
                "status"=>"rd",
                "result"=>'http://radiology.globalhomedoc.com/reportPreview/?id='.$requestno
          );
      
	$fields = array
			(
				'to'		=> $registrationIds,
				'data'	=> $msg
			);
	
	
	$headers = array
			(
				'Authorization: key=' . API_ACCESS_KEY,
				'Content-Type: application/json'
			);
#Send Reponse To FireBase Server	
		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch );
		curl_close( $ch );

            $dbSuccsess = true;
        }catch(Exception $ex)
        {
            $error = $ex->getMessage();
        }
        finally
        {
            if($dbSuccsess)
            {
                DB::commit();
                return redirect()->back()->with( 'db' , $dbSuccsess);
            }
            else{
                DB::rollback();
                return redirect()->back()->with('db' , $dbSuccsess , $error);
            }
        }
    }

    public function reportAjax(Request $request)
    {
        $requestNo = $request->input('id');

        if ($request->ajax())
        {
            $query = " Select report from patient_requests  where id = ".$requestNo;

            $answers = collect(DB::select($query))->toJson();
            return $answers;
        }
    }
    
    public function notifiy(Request $request)
    {

        if ($request->ajax())
        {
            $query = " Select count(id) as cid from patient_requests  WHERE `status` IS NULL";

            $answers = DB::select($query);
            
            foreach($answers as $r){
               $count=  $r->cid;
            }
            
            
            return $count;
        }
    }
    
    
    public function reportuploads(Request $request)
    {
        $requestNo = $request->input('id');

        if ($request->ajax())
        {
            $query = " Select uploads from patient_requests  where id = ".$requestNo;
            
            $uploads = DB::select($query);
            $count =array();
            foreach($uploads as $r){
               $count[]= explode(',', $r->uploads);
            }
            $res = ['count'=>count($count[0]) , 'uploads'=>$count[0]];
            //dd($count[0);
            //$count= explode(',', $uploads[0]);
            //dd(count($count[0]));
            $answers = json_encode($res);
            return $answers;
        }
    }    
}
