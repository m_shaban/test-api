<?php
namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Center;
use App\CenterResponse;
use App\RadiologyType;
use App\RadiologyTypeCategory;
use Illuminate\Http\Request;
use App\PatientRequest;
use App\Patient;
use App\PatientLogin;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class HomeController extends BaseController
{
    public function index()
    {
        $centers = Center::Select('id','name')->get();
        $radiologyTypesCategory = RadiologyTypeCategory::all();
        $radiologyTypes = RadiologyType::Select('id','en_name','type_group_id')->get()->toJson();
        $query = "
                    Select * from v_patientrequest
                 ";
        $requests = collect(DB::select($query));
        
        //$querycount_unprocessed_current = "select count(id) as count_unprocessed_current  from patient_requests where status in ('cr') and cast(created_at as date) = cast(now() as date);";
        $count_unprocessed_current = PatientRequest::whereIn('status',['cr'])
                                    ->whereDate('created_at',date('Y-m-d'))
                                    ->count();
        
//count_inprogress_current
        $count_inprogress_current = PatientRequest::whereIn('status',['rw','qw'])
                ->whereDate('created_at',date('Y-m-d'))
                ->count();
//   count_inprogress_current
        //
        $count_confirmed_current = PatientRequest::whereIn('status',['qa'])
                ->whereDate('created_at',date('Y-m-d'))
                ->count();
        //
          $count_canceled_current = PatientRequest::whereIn('status',['rr','qr'])
                ->whereDate('created_at',date('Y-m-d'))
                ->count();
//
          
       $total_count = DB::table('patient_requests')->whereDate('created_at',date('Y-m-d'))->count();
       if($total_count==0)
       {
           $total_count =1 ; 
       }
       //Precentage 
       $count_unprocessed_currentprec =$count_unprocessed_current / $total_count;
               $count_inprogress_currentprec = $count_inprogress_current /$total_count;
               $count_confirmed_currentprec = $count_confirmed_current/  $total_count;
               $count_canceled_currentprec = $count_canceled_current/ $total_count;
       /*
        * Calculate for all
        */ 
               $total_countcurrent = DB::table('patient_requests')->count();
               $total_countcurrentqa = PatientRequest::whereIn('status',['qa'])
                ->count();
               $total_countcurrentrr = PatientRequest::whereIn('status',['qr','rr'])
                       ->count();
               
               /*
                * Calculate for current 
                */
               $total_countcurrentdate = DB::table('patient_requests')->whereDate('created_at',date('Y-m-d'))->count();
               $total_countcurrentdateqa = PatientRequest::whereIn('status',['qa'])
                       ->whereDate('created_at',date('Y-m-d'))
                ->count();
               $total_countcurrentdaterr = PatientRequest::whereIn('status',['qr','rr'])
                       ->whereDate('created_at',date('Y-m-d'))
                       ->count();
               $total_countcurrentdateqw = PatientRequest::whereIn('status',['rw','qw'])
                       ->whereDate('created_at',date('Y-m-d'))
                       ->count();
               /*
                * For month
                */
                $total_countcurrentmonth = DB::table('patient_requests')->whereMonth('created_at',date('m'))->count();
               $total_countcurrentmonthqa = PatientRequest::whereIn('status',['qa'])
                       ->whereMonth('created_at',date('m'))
                ->count();
               $total_countcurrentmonthrr = PatientRequest::whereIn('status',['qr','rr'])
                       ->whereMonth('created_at',date('m'))
                       ->count();
               $total_countcurrentmonthqw = PatientRequest::whereIn('status',['rw','qw'])
                       ->whereMonth('created_at',date('m'))
                       ->count();
               /*
                * For year
                */
                $total_countcurrentyear = DB::table('patient_requests')->whereYear('created_at',date('Y'))->count();
               $total_countcurrentyearqa = PatientRequest::whereIn('status',['qa'])
                       ->whereYear('created_at',date('Y'))
                ->count();
               $total_countcurrentyearrr = PatientRequest::whereIn('status',['qr','rr'])
                       ->whereYear('created_at',date('Y'))
                       ->count();
               $total_countcurrentyearqw = PatientRequest::whereIn('status',['rw','qw'])
                       ->whereYear('created_at',date('Y'))
                       ->count();               
               
        return view('home',compact('requests' ,'total_countcurrentyear','total_countcurrentyearqa','total_countcurrentyearrr','total_countcurrentyearqw','total_countcurrentmonth','total_countcurrentmonthqa','total_countcurrentmonthrr','total_countcurrentmonthqw','total_countcurrentdate','total_countcurrentdateqa','total_countcurrentdaterr','total_countcurrentdateqw','total_countcurrent','total_countcurrentqa','total_countcurrentrr','total_count','count_unprocessed_current','count_inprogress_current','count_confirmed_current','count_canceled_current','count_unprocessed_currentprec','count_inprogress_currentprec','count_confirmed_currentprec','count_canceled_currentprec', 'centers' ,'radiologyTypesCategory' , 'radiologyTypes'));
    }
 
}
