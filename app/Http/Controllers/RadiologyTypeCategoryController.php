<?php
namespace App\Http\Controllers;

use App\RadiologyTypeCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class RadiologyTypeCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $radiologyCats = RadiologyTypeCategory::all();
        return view('radiologytypecategories.index',compact('radiologyCats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('radiologytypecategories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dbSuccsess = false;
        $error = 'Unknown error - Store';

        DB::beginTransaction();
        try{
            RadiologyTypeCategory::create($request->input());
            $dbSuccsess = true;
        }catch(Exception $ex)
        {
            $error = $ex->getMessage();
        }finally
        {
            if($dbSuccsess)
            {
                DB::commit();
                return redirect()->back()
                    ->with( 'db' , $dbSuccsess);
            }
            else{
                //DB::rollback();
                return redirect()->back()
                    ->with( 'db' , $dbSuccsess)
                    ->with( 'error' , $error);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cat = RadiologyTypeCategory::find($id);
        return view('radiologytypecategories.show',compact('cat'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cat = RadiologyTypeCategory::find($id);
        return view('radiologytypecategories.edit',compact('cat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cat = RadiologyTypeCategory::find($id);

        $dbSuccsess = false;

        $error = 'Unknown error - Store';

        DB::beginTransaction();
        try {
            $cat->update($request->input());
            $dbSuccsess = true;
        }catch(Exception $ex)
        {
            $error = $ex->getMessage();
        }finally
        {
            if($dbSuccsess)
            {
                DB::commit();
                return redirect()
                    ->back()
                    ->with( 'db' , $dbSuccsess);
            }
            else{
                DB::rollback();
                return redirect()
                    ->back()
                    ->with( 'db' , $dbSuccsess)
                    ->with( 'error' , $error);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $cat = RadiologyTypeCategory::find($id);

        $dbSuccsess = false;

        $error = 'Unknown error - Store';

        DB::beginTransaction();
        try {
            $cat->delete();
            $dbSuccsess = true;
        }catch(Exception $ex)
        {
            $error = $ex->getMessage();
        }finally
        {
            if($dbSuccsess)
            {
                DB::commit();
                return redirect()
                    ->back()
                    ->with( 'db' , $dbSuccsess);
            }
            else{
                DB::rollback();
                return redirect()
                    ->back()
                    ->with( 'db' , $dbSuccsess)
                    ->with( 'error' , $error);
            }
        }
    }
}
