<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestDetail extends Model
{
    protected $table = 'RequestDetail';
    public $timestamps = false;
    protected $fillable = [
        'request_header_id',
        'vendor_item_id',
        'written_type_list',
        'is_active',
        'quantity'
    ];
     protected $attributes = [
        'is_active' => 1
    ];
     public function request_header(){
        return $this->belongsTo(RequestHeader::class,'request_header_id','id');
     }
        public function vendor_items(){
        return $this->belongsTo(VendorItems::class,'vendor_item_id','id');
        }
        public function request_vendor_branches(){
        return $this->hasMany(RequestVendorBranches::class,'request_details_id','id');
        }
     
}
