<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class vendoruploads extends Model
{
    public $timestamps = false;
        protected $table = 'VendorUploads';
    


    public function vendor_services(){
        return $this->belongsTo(vendorservices::class,'vendorservices_id','id');
    }
    public function service_files(){
        return $this->belongsTo(ServiceFiles::class,'service_files_id','id');
    }
    

}
