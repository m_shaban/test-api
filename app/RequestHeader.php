<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestHeader extends Model
{
    protected $table = 'RequestHeader';
    protected $fillable = [
        'requester_id',
        'requester_type',
        'recipient_id',
        'weight',
        'patient_address_id',
        'is_home',
        'is_stable',
        'is_emergency',
        'request_status_id',
        'rate',
        'comment',
        'has_insurance',
        'date',
        'center_id',
        'relation',
        'notes',
        'test_date_time',
        'delivery_estimate_dateTime',
        'request_type'
    ];
     public function user(){
        return $this->belongsTo(User::class,'requester_id','id');
        }
        public function request_detail(){
        return $this->hasMany(RequestDetail::class,'request_header_id','id');
     }
     public function request_uploads(){
        return $this->hasMany(RequestUploads::class,'request_header_id','id');
     }
     
      public function RequestQuestion(){
        return $this->hasMany(RequestQuestion::class,'request_id','id');
        }
        public function request_status(){
        return $this->belongsTo(request_status::class,'request_status_id','id');
        }
        public function RequestLog(){
        return $this->hasMany(RequestLog::class,'request_header_id','id');
        }
}
