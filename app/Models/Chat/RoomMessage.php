<?php

namespace App\Models\Chat;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class RoomMessage extends Model
{
    protected $table= 'room_messages';
    protected $fillable = ['room_id','message','message_type','user_type'];
    protected $appends = ['CreatedBy'];
    public function chat_room(){
        return $this->belongsTo(ChatRoom::class,'room_id','id');
    }

    public function getCreatedByAttribute(){
        if($this->user_type == 'd'){
            return $this->chat_room->doctor->name;
        }else{
            return $this->chat_room->patient->name;
        }
    }

    public function getCreatedAt(){
        $date = Carbon::parse($this->created_at);
        return $date->format('d/m/Y h:i:s A');
    }
}
