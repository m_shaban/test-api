<?php

namespace App\Listeners;


use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Broadcast;

class DoctorStopTyping
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DoctorStopTyping  $event
     * @return void
     */
    public function handle(\App\Events\DoctorStopTyping $event)
    {
        Broadcast::channel($event->roomName, function ($user, $userId) {
            return true;
        });
    }
}
