<?php

namespace App\Listeners;

use App\Events\DoctorJoinedRoomEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Broadcast;

class DoctorJoinedRoomListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DoctorJoinedRoomEvent  $event
     * @return void
     */
    public function handle(DoctorJoinedRoomEvent $event)
    {
        Broadcast::channel($event->roomName, function () { return true;});
    }
}
