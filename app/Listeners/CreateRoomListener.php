<?php

namespace App\Listeners;

use App\Events\CreateRoomEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Broadcast;

class CreateRoomListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CreateRoomEvent  $event
     * @return void
     */
    public function handle(CreateRoomEvent $event)
    {
        Broadcast::channel($event->roomName,function(){return true;});
    }
}
