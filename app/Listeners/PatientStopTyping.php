<?php

namespace App\Listeners;


use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Broadcast;

class PatientStopTyping
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PatientStopTyping  $event
     * @return void
     */
    public function handle(\App\Events\PatientStopTyping $event)
    {
        Broadcast::channel($event->roomName, function ($user, $userId) {
            return true;
        });
    }
}
