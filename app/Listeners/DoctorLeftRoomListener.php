<?php

namespace App\Listeners;

use App\Events\DoctorLeftRoomEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Broadcast;

class DoctorLeftRoomListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DoctorLeftRoomEvent  $event
     * @return void
     */
    public function handle(DoctorLeftRoomEvent $event)
    {
        Broadcast::channel($event->roomName, function () { return true;});
    }
}
