<?php

namespace App\Listeners;


use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Broadcast;

class DoctorTyping
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DoctorTyping  $event
     * @return void
     */
    public function handle(\App\Events\DoctorTyping $event)
    {
        Broadcast::channel($event->roomName, function ($user, $userId) {
            return true;
        });
    }
}
