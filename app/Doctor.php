<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    protected $table='doctor';

    protected $hidden = ['password'];

    public function logins(){
        return $this->hasMany(DoctorLogin::class,'doctor_id','id');
    }
}
