
create view v_vendors
as
select v.id vendor_id , v.name vendor_name , vb.id vendor_branch_id , vb.name as vendor_branch_name , vb.address , vb.lon , vb.lat , vb.head_quarter , vb.map_link 
from vendor v left join vendorbranches vb
on (v.id=vb.vendor_id)
where v.is_active = 1;


create view v_vendor_phone
as
SELECT v.*,vc.phone 
from v_vendors v left JOIN vendorcontacts vc 
on (v.vendor_branch_id=vc.vendor_branch_id)
where name = 'branch'
Union 
SELECT v.*,vc.mobile 
from v_vendors v left JOIN vendorcontacts vc 
on (v.vendor_branch_id=vc.vendor_branch_id)
where name = 'branch';

create view v_services
as
select s.id service_id , s.name service_name , st.id service_type_id , st.name service_type_name 
from services s LEFT JOIN servicetypes st
on (s.service_type_id=st.id)

alter view v_vendor_services
as 
select v.* , s.*, vs.id vendor_service_type_id
from v_vendors v left join vendorservices vs 
on (v.vendor_id= vs.vendor_id)
LEFT JOIN v_services s
on (vs.service_id=s.service_id)



create view v_vendor_items
as
select i.id vendor_item_id 
     , en_name vendor_item_name_en 
     , ar_name vendor_item_name_ar 
     , description vendor_item_description 
     , is_active 
     , v.*
from vendoritems i LEFT JOIN v_vendor_services v
on (i.vendor_service_type_id=v.vendor_service_type_id)
where i.is_active = 1;

Create view v_vendor_item_branches as 
AS SELECT
	`i`.`vendor_item_id` AS `vendor_item_id`,
	`vb`.`id` AS `vendor_branch_item_id`,
	`vb`.`price` AS `price`,
	`i`.`vendor_item_name_en` AS `vendor_item_name_en`,
	`i`.`vendor_item_name_ar` AS `vendor_item_name_ar`,
	`i`.`vendor_item_description` AS `vendor_item_description`,
	`i`.`vendor_id` AS `vendor_id`,
	`i`.`vendor_name` AS `vendor_name`,
	`i`.`vendor_branch_id` AS `vendor_branch_id`,
	`i`.`vendor_branch_name` AS `vendor_branch_name`,
	`i`.`address` AS `address`,
	`i`.`lon` AS `lon`,
	`i`.`lat` AS `lat`,
	`i`.`map_link` AS `map_link`,
	`i`.`service_id` AS `service_id`,
	`i`.`service_name` AS `service_name`,
	`i`.`service_type_id` AS `service_type_id`,
	`i`.`service_type_name` AS `service_type_name`,
	`i`.`vendor_service_type_id` AS `vendor_service_type_id`
FROM
	(
		`vendoritembranches` `vb`
		LEFT JOIN `v_vendor_items` `i` ON(
			(
				(
					`vb`.`vendor_item_id` = `i`.`vendor_item_id`
				)
				AND(
					`vb`.`branch_id` = `i`.`vendor_branch_id`
				)
			)
		)
	)
WHERE
	(`vb`.`is_active` = 1)



Create view v_requests
as
select rh.id request_header_id , requester_type , is_home , is_stable , is_emergency , weight , request_status_id , patient_address_id , rd.id AS request_detail_id , p.PatientName , p.PatientGender  
from requestheader rh LEFT JOIN requestdetail rd
on (rh.id=rd.request_header_id)
left join v_patient p
on (p.PatientCode = rh.requester_id);

create view request_vendor_branch_item_id
AS
select * 
from v_requests r left join requestvendorbranches rvb 
on (r.request_detail_id=request_details_id)
left join v_vendor_item_branches  i
on (rvb.vendor_item_branches_id=i.vendor_branch_item_id)
