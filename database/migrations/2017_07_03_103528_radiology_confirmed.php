<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RadiologyConfirmed extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('RadiologyConfirmed', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('request_id')->unsigned();
            $table->integer('center_id')->unsigned();
            $table->integer('radiology_type_id')->unsigned();
            $table->timestamps();
            $table->foreign('request_id')->references('id')->on('patient_requests');
            $table->foreign('center_id')->references('id')->on('centers');
            
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('centers');
        //
    }
}
