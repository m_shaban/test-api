<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePharmacyTeable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('Pharmacy', function (Blueprint $table) {
            $table->integer('id');
            $table->text('name_en');
            $table->text('name_ar');
            $table->text('active_ingradiant');
            $table->text('manufacture');
            $table->text('primary_unit');
            $table->text('secondry_unit');
            $table->text('secondry_unit_price');
            $table->text('price');
            $table->integer('vendor_item_id')->unsigned();
            $table->foreign('vendor_item_id')->references('id')->on('vendoritems');     
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Pharmacy');
    }
}
