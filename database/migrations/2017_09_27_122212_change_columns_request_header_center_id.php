<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnsRequestHeaderCenterId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
        Schema::table('RequestHeader', function($table) {
            DB::statement('ALTER TABLE `RequestHeader` CHANGE `center_id` `center_id` INT(11) NULL;');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
            Schema::table('RequestHeader', function($table) {
            $table->dropColumn('center_id');
           
        });
    }
}
