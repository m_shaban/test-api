<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnVendorRequestReview extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
   {
        Schema::table('RequestDetail', function (Blueprint $table) {
            $table->integer('rate');     
            $table->string('comment');  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('RequestDetail', function (Blueprint $table) {
            $table->dropColumn(['rate']);
            $table->dropColumn(['comment']);
        });
    }
}
