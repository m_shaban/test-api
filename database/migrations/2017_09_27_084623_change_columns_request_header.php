<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnsRequestHeader extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
    {
        Schema::table('RequestHeader', function($table) {
            DB::statement('ALTER TABLE `RequestHeader` CHANGE `rate` `rate` INT(11) NULL;');
            DB::statement('ALTER TABLE `RequestHeader` CHANGE `comment` `comment` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
            Schema::table('RequestHeader', function($table) {
            $table->dropColumn('rate');
            $table->dropColumn('comment');
        });
    }
}
