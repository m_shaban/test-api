<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('RequestDetail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('request_header_id')->unsigned();
            $table->integer('vendor_item_id')->unsigned();
            $table->date('date');
            $table->boolean('is_active');
            $table->foreign('request_header_id')->references('id')->on('RequestHeader');              
            $table->foreign('vendor_item_id')->references('id')->on('VendorItems');    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('RequestDetail');
    }
}
