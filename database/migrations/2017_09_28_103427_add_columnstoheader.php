<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnstoheader extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('RequestHeader', function($table) {
            $table->text('notes')->nullable();
            $table->text('test_date_time')->nullable();
            $table->text('delivery_estimate_dateTime')->nullable();
      
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('RequestHeader', function (Blueprint $table) {
            $table->dropColumn(['notes']);
      $table->dropColumn(['test_date_time']);
      $table->dropColumn(['delivery_estimate_dateTime']);
        });
    }
}
