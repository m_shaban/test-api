<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResponseRequestStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('RequestHeader', function (Blueprint $table) {
            $table->integer('request_status_id')->unsigned();
            $table->foreign('request_status_id')->references('id')->on('RequestStatus');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('RequestHeader', function (Blueprint $table) {
            $table->dropForeign(['status']);
           
        });
    }
}
