<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditVendorItemIdRequestDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
        Schema::table('RequestDetail', function($table) {
            DB::statement('ALTER TABLE `RequestDetail` CHANGE `vendor_item_id` `vendor_item_id` INT(10) UNSIGNED NULL;');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
            Schema::table('RequestDetail', function($table) {
            $table->dropColumn('vendor_item_id');
            
        });
    }
}
