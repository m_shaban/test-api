<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsRequestHeader extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('RequestHeader', function($table) {
            $table->integer('has_insurance');
            $table->text('date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('RequestHeader', function (Blueprint $table) {
            $table->dropColumn(['has_insurance']);
            $table->dropColumn(['date']);
        });
    }
}
