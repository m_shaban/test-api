<?php

namespace Pusher;

class PusherInstance
{
    private static $instance = null;
    private static $app_id = '401630';
    private static $secret = '408dfc64eed2e3beb0b0';
    private static $api_key = '988e897789f5b46cfa0d';

    public static function get_pusher()
    {
        if (self::$instance !== null) {
            return self::$instance;
        }

        self::$instance = new Pusher(
            self::$api_key,
            self::$secret,
            self::$app_id
        );

        return self::$instance;
    }
}
