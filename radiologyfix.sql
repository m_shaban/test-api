
CREATE DATABASE IF NOT EXISTS `radiology` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `radiology`;

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
CREATE TABLE IF NOT EXISTS `address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) DEFAULT NULL,
  `country_code` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `longtiude` varchar(45) DEFAULT NULL,
  `latitude` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `apps_cities`
--

DROP TABLE IF EXISTS `apps_cities`;
CREATE TABLE IF NOT EXISTS `apps_cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `apps_cities`
--

INSERT INTO `apps_cities` (`id`, `country_id`, `city`) VALUES
(1, 'EG', 'Cairo'),
(2, 'EG', '');

-- --------------------------------------------------------

--
-- Table structure for table `apps_countries`
--

DROP TABLE IF EXISTS `apps_countries`;
CREATE TABLE IF NOT EXISTS `apps_countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_code` varchar(2) NOT NULL DEFAULT '',
  `country_name` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=88 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `apps_countries`
--

INSERT INTO `apps_countries` (`id`, `country_code`, `country_name`) VALUES
(1, 'AF', 'Afghanistan'),
(2, 'AL', 'Albania'),
(3, 'DZ', 'Algeria'),
(4, 'DS', 'American Samoa'),
(5, 'AD', 'Andorra'),
(6, 'AO', 'Angola'),
(7, 'AI', 'Anguilla'),
(8, 'AQ', 'Antarctica'),
(9, 'AG', 'Antigua and Barbuda'),
(10, 'AR', 'Argentina'),
(11, 'AM', 'Armenia'),
(12, 'AW', 'Aruba'),
(13, 'AU', 'Australia'),
(14, 'AT', 'Austria'),
(15, 'AZ', 'Azerbaijan'),
(16, 'BS', 'Bahamas'),
(17, 'BH', 'Bahrain'),
(18, 'BD', 'Bangladesh'),
(19, 'BB', 'Barbados'),
(20, 'BY', 'Belarus'),
(21, 'BE', 'Belgium'),
(22, 'BZ', 'Belize'),
(23, 'BJ', 'Benin'),
(24, 'BM', 'Bermuda'),
(25, 'BT', 'Bhutan'),
(26, 'BO', 'Bolivia'),
(27, 'BA', 'Bosnia and Herzegovina'),
(28, 'BW', 'Botswana'),
(29, 'BV', 'Bouvet Island'),
(30, 'BR', 'Brazil'),
(31, 'IO', 'British Indian Ocean Territory'),
(32, 'BN', 'Brunei Darussalam'),
(33, 'BG', 'Bulgaria'),
(34, 'BF', 'Burkina Faso'),
(35, 'BI', 'Burundi'),
(36, 'KH', 'Cambodia'),
(37, 'CM', 'Cameroon'),
(38, 'CA', 'Canada'),
(39, 'CV', 'Cape Verde'),
(40, 'KY', 'Cayman Islands'),
(41, 'CF', 'Central African Republic'),
(42, 'TD', 'Chad'),
(43, 'CL', 'Chile'),
(44, 'CN', 'China'),
(45, 'CX', 'Christmas Island'),
(46, 'CC', 'Cocos (Keeling) Islands'),
(47, 'CO', 'Colombia'),
(48, 'KM', 'Comoros'),
(49, 'CG', 'Congo'),
(50, 'CK', 'Cook Islands'),
(51, 'CR', 'Costa Rica'),
(52, 'HR', 'Croatia (Hrvatska)'),
(53, 'CU', 'Cuba'),
(54, 'CY', 'Cyprus'),
(55, 'CZ', 'Czech Republic'),
(56, 'DK', 'Denmark'),
(57, 'DJ', 'Djibouti'),
(58, 'DM', 'Dominica'),
(59, 'DO', 'Dominican Republic'),
(60, 'TP', 'East Timor'),
(61, 'EC', 'Ecuador'),
(62, 'EG', 'Egypt'),
(63, 'SV', 'El Salvador'),
(64, 'GQ', 'Equatorial Guinea'),
(65, 'ER', 'Eritrea'),
(66, 'EE', 'Estonia'),
(67, 'ET', 'Ethiopia'),
(68, 'FK', 'Falkland Islands (Malvinas)'),
(69, 'FO', 'Faroe Islands'),
(70, 'FJ', 'Fiji'),
(71, 'FI', 'Finland'),
(72, 'FR', 'France'),
(73, 'FX', 'France, Metropolitan'),
(74, 'GF', 'French Guiana'),
(75, 'PF', 'French Polynesia'),
(76, 'TF', 'French Southern Territories'),
(77, 'GA', 'Gabon'),
(78, 'GM', 'Gambia'),
(79, 'GE', 'Georgia'),
(80, 'DE', 'Germany'),
(81, 'GH', 'Ghana'),
(82, 'GR', 'Greece'),
(83, 'GD', 'Grenada'),
(84, 'NA', 'Namibia'),
(85, 'NP', 'Nepal'),
(86, 'PA', 'Panama'),
(87, 'PN', 'Pitcairn');

-- --------------------------------------------------------

--
-- Table structure for table `centers`
--

DROP TABLE IF EXISTS `centers`;
CREATE TABLE IF NOT EXISTS `centers` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `map_location` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `centers`
--

INSERT INTO `centers` (`id`, `name`, `address`, `map_location`, `created_at`, `updated_at`) VALUES
(1, 'A1', 'Address 1', '123,345', '2017-06-03 08:09:33', '2017-06-03 08:09:33'),
(2, 'A2', 'Address 2', '225,657', '2017-06-03 08:09:33', '2017-06-03 08:09:33'),
(3, 'A3', 'Address 3', '325,789', '2017-06-03 08:09:33', '2017-06-03 08:09:33');

-- --------------------------------------------------------

--
-- Table structure for table `center_phones`
--

DROP TABLE IF EXISTS `center_phones`;
CREATE TABLE IF NOT EXISTS `center_phones` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `center_id` int(10) UNSIGNED NOT NULL,
  `phone` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `center_phones_center_id_foreign` (`center_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `center_phones`
--

INSERT INTO `center_phones` (`id`, `center_id`, `phone`, `created_at`, `updated_at`) VALUES
(1, 1, '123456', '2017-06-03 08:09:33', '2017-06-03 08:09:33'),
(2, 2, '223856', '2017-06-03 08:09:33', '2017-06-03 08:09:33'),
(3, 3, '323454', '2017-06-03 08:09:34', '2017-06-03 08:09:34'),
(4, 3, '323459', '2017-06-03 08:09:34', '2017-06-03 08:09:34');

-- --------------------------------------------------------

--
-- Table structure for table `center_responses`
--

DROP TABLE IF EXISTS `center_responses`;
CREATE TABLE IF NOT EXISTS `center_responses` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `delivery_est_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `request_id` int(10) UNSIGNED NOT NULL,
  `radiology_datetime` timestamp NULL DEFAULT NULL,
  `is_at_home` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `center_responses_request_id_foreign` (`request_id`)
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `center_responses`
--

INSERT INTO `center_responses` (`id`, `delivery_est_datetime`, `request_id`, `radiology_datetime`, `is_at_home`, `created_at`, `updated_at`) VALUES
(3, '2017-06-20 08:00:00', 1, '2017-06-21 08:00:00', 0, '2017-06-03 08:11:28', '2017-06-03 08:11:28'),
(4, '2017-06-08 19:41:32', 2, NULL, 1, '2017-06-03 08:11:28', '2017-06-03 08:11:28'),
(26, '2017-06-08 21:30:42', 1, NULL, 0, '2017-06-08 19:30:42', '2017-06-08 19:30:42'),
(27, '2017-06-08 21:32:37', 1, NULL, 0, '2017-06-08 19:32:37', '2017-06-08 19:32:37'),
(28, '2017-06-08 21:34:29', 1, NULL, 0, '2017-06-08 19:34:29', '2017-06-08 19:34:29'),
(29, '2017-06-08 21:34:48', 1, NULL, 0, '2017-06-08 19:34:48', '2017-06-08 19:34:48'),
(30, '2017-06-08 21:36:39', 1, NULL, 0, '2017-06-08 19:36:39', '2017-06-08 19:36:39'),
(31, '2017-06-08 21:37:10', 1, NULL, 0, '2017-06-08 19:37:10', '2017-06-08 19:37:10'),
(32, '2017-06-08 22:59:49', 1, NULL, 0, '2017-06-08 20:59:49', '2017-06-08 20:59:49'),
(33, '2017-06-08 23:03:04', 1, NULL, 0, '2017-06-08 21:03:04', '2017-06-08 21:03:04'),
(34, '2017-06-08 23:03:19', 1, NULL, 0, '2017-06-08 21:03:19', '2017-06-08 21:03:19'),
(35, '2017-06-08 23:07:20', 1, NULL, 0, '2017-06-08 21:07:20', '2017-06-08 21:07:20'),
(36, '2017-06-08 23:08:29', 1, NULL, 0, '2017-06-08 21:08:29', '2017-06-08 21:08:29'),
(37, '2017-06-08 23:09:16', 1, NULL, 0, '2017-06-08 21:09:16', '2017-06-08 21:09:16'),
(38, '2017-06-08 23:10:02', 1, NULL, 0, '2017-06-08 21:10:02', '2017-06-08 21:10:02'),
(39, '2017-06-08 23:21:18', 1, NULL, 0, '2017-06-08 21:21:18', '2017-06-08 21:21:18'),
(40, '2017-06-08 23:21:42', 1, NULL, 0, '2017-06-08 21:21:42', '2017-06-08 21:21:42'),
(41, '2017-06-08 23:23:47', 1, NULL, 0, '2017-06-08 21:23:47', '2017-06-08 21:23:47'),
(42, '2017-06-08 23:29:45', 1, NULL, 0, '2017-06-08 21:29:45', '2017-06-08 21:29:45'),
(43, '2017-06-08 23:30:25', 1, NULL, 0, '2017-06-08 21:30:25', '2017-06-08 21:30:25'),
(44, '2017-06-08 23:36:41', 1, NULL, 0, '2017-06-08 21:36:41', '2017-06-08 21:36:41'),
(45, '2017-06-09 01:56:19', 1, NULL, 0, '2017-06-08 23:56:19', '2017-06-08 23:56:19'),
(46, '2017-06-09 02:22:28', 1, NULL, 0, '2017-06-09 00:22:28', '2017-06-09 00:22:28'),
(47, '2017-06-09 02:27:18', 1, NULL, 0, '2017-06-09 00:27:18', '2017-06-09 00:27:18'),
(48, '2017-06-09 02:28:34', 1, NULL, 0, '2017-06-09 00:28:34', '2017-06-09 00:28:34'),
(49, '2017-06-09 02:30:50', 1, NULL, 0, '2017-06-09 00:30:50', '2017-06-09 00:30:50'),
(50, '2017-06-09 08:52:25', 1, NULL, 0, '2017-06-09 06:52:25', '2017-06-09 06:52:25'),
(51, '2017-06-09 08:52:51', 1, NULL, 0, '2017-06-09 06:52:51', '2017-06-09 06:52:51'),
(52, '2017-06-09 08:53:06', 1, NULL, 0, '2017-06-09 06:53:06', '2017-06-09 06:53:06'),
(53, '2017-06-09 08:54:04', 1, NULL, 0, '2017-06-09 06:54:04', '2017-06-09 06:54:04'),
(54, '2017-06-09 08:56:18', 1, NULL, 0, '2017-06-09 06:56:18', '2017-06-09 06:56:18'),
(55, '2017-06-09 08:57:27', 1, NULL, 0, '2017-06-09 06:57:27', '2017-06-09 06:57:27'),
(56, '2017-06-09 08:58:40', 1, NULL, 0, '2017-06-09 06:58:40', '2017-06-09 06:58:40'),
(57, '2017-06-09 08:59:03', 1, NULL, 0, '2017-06-09 06:59:03', '2017-06-09 06:59:03'),
(58, '2017-06-09 08:59:48', 1, NULL, 0, '2017-06-09 06:59:48', '2017-06-09 06:59:48'),
(59, '2017-06-09 09:00:08', 1, NULL, 0, '2017-06-09 07:00:08', '2017-06-09 07:00:08'),
(60, '2017-06-09 09:04:42', 1, NULL, 0, '2017-06-09 07:04:42', '2017-06-09 07:04:42'),
(61, '2017-06-09 09:08:37', 1, NULL, 0, '2017-06-09 07:08:37', '2017-06-09 07:08:37'),
(62, '2017-06-09 09:10:33', 1, NULL, 0, '2017-06-09 07:10:33', '2017-06-09 07:10:33'),
(63, '2017-06-09 09:11:15', 1, NULL, 0, '2017-06-09 07:11:15', '2017-06-09 07:11:15'),
(64, '2017-06-09 09:12:32', 1, NULL, 0, '2017-06-09 07:12:32', '2017-06-09 07:12:32'),
(65, '2017-06-09 09:12:54', 1, NULL, 0, '2017-06-09 07:12:54', '2017-06-09 07:12:54'),
(66, '2017-06-09 09:15:04', 1, NULL, 0, '2017-06-09 07:15:04', '2017-06-09 07:15:04'),
(67, '2017-06-09 09:16:10', 1, NULL, 0, '2017-06-09 07:16:10', '2017-06-09 07:16:10'),
(68, '2017-06-09 09:16:55', 1, NULL, 0, '2017-06-09 07:16:55', '2017-06-09 07:16:55'),
(69, '2017-06-09 09:17:37', 1, NULL, 0, '2017-06-09 07:17:37', '2017-06-09 07:17:37'),
(70, '2017-06-09 09:18:02', 1, NULL, 0, '2017-06-09 07:18:02', '2017-06-09 07:18:02'),
(71, '2017-06-09 09:19:41', 1, NULL, 0, '2017-06-09 07:19:41', '2017-06-09 07:19:41'),
(72, '2017-06-09 09:22:40', 1, NULL, 0, '2017-06-09 07:22:40', '2017-06-09 07:22:40'),
(73, '2017-06-09 09:23:16', 1, NULL, 0, '2017-06-09 07:23:16', '2017-06-09 07:23:16'),
(74, '2017-06-09 09:31:00', 1, NULL, 0, '2017-06-09 07:31:00', '2017-06-09 07:31:00'),
(75, '2017-06-09 09:31:16', 1, NULL, 0, '2017-06-09 07:31:16', '2017-06-09 07:31:16'),
(76, '2017-06-09 09:31:21', 1, NULL, 0, '2017-06-09 07:31:21', '2017-06-09 07:31:21'),
(77, '2017-06-09 09:32:59', 1, NULL, 0, '2017-06-09 07:32:59', '2017-06-09 07:32:59'),
(78, '2017-06-09 09:34:10', 1, NULL, 0, '2017-06-09 07:34:10', '2017-06-09 07:34:10'),
(79, '2017-06-09 09:41:36', 1, NULL, 0, '2017-06-09 07:41:36', '2017-06-09 07:41:36'),
(80, '2017-06-09 09:42:55', 1, NULL, 0, '2017-06-09 07:42:55', '2017-06-09 07:42:55'),
(81, '2017-06-09 09:44:07', 1, NULL, 0, '2017-06-09 07:44:07', '2017-06-09 07:44:07'),
(82, '2017-06-09 09:44:24', 1, NULL, 0, '2017-06-09 07:44:24', '2017-06-09 07:44:24'),
(83, '2017-06-09 09:47:12', 1, NULL, 0, '2017-06-09 07:47:12', '2017-06-09 07:47:12'),
(84, '2017-06-09 09:47:52', 1, NULL, 0, '2017-06-09 07:47:52', '2017-06-09 07:47:52'),
(85, '2017-06-09 09:48:45', 1, NULL, 0, '2017-06-09 07:48:45', '2017-06-09 07:48:45'),
(86, '2017-06-09 09:49:21', 1, NULL, 0, '2017-06-09 07:49:21', '2017-06-09 07:49:21'),
(87, '2017-06-09 09:50:20', 1, NULL, 0, '2017-06-09 07:50:20', '2017-06-09 07:50:20'),
(88, '2017-06-09 09:52:01', 1, NULL, 0, '2017-06-09 07:52:01', '2017-06-09 07:52:01'),
(89, '2017-06-09 09:53:44', 1, NULL, 0, '2017-06-09 07:53:44', '2017-06-09 07:53:44'),
(90, '2017-06-09 10:08:46', 1, NULL, 0, '2017-06-09 08:08:46', '2017-06-09 08:08:46'),
(91, '2017-06-09 10:09:10', 1, NULL, 0, '2017-06-09 08:09:10', '2017-06-09 08:09:10'),
(92, '2017-06-09 10:09:31', 1, NULL, 0, '2017-06-09 08:09:31', '2017-06-09 08:09:31'),
(93, '2017-06-09 10:14:34', 1, NULL, 0, '2017-06-09 08:14:34', '2017-06-09 08:14:34'),
(94, '2018-12-31 10:59:00', 2, '2017-12-31 10:59:00', 1, '2017-06-09 08:26:08', '2017-06-09 08:26:08'),
(95, '2018-12-31 10:59:00', 2, '2017-12-31 10:59:00', 1, '2017-06-09 08:28:52', '2017-06-09 08:28:52'),
(96, '2018-12-31 10:59:00', 2, '2017-12-31 10:59:00', 1, '2017-06-09 08:29:57', '2017-06-09 08:29:57'),
(97, '2017-12-31 10:59:00', 2, '2016-12-31 10:58:00', 1, '2017-06-09 09:01:10', '2017-06-09 09:01:10'),
(98, '2017-12-31 10:59:00', 2, '2016-12-31 10:58:00', 1, '2017-06-09 09:03:09', '2017-06-09 09:03:09'),
(99, '2017-06-09 11:05:04', 1, NULL, 0, '2017-06-09 09:05:04', '2017-06-09 09:05:04'),
(100, '2017-12-31 10:59:00', 2, '2017-12-27 10:59:00', 1, '2017-06-09 09:26:55', '2017-06-09 09:26:55'),
(101, '2017-06-09 21:36:07', 1, NULL, 0, '2017-06-09 19:36:07', '2017-06-09 19:36:07'),
(102, '2017-06-12 15:33:11', 1, NULL, 0, '2017-06-12 13:33:11', '2017-06-12 13:33:11'),
(103, '2017-06-12 15:35:54', 1, NULL, 0, '2017-06-12 13:35:54', '2017-06-12 13:35:54'),
(107, '2018-12-31 10:59:00', 2, '2017-12-31 10:59:00', 1, '2017-06-12 14:04:30', '2017-06-12 14:04:30'),
(108, '2018-11-30 23:00:00', 2, '2017-12-31 10:59:00', 1, '2017-06-12 14:09:26', '2017-06-12 14:09:26'),
(109, '2017-06-12 16:16:31', 1, NULL, 0, '2017-06-12 14:16:31', '2017-06-12 14:16:31'),
(110, '2017-06-12 17:52:43', 1, NULL, 0, '2017-06-12 15:52:43', '2017-06-12 15:52:43'),
(111, '2017-06-13 11:27:04', 1, NULL, 0, '2017-06-13 09:27:04', '2017-06-13 09:27:04'),
(112, '2017-06-13 11:32:09', 1, NULL, 0, '2017-06-13 09:32:09', '2017-06-13 09:32:09'),
(113, '2017-12-31 10:59:00', 2, '2017-06-13 10:59:00', 1, '2017-06-13 09:33:14', '2017-06-13 09:33:14'),
(120, '2017-06-13 11:57:43', 1, NULL, 0, '2017-06-13 09:57:43', '2017-06-13 09:57:43');

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

DROP TABLE IF EXISTS `config`;
CREATE TABLE IF NOT EXISTS `config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `min_working_version` varchar(45) DEFAULT NULL,
  `newest_version` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `config`
--

INSERT INTO `config` (`id`, `min_working_version`, `newest_version`) VALUES
(1, '1.0', '1');

-- --------------------------------------------------------

--
-- Table structure for table `doctor`
--

DROP TABLE IF EXISTS `doctor`;
CREATE TABLE IF NOT EXISTS `doctor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_state` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_credit` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_img` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `graduation_year` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `short_bio_arabic` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_bio_english` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `neighborhood` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `doctor_level_id` int(11) NOT NULL,
  `doctor_speciality_id` int(11) NOT NULL,
  `front_card_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `back_card_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `referral_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `practicing` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=164 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `doctor`
--

INSERT INTO `doctor` (`id`, `name`, `email`, `gender`, `user_state`, `current_credit`, `profile_img`, `password`, `graduation_year`, `short_bio_arabic`, `short_bio_english`, `city`, `country`, `neighborhood`, `doctor_level_id`, `doctor_speciality_id`, `front_card_id`, `back_card_id`, `referral_number`, `practicing`) VALUES
(11, 'asmaa', 'asmaa.kohla@gmail.com', 'M', '0', NULL, 'doc_11_2017-04-1816:10:43.jpg', 'uWpk7AASn+mrBYTYt2rcMrPOumvVCWi0rvJYsKOP/vk=', '2011', '', '', '', '', '', 0, 0, '', '', '', ''),
(13, 'hfhf', 'gdhf@hdj.com', 'M', '0', NULL, NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '2000', '', '', '', '', '', 0, 0, '', '', '', ''),
(14, 'chg', 'fnv@nm.com', 'M', '0', NULL, NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '2580', '', '', '', '', '', 0, 0, '', '', '', ''),
(15, 'bc', 'c@jv.com', 'M', '0', NULL, NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '258', '', '', '', '', '', 0, 0, '', '', '', ''),
(16, 'cjb', 'fhv@hkn.com', 'M', '0', NULL, NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '580', '', '', '', '', '', 0, 0, '', '', '', ''),
(17, 'dbb', 'fhh@fhj.com', 'M', '0', NULL, NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '0852', '', '', '', '', '', 0, 0, '', '', '', ''),
(18, 'fhg', 'fhv@fhv.com', 'M', '0', NULL, NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '5800', '', '', '', '', '', 0, 0, '', '', '', ''),
(19, 'bchf', 'gfg@hck.com', 'M', '0', NULL, NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '2580', '', '', '', '', '', 0, 0, '', '', '', ''),
(20, 'fjjf', 'fhc@vhj.com', 'M', '0', NULL, NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '0852', '', '', '', '', '', 0, 0, '', '', '', ''),
(21, 'cjf', 'bcb@jv.com', 'M', '0', NULL, NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '2580', 'ط£ع©ط¢ع¾ط£ع©ط¢آ¹ط£ع©ط¢آ±ط£â„¢ط¥آ ط£â„¢ط¢ظ¾ ط£â„¢أ¢â‚¬â€چط£â„¢أ¢â‚¬â€چط£ع©ط¢آ¯ط£â„¢ط¦â€™ط£ع©ط¢ع¾ط£â„¢ط«â€ ط£ع©ط¢آ±', 'english bio', '1', 'EG', 'agamy', 4, 30, '', '', '', ''),
(22, 'Asmaa Ali', 'asmaa.kohla@yahoo.com', 'M', '0', NULL, NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '2011', '', '', '', '', '', 0, 0, 'doc_22_2017-04-0414:23:04.jpg', '', '', ''),
(23, 'Asmaa Ali', 'asmaa@gmail.com', 'F', '0', NULL, 'doc_23_2017-04-0414:36:31.jpg', 'uWpk7AASn+mrBYTYt2rcMrPOumvVCWi0rvJYsKOP/vk=', '2011', 'ط£â„¢ط¦â€™ط£â„¢أ¢â‚¬â€چط£ع©ط¢آ§ط£â„¢أ¢â‚¬آ¦ ط£ع©ط¢آ¹ط£ع©ط¢آ±ط£ع©ط¢آ¨ط£â„¢ط¥آ ', 'english txt', '1', 'EG', 'agamy', 4, 30, 'doc_23_2017-04-0414:29:32.jpeg', '', '', ''),
(24, 'djv', 'fhb@hgf.com', 'M', '0', NULL, NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '2020', 'ط£ع©ط¢آ¹ط£ع©ط¢آ±ط£ع©ط¢آ¨ط£â„¢ط¥آ ', 'english', '1', 'EG', 'agamy', 4, 23, 'doc_24_2017-04-0507:29:57.jpeg', '', '', ''),
(25, 'oooo', 'oooo@oo.com', 'M', '0', NULL, NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '1234', 'ط£ع©ط¢آ¹ط£ع©ط¢آ±ط£ع©ط¢آ¨ط£â„¢ط¥آ ', 'english words', '1', 'EG', 'place', 4, 30, 'doc_25_2017-04-1816:14:24.jpeg', '', '', ''),
(26, 'sdfs', '444@gjh.com22', 'male', '0', NULL, NULL, 'r40tXLqSv9m/peVnAhDM+o7JSqE0qbz7S04PNk3qTi4=', '2017', '', '', '', '', '', 0, 0, '', '', 'sdfs3tsmo', '1234'),
(27, 'sdfs', '444@gjh.com', 'male', '0', NULL, NULL, 'r40tXLqSv9m/peVnAhDM+o7JSqE0qbz7S04PNk3qTi4=', '2017', '', '', '', '', '', 0, 0, '', '', 'sdfs1sop9', '1234'),
(29, 'Ali', 'ali@gmail.com', 'male', '0', NULL, NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '2000', '', '', '', '', '', 0, 0, '', '', 'Alikg7rw', '123456'),
(30, 'asmaa', 'asmaa@g.com', 'male', '0', NULL, NULL, 'uWpk7AASn+mrBYTYt2rcMrPOumvVCWi0rvJYsKOP/vk=', '2011', '', '', '', '', '', 0, 0, '', '', 'asmaa9p27w', '0122541'),
(32, 'Mohamed Metwally', 'metwally@gmail.com', 'M', '0', NULL, 'doc_32_2017-04-2501:16:30.jpg', 'rEqbiBtYxeOa4ZSRiIuwkVh532h7w2Ldbgtv+UJ47ek=', '2000', 'ط£ع©ط¢آ§ط£â„¢أ¢â‚¬â€چط£ع©ط¢آ¯ط£â„¢ط¦â€™ط£ع©ط¢ع¾ط£â„¢ط«â€ ط£ع©ط¢آ± ط£â„¢أ¢â‚¬آ¦ط£ع©ط¢آ­ط£â„¢أ¢â‚¬آ¦ط£ع©ط¢آ¯ ط£â„¢أ¢â‚¬آ¦ط£ع©ط¢ع¾ط£â„¢ط«â€ ط£â„¢أ¢â‚¬â€چط£â„¢ط¥آ ', 'Dr. Mohamed Metwally ', '1', 'EG', 'Shobra', 2, 30, 'doc_32_2017-04-2501:13:09.jpg', '', '', ''),
(33, 'ppp', 'pp@pp.cpp', 'male', '0', NULL, NULL, 'r40tXLqSv9m/peVnAhDM+o7JSqE0qbz7S04PNk3qTi4=', '2017', '', '', '', '', '', 0, 0, '', '', 'pppc7w0q', '123'),
(35, 'mo', 'mo@ex.com', 'male', '0', NULL, NULL, 'nGMxe/VCqcKxgMTVFMQSFO0IUevB9JHi22JFtvtl/H8=', '2017', '', '', '', '', '', 0, 0, '', '', 'mos23g8', '1234'),
(36, 'ko', 'ko@ko.com', 'male', '0', NULL, NULL, 'r40tXLqSv9m/peVnAhDM+o7JSqE0qbz7S04PNk3qTi4=', '2017', '', '', '', '', '', 0, 0, '', '', 'kobpwm0', '111'),
(37, 'ko', 'ko@ko.com1', 'male', '0', NULL, NULL, 'r40tXLqSv9m/peVnAhDM+o7JSqE0qbz7S04PNk3qTi4=', '2017', '', '', '', '', '', 0, 0, '', '', 'ko1wp7v', '111'),
(40, 'oo', 'oo@oo.com', 'male', '0', NULL, NULL, 'nGMxe/VCqcKxgMTVFMQSFO0IUevB9JHi22JFtvtl/H8=', '2017', '', '', '', '', '', 0, 0, '', '', 'oo8zdxn', '200'),
(41, 'mo', 'mo@pp.com', 'male', '0', NULL, NULL, 'r40tXLqSv9m/peVnAhDM+o7JSqE0qbz7S04PNk3qTi4=', '2017', '', '', '', '', '', 0, 0, '', '', 'mo756ic', '2033'),
(49, 'klksdf', 'mohammad.shabaan@gmail.com221', 'male', '0', NULL, NULL, 'r40tXLqSv9m/peVnAhDM+o7JSqE0qbz7S04PNk3qTi4=', '2017', '', '', '', '', '', 0, 0, '', '', 'klksdf913qo', '2222'),
(50, 'jkl', 'jkl@jlk.sdf', 'male', '0', NULL, NULL, 'r40tXLqSv9m/peVnAhDM+o7JSqE0qbz7S04PNk3qTi4=', '2017', '', '', '', '', '', 0, 0, '', '', 'jkl3h8x5', '234'),
(51, 'Tester', 'Tester@test.com', 'male', '0', NULL, NULL, 'uWpk7AASn+mrBYTYt2rcMrPOumvVCWi0rvJYsKOP/vk=', '2011', '', '', '', '', '', 0, 0, '', '', 'Testerik9a6', '8896512'),
(52, 'Jcjc', 'Hcjv@hchf.com', 'male', '0', NULL, NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '2008', '', '', '', '', '', 0, 0, '', '', 'Jcjcxm319', '8368'),
(53, 'Dhf', 'Hfhf@fkh.hjf', 'male', '0', NULL, NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '2589', '', '', '', '', '', 0, 0, '', '', 'Dhfnc6dv', '8513685'),
(54, 'Dghfd', 'Fjhd@gjg.fj', 'male', '0', NULL, NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '2549', '', '', '', '', '', 0, 0, '', '', 'Dghfdwwtfs', '55887555'),
(55, 'Gxhf', 'Hgjfu@kh.jf', 'male', '0', NULL, NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '2011', '', '', '', '', '', 0, 0, '', '', 'Gxhf2nfpf', '6525'),
(56, 'mk', 'mk@mk.com', 'male', '2', NULL, NULL, 'r40tXLqSv9m/peVnAhDM+o7JSqE0qbz7S04PNk3qTi4=', '2017', '', '', '', '', '', 0, 0, '', '', 'mkqcg63', '201'),
(57, 'mk', 'mk@mk.com1', 'male', '2', NULL, NULL, 'r40tXLqSv9m/peVnAhDM+o7JSqE0qbz7S04PNk3qTi4=', '2017', '', '', '', '', '', 0, 0, '', '', 'mk2idtp', '201'),
(58, 'mk', 'mk@mk.com12', 'male', '2', NULL, NULL, 'r40tXLqSv9m/peVnAhDM+o7JSqE0qbz7S04PNk3qTi4=', '2017', '', '', '', '', '', 0, 0, '', '', 'mk5woth', '201'),
(64, 'mohamed', 'mohammad.shabaan@gmail.com', 'male', '2', NULL, NULL, 'r40tXLqSv9m/peVnAhDM+o7JSqE0qbz7S04PNk3qTi4=', '2017', '', '', '', '', '', 0, 0, '', '', 'mohamedwlpar', '123'),
(66, 'mshaban', 'mohammad.shab@gmail.com', 'm', '2', NULL, NULL, 'r40tXLqSv9m/peVnAhDM+o7JSqE0qbz7S04PNk3qTi4=', '', 'testarabic', 'testeng', 'alex', 'Eg', 'testneigh', 1, 1, '', '', '', ''),
(67, 'Mohamed Metwally', 'hamanny@gmail.com', 'male', '2', NULL, NULL, 'XnCOFXzvzFGHXS/GZ5kVEZ9PAE2N+oCeqydK87yGuwo=', '1990', '', '', '', '', '', 0, 0, '', '', 'Mohamed Metwallyp2mjr', '12548'),
(68, 'ko', 'ko@ko.cko', 'male', '0', NULL, NULL, 'r40tXLqSv9m/peVnAhDM+o7JSqE0qbz7S04PNk3qTi4=', '22', '', '', '', '', '', 0, 0, '', '', 'kobczzh', '11'),
(69, 'OI', 'io@io.com', 'male', '0', NULL, NULL, 'r40tXLqSv9m/peVnAhDM+o7JSqE0qbz7S04PNk3qTi4=', '897', '', '', '', '', '', 0, 0, '', '', 'OI6vmdq', '123'),
(70, 'ko', 'ko@co.com', 'male', '0', NULL, NULL, 'r40tXLqSv9m/peVnAhDM+o7JSqE0qbz7S04PNk3qTi4=', '2017', '', '', '', '', '', 0, 0, '', '', 'kowl2ju', '123'),
(71, 'mohamed335', 'kk@kk.kk', 'male', '0', NULL, NULL, 'r40tXLqSv9m/peVnAhDM+o7JSqE0qbz7S04PNk3qTi4=', '2004', '', '', '', '', '', 0, 0, '', '', 'kkuw6gy', '12'),
(72, 'oo', 'oo@ii.com', 'male', '0', NULL, NULL, 'r40tXLqSv9m/peVnAhDM+o7JSqE0qbz7S04PNk3qTi4=', '2016', '', '', '', '', '', 0, 0, '', '', 'oohm805', '22'),
(74, 'oo', 'oo@ii.com1', 'male', '0', NULL, NULL, 'r40tXLqSv9m/peVnAhDM+o7JSqE0qbz7S04PNk3qTi4=', '2016', '', '', '', '', '', 0, 0, '', '', 'oomxjv6', '22'),
(75, 'oo', 'oo@ii.com12', 'male', '0', NULL, NULL, 'r40tXLqSv9m/peVnAhDM+o7JSqE0qbz7S04PNk3qTi4=', '2016', '', '', '', '', '', 0, 0, '', '', 'oovh6rw', '22'),
(76, 'op', 'op@op.op', 'male', '2', NULL, NULL, 'r40tXLqSv9m/peVnAhDM+o7JSqE0qbz7S04PNk3qTi4=', '2016', '', '', '', '', '', 0, 0, '', '', 'opde7b5', '255'),
(77, 'Fhgf', 'Ghhrt@fhhh.com', 'male', '0', NULL, NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '2016', '', '', '', '', '', 0, 0, '', '', 'Fhgfudrx2', '56552'),
(78, 'Cghgf', 'Vhhcf@gj.hj', 'male', '2', NULL, NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '2016', '', '', '', '', '', 0, 0, '', '', 'Cghgfl04oa', '53842'),
(79, 'Fhhff', 'Fghf@gj.fg', 'male', '2', NULL, NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '2016', '', '', '', '', '', 0, 0, '', '', 'Fhhffucr1r', '56985'),
(80, 'ط£ع©ط¢آ¨ط£ع©ط¢آ§ط£â„¢أ¢â‚¬آ ', 'Hfd@dg.hg', 'male', '0', NULL, NULL, 'r40tXLqSv9m/peVnAhDM+o7JSqE0qbz7S04PNk3qTi4=', '2016', '', '', '', '', '', 0, 0, '', '', 'ط£ع©ط¢آ¨ط£ع©ط¢آ§ط£â„¢أ¢â‚¬آ 2d1fe', '01234567899'),
(82, 'Soliman ', 'Soli.abdallah@gmail.com', 'male', '2', NULL, NULL, 'kFJe9rA8OqJxpEyOyQ+POGiqb7oIlnLkeqMAqz5wZlA=', '2016', '', '', '', '', '', 0, 0, '', '', 'Soliman d6hkv', '123456'),
(83, 'Test1', 'Test1@gmail.com ', 'female', '0', NULL, NULL, 'B9UIJx+3GE/mgtZV2ndAQwR7JZ03BudcWCzWwBO+5IE=', '2014', '', '', '', '', '', 0, 0, '', '', 'Test1wwzd9', '123456789'),
(84, 'Test2', 'Test2@gmail.com', 'female', '0', NULL, NULL, 'ymwdWjJO/qEymHGhFZJyMkzggH3wYsE4cu63lPaTB4s=', '2014', '', '', '', '', '', 0, 0, '', '', 'Test29ne43', '987654321'),
(85, 'Test3', 'Test3@gmail.comtest2', 'female', '0', NULL, NULL, '4fuP91R7mpG2raGyHKs9fz53JJzOCgYwoVrsGpP6pn4=', '2016', '', '', '', '', '', 0, 0, '', '', 'Test378ls4', '0123456789'),
(86, 'Test4', 'Test4@gmail.com', 'male', '0', NULL, NULL, 'VBBC/eK3LaLPi7NvdJW29UT17c4g5N/oWenejBKWgY8=', '2016', '', '', '', '', '', 0, 0, '', '', 'Test4ymqv2', '7387584562347'),
(87, 'Samar', 'Samar@gmail.com', 'female', '2', NULL, NULL, 'CR1dy637kT1v27d9+pNTdKpANJGZxp7/aXamZSZJWBA=', '2016', '', '', '', '', '', 0, 0, '', '', 'Samarknswy', '252675721'),
(88, 'Test5', 'Test5@gmail.com ', 'female', '2', NULL, NULL, 'rEqbiBtYxeOa4ZSRiIuwkVh532h7w2Ldbgtv+UJ47ek=', '2016', '', '', '', '', '', 0, 0, '', '', 'Test591bui', '846978779'),
(89, 'Test7', 'Test7@gmail.com', 'male', '2', NULL, NULL, '+ZmkMs4kcY/qCdNOAfj+zxPxwXkQE6s4Tz3mEUqTkgs=', '2016', '', '', '', '', '', 0, 0, '', '', 'Test77ee7t', '603503540364'),
(90, 'Samartest', 'Samartest@gmail.com', 'female', '0', NULL, NULL, 'nGMxe/VCqcKxgMTVFMQSFO0IUevB9JHi22JFtvtl/H8=', '2016', '', '', '', '', '', 0, 0, '', '', 'Samartestknn0s', '8353538383'),
(91, 'Samartest1', 'Samartest1@gmail.com', 'male', '0', NULL, NULL, 'nGMxe/VCqcKxgMTVFMQSFO0IUevB9JHi22JFtvtl/H8=', '2016', '', '', '', '', '', 0, 0, '', '', 'Samartest1w7x0j', '583835353'),
(92, 'Test8 ', 'Test8@gmail.com', 'male', '0', NULL, NULL, 'nGMxe/VCqcKxgMTVFMQSFO0IUevB9JHi22JFtvtl/H8=', '2016', '', '', '', '', '', 0, 0, '', '', 'Test8 t4r63', '6437383865'),
(93, 'samartest3', 'Samartest3@gmail.com', 'female', '0', NULL, NULL, 'd+qzEBf2Z4wohk82rfjcfecllq3Yy00v6S/4gc3IlQk=', '2016', '', '', '', '', '', 0, 0, '', '', 'samartest347efe', '4567756745'),
(94, 'hh', 'hh@gg.nn', 'male', '0', NULL, NULL, 'r40tXLqSv9m/peVnAhDM+o7JSqE0qbz7S04PNk3qTi4=', '2016', '', '', '', '', '', 0, 0, '', '', 'hhvyjy3', '22'),
(95, 'SamarNewtest', 'SamarnewTest@gmail.com', 'male', '0', NULL, NULL, 'rEqbiBtYxeOa4ZSRiIuwkVh532h7w2Ldbgtv+UJ47ek=', '2016', '', '', '', '', '', 0, 0, '', '', 'SamarNewtest1d2ik', '4234244'),
(96, 'SamarTest2', 'SamarTest2@gmail.com', 'male', '0', NULL, NULL, 'nGMxe/VCqcKxgMTVFMQSFO0IUevB9JHi22JFtvtl/H8=', '2016', '', '', '', '', '', 0, 0, '', '', 'SamarTest2yeidz', '4455456633'),
(97, 'mshaban', 'mohammad.shabee@gmail.com', 'm', '4', NULL, 'doc_97_2017-05-0604:01:58.jpeg', 'rEqbiBtYxeOa4ZSRiIuwkVh532h7w2Ldbgtv+UJ47ek=', '', '%D8%AF%D8%AF%D9%85%D8%AD%D9%85%D9%87%D9%87%D8%AF', 'testeng', 'alex', 'Eg', 'testneigh', 1, 1, 'doc_97_2017-05-0603:03:37.jpeg', '', '', ''),
(98, 'docdoctor1', 'docdoctor1@gmaol.com', 'M', '3', NULL, NULL, 'rEqbiBtYxeOa4ZSRiIuwkVh532h7w2Ldbgtv+UJ47ek=', '2014', '', '', '', '', '', 0, 0, 'doc_98_2017-05-0708:42:47.jpg', '', '', ''),
(99, 'Dgj', 'Sfh@fhj.com', 'male', '2', NULL, NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '2015', '', '', '', '', '', 0, 0, '', '', 'Dgjiyumr', '5685'),
(100, 'Hfjfjg', 'Hfhf@jfhf.jg', 'male', '2', NULL, NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '2016', '', '', '', '', '', 0, 0, '', '', 'Hfjfjgmtlc1', '654895'),
(101, 'lkjdf', 'asdf@lkj.com', 'male', '2', NULL, NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '2016', '', '', '', '', '', 0, 0, '', '', 'lkjdf9s5nx', '25557'),
(102, 'Cnnvg', 'Djvf@hkk.tu', 'male', '2', NULL, NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '2016', '', '', '', '', '', 0, 0, '', '', 'Cnnvg0pho9', '66952'),
(103, 'Fhbfdng', 'Gnnvv@fjg.gkn', 'male', '2', NULL, NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '2016', '', '', '', '', '', 0, 0, '', '', 'Fhbfdngmul2r', '25625'),
(104, 'Sfyg', 'Dgd@fgh.vom', 'male', '2', NULL, NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '2016', '', '', '', '', '', 0, 0, '', '', 'Sfyg4dbct', '85255'),
(105, 'mo', 'mo@mo.mo', 'male', '2', NULL, NULL, 'r40tXLqSv9m/peVnAhDM+o7JSqE0qbz7S04PNk3qTi4=', '2016', '', '', '', '', '', 0, 0, '', '', 'mo1qsb7', '554'),
(106, 'Hchf', 'Gfhf@jv.jv', 'male', '2', NULL, NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '2016', '', '', '', '', '', 0, 0, '', '', 'Hchf6yce6', '85245'),
(107, 'Vdhfh', 'Asmaa.kohla@gmail.cd', 'male', '2', NULL, NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '2016', '', '', '', '', '', 0, 0, '', '', 'Vdhfhxrg4p', '56845'),
(108, 'ko', 'pp@pp.cpp1', 'male', '2', NULL, NULL, 'r40tXLqSv9m/peVnAhDM+o7JSqE0qbz7S04PNk3qTi4=', '2016', '', '', '', '', '', 0, 0, '', '', 'ko1h2co', '2342'),
(110, 'ko', 'pp@pp.cpp12', 'male', '0', NULL, NULL, 'r40tXLqSv9m/peVnAhDM+o7JSqE0qbz7S04PNk3qTi4=', '2016', '', '', '', '', '', 0, 0, '', '', 'kowwknf', '2342'),
(113, 'ko', 'pp@pp.cpp12345', 'male', '2', NULL, NULL, 'r40tXLqSv9m/peVnAhDM+o7JSqE0qbz7S04PNk3qTi4=', '2016', '', '', '', '', '', 0, 0, '', '', 'kowd6wk', '2342'),
(114, 'Chhf', 'Tester@test.co', 'male', '2', NULL, NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '2016', '', '', '', '', '', 0, 0, '', '', 'Chhf9vyu0', '530555'),
(115, 'kkk', 'kkk@iii.oo', 'male', '0', NULL, NULL, 'r40tXLqSv9m/peVnAhDM+o7JSqE0qbz7S04PNk3qTi4=', '2016', '', '', '', '', '', 0, 0, '', '', 'kkkrrqzy', '123'),
(118, 'kkk', 'kkk@iii.oo5', 'male', '0', NULL, NULL, 'r40tXLqSv9m/peVnAhDM+o7JSqE0qbz7S04PNk3qTi4=', '2016', '', '', '', '', '', 0, 0, '', '', 'kkk1ftfv', '123'),
(120, 'Cubdv', 'Vhdgg@fhh.gi', 'male', '2', NULL, NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '2016', '', '', '', '', '', 0, 0, '', '', 'Cubdvt0tol', '88588'),
(121, 'kio', 'kio@kio.kk', 'male', '2', NULL, NULL, 'r40tXLqSv9m/peVnAhDM+o7JSqE0qbz7S04PNk3qTi4=', '2016', '', '', '', '', '', 0, 0, '', '', 'kioat0oc', '22'),
(122, 'kio', 'kio@kio.kk32', 'male', '2', NULL, NULL, 'r40tXLqSv9m/peVnAhDM+o7JSqE0qbz7S04PNk3qTi4=', '2016', '', '', '', '', '', 0, 0, '', '', 'kioxors9', '22'),
(123, 'kio', 'kio@kio.kk325', 'male', '2', NULL, NULL, 'r40tXLqSv9m/peVnAhDM+o7JSqE0qbz7S04PNk3qTi4=', '2016', '', '', '', '', '', 0, 0, '', '', 'kiox0ctt', '22'),
(124, 'kio', 'kio@kio.kk3254', 'male', '2', NULL, NULL, 'r40tXLqSv9m/peVnAhDM+o7JSqE0qbz7S04PNk3qTi4=', '2016', '', '', '', '', '', 0, 0, '', '', 'kio9967l', '22'),
(125, 'kk', 'kk@ee.ww', 'male', '2', NULL, NULL, 'r40tXLqSv9m/peVnAhDM+o7JSqE0qbz7S04PNk3qTi4=', '2016', '', '', '', '', '', 0, 0, '', '', 'kkfqfrp', '23'),
(126, 'Chbc', 'Fhgd@fhhn.vh', 'male', '2', NULL, NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '2016', '', '', '', '', '', 0, 0, '', '', 'Chbc12pnz', '56852'),
(127, 'Sdfsdf', 'ASDASDASFDSF@SDF.COM', 'male', '2', NULL, NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '2016', '', '', '', '', '', 0, 0, '', '', 'Sdfsdf9daw2', '23535'),
(128, 'Sdfsdf', 'Sdfsd@sdf.com', 'male', '2', NULL, NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '2016', '', '', '', '', '', 0, 0, '', '', 'Sdfsdf1j9o2', '98654'),
(129, 'TestError', 'TestError@gmail.com', 'male', '0', NULL, NULL, 'rEqbiBtYxeOa4ZSRiIuwkVh532h7w2Ldbgtv+UJ47ek=', '2016', '', '', '', '', '', 0, 0, '', '', 'TestError37b9i', '4543546567'),
(130, 'zzzzz', 'Zzz@gmail.com', 'male', '0', NULL, NULL, 'XnCOFXzvzFGHXS/GZ5kVEZ9PAE2N+oCeqydK87yGuwo=', '2016', '', '', '', '', '', 0, 0, '', '', 'zzzzzy4t4n', '123456'),
(131, 'ddd', 'Ddd@gmail.com', 'male', '0', NULL, NULL, 'XnCOFXzvzFGHXS/GZ5kVEZ9PAE2N+oCeqydK87yGuwo=', '2016', '', '', '', '', '', 0, 0, '', '', 'dddw24sc', '4356566776'),
(132, 'Rrrr', 'Rrr@gmail.com', 'male', '0', NULL, NULL, 'XnCOFXzvzFGHXS/GZ5kVEZ9PAE2N+oCeqydK87yGuwo=', '2016', '', '', '', '', '', 0, 0, '', '', 'Rrrrg52wp', '123455677878'),
(133, 'Ttt', 'Ttt@gmail.com', 'female', '2', NULL, NULL, 'XnCOFXzvzFGHXS/GZ5kVEZ9PAE2N+oCeqydK87yGuwo=', '2016', '', '', '', '', '', 0, 0, '', '', 'Tttl7dgs', '123456788'),
(134, 'Www', 'Www@gmail.com', 'male', '0', NULL, NULL, 'XnCOFXzvzFGHXS/GZ5kVEZ9PAE2N+oCeqydK87yGuwo=', '2016', '', '', '', '', '', 0, 0, '', '', 'Wwwpk0uy', '2354565465'),
(135, 'Iii', 'Iii@gmail.com', 'male', '0', NULL, NULL, 'XnCOFXzvzFGHXS/GZ5kVEZ9PAE2N+oCeqydK87yGuwo=', '2016', '', '', '', '', '', 0, 0, '', '', 'Iiiczt3n', '1234567898'),
(136, 'Ttt', 'Cccc@gmail.com', 'male', '0', NULL, NULL, 'XnCOFXzvzFGHXS/GZ5kVEZ9PAE2N+oCeqydK87yGuwo=', '2016', '', '', '', '', '', 0, 0, '', '', 'Tttp532e', '1234567'),
(137, 'Ooo', 'OOo@gmail.com', 'male', '0', NULL, NULL, 'XnCOFXzvzFGHXS/GZ5kVEZ9PAE2N+oCeqydK87yGuwo=', '2016', '', '', '', '', '', 0, 0, '', '', 'Ooop8fek', '23245455'),
(138, 'Qqq', 'Qqq@gmail.com', 'male', '0', NULL, NULL, 'XnCOFXzvzFGHXS/GZ5kVEZ9PAE2N+oCeqydK87yGuwo=', '2016', '', '', '', '', '', 0, 0, '', '', 'Qqq44twy', '1234567'),
(139, 'Test10', 'Test10@gmail.com', 'male', '0', NULL, NULL, 'XnCOFXzvzFGHXS/GZ5kVEZ9PAE2N+oCeqydK87yGuwo=', '2016', '', '', '', '', '', 0, 0, '', '', 'Test104boey', '344325565'),
(140, 'Test12', 'Test12@gmail.com', 'male', '0', NULL, NULL, 'XnCOFXzvzFGHXS/GZ5kVEZ9PAE2N+oCeqydK87yGuwo=', '2013', '', '', '', '', '', 0, 0, '', '', 'Test12tx3u2', '12344567'),
(141, 'Referall', 'Referall@gmail.com', 'male', '2', NULL, NULL, 'rEqbiBtYxeOa4ZSRiIuwkVh532h7w2Ldbgtv+UJ47ek=', '2016', '', '', '', '', '', 0, 0, '', '', 'Referallftzzq', '861946'),
(142, 'TestR2', 'TestR2@gmail.com', 'male', '0', NULL, NULL, 'rEqbiBtYxeOa4ZSRiIuwkVh532h7w2Ldbgtv+UJ47ek=', '2016', '', '', '', '', '', 0, 0, '', '', 'TestR2r98bw', '980990990'),
(143, 'mohamed', 'm.shaban@gmail.com', 'male', '0', NULL, NULL, '+fGGkTguJBbqV4lsNlqxd294ZZcywzRPmstIS2uLgH4=', '2016', '', '', '', '', '', 0, 0, '', '', 'mohamedr7tdo', '12'),
(144, 'mohamed', 'm.shaban@gmail.com1', 'male', '0', NULL, NULL, 'FylvGt6Yyb6n+zTXcJHwjBawOY/w3QSZxF7rdUJLqhA=', '2016', '', '', '', '', '', 0, 0, '', '', 'mohamed0y10t', '12'),
(145, 'mohamed', 'm.shaban@gmail.com12', 'male', '0', NULL, NULL, 'W9kzudNdF+YLQujrnf53XMEL02a16/ES3qi1GRNQzGU=', '2016', '', '', '', '', '', 0, 0, '', '', 'mohamedahhiq', '12'),
(146, 'Dhjcd', 'Fjgd@fhbf.xom', 'male', '0', NULL, NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '2016', '', '', '', '', '', 0, 0, '', '', 'Dhjcdd9gwf', '854265'),
(147, 'Bcjd', 'Gxhc@gc.hf', 'male', '2', NULL, NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '2016', '', '', '', '', '', 0, 0, '', '', 'Bcjdhwnzg', '05695'),
(148, 'ط£â„¢ط¦â€™ط£â„¢أ¢â‚¬â€چط£ع©ط¢آ§ط£â„¢أ¢â‚¬آ¦', 'Afgc@djv.vhv', 'male', '0', NULL, NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '2016', '', '', '', '', '', 0, 0, '', '', 'ط£â„¢ط¦â€™ط£â„¢أ¢â‚¬â€چط£ع©ط¢آ§ط£â„¢أ¢â‚¬آ¦73utj', '43846'),
(149, 'mohamed.shaban', 'qww@we.com', 'male', '0', NULL, NULL, 'r40tXLqSv9m/peVnAhDM+o7JSqE0qbz7S04PNk3qTi4=', '2016', '', '', '', '', '', 0, 0, '', '', 'mohamed.shaban21j5p', '123'),
(151, 'mohamed.shaban', 'qww@we.com12', 'male', '2', NULL, NULL, '+fGGkTguJBbqV4lsNlqxd294ZZcywzRPmstIS2uLgH4=', '2016', '', '', '', '', '', 0, 0, '', '', 'mohamed.shabanz8wmj', '123'),
(152, 'ط£â„¢أ¢â‚¬آ¦ط£ع©ط¢آ­ط£â„¢أ¢â‚¬آ¦ط£ع©ط¢آ¯ ط£ع©ط¢آ´ط£ع©ط¢آ¹ط£ع©ط¢آ¨ط£ع©ط¢آ§ط£â„¢أ¢â‚¬آ ', 'pp@pp.cpp1236', 'male', '0', NULL, NULL, 'FylvGt6Yyb6n+zTXcJHwjBawOY/w3QSZxF7rdUJLqhA=', '2016', '', '', '', '', '', 0, 0, '', '', 'ط£â„¢أ¢â‚¬آ¦ط£ع©ط¢آ­ط£â„¢أ¢â‚¬آ¦ط£ع©ط¢آ¯ ط£ع©ط¢آ´ط£ع©ط¢آ¹ط£ع©ط¢آ¨ط£ع©ط¢آ§ط£â„¢أ¢â‚¬آ vge5n', '2015'),
(155, 'ط£â„¢أ¢â‚¬آ ط£â„¢أ¢â‚¬آ ط£â„¢أ¢â‚¬آ ط£â„¢أ¢â‚¬آ ', 'pp@pp.cpp987', 'male', '2', NULL, NULL, '+fGGkTguJBbqV4lsNlqxd294ZZcywzRPmstIS2uLgH4=', '2016', '', '', '', '', '', 0, 0, '', '', 'ط£â„¢أ¢â‚¬آ ط£â„¢أ¢â‚¬آ ط£â„¢أ¢â‚¬آ ط£â„¢أ¢â‚¬آ 7kmjw', '9685'),
(156, 'Mmm', 'Mmm@gmail.com', 'male', '0', NULL, NULL, 'rEqbiBtYxeOa4ZSRiIuwkVh532h7w2Ldbgtv+UJ47ek=', '2016', '', '', '', '', '', 0, 0, '', '', 'Mmmb1hn7', '1234567'),
(157, 'Ppp', 'Ppp@gmail.com', 'male', '0', NULL, NULL, 'nGMxe/VCqcKxgMTVFMQSFO0IUevB9JHi22JFtvtl/H8=', '2016', '', '', '', '', '', 0, 0, '', '', 'Pppabw7b', '44234234'),
(158, 'Asmaa', 'Asmaa@fgh.fgh', 'male', '2', NULL, NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '2016', '', '', '', '', '', 0, 0, '', '', 'Asmaafgbwq', '23568'),
(159, 'mohamed sha', 'mm@ww.dfd', 'male', '2', NULL, NULL, '+fGGkTguJBbqV4lsNlqxd294ZZcywzRPmstIS2uLgH4=', '2016', '', '', '', '', '', 0, 0, '', '', 'mohamed shan4b80', '12'),
(160, 'Zeft', 'Zeft@gg.hh', 'male', '2', NULL, NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '2016', '', '', '', '', '', 0, 0, '', '', 'Zeftwvwpf', '866885'),
(161, 'mm', 'oo@kk.ii', 'male', '2', NULL, NULL, '+fGGkTguJBbqV4lsNlqxd294ZZcywzRPmstIS2uLgH4=', '2016', '', '', '', '', '', 0, 0, '', '', 'mmz83gs', '12'),
(162, 'Zeft', 'Zeftt@fgh.jgd', 'male', '2', NULL, NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '2016', '', '', '', '', '', 0, 0, '', '', 'Zeftn9ryt', '568488'),
(163, 'Zeft', 'Zeft@gg.hhGc', 'male', '2', NULL, NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY=', '2016', '', '', '', '', '', 0, 0, '', '', 'Zeftoo5x8', '526585');

-- --------------------------------------------------------

--
-- Table structure for table `doctor_addresses`
--

DROP TABLE IF EXISTS `doctor_addresses`;
CREATE TABLE IF NOT EXISTS `doctor_addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_code` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `latitude` varchar(45) DEFAULT NULL,
  `longitude` varchar(45) DEFAULT NULL,
  `doctor_id` varchar(45) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `doctor_addresses`
--

INSERT INTO `doctor_addresses` (`id`, `country_code`, `city`, `latitude`, `longitude`, `doctor_id`, `address`) VALUES
(1, '2134', 'alex', '31.1256852', '29.7836783', '8', 'testaddresses'),
(2, '2134', 'alex', '31.1256852', '29.7836783', '8', 'testaddresses'),
(3, '2134', 'alex', '31.1256852', '29.7836783', '8', 'testaddresses'),
(4, '2134', 'alex', '31.1256852', '29.7836783', '8', 'testaddresses'),
(5, '2134', 'alex', '31.1256852', '29.7836783', '8', 'testaddresses'),
(6, '2134', 'alex', '31.1256852', '29.7836783', '8', 'testaddresses'),
(7, '2134', 'alex', '31.1256852', '29.7836783', '8', 'testaddresses'),
(8, '2134', '1', '31.1256852', '29.7836783', '8', 'testaddresses'),
(9, '2134', '1', '31.1256852', '29.7836783', '8', 'testaddresses'),
(11, '2134', '1', '31.1256852', '29.7836783', '8', 'testaddresses'),
(15, '2134', 'alex', '31.1256852', '29.7836783', '66', 'testaddresses');

-- --------------------------------------------------------

--
-- Table structure for table `doctor_login`
--

DROP TABLE IF EXISTS `doctor_login`;
CREATE TABLE IF NOT EXISTS `doctor_login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doctor_id` int(11) DEFAULT NULL,
  `api_key` varchar(255) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `device_token` varchar(255) NOT NULL,
  `device_id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `api_key_UNIQUE` (`api_key`)
) ENGINE=InnoDB AUTO_INCREMENT=263 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `doctor_login`
--

INSERT INTO `doctor_login` (`id`, `doctor_id`, `api_key`, `datetime`, `device_token`, `device_id`) VALUES
(27, 7, '8c96a4701c3e4514e808c8c02c60f40d', NULL, '', ''),
(50, 11, '9f2716a46a3f48813adb13a470362cf8', NULL, '', ''),
(51, 11, '96403edfe6bda993d487e76a888af09d', NULL, '', ''),
(52, 11, 'a0b9f02cd4f30b6fccac097422cb0a22', NULL, '', ''),
(53, 11, '0fa120b9288fee630afd0a8cdaa38b29', NULL, '', ''),
(55, 11, 'ea91435448f16633d8da8a41dc161c42', NULL, '', ''),
(56, 11, '1ac2f383b2f2ab30cb518972abc0b23a', NULL, '', ''),
(57, 11, '80efc8f1f780e36e976d22ac15474d00', NULL, '', ''),
(58, 11, '2fc7960f6ca3301ee874bf28cc28b81f', NULL, '', ''),
(59, 11, 'e95cf2ff7679ae4456a6d0acc51fda3b', NULL, '', ''),
(60, 11, '502211b359d1fe960a633cb17b352a8a', NULL, '', ''),
(69, 20, '8ed1cf5708e743b01524c6e091d2a716', NULL, '', ''),
(70, 20, 'd519159b048c9d79fb1ae7ea74a90542', NULL, '', ''),
(71, 20, 'f12816f17c0a2253c703e682518b3a0b', NULL, '', ''),
(74, 21, 'e7552d3fdeefe3fb961d083fdf95e897', NULL, '', ''),
(75, 21, 'c4b5e87234b799458f547d2a479f9fd3', NULL, '', ''),
(83, 24, 'ecb458a37c3344adc83b9d9aeec0c644', NULL, '', ''),
(86, 11, 'cee74a88d8828a8f007ea1041bb3e97e', NULL, '', ''),
(87, 11, 'bf7c543f998763602a598dcc75fa1865', NULL, '', ''),
(89, 11, '7401101ce0faf369ce930fc70d104e01', NULL, '', ''),
(90, 11, '677fd77bb4b85cd738e9068a027abd97', NULL, '', ''),
(92, 27, '19476ffdd923cba041712429106caf60', NULL, '', ''),
(93, 29, 'c1f75ec772bc53875ce8c6f24dad86ae', NULL, '', ''),
(94, 30, 'c7e460c94f39e6858121aaced7db3078', NULL, '', ''),
(95, 32, '2fa22be8b3a1bae5fb5010737afa4a26', NULL, '', ''),
(96, 33, 'ec8c2bff395d8a0f4e812569a53660ed', NULL, '', ''),
(97, 35, '402e0a79f52ba8edd623476461946b55', NULL, '', ''),
(98, 36, '310f4dd7d75944813cc9ccf37bf9aa74', NULL, '', ''),
(99, 37, '26f01438169ffe5a1defd9e6e0ea657d', NULL, '', ''),
(100, 40, '56f3c6949e1e0b75b6228a57ddc726a2', NULL, '', ''),
(101, 41, 'aba6a33a6a6869a274d664924dadb3ae', NULL, '', ''),
(102, 49, '86b11f913d55296bb0403c0d4b2653be', NULL, '', ''),
(103, 50, '95243c36fb88f307a75319ddd3dee642', NULL, '', ''),
(104, 51, '9425742a41b920201f8c3964afdaadd0', NULL, '', ''),
(105, 52, '2c5390e414d431f26a8555f7f87ea245', NULL, '', ''),
(106, 53, '55704166cf1ccb275bf81ff30b7c87ea', NULL, '', ''),
(107, 54, 'e496860f8f739ea981bbb73af55337b0', NULL, '', ''),
(108, 55, 'eff8b069a4a5806e868f8a9e0fb9ca78', NULL, '', ''),
(109, 56, '5a656e8861ace2679cf7189fa6b5c8cf', NULL, '', ''),
(110, 56, 'af3e4a72f41be14a7cac24828c17ee5d', NULL, '', ''),
(111, 57, '6b69afb62a2e767e43e206595a9e4dbf', NULL, '', ''),
(112, 57, '88b8ba88284c407301f8b2a55416905d', NULL, '', ''),
(113, 58, '1c13c31915d4028b7a3ad8122f34692f', NULL, '', ''),
(114, 58, 'ffb374d235be5571a1c3c9e0e89c3fa4', NULL, '', ''),
(115, 59, '0b088b49cc05a0a15124b98b61578e4f', NULL, '', ''),
(116, 60, 'd5f2a0d7ab1154497882873b7e827006', NULL, '', ''),
(117, 61, 'cf4216ec0f8f6f2c5187e3daa979109b', NULL, '', ''),
(118, 62, '833a0c4bd7af4d75b5cd8f4d6b47f5c4', NULL, '', ''),
(119, 64, '7366ab77f0cdf2ee7ffb0c01ec33da2e', NULL, '', ''),
(120, 64, '001d21eb2c5e01912dfc1cc8fcb0c2c4', NULL, '', ''),
(122, 66, 'dbdb8dab64b044ed55c45360f235a442', NULL, '', ''),
(124, 67, '4b5655159ba4237e5d52dc3ab5b2ff4f', NULL, '', ''),
(125, 67, '96f6f974cf9d887cd8be2924464c73f5', NULL, '', ''),
(126, 68, '1b78b1870b94b2f4c5177e2b1638b8f4', NULL, '', ''),
(127, 69, 'a2201fb39c47d69ff6ae7b162fa8fc13', NULL, '', ''),
(128, 70, 'd09df02888b9262d56713d79a2a5ea6e', NULL, '', ''),
(129, 71, '56d79e2ffa95b5876689669b797ef656', NULL, '', ''),
(130, 71, '0575fc8a09f019e59e312df00f84a820', NULL, '', ''),
(131, 71, '90a605447a98288376e2a3740e446fbd', NULL, '', ''),
(132, 71, '1fa809a1ee17e304d36ebc386d5d1835', NULL, '', ''),
(133, 0, 'db7ba106d37a007f4b7cb93ebc83f007', NULL, '', ''),
(134, 71, 'bdb44ec4af26b7fabca52f4322659a8e', NULL, '', ''),
(135, 71, 'd543b96457e433c7e1756835bfd43be7', NULL, '', ''),
(136, 72, '9d8e2b1e2598ff5e254a5c98cb0bb458', NULL, '', ''),
(137, 74, '46782f7e8dc2a2c91b8839fdc0d9b4f1', NULL, '', ''),
(138, 75, 'f912de4d257180bf7fd03b88b19ba4f0', NULL, '', ''),
(139, 76, 'bdbf8f52d23399b619d8781b4de08084', NULL, '', ''),
(140, 76, '1dfac892548705833c8b0a820ce550d0', NULL, '', ''),
(141, 77, '3a5278034a7d1d1bf8d731209a04d21b', NULL, '', ''),
(142, 78, '7517de60357fb39f453fd42a014ea21d', NULL, '', ''),
(143, 78, 'ae991bc7d0bbc7c31e56acd7e18df891', NULL, '', ''),
(144, 79, '5b70495a01de235b4533cb9351e68744', NULL, '', ''),
(145, 79, '2ab8120454af70c5cb1f4e208b06ddef', NULL, '', ''),
(146, 80, '1e4fbf60575386905183dc574ae9e278', NULL, '', ''),
(147, 82, '61a4ef8d601b38aab7ffbdd566c1d346', NULL, '', ''),
(148, 82, '61b76a649d3fb82ef395bf41a81893e6', NULL, '', ''),
(149, 83, 'd197210c3b7c71e4029c6f3bf0e2a7d7', NULL, '', ''),
(150, 84, 'c189433bd921e1869df01de5b6779002', NULL, '', ''),
(151, 85, '125efdd14a2cda20c7fbf4d109f6a0ab', NULL, '', ''),
(152, 86, '8675bcf75418877e5f10eb3c48fe5a77', NULL, '', ''),
(153, 87, '956b18badd8bc26d0540b874800c2383', NULL, '', ''),
(154, 87, 'a8a606b685e465f955df8cea5a8945af', NULL, '', ''),
(155, 88, 'edbc94c9883c50b3389c11c921b5e77e', NULL, '', ''),
(156, 88, '3a829fbc3cf6af06d5fae3efc8e9f050', NULL, '', ''),
(157, 89, '4300952219a93704cb72ae833ae9eb52', NULL, '', ''),
(158, 89, 'af5c86fdd6f8d9f3ae58819635cc3ea4', NULL, '', ''),
(159, 90, '85081e39419ce054dce23c4af79eeebe', NULL, '', ''),
(160, 91, '245bceb814419a01561a4a79b526f372', NULL, '', ''),
(161, 92, '0f551697ad52af98fd6b6b6b77aa0a3f', NULL, '', ''),
(162, 93, 'ceaa1e645dbe3458283bca77454af336', NULL, '', ''),
(163, 94, 'e3b36339f5b94ae63c33c0b2594f2ad8', NULL, '', ''),
(164, 95, 'cd6d638a2f710de04f371d7e43845c5d', NULL, '', ''),
(165, 96, '393532aa12c8cc696374565446eb84c7', NULL, '', ''),
(166, 97, '794d551af8133c687f137fa6f4e7c34b', NULL, '', ''),
(168, 99, '1fa1bafc525e5dfa29c978885292f7f8', NULL, '', ''),
(169, 99, '331ed9799dedf5e7e99b5ddccd994036', NULL, '', ''),
(170, 100, '6ff25dd3125d5ce43b63e5792c9fcb07', NULL, '', ''),
(171, 100, '30148f16ca0dcef2e74872bb29a46183', NULL, '', ''),
(172, 101, '5489a8012d7f6f564465268c559339c6', NULL, '', ''),
(173, 101, 'b3a2c2eacde44a3aa64b1e7cf1dad866', NULL, '', ''),
(174, 102, '00eb65dd1a000d738e4c356f45af19ff', NULL, '', ''),
(175, 102, '02e9757390ec09975d80e5fbd9c8b584', NULL, '', ''),
(176, 103, '6e8a6c590635d7f19059acecb914f547', NULL, '', ''),
(177, 103, '5860004288c09f2f1db5f112f7f313cd', NULL, '', ''),
(178, 104, '30891612eeda0a55548c7b94ad19cec7', NULL, '', ''),
(179, 104, 'ff480d549bbd1dfc77e612af9f150230', NULL, '', ''),
(180, 105, 'e20217150c0867c3b0da6e1c8501b6a0', NULL, '', ''),
(181, 105, 'e2ccca75738433a8252a2200eb127b68', NULL, '', ''),
(182, 106, '9a9297b6a252d80e36f1b93442423a3b', NULL, '', ''),
(183, 106, 'e9cfafc5167855692c2b23ae40187fec', NULL, '', ''),
(184, 107, '6ecc6ec6fc2082e2e692ad586710538d', NULL, '', ''),
(185, 107, 'e90bd23e92eb4befe48070f39a3b62cd', NULL, '', ''),
(186, 108, 'f060df6a68d33628642de373025509b3', NULL, '', ''),
(187, 108, '26956869fc2504e98700955f9a823d14', NULL, '', ''),
(188, 108, '2090872486981ed2d433b2f8d72e1f33', NULL, '', ''),
(189, 110, '9a3a0559382acb010bcb151b39691571', NULL, '', ''),
(190, 113, 'a699e2c1d989c51e75542b8adca70143', NULL, '', ''),
(191, 113, '824446eaf5dafe87be677fb021929cd7', NULL, '', ''),
(192, 114, 'e80c47154a92c54cac9c7b3a5c269cff', NULL, '', ''),
(193, 114, '76cccdfda30ee2aea8c651c1e7030943', NULL, '', ''),
(194, 115, '389261af1fd8c14bc23b7e11ce7d7d00', NULL, '', ''),
(195, 118, '5b8ce28e66b0fb72cffd6c0985f2e3e5', NULL, '', ''),
(196, 120, 'f76d094ea9fa9dd1c1fa3933c1a1c029', NULL, '', ''),
(197, 120, '2106fec9a4f597f9a64996859dc7470f', NULL, '', ''),
(198, 121, '492c775ee279001056486be561c12407', NULL, '', ''),
(199, 121, '83859640220024348cb6980ac1c0e0b6', NULL, '', ''),
(200, 121, 'ff69ded1bb460847cda7bfd55828f1b0', NULL, '', ''),
(201, 121, '673ca8f9dff88a0c13861e3c194b1a7a', NULL, '', ''),
(202, 121, '48ea59139ea6e991fa6ce741b03b1d8a', NULL, '', ''),
(203, 121, 'd02493bc6c785f0b009cf0ef3f95882a', NULL, '', ''),
(204, 121, 'ff424ab4f9ff873c7036725647eb3681', NULL, '', ''),
(205, 122, 'f624c83d962cf466f7f0f87e13c38363', NULL, '', ''),
(206, 122, 'e670026f68f07e9ce9d8d407535c4fb9', NULL, '', ''),
(207, 123, 'bc8b453ddb37c1eff0f7e2c6bced2e2e', NULL, '', ''),
(208, 123, '4ebad0b735eb8ccb11e830c17c40920d', NULL, '', ''),
(209, 124, '3ae17e3d9b8c7a651cf17900193b755d', NULL, '', ''),
(210, 124, '203ff2742723dc47c93d964488ec6ac1', NULL, '', ''),
(211, 125, '2b085b5a075ab497a3c0e80d5f2895dd', NULL, '', ''),
(212, 125, 'ad9f3ada13f17a472a3a59c20ee53d7e', NULL, '', ''),
(213, 126, '51719d69639381a275975ce6576dc458', NULL, '', ''),
(214, 126, '1bea1d6553a2448c46d49debb3336179', NULL, '', ''),
(215, 127, 'f4d689704a3b5d0348afa6cdcad1d1f3', NULL, '', ''),
(216, 127, '32a6f2b830ffddeab1fed609c3560346', NULL, '', ''),
(217, 128, 'caec98c705a156e8ab60356ccd2e3d93', NULL, '', ''),
(218, 128, '27a7453cb5fd7b20de4270f023d84a4e', NULL, '', ''),
(219, 129, '03b70bfe4fd036f743298bc05cb8b72b', NULL, '', ''),
(220, 130, 'ad5d3cfc63683818992892b6f35b7181', NULL, '', ''),
(221, 131, '58be12725f6df82d53dd7bc2a4e998c5', NULL, '', ''),
(222, 132, '2369fbdb4077a5a9f1e23820f9b9814d', NULL, '', ''),
(223, 133, '08d3517187b577d523999dd6d6a7a93d', NULL, '', ''),
(224, 133, '93b58d027b3a29757db962708ee7698b', NULL, '', ''),
(225, 134, '58f41ea9f76c8153fd9587a64d8b3e89', NULL, '', ''),
(226, 135, '79ba2908560eaf80430a4b87ee5ae64a', NULL, '', ''),
(227, 136, '57ebcde4e3e567af556485cab41b3062', NULL, '', ''),
(228, 137, '5386e51aa9ffe5819d6edda80e93199d', NULL, '', ''),
(229, 138, '5a86598fe9abaf83d2c5aba892391879', NULL, '', ''),
(230, 139, '390446c8997aaf61ee45279c2f767507', NULL, '', ''),
(231, 140, '18064cda77d0add8c65222cdfd26b8bd', NULL, '', ''),
(232, 141, '7be8bd971e5ff907bb36a7d61a6c09ac', NULL, '', ''),
(233, 141, '012c29be332c7dcd80c7518545ea224a', NULL, '', ''),
(234, 142, '151a02f06b96043604c57f701018a7e6', NULL, '', ''),
(235, 143, '37027281bc96d8ae5b63e07deeea7179', NULL, '', ''),
(236, 144, '908c6aa95ca0a4d7fe9ecd0b65560ad8', NULL, '', ''),
(237, 145, 'e3a0215c296ec16ea93583f113e954cf', NULL, '', ''),
(238, 146, 'ff38559df3f68936f44581d7e94b53d4', NULL, '', ''),
(239, 147, '158fbbf1d3fb62a0ff796608beda0a50', NULL, '', ''),
(240, 147, '79bd7d6928af040a14fc76ef436a4e7b', NULL, '', ''),
(241, 148, '90c0ff0698954bccf4913e9ebdca6bdf', NULL, '', ''),
(242, 149, '09e60fdf7f645030a20761713092f892', NULL, '', ''),
(243, 151, 'd38cc4e5346b70e4d3d0d25bcaef25e1', NULL, '', ''),
(244, 151, 'e26754afa71a708e8dc9cdc29318f68e', NULL, '', ''),
(245, 152, '9d5cf00d7b70aa933dc73d2e82d04a3e', NULL, '', ''),
(246, 155, 'be3b8830ebfa58494cfd650aef2dedcb', NULL, '', ''),
(247, 155, 'c191caf23c8806b26534cc74356d56ba', NULL, 'sdsf', '123456'),
(248, 156, 'fb6e6fce86f01dfd7b074523a12aca76', NULL, '', ''),
(249, 157, '39ec90cd303d7639c1ccacfda800ebf3', NULL, '', ''),
(250, 158, 'd4aff58d7f689df077102615b672449c', NULL, '', ''),
(251, 158, '2504d1be9359759075c3b726dce2dc04', NULL, '', ''),
(252, 159, 'e402ebc2bb5d8b58842f41c1c9b83755', NULL, '', ''),
(253, 159, '478b17b4d33286e183307157f2a9d926', NULL, '', ''),
(254, 160, '6dc5837affd7cb7593decc36ccf82d77', NULL, '', ''),
(255, 160, '188a2e67d128869f2fc7dac2aabdb462', NULL, '', ''),
(256, 161, '6cd49cefc01e9b132682c91cc614f44e', NULL, '', ''),
(257, 161, '11ac9c321f3acc5c5e88b2133a20c99e', NULL, '', ''),
(258, 162, '133f24a3ccdb854b12955fb80e41f163', NULL, '', ''),
(259, 162, '6ecf6845c1460fe04c35d1d9497a7e2f', NULL, '', ''),
(260, 163, '55e6b3666f12287708fc55f8a9897068', NULL, '', ''),
(261, 163, 'af7a48fe9895ddccbf75fa1e29d9002c', NULL, '', ''),
(262, 71, '261135cef609c0edaee0a9aa0274d6af', NULL, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `doctor_phone`
--

DROP TABLE IF EXISTS `doctor_phone`;
CREATE TABLE IF NOT EXISTS `doctor_phone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_code` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `default` int(11) DEFAULT NULL,
  `doctor_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `doctor_phone`
--

INSERT INTO `doctor_phone` (`id`, `country_code`, `phone`, `default`, `doctor_id`) VALUES
(31, '20', '4877777777', NULL, 20),
(32, '20', '0126666666', NULL, 21),
(33, '20', '01226427786', NULL, 23),
(34, '20', '07538458', NULL, 24),
(35, '20', '085246', NULL, 11),
(36, '2', '01285819291', NULL, 56),
(37, '2', '01285819291', NULL, 57),
(38, '2', '01285819291', NULL, 58),
(39, '2', '01285819291', NULL, 64),
(40, '2', '01285819291', NULL, 66),
(41, '2', '01001420808', NULL, 67),
(42, '2', '01285819291', NULL, 76),
(43, '2', '01226427786', NULL, 78),
(44, '2', '01226427786', NULL, 79),
(45, '2', '01007893000', NULL, 82),
(46, '2', '01093103001', NULL, 87),
(47, '2', '01093103001', NULL, 88),
(48, '2', '01093103001', NULL, 89),
(49, '2', '01226427786', NULL, 99),
(50, '2', '01226427786', NULL, 100),
(51, '2', '01226427786', NULL, 101),
(52, '2', '01226427786', NULL, 102),
(53, '2', '01226427786', NULL, 103),
(54, '2', '01226427786', NULL, 104),
(55, '2', '01100222908', NULL, 105),
(56, '2', '01226427786', NULL, 106),
(57, '2', '01226427786', NULL, 107),
(58, '2', '01100222908', NULL, 108),
(59, '', '01100222908', NULL, 108),
(60, '', '01100222908', NULL, 0),
(61, '2', '01100222908', NULL, 113),
(62, '2', '01226427786', NULL, 114),
(63, '2', '01226427786', NULL, 120),
(64, '', '01100222908', NULL, 0),
(65, '2', '01100222908', NULL, 121),
(66, '', '01100222908', NULL, 0),
(67, '', '01100222908', NULL, 0),
(68, '', '01100222908', NULL, 0),
(69, '', '01100222908', NULL, 0),
(70, '', '01100222908', NULL, 0),
(71, '', '01100222908', NULL, 121),
(72, '', '01100222908', NULL, 121),
(73, '', '01100222908', NULL, 121),
(74, '', '01100222908', NULL, 121),
(75, '', '01100222908', NULL, 121),
(76, '2', '01100222908', NULL, 122),
(77, '2', '01100222908', NULL, 123),
(78, '2', '01100222908', NULL, 124),
(79, '2', '01100222908', NULL, 125),
(80, '2', '01226427786', NULL, 126),
(81, '2', '01226427786', NULL, 127),
(82, '2', '01226427786', NULL, 128),
(83, '2', '01093103001', NULL, 133),
(84, '2', '01093103001', NULL, 141),
(85, '2', '01226427786', NULL, 147),
(86, '2', '01285819291', NULL, 151),
(87, '2', '01285819291', NULL, 155),
(88, '2', '01226427786', NULL, 158),
(89, '2', '01285819291', NULL, 159),
(90, '2', '01226427786', NULL, 160),
(91, '2', '01285819291', NULL, 161),
(92, '2', '01226427786', NULL, 162),
(93, '2', '01226427786', NULL, 163);

-- --------------------------------------------------------

--
-- Table structure for table `doctor_phone_temp`
--

DROP TABLE IF EXISTS `doctor_phone_temp`;
CREATE TABLE IF NOT EXISTS `doctor_phone_temp` (
  `id` int(11) NOT NULL,
  `country_code` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `default` int(11) DEFAULT NULL,
  `doctor_id` int(11) DEFAULT NULL,
  `activation_code` varchar(45) DEFAULT NULL,
  `exp_time` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `doctor_phone_temp`
--

INSERT INTO `doctor_phone_temp` (`id`, `country_code`, `phone`, `default`, `doctor_id`, `activation_code`, `exp_time`) VALUES
(0, '2', '0128581929', NULL, 68, '93738', '2017-04-29 11:57'),
(0, '2', '01285819291', NULL, 69, '02980', '2017-04-29 12:02'),
(0, '2', '01285819291', NULL, 70, '86218', '2017-04-29 12:09'),
(0, '2', '01285819291', NULL, 71, '13920', '2017-04-29 13:20'),
(0, '2', '01285819291', NULL, 72, '27348', '2017-04-30 14:23'),
(0, '2', '01285819291', NULL, 74, '47917', '2017-04-30 14:25'),
(0, '2', '01285819291', NULL, 75, '66056', '2017-04-30 14:27'),
(0, '2', '84285685685', NULL, 77, '73328', '2017-04-30 17:20'),
(0, '2', '12345678999', NULL, 80, '74161', '2017-04-30 20:46'),
(0, '2', '01123456789', NULL, 83, '16576', '2017-05-02 06:15'),
(0, '2', '01223456789', NULL, 84, '19044', '2017-05-02 06:30'),
(0, '2', '00123456779', NULL, 85, '96336', '2017-05-02 08:04'),
(0, '2', '98234908348', NULL, 86, '17476', '2017-05-02 11:28'),
(0, '2', '01093103001', NULL, 90, '91894', '2017-05-02 12:10'),
(0, '2', '01093103001', NULL, 91, '84563', '2017-05-02 12:11'),
(0, '2', '01093103001', NULL, 92, '41302', '2017-05-02 12:19'),
(0, '2', '01093103001', NULL, 93, '43219', '2017-05-02 13:52'),
(0, '2', '01285819291', NULL, 94, '55359', '2017-05-02 14:00'),
(0, '2', '01093103001', NULL, 95, '83775', '2017-05-02 14:10'),
(0, '2', '01093103001', NULL, 96, '15014', '2017-05-02 14:29'),
(0, '2', '01100222908', NULL, 110, '20250', '2017-05-07 13:46'),
(0, '2', '01100222908', NULL, 115, '45215', '2017-05-07 14:17'),
(0, '2', '01100222908', NULL, 118, '77814', '2017-05-07 14:21'),
(0, '2', '12345678987', NULL, 129, '79924', '2017-05-08 11:55'),
(0, '2', '12345678987', NULL, 130, '28897', '2017-05-08 12:00'),
(0, '2', '98765432123', NULL, 131, '50241', '2017-05-08 12:01'),
(0, '2', '12345678765', NULL, 132, '73042', '2017-05-08 12:09'),
(0, '2', '87654329123', NULL, 134, '96357', '2017-05-08 14:01'),
(0, '2', '12345677823', NULL, 135, '74359', '2017-05-08 16:12'),
(0, '2', '12345676543', NULL, 136, '00025', '2017-05-08 16:36'),
(0, '2', '12345678987', NULL, 137, '25085', '2017-05-08 16:44'),
(0, '2', '12365478954', NULL, 138, '51736', '2017-05-08 16:59'),
(0, '2', '01235468792', NULL, 139, '03042', '2017-05-09 05:51'),
(0, '2', '12365478964', NULL, 140, '74426', '2017-05-09 05:59'),
(0, '2', '11121212524', NULL, 142, '26841', '2017-05-09 11:12'),
(0, '2', '01285819291', NULL, 143, '48162', '2017-05-09 13:24'),
(0, '2', '01285819291', NULL, 144, '47360', '2017-05-09 13:25'),
(0, '2', '01285819291', NULL, 145, '41052', '2017-05-09 13:26'),
(0, '2', '01226427786', NULL, 146, '23574', '2017-05-09 13:46'),
(0, '2', '01226427786', NULL, 148, '23945', '2017-05-09 14:37'),
(0, '2', '01285819291', NULL, 149, '55188', '2017-05-10 12:12'),
(0, '2', '01285819291', NULL, 152, '90251', '2017-05-10 15:16'),
(0, '2', '03216547895', NULL, 156, '99299', '2017-05-11 07:31'),
(0, '2', '03216549874', NULL, 157, '49325', '2017-05-11 08:22');

-- --------------------------------------------------------

--
-- Table structure for table `doctor_referred`
--

DROP TABLE IF EXISTS `doctor_referred`;
CREATE TABLE IF NOT EXISTS `doctor_referred` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `referring_doctor` int(11) NOT NULL,
  `referred_doctor` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `referred_doctor` (`referred_doctor`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `doctor_referred`
--

INSERT INTO `doctor_referred` (`id`, `referring_doctor`, `referred_doctor`, `date`) VALUES
(1, 11, 143, '0000-00-00 00:00:00'),
(2, 143, 144, '0000-00-00 00:00:00'),
(3, 143, 145, '0000-00-00 00:00:00'),
(4, 11, 146, '0000-00-00 00:00:00'),
(5, 11, 147, '2017-05-09 13:51:07'),
(7, 11, 148, '2017-05-09 14:37:33'),
(8, 11, 149, '2017-05-10 12:12:06'),
(9, 11, 151, '2017-05-10 12:14:08'),
(10, 11, 152, '2017-05-10 15:16:44'),
(11, 11, 155, '2017-05-10 15:22:03'),
(12, 11, 156, '2017-05-11 07:31:01'),
(13, 11, 157, '2017-05-11 08:22:39'),
(14, 11, 158, '2017-05-11 14:17:37'),
(15, 11, 159, '2017-05-11 14:26:22'),
(16, 11, 160, '2017-05-11 15:11:33'),
(17, 11, 161, '2017-05-11 15:17:45'),
(18, 11, 162, '2017-05-11 15:47:44'),
(19, 11, 163, '2017-05-11 15:55:38');

-- --------------------------------------------------------

--
-- Table structure for table `doctor_services`
--

DROP TABLE IF EXISTS `doctor_services`;
CREATE TABLE IF NOT EXISTS `doctor_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doctor_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `service_state` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctor_services`
--

INSERT INTO `doctor_services` (`id`, `doctor_id`, `service_id`, `service_state`) VALUES
(1, 71, 1, '2');

-- --------------------------------------------------------

--
-- Table structure for table `favorite_doctor`
--

DROP TABLE IF EXISTS `favorite_doctor`;
CREATE TABLE IF NOT EXISTS `favorite_doctor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `temp` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `favorite_doctor`
--

INSERT INTO `favorite_doctor` (`id`, `patient_id`, `doctor_id`, `temp`) VALUES
(6, 16, 82, ''),
(7, 16, 81, ''),
(11, 120, 56, ''),
(12, 120, 66, ''),
(13, 120, 87, '');

-- --------------------------------------------------------

--
-- Table structure for table `levels`
--

DROP TABLE IF EXISTS `levels`;
CREATE TABLE IF NOT EXISTS `levels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title_en` varchar(45) DEFAULT NULL,
  `title_ar` varchar(45) DEFAULT NULL,
  `offline_question` varchar(255) NOT NULL,
  `chat` varchar(255) NOT NULL,
  `call` varchar(255) NOT NULL,
  `visit` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `levels`
--

INSERT INTO `levels` (`id`, `title_en`, `title_ar`, `offline_question`, `chat`, `call`, `visit`) VALUES
(1, 'GP Doctor', NULL, '5', '10', '15', '20'),
(2, 'Specialist', NULL, '5', '10', '15', '20'),
(3, 'Consultant', NULL, '5', '10', '15', '20'),
(4, 'Professor', NULL, '5', '10', '15', '20');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(72, '2014_10_12_000000_create_users_table', 1),
(73, '2014_10_12_100000_create_password_resets_table', 1),
(74, '2017_05_26_093125_create_patient_requests_table', 1),
(75, '2017_05_30_070329_create_patient_uploads_table', 1),
(76, '2017_05_30_121145_create_radiology_type_categories_table', 1),
(77, '2017_05_30_121235_create_radiology_types_table', 1),
(78, '2017_05_31_054338_create_centers_table', 1),
(79, '2017_05_31_054525_create_center_phones_table', 1),
(80, '2017_05_31_054923_create_center_responses_table', 1),
(81, '2017_05_31_055005_create_response_radiology_items_table', 1),
(82, '2017_05_31_055019_create_response_centers_table', 1),
(83, '2017_05_31_115249_create_radiology_questions_table', 1),
(84, '2017_05_31_121210_create_request_answers_table', 1),
(98, '2017_06_03_081723_create_user_types_table', 2),
(99, '2017_06_03_082026_Add_UserTypeId_To_Users_Table', 2),
(103, '2017_06_03_125553_create_url_groups_table', 3),
(104, '2017_06_03_125730_create_urls_table', 3),
(105, '2017_06_03_131641_create_user_type_urls_table', 3),
(106, '2017_06_06_015834_add_request_status_to_patient_requests', 4),
(107, '2017_06_06_021143_alter_request_status_to_patient_requests', 5),
(108, '2017_06_06_022033_add_request_status_nullable_to_patient_requests', 6),
(109, '2017_06_10_130244_create_View_v_patient', 7),
(110, '2017_06_10_130316_create_View_v_patientrequestuploads', 7),
(111, '2017_06_10_130347_create_View_v_patientanswers', 7),
(112, '2017_06_10_131700_create_View_v_patientrequests', 7),
(113, '2017_06_10_141431_add_column_comment_to_patient_requests', 8);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `patient`
--

DROP TABLE IF EXISTS `patient`;
CREATE TABLE IF NOT EXISTS `patient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `gender` varchar(45) DEFAULT NULL,
  `user_state` varchar(45) DEFAULT NULL,
  `current_credit` varchar(45) DEFAULT '0',
  `profile_img` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=123 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `patient`
--

INSERT INTO `patient` (`id`, `name`, `email`, `gender`, `user_state`, `current_credit`, `profile_img`, `password`) VALUES
(1, 'z', 'U54jgm+A4PI4nbyXIjXgi5m5DgGj3H01CdLFBtL9SsE=', 'M', '0', '0', NULL, 'r40tXLqSv9m/peVnAhDM+o7JSqE0qbz7S04PNk3qTi4='),
(2, 'Asmaa Kohla', 'asmaa.kohla@gmail.com', 'F', '2', '0', 'pat_2_2017-04-1910:48:49.jpeg', 'uWpk7AASn+mrBYTYt2rcMrPOumvVCWi0rvJYsKOP/vk='),
(3, 'Mohamed Metwally', 'metwally@gmail.com', 'M', '0', '0', NULL, 'sdA7D8/j8M5k+jQ9xMEYIO41JZf0oO2L+9Z6pWqoNRs='),
(4, 'z', 'mohammad.shabaan@gmail.com12', 'M', '0', '0', NULL, 'XnCOFXzvzFGHXS/GZ5kVEZ9PAE2N+oCeqydK87yGuwo='),
(5, 'Samar', 'samaribrahimdoc@gmail.com', 'M', '0', '0', NULL, 'CR1dy637kT1v27d9+pNTdKpANJGZxp7/aXamZSZJWBA='),
(8, 'samar Ibrahim ', 'samaribrahim@test.com', 'F', '2', '0', NULL, 'XnCOFXzvzFGHXS/GZ5kVEZ9PAE2N+oCeqydK87yGuwo='),
(9, 'User Name', 'user@gmail.com', 'M', '0', '0', NULL, 'uWpk7AASn+mrBYTYt2rcMrPOumvVCWi0rvJYsKOP/vk='),
(10, 'aaa', 'aaa@aaa.com', 'M', '0', '0', NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY='),
(11, 'test1', 'change2test1@gmail.com', 'M', '0', '0', NULL, 'f+w13OzxbAH4TGL15ol3l03UWeBXPRXdW/eOgucErGY='),
(12, 'test2', 'test2@gmail.com', 'M', '0', '0', NULL, 'ymwdWjJO/qEymHGhFZJyMkzggH3wYsE4cu63lPaTB4s='),
(16, 'z', 'z@z.com', 'M', '0', '0', NULL, 'r40tXLqSv9m/peVnAhDM+o7JSqE0qbz7S04PNk3qTi4='),
(18, 'test3', 'test3@gmail.com', 'M', '0', '0', NULL, '4fuP91R7mpG2raGyHKs9fz53JJzOCgYwoVrsGpP6pn4='),
(19, 'test5', 'test5@gmail.com', 'M', '0', '0', NULL, 'B7y6L7590m/N/2UlBNABvOAYWpRddhqEGGXSYFs7Sxo='),
(20, 'test6', 'test6@gmail.com', 'M', '0', '0', NULL, 'Bl7mNuEvLd/fADFBhYADNRpnpH6Y9tcY7ebHoppiW7U='),
(21, 'test7', 'test7@gmail.com', 'M', '0', '0', NULL, '+ZmkMs4kcY/qCdNOAfj+zxPxwXkQE6s4Tz3mEUqTkgs='),
(23, 'test8', 'test8@gmail.com', 'M', '0', '0', NULL, 'PIjv6Feq0a1DepGteVXDETnu1oo6XYB6JxiRzrDvK/0='),
(24, 'test user', 'test@test.com', 'M', '0', '0', NULL, 'JRBw5O+bz/d5hA28rIIjG0EVQnsxmuXCTTitlh7nKKk='),
(26, 'aaa aaa', 'aaaa@aaa.com', 'M', '0', '0', NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY='),
(27, 'bbb', 'bbb@bbb.com', 'M', '0', '0', NULL, 's/d24psAFabWoO1zihUfFnPPhHn2mf6w5vLx73Vpcz8='),
(28, 'ccc ccc', 'ccc@ccc.com', 'M', '0', '0', NULL, 'zCo5TJJWXzt7wQOKYfC317t8T+Kp9GaTo0DiXo8VJPY='),
(29, 'ddd ddd', 'ddd@ddd.com', 'M', '0', '0', NULL, 'gRWf2J/HLq6zDat/j93MrhQzP/0Cxqx/nfLQsmkUnk4='),
(30, 'xxx', 'xxx@xxx.com', 'M', '0', '0', NULL, 'qi9HEDWoni31hpZgCMzdj+tNC6TvusgJVXMhXYVWjYI='),
(31, 'bbbb', 'bbbbb@bbb.com', 'M', '0', '0', NULL, 's/d24psAFabWoO1zihUfFnPPhHn2mf6w5vLx73Vpcz8='),
(32, 'dfff', 'ddddd@ddd.com', 'M', '0', '0', NULL, 'gRWf2J/HLq6zDat/j93MrhQzP/0Cxqx/nfLQsmkUnk4='),
(33, 'gggg', 'ggg@ggg.com', 'M', '0', '0', NULL, 'JYuVpm6Ct4uFVyHBOh4s2rcjJ46BT5WgApRo8nl5ohk='),
(34, 'nnn nnn', 'nnn@nnn.com', 'M', '0', '0', NULL, 'IMRu2DRz9hhTb0W3uOn4iyIKvpUDUrxBIkw8YB4iKPs='),
(35, 'fgf', 'fb@cb.com', 'M', '0', '0', NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY='),
(36, 'fsbchd', 'gxf@hcyf.com', 'M', '0', '0', NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY='),
(37, 'gdhg', 'gxmv_@jcn.com', 'M', '0', '0', NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY='),
(38, 'dgjk', 'sgj@vbn.com', 'M', '0', '0', NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY='),
(39, 'asfhj', 'asmaa@gmail.com', 'M', '0', '0', NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY='),
(44, 'z', 'z12@z.com', 'M', '0', '0', NULL, 'r40tXLqSv9m/peVnAhDM+o7JSqE0qbz7S04PNk3qTi4='),
(45, 'z', 'z123@z.com', 'M', '0', '0', NULL, 'r40tXLqSv9m/peVnAhDM+o7JSqE0qbz7S04PNk3qTi4='),
(47, 'ay kalam', 'asdf@bgds.com', 'M', '0', '0', NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY='),
(49, 'ay kalam', 'asdf@bgchds.com', 'M', '0', '0', NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY='),
(50, 'mm', 'mm@mm.com', 'M', '0', '0', NULL, 'XnCOFXzvzFGHXS/GZ5kVEZ9PAE2N+oCeqydK87yGuwo='),
(52, 'sgjlb', 'dgj@vjj.com', 'M', '0', '0', NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY='),
(53, 'mm', 'mm@mm.comq', 'M', '0', '0', NULL, 'XnCOFXzvzFGHXS/GZ5kVEZ9PAE2N+oCeqydK87yGuwo='),
(55, 'mm', 'mm@mm.comqp', 'M', '0', '0', NULL, 'XnCOFXzvzFGHXS/GZ5kVEZ9PAE2N+oCeqydK87yGuwo='),
(57, 'mm', 'mm@mm.comqpe', 'M', '0', '0', NULL, 'XnCOFXzvzFGHXS/GZ5kVEZ9PAE2N+oCeqydK87yGuwo='),
(58, 'dggf', 'asmaaa@gmail.com', 'M', '0', '0', NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY='),
(61, 'hhh', 'hh@hh.com', 'M', '0', '0', NULL, 'V2Bllcqo97XwtyIvsYRVIqzWIzF/5uV4z4u97lxXIIo='),
(72, '2535 538535', 'mondona@yahoo.com', 'M', '0', '0', NULL, 'sdA7D8/j8M5k+jQ9xMEYIO41JZf0oO2L+9Z6pWqoNRs='),
(73, 'z', 'z32@z.com', 'M', '0', '0', NULL, 'r40tXLqSv9m/peVnAhDM+o7JSqE0qbz7S04PNk3qTi4='),
(74, 'Soly', 'soli.abdallah@gmail.com', 'M', '0', '0', 'pat_74_2017-03-2810:19:51.jpg', 'ryLue5pTtekh4TLEBjLsTn/ZnaZ1Fcj3Z/IF3wXXXoY='),
(75, 'xyz', 'xyz@xyz.com', 'M', '0', '0', NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY='),
(76, 'hgfs chjk', 'asd@asd.com', 'M', '0', '0', NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY='),
(77, 'z', 'zx@z.com', 'M', '0', '0', NULL, 'rEqbiBtYxeOa4ZSRiIuwkVh532h7w2Ldbgtv+UJ47ek='),
(78, 'lkjhg', 'lkj@lkj.com', 'M', '0', '0', NULL, 's/d24psAFabWoO1zihUfFnPPhHn2mf6w5vLx73Vpcz8='),
(80, 'name', 'name@gmail.com', 'M', '0', '0', NULL, 'ot9EOaSyPowvQ5sC2s328GaOAFmkDYIqm+9pp6qOPLI='),
(82, 'test', 'testuser@test.com', 'M', '0', '0', NULL, 'CCpTu71klQqH8BTL7MUeQPabBRZuSRfmmcXtW0HfByA='),
(83, 'tester', 'tester@test.com', 'M', '0', '0', NULL, 'MDUsLNBnPcCTf4eP7HTyk8wiAmB8/3eFlju9NuhLtpA='),
(84, 'userGedid', 'userGedid@gmail.com', 'M', '0', '0', NULL, 'YsSPV0WT7R+2Eu3XMnIrRiEd4oDMLWFO6cHw7gMCSbs='),
(85, 'data', 'data@gmail.com', 'M', '0', '0', NULL, 'qCrmmfxGX2+Xww8QbQkv2aCv4wzMSPy8cS/s5hpSO30='),
(86, 'mmm', 'mmm@mmm.com', 'M', '0', '0', NULL, 'uWEOvaUiMb0oKdLFknxsE9E/Ljkwtt3K6P6rhtGbDYc='),
(87, 'uuuu', 'uuu@uuu..com', 'M', '0', '0', NULL, 'c5iOy28HOPAfYwS3IyBbsYkCtjx2bNkEkcXi23xHhCU='),
(88, 'ft', 'tt@h.com', 'M', '0', '0', NULL, 'HMBCFVried7lvDzrZOB/Ep/l2Q5GEQb5hWzTU50LPOQ='),
(89, 'ft', 'ft@ff.com', 'M', '0', '0', NULL, 'uyrdE6MJZIRp3U2iZe1Nkbz2o3i+EgiOIGJKdmjW6pY='),
(90, 'asas', 'asas@as.com', 'M', '0', '0', NULL, 'B5D5E0nE/bgTMwTZoqLKn87TQVBxGsRrBszrqWHQ89s='),
(92, 'mmm', 'mmm@mmmm.com', 'M', '0', '0', NULL, 'uWEOvaUiMb0oKdLFknxsE9E/Ljkwtt3K6P6rhtGbDYc='),
(93, 'add', 'add@gmail.com', 'M', '0', '0', NULL, 'f69Aqk5tHyqjQgStNhXf+B9FI2V2DMTrn36e13/Z0FI='),
(94, 'ggg', 'ggg@ggg.ggg', 'M', '0', '0', NULL, 'OKNaAki7zCM11Mc4S+wQ0gNiwBUxz7Duxqo6190MNog='),
(95, 'ppp', 'ppp@ppp.com', 'M', '0', '0', NULL, 'a8xCZyt1tABHxVBKiAcCWsE4QyOFr9ZzwaAXNZMI+ag='),
(96, 'like', 'lkj@ljk.com', 'M', '0', '0', NULL, 'LfenBtdyUxKDCKxUt8cVVZ3oWnO+rd6BF+Tq94uLvAc='),
(97, 'add', 'sdf@sdf.com', 'M', '0', '0', NULL, 'KugmRz5clqdYfXLqDm55NxPynZu/D+/YikVddHQY/nw='),
(98, 'add', 'sdff@gdsg.com', 'M', '0', '0', NULL, 'KugmRz5clqdYfXLqDm55NxPynZu/D+/YikVddHQY/nw='),
(99, 'aa', 'hghg@hghg.jh', 'M', '0', '0', NULL, 'KugmRz5clqdYfXLqDm55NxPynZu/D+/YikVddHQY/nw='),
(100, 'fff', 'fff@fff.com', 'M', '0', '0', NULL, '7FoPn/QgydapOZ7xcXBceQtAB3u1l3thCgDznq5+FZk='),
(101, 'aa', 'aa@aa.com', 'M', '0', '0', NULL, 'KugmRz5clqdYfXLqDm55NxPynZu/D+/YikVddHQY/nw='),
(102, 'dgjf', 'fhhc@hfyh.com', 'M', '0', '0', NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY='),
(103, 'test10', 'test10@gmail.com', 'M', '0', '0', NULL, 'rO5s5LZi+q+cTRjBh+tWwslV46BiSrftyDw4lUF6xXE='),
(104, 'test12', 'test12@gmail.com', 'M', '0', '0', NULL, 'UF0RYWyXjLuDmlYHb/QYxuh93rdeN/DWanUlRnGho9I='),
(105, 'test15', 'test15@gmail.com', 'M', '0', '0', NULL, 'XwVEWy93u/UAmNAiIB+2kzN1N1P9t4NBLdzkb0OA71g='),
(106, 'moha', 'test16@gmail.com', 'F', '0', '0', NULL, 'MsqustRb4DwzkKXlQUgfp90i9xmP5Evd6Tw9pCdHnao='),
(107, 'ggg', 'gg@gg.com', 'M', '0', '0', NULL, 'GNOF+yWvmyrfZ7G7Rm/3xzmxH2hbfxkMo+7zCdNz7qg='),
(108, 'mo', 'mohammad.shabaan@gmail.com', 'm', '0', '0', NULL, 'rEqbiBtYxeOa4ZSRiIuwkVh532h7w2Ldbgtv+UJ47ek='),
(110, 'fhcf', 'fgcx@gjbv.com', 'M', '0', '0', NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY='),
(111, 'dhjc', 'dhh@fyk.com', 'M', '0', '0', NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY='),
(112, 'ooo', 'ooo@ooo.com', 'M', '0', '0', NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY='),
(113, 'moh', 'mm@xx.com', 'M', '2', '0', NULL, 'umYegUdfFQoSW+hCYfP2g7SAXSsUJSyGVLWPEYjN4Ec='),
(114, 'oo', 'qq@tt.com', 'M', '2', '0', NULL, 'umYegUdfFQoSW+hCYfP2g7SAXSsUJSyGVLWPEYjN4Ec='),
(115, 'testpush', 'testpush@gmail.com', 'M', '2', '0', NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY='),
(116, 'data', 'data@data.com', 'M', '2', '0', NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY='),
(117, 'ko', 'ko@ko.com', 'M', '2', '0', NULL, 'umYegUdfFQoSW+hCYfP2g7SAXSsUJSyGVLWPEYjN4Ec='),
(118, 'mahmoudhosny', 'mahmoud.e.hosny84@gmail.com', 'M', '2', '0', NULL, 'XeFp7qWoRH8epeLjJQYI7FmNPFHeGd+nDygHasU5QgE='),
(119, 'abc', 'abc@abc.com', 'M', '2', '0', NULL, 'tFsZfqE6QwfraAFq9Z++fA7S4OQlyQFlHr9T1gW3jPY='),
(120, 'tototo', 'toto@toto.com', 'M', '2', '0', NULL, 'uWpk7AASn+mrBYTYt2rcMrPOumvVCWi0rvJYsKOP/vk='),
(121, 'ehab khamis', 'ikdm_1@yahoo.com', 'M', '0', '0', NULL, 'ehnlJQX8w7s21Djdv60fI8O73jPB74M3D3DviGhdCm0='),
(122, 'xxx', 'xxx@gmail.com', 'F', '0', '0', NULL, 'qi9HEDWoni31hpZgCMzdj+tNC6TvusgJVXMhXYVWjYI=');

-- --------------------------------------------------------

--
-- Table structure for table `patient_addresses`
--

DROP TABLE IF EXISTS `patient_addresses`;
CREATE TABLE IF NOT EXISTS `patient_addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_code` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `latitude` varchar(45) DEFAULT NULL,
  `longitude` varchar(45) DEFAULT NULL,
  `patient_id` varchar(45) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `patient_addresses`
--

INSERT INTO `patient_addresses` (`id`, `country_code`, `city`, `latitude`, `longitude`, `patient_id`, `address`) VALUES
(34, 'EG', '1', '31.1256852', '29.7836783', '78', 'data data'),
(35, 'EG', '', '31.1256852', '29.7836783', '78', ''),
(36, 'EG', 'alex', '31.1256852', '29.7836783', '8', 'testaddresses'),
(37, 'EG', 'alex', '31.1256852', '29.7836783', '8', 'testaddresses'),
(38, 'EG', 'alex', '31.1256852', '29.7836783', '8', 'testaddresses'),
(41, 'EG', '1', '31.1256852', '29.7836783', '105', 'xxx st'),
(42, 'EG', '', '31.1256852', '29.7836783', '105', ''),
(43, 'EG', '1', '31.1256852', '29.7836783', '105', 'xxxx st'),
(44, 'EG', '1', '31.1256852', '29.7836783', '105', 'xxxxx ST'),
(45, 'EG', '', '31.1256852', '29.7836783', '105', ''),
(46, 'EG', '', '31.1256852', '29.7836783', '105', ''),
(47, 'EG', '', '31.1256852', '29.7836783', '105', ''),
(48, 'EG', '', '31.1256852', '29.7836783', '105', ''),
(49, 'EG', '1', '', '', '114', 'test aa'),
(50, 'EG', '1', '', '', '114', 'demo'),
(51, 'EG', '', '31.1269407', '29.782796', '120', ''),
(52, 'EG', '', '31.20212392772522', '29.96232323348522', '120', '');

-- --------------------------------------------------------

--
-- Table structure for table `patient_device_data`
--

DROP TABLE IF EXISTS `patient_device_data`;
CREATE TABLE IF NOT EXISTS `patient_device_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `api_key` varchar(45) DEFAULT NULL,
  `push_token` varchar(45) DEFAULT NULL,
  `device_id` varchar(45) DEFAULT NULL,
  `device_verision` varchar(45) DEFAULT NULL,
  `device_model` varchar(45) DEFAULT NULL,
  `data_updated` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `patient_login`
--

DROP TABLE IF EXISTS `patient_login`;
CREATE TABLE IF NOT EXISTS `patient_login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) DEFAULT NULL,
  `api_key` varchar(255) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `device_token` varchar(255) NOT NULL,
  `device_id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `api_key_UNIQUE` (`api_key`)
) ENGINE=InnoDB AUTO_INCREMENT=588 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `patient_login`
--

INSERT INTO `patient_login` (`id`, `patient_id`, `api_key`, `datetime`, `device_token`, `device_id`) VALUES
(288, 16, '326ed0520be4ca971751758f4721d931', NULL, '', ''),
(290, 16, '7a07e56671b4a3754140cd0442a667b0', NULL, '', ''),
(291, 16, '1d0cd604781768d9b9c4c17f1bc7b7bf', NULL, '', ''),
(304, 80, '0d97943d347946d6319267d0f95d9db9', NULL, '', ''),
(305, 82, 'c8245e0e28eee203e62d186a3d1ba9c4', NULL, '', ''),
(306, 83, 'e121ebc7b9e343c4eaab67ebafc1b2b8', NULL, '', ''),
(307, 84, '6603da17372cf998bb385387cc294f91', NULL, '', ''),
(308, 8, '710e294ee70f0686d2b9ee4ff4a4b16f', NULL, '', ''),
(309, 8, '0825a05eae409d40df57ba67e51df988', NULL, '', ''),
(310, 85, 'cfe1856c6c2c325968a26939e534e083', NULL, '', ''),
(311, 8, 'c631b33bd6bade98101a18d6894438de', NULL, '', ''),
(312, 86, '3bd26a091eb170e130b40f2595341be6', NULL, '', ''),
(313, 87, '7c32e40f864d020c814a44a0eb8ebf7d', NULL, '', ''),
(314, 88, 'dc1a3b538b1a00609c940a9b5ab7a512', NULL, '', ''),
(315, 89, 'ab439542d1bb724071faf670fa7d7bfb', NULL, '', ''),
(316, 90, '23dfb80fce7c376363384fa976172ee1', NULL, '', ''),
(317, 92, '4eed3073aabac98b5b4315c03559b2be', NULL, '', ''),
(318, 93, '1d9b9b5557024c31314e7861b085b58f', NULL, '', ''),
(319, 94, '224ff842ffd316ff774ceca9cc8fe2ff', NULL, '', ''),
(320, 95, '1a80a6f40cc67850d7911cea22ef1009', NULL, '', ''),
(321, 96, '06be332552bb734fa67d37cfc7edf5be', NULL, '', ''),
(322, 97, '129a50287a0effe27806e448e9ccc94a', NULL, '', ''),
(323, 98, 'c97aa67e804c42c25e411f720778de23', NULL, '', ''),
(324, 99, '32febfeb1e8c4c9caf6b88725741fc79', NULL, '', ''),
(325, 100, '74431e8c83092c7c6bc73a8a0e14ac2d', NULL, '', ''),
(326, 101, '5b8067e52b9f8d3d627abf27997c7357', NULL, '', ''),
(330, 11, '5b43678fd08a446809cbf502c0bca5a7', NULL, '', ''),
(331, 11, '6cc05a914a24b8bdaf1fc03e725a3e42', NULL, '', ''),
(332, 11, '348d8085251b0b18d78538e7886c43d5', NULL, '', ''),
(334, 102, '1379ae39a28d62c1ee644848381d50d7', NULL, '', ''),
(343, 105, 'd19194371c76f28ea6c5a659b74d2470', NULL, '', ''),
(344, 105, '259c69aea7772a8f06aedc90cef7b0ce', NULL, '', ''),
(345, 105, '37296d48de6d01f3a344246aa8236f17', NULL, '', ''),
(346, 105, '73c207d95551139e4a7e85baf5f27e2b', NULL, '', ''),
(348, 106, 'e70f7c5ef19b75f91eb1c01f0c49d8a1', NULL, '', ''),
(349, 105, 'e9ec39df3580c5dbe671c141dcf8e50a', NULL, '', ''),
(350, 105, '0b0925242c7d51c0d757c10180271f0a', NULL, '', ''),
(351, 106, '3371de86f6475d4313fec9cac4e5538e', NULL, '', ''),
(352, 106, 'e04a503558ccd5909d5455f873697364', NULL, '', ''),
(353, 105, '76dacbf1ab69e615bac8a57e73f3af5a', NULL, '', ''),
(354, 105, '66302d61962d8122e9b7cfc55eb4059a', NULL, '', ''),
(355, 105, 'a60fa812dbb5acea72e4323e1bab302b', NULL, '', ''),
(356, 105, '4a82fc9cc20922d4f0a40f69b0092a3c', NULL, '', ''),
(357, 105, 'ff78aba034b4b8711a92879a4bded46a', NULL, '', ''),
(358, 105, '79bbe6d3e3fd90e21fd50e24ee3e6237', NULL, '', ''),
(359, 105, '1dd689e823b53b94a075956ca1fa1651', NULL, '', ''),
(360, 105, '256ae4bd639bd47c7d4280dfa8aadb37', NULL, '', ''),
(361, 105, '3958a9afe66702833b898af9c3ad85cd', NULL, '', ''),
(362, 105, '82397ff9feef8fe28f52126c4f7effa4', NULL, '', ''),
(363, 105, '06512357045265118410478b5242c987', NULL, '', ''),
(364, 106, '9a01f14678213a46ac34e9d993af0615', NULL, '', ''),
(365, 105, '2bdbb2f4e42e05919a3ec54f4ea8d11a', NULL, '', ''),
(366, 105, '3480b793118bc11f58396fa2524b724c', NULL, '', ''),
(367, 105, '55c47c1933315fb2ac3b2bcee134717a', NULL, '', ''),
(368, 105, '0163bccfa4182652697767b7bfe1a624', NULL, '', ''),
(369, 105, '142cfa8638bf043ccc1c40c422444ddc', NULL, '', ''),
(370, 105, 'e07f10789a6f18a2956f98c76d5939f4', NULL, '', ''),
(371, 105, 'e641e9c4b7ad1513a55c53add95eed0a', NULL, '', ''),
(372, 105, '4e4f14bacf20ef40a4606f01206da6d6', NULL, '', ''),
(373, 105, '201333bce1cf25be39e29b6f3e5e4c97', NULL, '', ''),
(374, 105, '1f380c6440669803e18c2ce8add07a47', NULL, '', ''),
(375, 105, '6be665efdf46323939d6292bf88c0d15', NULL, '', ''),
(376, 105, 'b7d2e1012e950e9574f3f5d73eee1571', NULL, '', ''),
(377, 106, '5e29f14f653cc0288a4a440ce3f1c7fb', NULL, '', ''),
(379, 106, '117e35fe4c7e719c128cae00dfae3e65', NULL, '', ''),
(381, 107, '78aa30f8eb3e7bb96a3f0a83d87049be', NULL, '', ''),
(382, 107, '58e7bec7b0482949c98f8d82b5b895b1', NULL, '', ''),
(384, 108, '20e5995f9a1add2bda349e83e5af7677', NULL, '', ''),
(389, 106, '592dbd055acd1a5c57b490a18f46b69b', NULL, '', ''),
(390, 0, 'fc1230bae352909a38cd4b6772451d3e', NULL, '', ''),
(391, 0, '81989455e4252839509005704a6989c8', NULL, '', ''),
(392, 0, '1ca79cd9b48b00091664292e0fb5d7a1', NULL, '', ''),
(393, 106, '5de8b3807887cbd9e29ef664d7f2979d', NULL, '', ''),
(394, 106, 'b6712eda82805718737917ccd0cdbdb6', NULL, '', ''),
(395, 106, '1731fa7751959237a68676f71bc809b4', NULL, '', ''),
(396, 106, 'd7c44a08fb4745d1f6ed1edb4e1d1f98', NULL, '', ''),
(397, 8, 'c9171982a319fbb3e1148ee005a4e35f', NULL, '', ''),
(398, 8, '41ff27d61bdaaac10c71607eeda71538', NULL, '', ''),
(399, 106, 'b9cb3ff7857bfdd07cfcd631f2aa8e2b', NULL, '', ''),
(400, 106, 'b4b13464c0e1e55356b4b723f66c5773', NULL, '', ''),
(401, 2, 'aa7117cc4bb2becd39efef30defa6615', NULL, '', ''),
(402, 2, '71c50ccd40e4072c1effe40ffa032066', NULL, 'sdsf', '123456'),
(403, NULL, NULL, NULL, '', ''),
(405, 113, 'bbfd96763702f2de681bbd570ddcfbdc', NULL, '', ''),
(408, 114, '393c9209f70592f600024abc0eddecb7', NULL, '', ''),
(409, 114, '2154446d813a0786df1e212bc519057b', NULL, 'testtoken', '12'),
(410, 114, '3aecabd957a8f098524d5a364ebe1b07', NULL, 'testtoken88', '12'),
(412, 114, '4895fceb79cb068e36afabea3b848d56', NULL, '', ''),
(413, 114, '7f59c9da43e69d5fb4e18452a95ea279', NULL, '', ''),
(414, 114, '07796899a6a1a1ca7aba0fbbd70676e5', NULL, '', ''),
(416, 106, '751c6f2a862197efb4d31250ea1523ee', NULL, '', ''),
(418, 106, 'efe2b5dac08c0d7e4a349a67e3579fbd', NULL, '', ''),
(419, 106, '96e70ce15714576c7f1bb7169d4a7125', NULL, '', ''),
(420, 106, 'b99846f258296f1ad37687412522d7de', NULL, '', ''),
(423, 2, '9a19126efed93182bc8102f3f0aca8f6', NULL, 'dIvNX5gF6NQ:APA91bE2duXJfHeSTwKD9Y9sxNiPiYy7ZyfARv1zWuDn98kvYZAUmX2zFF9ggOQePPNIoMj43OTuMJlMi8qmRvlCGRjzm4TSBGEfvr2o7LUvzO_7u-5NiGjHsn_FbRSg3dnFRzt0-Wl3', '8e0c6cf1aead475e'),
(425, 115, '1d25c93b15e9364ff62e2a40bf581509', NULL, '', ''),
(426, 115, 'd0120c4e1822596b04b93af4685977b4', NULL, '', ''),
(428, 116, 'cd76cf3bfacfb7366ce00dd534683631', NULL, 'dIvNX5gF6NQ:APA91bE2duXJfHeSTwKD9Y9sxNiPiYy7ZyfARv1zWuDn98kvYZAUmX2zFF9ggOQePPNIoMj43OTuMJlMi8qmRvlCGRjzm4TSBGEfvr2o7LUvzO_7u-5NiGjHsn_FbRSg3dnFRzt0-Wl3', '8e0c6cf1aead475e'),
(429, 116, '6c9824720c2d8ae0d84428e87bb853eb', NULL, '', ''),
(430, 116, 'aafc1c5584827d44474a4da32e2df4a8', NULL, '', ''),
(431, 116, 'f62527dea04fa9cd37118c6231d0947f', NULL, '', ''),
(432, 116, 'df5ced00f000aedf8795092d14ffa100', NULL, '', ''),
(433, 117, '29af8f04bd1054c1f35595d5c5b767bc', NULL, '', ''),
(434, 117, 'b84e3c10e1da305a857558d93001dcd7', NULL, 'fAZimL3SM0g:APA91bF7Xl8oKDCaWNmeQjuxku3930PGWAxiqqrqlYqrusPCuT2EucZlFm4jBLwIUP_fXnjK83Fui4na6ECEOvXaHGyxofEYq_SUlgCoLrE8jzjL3XJXRNyF1L12zc_K1s9fqHXjHE9I', 'e90789852dbaa4f8'),
(435, 117, 'a7a3b84102ec30c334a7b927d8f2b85f', NULL, '', ''),
(436, 117, '5200ae64805a1991725f82bd5ce53288', NULL, '', ''),
(437, 117, 'a2a032b107f41134a719e0bb559abe12', NULL, '', ''),
(438, 118, '25fd5277a4ac91cc00e379208646346b', NULL, '', ''),
(439, 118, 'ad2f01fe05a73a27fa04500b2129dd3b', NULL, '', ''),
(440, 118, '0f7559fda2a19e60b081aa6a98cf9192', NULL, '', ''),
(441, 118, '9c32aefe76a8cfe0f12ac8076ae9021b', NULL, '', ''),
(442, 118, '718dca5c564313e18cf98ec40cd8c5b9', NULL, '', ''),
(443, 116, '757fdb701ff38bf83edf7bac13a2843e', NULL, '', ''),
(445, 119, '8cdbb1a9816363ef387de6c64ad0cb03', NULL, 'dIvNX5gF6NQ:APA91bE2duXJfHeSTwKD9Y9sxNiPiYy7ZyfARv1zWuDn98kvYZAUmX2zFF9ggOQePPNIoMj43OTuMJlMi8qmRvlCGRjzm4TSBGEfvr2o7LUvzO_7u-5NiGjHsn_FbRSg3dnFRzt0-Wl3', '8e0c6cf1aead475e'),
(446, 119, 'bfb2d80506d364963741243b57a0da8f', NULL, '', ''),
(447, 119, '32987c3f253d0b2381168cef5bb17ee4', NULL, '', ''),
(448, 119, '6674b76d20f4c254cd97dc30fee19b19', NULL, '', ''),
(449, 119, '05cf1af4978438a88f8397d7f94b5d4e', NULL, '', ''),
(450, 119, 'f3348d73057ab479e636297cc4bb31d7', NULL, '', ''),
(451, 119, 'fa45f478ccb437d20c8bc874c13cf3cd', NULL, '', ''),
(452, 119, '522e6b88beb3051c5b8657896d6597ea', NULL, '', ''),
(453, 119, '5e4270f5fb7aa1317ed84aca2ef9d59b', NULL, '', ''),
(464, 120, '233beb34e430aeeee3266aa98e03282c', NULL, '', ''),
(465, 120, '591a61314ff2a3543e92d35f0cbdfb6b', NULL, '', ''),
(466, 120, '10b143e13d88cce5ea371534088d6273', NULL, '', ''),
(467, 120, '255c69c297f02394db7b243099f36b09', NULL, '', ''),
(468, 120, '8405d7c98c1c13c1cce03b2e59151f45', NULL, '', ''),
(469, 120, '590a3968e5710d48765bcbbb510263f0', NULL, '', ''),
(470, 120, 'a2a9dcdbaee12b2aaae9f633957f9ed7', NULL, '', ''),
(471, 121, 'bed6461f87aaff15cd0baf3251572637', NULL, '', ''),
(472, 121, 'aa5ac419ddb96cacdf5f42265c830885', NULL, '', ''),
(473, 121, '8217d4446f730aa90009a130db2f8c15', NULL, '', ''),
(474, 121, '2f8e64c2095431a9ff1699ffab52dbec', NULL, '', ''),
(475, 121, 'f21da1c84249de6d9617f386b123c8c7', NULL, '', ''),
(476, 121, '96835ee79a4ac7f0e49f18996fd1a2e9', NULL, '', ''),
(477, 121, 'e69aba6441ece0fbb50d018daa6ce05c', NULL, '', ''),
(478, 121, 'ca57c96014ea5c203e68bac9fd019a37', NULL, '', ''),
(479, 121, 'f3bbb9411fa58f311a42f7a6dbdd546d', NULL, '', ''),
(480, 121, '9f1c3b4b9c7592fd93a2fbe87ec87a32', NULL, '', ''),
(481, 121, '167fefb1df772a1865fde93edca00de6', NULL, '', ''),
(482, 121, '1813dec568ba68c9815264d7ebb65331', NULL, '', ''),
(483, 121, 'b8260142d646532a2f30afb209055605', NULL, '', ''),
(484, 120, 'f4dbfe80ef0e20fa4c74fb13613d76d5', NULL, '', ''),
(485, 120, '8db6a33e696f212dff2ad593e7fdb0dc', NULL, '', ''),
(486, 120, '9be4f268427e186ab515127a9886dccf', NULL, '', ''),
(487, 120, '02db02222ad69f90a3059c92c0244587', NULL, '', ''),
(488, 120, '71047365ac5b59cc6e95747bcf3a44bc', NULL, '', ''),
(489, 122, '5735131e394b71d0c23c0e8365e52767', NULL, '', ''),
(490, 122, '5a280b771a9c7246e6c2ec76880c4813', NULL, '', ''),
(491, 122, '6c163e062edb9ab22475a1101a646f60', NULL, '', ''),
(492, 122, '411e267d125b8d77c43492e4d79e5af1', NULL, '', ''),
(493, 120, '580a425f03e7a2382d890b0badb0beee', NULL, '', ''),
(494, 122, '562a51f50d9b7238dc87a26ea8e620c2', NULL, '', ''),
(495, 120, 'd07839faf8e7bc6c6b0f5399ad8ca634', NULL, '', ''),
(496, 122, '932a8ba1c6855a92e562c37287b372c1', NULL, '', ''),
(497, 122, '6bccb62c724f2079b7f0062ddad6c341', NULL, '', ''),
(498, 122, 'c98afd0189ea8ab16167cb2f18385f82', NULL, '', ''),
(499, 122, '48ff59ab0b27aa965d429585b0e9636c', NULL, '', ''),
(500, 122, 'ffcb9be338c13cdec5adc4b996b7b7ca', NULL, '', ''),
(501, 122, '01450195c43d58e5083fc1e094034d86', NULL, '', ''),
(502, 122, '73e427322417ac88cdc6db67a099fdc0', NULL, '', ''),
(503, 122, 'b677162009cdc7a1ff52ed51b170a10f', NULL, '', ''),
(504, 122, '7c83cc25a5c19d6a6507837a32664bb9', NULL, '', ''),
(505, 122, 'ee6fb1b347a49b6091d2d36e77088311', NULL, '', ''),
(506, 122, 'da7a78b128dc679d9dfa1db477d66dde', NULL, '', ''),
(507, 122, '86530b6a89ad1bbc42cfbe5db1512e47', NULL, '', ''),
(508, 122, '72f48087f86acdc60f786a7be752daa6', NULL, '', ''),
(509, 122, '6eda113533ed8d3014ce6a201fac60b9', NULL, '', ''),
(510, 122, 'f32b5bc95d9208345ed6c021fb6c0f24', NULL, '', ''),
(511, 122, '6ee5d16b119f7d741a1f1f89c762c6bf', NULL, '', ''),
(512, 122, '9854d58512c1a8353b70a161672fc492', NULL, '', ''),
(513, 122, '82cbde96effd478ad5229f373afe4c31', NULL, '', ''),
(514, 120, '47124dfea03e238fa65390bad8c2a348', NULL, '', ''),
(515, 122, '0a9e6c50ee94bda7eb95dca5f3542b9b', NULL, '', ''),
(516, 122, 'f2ff7d07d6f1e280bca7e97e87003e26', NULL, '', ''),
(517, 122, '06bae13ca863027b26cea704d4a4ded1', NULL, '', ''),
(518, 122, 'f46140ccc2799d415542123228139ec1', NULL, '', ''),
(519, 122, 'f8f36768f7081965e1145a15b4bfe78a', NULL, '', ''),
(520, 120, '1910ad2b8e6614a37ca1190011bd6736', NULL, '', ''),
(521, 122, '30854bd34b424c5c278befec90cdf07e', NULL, '', ''),
(522, 122, '9de3a66eb2ba48cf79151ee5635626e3', NULL, '', ''),
(523, 122, 'a67124e9d93794af77d65b82e6e61602', NULL, '', ''),
(524, 120, '58c42356704ce0d397538924e914eddd', NULL, '', ''),
(525, 120, '84f54753ddd1d716043d98a3b58fe129', NULL, '', ''),
(526, 122, 'c148dd9628a3890260b148a3710eae8d', NULL, '', ''),
(527, 122, '488e3c0237ea11279f4f694cee49b308', NULL, '', ''),
(528, 122, '73a2779866cfd7620792806705df07cb', NULL, '', ''),
(529, 122, '451d17d39e6bb00b3a2837802c211f41', NULL, '', ''),
(530, 120, '3f6649e6e93afa7062a87ccbc0dc86cf', NULL, '', ''),
(531, 122, 'ed22272a5f3100a9f4e18cd490fb6048', NULL, '', ''),
(532, 120, '6fe637f87992957721ca77262456f840', NULL, '', ''),
(533, 120, '6cb79c5c37d0e2200d1fbabd87d28618', NULL, '', ''),
(534, 120, '212b888f33ced2bec5950a19f0cd316e', NULL, '', ''),
(535, 120, '88d408db49b55a48dd2e9e121c61459a', NULL, '', ''),
(536, 122, '3ca351dad30f931f47454dac89b42fa9', NULL, '', ''),
(537, 122, '2bd9f6f0ee1b6d391da46133c65fc627', NULL, '', ''),
(538, 122, '0f1dacd52cf1b8b91b613eeafb7213a5', NULL, '', ''),
(539, 122, '23773a7b4ad84daf68cc0f3539dd3aa4', NULL, '', ''),
(540, 122, '295f1bb498cee780f450bf4204777907', NULL, '', ''),
(541, 122, '0c8e69f409633c8b93770f1e529f2448', NULL, '', ''),
(542, 120, 'af622d03af62c01122d5eec124dda16a', NULL, '', ''),
(543, 120, '0bd78b0468a097f697bff642781d48f3', NULL, '', ''),
(544, 122, '2e5b072633d290916b5fb07094c688d9', NULL, '', ''),
(545, 122, '20225772b8615f3c2cd5b8839244f4a1', NULL, '', ''),
(546, 122, 'e81ecee20c4ec6b45764b80d883bd485', NULL, '', ''),
(547, 122, '316e545adfd25a66656234b7d465e3ea', NULL, '', ''),
(548, 122, 'f283d4d9cf6fd7059a42b748209222a0', NULL, '', ''),
(549, 122, '3a6c49bc9e42c023522658094516eb65', NULL, '', ''),
(550, 122, 'fda1243350aeb34a8ffe796efdce2eba', NULL, '', ''),
(551, 122, '9bd30c7915e0f3fecc961baf3413cfd6', NULL, '', ''),
(552, 122, 'e220c58cfb54aaf1341a80e0d0ad3997', NULL, '', ''),
(553, 122, 'ee7bbfdea2e8ae1d08f3179b4eff1f11', NULL, '', ''),
(554, 122, '9f0c8dc835882853c7db9e04fb1d6c85', NULL, '', ''),
(555, 122, 'f8396ca0feb5108b55d440f0b3e12f57', NULL, '', ''),
(556, 122, 'bab2e477a8878814039452792d044cf2', NULL, '', ''),
(557, 122, '1e4caa011cfb25448d6640e686f5100a', NULL, '', ''),
(558, 122, '8271898e988238bb5a46a81335a38741', NULL, '', ''),
(559, 122, 'f6925f8a2f9889e6d9c5977fa431a605', NULL, '', ''),
(560, 122, '5fc1779e22dd5d17d8337f0c2dad2034', NULL, '', ''),
(561, 122, '85722e227bf27e5239a6f511c3a1b577', NULL, '', ''),
(562, 122, '5eaf284f9ebe49ff80db28da8bced4b6', NULL, '', ''),
(563, 120, '7e58d7263e6921e035b222f0374dc550', NULL, '', ''),
(564, 120, '9b9d846ec9b29ba8f5a8858d1c976c16', NULL, '', ''),
(565, 120, 'f18b1fbe4358d5b949320471e7911307', NULL, '', ''),
(566, 120, '03a83e30b286d30c4fa924fbfd8a9460', NULL, '', ''),
(567, 120, '1b0548812cb09b74d53e87577692ff7c', NULL, '', ''),
(568, 120, 'aa5a0890f502a7bd3db04fcd2cb1584b', NULL, '', ''),
(569, 120, '5a790098b3519774f43ca3afc9e0941a', NULL, '', ''),
(570, 120, '64f34d64a2275fecfc3e731f61d7244e', NULL, '', ''),
(571, 120, 'f94b41b45dc7829dd1eae140a0216406', NULL, '', ''),
(572, 120, '22baae51c8c6f6695bb45dc911e965fd', NULL, '', ''),
(573, 120, '9d07919824707addb18155b3e3b99380', NULL, '', ''),
(574, 120, 'fe9ca7034edec7ba8ae124518fbc2781', NULL, '', ''),
(575, 120, '73fe321ac6e428174295e9d0bb38f9dc', NULL, '', ''),
(576, 120, 'ead920030923082b3a7a370b6259b971', NULL, '', ''),
(577, 120, '235f3ecd2b9b5fa6d0e8b30d38e8d2e0', NULL, '', ''),
(578, 120, '051d7e72fec1eee471e47f4d5bc0a323', NULL, '', ''),
(579, 120, '47f233195388f7f935f5e0f66bf2f2aa', NULL, '', ''),
(580, 120, 'b858f5a38abbae6655fd694bb5e3c20a', NULL, '', ''),
(581, 120, '7f3f8466333c53432bff52a76e1abd56', NULL, '', ''),
(582, 120, 'd481383cbc3e4994094598d74975b997', NULL, '', ''),
(583, 120, '3f49d3b8fe0c0e6eac1601e978861a32', NULL, '', ''),
(584, 120, '3b6423956cefc638ec401ac4f150c8c1', NULL, '', ''),
(585, 120, 'e4e0ebbe6d47c149443a0fbc80ea00f4', NULL, '', ''),
(586, 120, 'a17350724fed8fc43595405b317e73df', NULL, '', ''),
(587, 120, 'c4a1560236be366a38c5c22cbe6c06c1', NULL, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `patient_phone`
--

DROP TABLE IF EXISTS `patient_phone`;
CREATE TABLE IF NOT EXISTS `patient_phone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_code` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `default` int(11) DEFAULT NULL,
  `patient_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `patient_phone`
--

INSERT INTO `patient_phone` (`id`, `country_code`, `phone`, `default`, `patient_id`) VALUES
(12, '02', '123', NULL, 4),
(13, '+20', '0134567', NULL, 23),
(14, ' 20', '0134567', NULL, 23),
(15, ' 20', '0134567', NULL, 23),
(16, '20', '01226427786', NULL, 39),
(18, '20', '01274036897', NULL, 39),
(30, '20', '0852365479', NULL, 39),
(31, '20', '1234', NULL, 39),
(32, '20', '08796542', NULL, 2),
(33, '02', '123', NULL, 58),
(34, '02', '12345', NULL, 58),
(35, '02', '12345', NULL, 73),
(36, '20', '0854658', NULL, 58),
(37, '20', '02589654', NULL, 75),
(38, '20', '085236', NULL, 76),
(39, '02', '123456', NULL, 16),
(40, '02', '1234567', NULL, 16),
(41, '20', '08974563', NULL, 78),
(43, '20', '78965422', NULL, 78),
(44, '20', '456923', NULL, 78),
(45, '20', '8525', NULL, 102),
(48, '20', '868684607', NULL, 106),
(53, '+376', '852147', NULL, 105),
(54, '+376', '96321', NULL, 105),
(55, '20', '86864834', NULL, 106),
(56, '+376', '321456', NULL, 105),
(57, '+376', '589632', NULL, 105),
(58, '+376', '123654', NULL, 105),
(59, '20', '1234', NULL, 107),
(60, '2', '01147670025', NULL, 112),
(61, '2', '01226427786', NULL, 2),
(62, '2', '01285819291', NULL, 114),
(63, '2', '01226427786', NULL, 115),
(64, '2', '01226427786', NULL, 116),
(65, '2', '01285819291', NULL, 117),
(66, '2', '01225529997', NULL, 118),
(67, '2', '01226427786', NULL, 119),
(68, '2', '01226427786', NULL, 120);

-- --------------------------------------------------------

--
-- Table structure for table `patient_phone_temp`
--

DROP TABLE IF EXISTS `patient_phone_temp`;
CREATE TABLE IF NOT EXISTS `patient_phone_temp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_code` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `default` int(11) DEFAULT NULL,
  `patient_id` int(11) DEFAULT NULL,
  `activation_code` varchar(45) DEFAULT NULL,
  `exp_time` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=151 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `patient_phone_temp`
--

INSERT INTO `patient_phone_temp` (`id`, `country_code`, `phone`, `default`, `patient_id`, `activation_code`, `exp_time`) VALUES
(34, '+20', '01243587', NULL, 19, '1234', '2017-03-15 19:59'),
(35, '+20', '909120932', NULL, 20, '1234', '2017-03-15 20:11'),
(36, '+20', '019373', NULL, 21, '1234', '2017-03-16 07:23'),
(38, '20', '01226427786', NULL, 26, '1234', '2017-03-22 13:41'),
(39, '20', '01226427786', NULL, 26, '1234', '2017-03-22 13:42'),
(40, '20', '01226427786', NULL, 27, '1234', '2017-03-22 13:56'),
(41, '20', '1478', NULL, 28, '1234', '2017-03-22 14:03'),
(46, '20', '01226427786', NULL, 33, '1234', '2017-03-22 17:50'),
(48, '20', '01226427786', NULL, 34, '1234', '2017-03-23 19:31'),
(49, '20', '05269', NULL, 35, '1234', '2017-03-23 19:47'),
(50, '20', '8855', NULL, 36, '1234', '2017-03-23 20:48'),
(51, '20', '9895#0#', NULL, 37, '1234', '2017-03-23 21:27'),
(52, '20', '01226427786', NULL, 38, '1234', '2017-03-23 21:46'),
(54, '20', '01252525', NULL, 39, '1234', '2017-03-24 19:42'),
(70, '20', '123456789', NULL, 0, '1234', '2017-03-27 10:04'),
(71, '20', '12345658', NULL, 0, '1234', '2017-03-27 10:05'),
(73, '20', '1001420808', NULL, 72, '1234', '2017-03-27 12:31'),
(74, '20', '1001420808', NULL, 72, '1234', '2017-03-27 12:31'),
(75, '02', '123', NULL, 4, '1234', '2017-03-27 20:57'),
(79, '20', '1007893000', NULL, 74, '1234', '2017-03-27 22:54'),
(83, '02', '123', NULL, 4, '1234', '2017-03-28 12:09'),
(84, '02', '123', NULL, 16, '1234', '2017-03-28 12:11'),
(91, '20', '0875246', NULL, 2, '1234', '2017-03-29 14:59'),
(93, '+376', '4678825 74', NULL, 103, '1234', '2017-04-10 17:58'),
(108, '1234', '01285819291', NULL, 107, '1234', '2017-04-26 01:38'),
(109, '1234', '01285819291', NULL, 107, '1234', '2017-04-26 01:39'),
(110, '1234', '01285819291', NULL, 107, '1234', '2017-04-26 09:33'),
(111, '1234', '01285819291', NULL, 107, '1234', '2017-04-26 09:33'),
(112, '1234', '01285819291', NULL, 107, '1234', '2017-04-26 09:38'),
(113, '1234', '01285819291', NULL, 107, '1234', '2017-04-26 09:39'),
(114, '1234', '01285819291', NULL, 107, 'hkub', '2017-04-26 09:46'),
(115, '1234', '201001420808', NULL, 107, '108n', '2017-04-26 09:52'),
(116, '1234', '201285819291', NULL, 107, 'ccc8', '2017-04-26 10:03'),
(117, '1234', '201285819291', NULL, 107, 'ievk', '2017-04-26 10:04'),
(118, '1234', '201285819291', NULL, 107, 'qizf', '2017-04-26 10:05'),
(119, '1234', '0201285819291', NULL, 107, 'vzpe', '2017-04-26 10:06'),
(120, '1234', '01285819291', NULL, 107, 'w5h5', '2017-04-26 10:07'),
(121, '2', '01285819291', NULL, 107, '8e63', '2017-04-26 10:07'),
(122, '20', '01147670025', NULL, 110, '87754', '2017-04-27 13:54'),
(123, '20', '01147670025', NULL, 111, '12935', '2017-04-27 14:12'),
(126, '2', '01285819291', NULL, 113, '87552', '2017-05-11 12:10'),
(127, '2', '01285819291', NULL, 113, '33240', '2017-05-11 12:11'),
(128, '2', '01285819291', NULL, 113, '47053', '2017-05-11 12:12'),
(137, '2', '01227447334', NULL, 121, '76220', '2017-05-16 09:44'),
(138, '2', '01227447334', NULL, 121, '60985', '2017-05-16 09:52'),
(139, '2', '01227447334', NULL, 121, '81551', '2017-05-16 09:53'),
(140, '2', '01227447334', NULL, 121, '20226', '2017-05-16 09:54'),
(141, '+376', '4656874543', NULL, 122, '19196', '2017-05-16 13:13'),
(142, '+376', '12345654', NULL, 122, '99876', '2017-05-17 02:41'),
(143, '+376', '123654789', NULL, 122, '67532', '2017-05-17 03:19'),
(144, '+20', '01093103001', NULL, 122, '02011', '2017-05-17 03:24'),
(145, '+376', '1324343', NULL, 122, '54614', '2017-05-17 04:06'),
(146, '+20', '01226427786', NULL, 122, '69555', '2017-05-17 04:19'),
(147, '+2', '01093103001', NULL, 122, '80345', '2017-05-17 05:09'),
(148, '+2', '01093103001', NULL, 122, '34103', '2017-05-17 06:41'),
(149, '+2', '01093103001', NULL, 122, '36138', '2017-05-17 06:50'),
(150, '+2', '01226427786', NULL, 122, '00504', '2017-05-17 06:52');

-- --------------------------------------------------------

--
-- Table structure for table `patient_requests`
--

DROP TABLE IF EXISTS `patient_requests`;
CREATE TABLE IF NOT EXISTS `patient_requests` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `patient_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `patient_age` int(10) UNSIGNED NOT NULL,
  `patient_weight` decimal(8,2) NOT NULL,
  `patient_address_id` int(11) DEFAULT NULL,
  `is_at_home` tinyint(1) NOT NULL,
  `is_able_to_be_stable` tinyint(1) NOT NULL,
  `number_of_scanned_prescriptions` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `refuse_comment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `patient_requests_patient_id_foreign` (`patient_id`),
  KEY `patient_requests_patient_address_id_foreign` (`patient_address_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `patient_requests`
--

INSERT INTO `patient_requests` (`id`, `patient_id`, `patient_name`, `patient_age`, `patient_weight`, `patient_address_id`, `is_at_home`, `is_able_to_be_stable`, `number_of_scanned_prescriptions`, `created_at`, `updated_at`, `status`, `refuse_comment`) VALUES
(1, 8, 'mahmoud', 33, '50.00', 36, 0, 1, 1, '2017-06-03 08:11:19', '2017-06-13 09:57:43', 'rw', NULL),
(2, 8, 'mahmoud', 33, '50.00', 36, 1, 1, 1, '2017-06-03 08:11:20', '2017-06-13 09:33:14', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `patient_uploads`
--

DROP TABLE IF EXISTS `patient_uploads`;
CREATE TABLE IF NOT EXISTS `patient_uploads` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `image_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `request_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `patient_uploads_request_id_foreign` (`request_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `patient_uploads`
--

INSERT INTO `patient_uploads` (`id`, `image_url`, `request_id`, `created_at`, `updated_at`) VALUES
(1, 'images/2.jpg', 1, NULL, NULL),
(2, 'images/2.jpg', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `radiology_questions`
--

DROP TABLE IF EXISTS `radiology_questions`;
CREATE TABLE IF NOT EXISTS `radiology_questions` (
  `id` int(10) UNSIGNED NOT NULL,
  `ar_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `en_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_for_women` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `radiology_questions`
--

INSERT INTO `radiology_questions` (`id`, `ar_name`, `en_name`, `is_for_women`, `created_at`, `updated_at`) VALUES
(1, 'ط¸â€،ط¸â€‍ ط·ع¾ط¸â€¦ ط·آ§ط·آ¬ط·آ±ط·آ§ط·طŒ ط¸ظ¾ط·آ­ط¸ث†ط·آµط·آ§ط·ع¾ ط·آ³ط·آ§ط·آ¨ط¸â€ڑط·آ© ط·آ¨ط·آ§ط¸â€‍ط¸â€¦ط·آ±ط¸ئ’ط·آ²ط·ع؛', 'Have the patient performed any other test at the center before?', '0', '2017-06-03 08:08:10', '2017-06-03 08:08:10'),
(2, 'ط¸â€،ط¸â€‍ ط·ع¾ط¸â€¦ ط·آ§ط·آ¬ط·آ±ط·آ§ط·طŒ ط·آ¹ط¸â€¦ط¸â€‍ط¸ظ¹ط·آ§ط·ع¾ ط·آ³ط·آ§ط·آ¨ط¸â€ڑط·آ©ط·ع؛', 'Have the patient had any surgerys before?', '0', '2017-06-03 08:08:10', '2017-06-03 08:08:10'),
(3, 'ط¸â€،ط¸â€‍ ط·ع¾ط¸â€¦ ط·ع¾ط·آ±ط¸ئ’ط¸ظ¹ط·آ¨ ط¸â€¦ط·آ³ط·آ§ط¸â€¦ط¸ظ¹ط·آ± ط·آ§ط¸ث† ط·آ´ط·آ±ط·آ§ط·آ¦ط·آ­ ط·آ§ط¸ث† ط·آ§ط·آ¬ط¸â€،ط·آ²ط·آ© ط·ع¾ط·آ¹ط¸ث†ط¸ظ¹ط·آ¶ط¸ظ¹ط·آ©ط·ع؛', 'have the patient had application of screws , plates or any Prosthetic devices?', '0', '2017-06-03 08:08:10', '2017-06-03 08:08:10'),
(4, 'ط¸â€،ط¸â€‍ ط¸ظ¹ط¸ث†ط·آ¬ط·آ¯ ط·آ¬ط¸â€،ط·آ§ط·آ² ط¸â€‍ط·ع¾ط¸â€ ط·آ¸ط¸ظ¹ط¸â€¦ ط·آ¶ط·آ±ط·آ¨ط·آ§ط·ع¾ ط·آ§ط¸â€‍ط¸â€ڑط¸â€‍ط·آ¨ط·ع؛', 'Do you have a pacemaker?', '0', '2017-06-03 08:08:10', '2017-06-03 08:08:10'),
(5, 'ط¸â€،ط¸â€‍ ط¸ظ¹ط¸ث†ط·آ¬ط·آ¯ ط·ع¾ط·آ±ط¸ئ’ط¸ظ¹ط·آ¨ط·آ§ط·ع¾ ط·آ§ط·آ³ط¸â€ ط·آ§ط¸â€  ط¸â€¦ط·آ¹ط·آ¯ط¸â€ ط¸ظ¹ط·آ©ط·ع؛', 'Had the patient any metal teeth?', '0', '2017-06-03 08:08:10', '2017-06-03 08:08:10'),
(6, 'ط¸â€،ط¸â€‍ ط·آ§ط¸â€‍ط¸â€¦ط·آ±ط¸ظ¹ط·آ¶ ط·آµط·آ§ط·آ¦ط¸â€¦ط·ع؛', 'Is the patient fasting?', '0', '2017-06-03 08:08:10', '2017-06-03 08:08:10'),
(7, 'ط¸â€،ط¸â€‍ ط·ع¾ط¸â€¦ ط·آ¹ط¸â€¦ط¸â€‍ ط·آ§ط·آ´ط·آ¹ط·آ© ط·آ¨ط·آ§ط¸â€‍ط·آµط·آ¨ط·ط›ط·آ© ط·آ³ط·آ§ط·آ¨ط¸â€ڑط·آ§ط·ع؛', 'did the patient undergo  a contrast radiography before?', '0', '2017-06-03 08:08:10', '2017-06-03 08:08:10'),
(8, 'ط¸â€،ط¸â€‍ ط¸ظ¹ط¸ث†ط·آ¬ط·آ¯ ط·آ­ط·آ³ط·آ§ط·آ³ط¸ظ¹ط·آ© ط¸â€¦ط¸â€  ط·آ§ط¸ظ¹ط·آ© ط·آ¹ط¸â€ڑط·آ§ط¸â€ڑط¸ظ¹ط·آ± ط·آ§ط¸ث† ط¸â€¦ط¸ث†ط·آ§ط·آ¯ ط·ط›ط·آ°ط·آ§ط·آ¦ط¸ظ¹ط·آ©ط·ع؛', 'Is the patient sensitive to any drug or food types?', '0', '2017-06-03 08:08:10', '2017-06-03 08:08:10'),
(9, 'ط¸â€،ط¸â€‍ ط·آ§ط¸â€‍ط¸â€¦ط·آ±ط¸ظ¹ط·آ¶ ط¸ظ¹ط·آ¹ط·آ§ط¸â€ ط¸ظ¹ ط¸â€¦ط¸â€  ط·آ§ط¸â€¦ط·آ±ط·آ§ط·آ¶ ط·آ¶ط·ط›ط·آ· ط·آ§ط¸â€‍ط·آ¯ط¸â€¦ط·ع؛', 'Is the patient suffering from any of the blood pressure diseases?', '0', '2017-06-03 08:08:10', '2017-06-03 08:08:10'),
(10, 'ط¸â€،ط¸â€‍ ط·آ§ط¸â€‍ط¸â€¦ط·آ±ط¸ظ¹ط·آ¶ ط¸ظ¹ط·آ¹ط·آ§ط¸â€ ط¸ظ¹ ط¸â€¦ط¸â€  ط¸â€¦ط·آ±ط·آ¶ ط·آ§ط¸â€‍ط·آ³ط¸ئ’ط·آ±ط·ع؛', 'Is the patient suffering from Diabetes?', '0', '2017-06-03 08:08:10', '2017-06-03 08:08:10'),
(11, 'ط¸â€،ط¸â€‍ ط·آ§ط¸â€‍ط¸â€¦ط·آ±ط¸ظ¹ط·آ¶ ط¸ظ¹ط·آ¹ط·آ§ط¸â€ ط¸ظ¹ ط¸â€¦ط¸â€  ط·آ§ط¸â€¦ط·آ±ط·آ§ط·آ¶ ط·آ¨ط·آ§ط¸â€‍ط¸ئ’ط¸â€‍ط¸ظ¹ط·ع؛', 'Is the patient suffering from any of the renal diseases?', '0', '2017-06-03 08:08:10', '2017-06-03 08:08:10'),
(12, 'ط¸â€،ط¸â€‍ ط·آ§ط¸â€‍ط¸â€¦ط·آ±ط¸ظ¹ط·آ¶ ط¸ظ¹ط·آ¹ط·آ§ط¸â€ ط¸ظ¹ ط¸â€¦ط¸â€  ط·آ§ط¸â€¦ط·آ±ط·آ§ط·آ¶ ط·آ¨ط·آ§ط¸â€‍ط·ط›ط·آ¯ط·آ© ط·آ§ط¸â€‍ط·آ¯ط·آ±ط¸â€ڑط¸ظ¹ط·آ©ط·ع؛', 'Is the patient suffering from any of the thyroid gland diseases?', '0', '2017-06-03 08:08:10', '2017-06-03 08:08:10'),
(13, 'ط¸â€،ط¸â€‍ ط·آ§ط¸â€‍ط¸â€¦ط·آ±ط¸ظ¹ط·آ¶ ط¸ظ¹ط·آ§ط·آ®ط·آ° ط·آ¹ط¸â€‍ط·آ§ط·آ¬ ط¸â€‍ط¸â€‍ط·ط›ط·آ¯ط·آ© ط·آ§ط¸â€‍ط·آ¯ط·آ±ط¸â€ڑط¸ظ¹ط·آ©ط·ع؛', 'Is the patient taking any of the thyroid gland drugs?', '0', '2017-06-03 08:08:10', '2017-06-03 08:08:10'),
(14, 'ط¸â€،ط¸â€‍ ط¸ظ¹ط·ع¾ط¸â€¦ ط·آ§ط·آ®ط·آ° ط·آ¹ط¸â€‍ط·آ§ط·آ¬ ط¸ئ’ط¸ظ¹ط¸â€¦ط·آ§ط¸ث†ط¸â€°ط·ع؛', 'Is the patient taking Chemotherapy?', '0', '2017-06-03 08:08:10', '2017-06-03 08:08:10'),
(15, 'ط¸â€¦ط·آ§ ط¸â€،ط¸ظ¹ ط·آ§ط·آ¯ط¸ث†ط¸ظ¹ط·آ© ط·آ§ط¸â€‍ط·آ³ط¸ئ’ط·آ± ط·آ§ط¸â€‍ط¸â€¦ط·آ³ط·ع¾ط·آ®ط·آ¯ط¸â€¦ط·آ©ط·ع؛', 'what is the Diabetic drugs do the patient take?', '0', '2017-06-03 08:08:10', '2017-06-03 08:08:10'),
(16, 'ط¸â€¦ط·آ§ ط¸â€،ط¸ث† ط¸â€¦ط¸ظ¹ط·آ¹ط·آ§ط·آ¯ ط·آ§ط·آ®ط·آ± ط·آ¬ط¸â€‍ط·آ³ط·آ© ط¸ئ’ط¸ظ¹ط¸â€¦ط·آ§ط¸ث†ط¸ظ¹ط·ع؛', 'when was the last Chemotheraputic session?', '0', '2017-06-03 08:08:10', '2017-06-03 08:08:10'),
(17, 'ط¸â€،ط¸â€‍ ط·آ§ط¸â€‍ط¸â€¦ط·آ±ط¸ظ¹ط·آ¶ط·آ© ط·آ­ط·آ§ط¸â€¦ط¸â€‍ط·ع؛', 'Is the patient pregnant?', '1', '2017-06-03 08:08:10', '2017-06-03 08:08:10'),
(18, 'ط¸â€¦ط·آ§ ط¸â€،ط¸ث† ط¸â€¦ط¸ظ¹ط·آ¹ط·آ§ط·آ¯ ط·آ§ط·آ®ط·آ± ط·آ¯ط¸ث†ط·آ±ط·آ©ط·ع؛', 'When was your last Menstruation?', '1', '2017-06-03 08:08:10', '2017-06-03 08:08:10');

-- --------------------------------------------------------

--
-- Table structure for table `radiology_types`
--

DROP TABLE IF EXISTS `radiology_types`;
CREATE TABLE IF NOT EXISTS `radiology_types` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ar_name` text COLLATE utf8_unicode_ci NOT NULL,
  `en_name` text COLLATE utf8_unicode_ci NOT NULL,
  `type_group_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `radiology_types_type_group_id_foreign` (`type_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=235 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `radiology_types`
--

INSERT INTO `radiology_types` (`id`, `ar_name`, `en_name`, `type_group_id`, `created_at`, `updated_at`) VALUES
(1, 'ط·آ§ط¸â€‍ط¸â€¦ط·آ®', 'Brain', 1, '2017-06-03 08:08:00', '2017-06-03 08:08:00'),
(2, 'ط·آ´ط·آ±ط·آ§ط¸ظ¹ط¸ظ¹ط¸â€  ط·آ§ط¸â€‍ط¸â€¦ط·آ®', 'MRA', 1, '2017-06-03 08:08:00', '2017-06-03 08:08:00'),
(3, 'ط·آ£ط¸ث†ط·آ±ط·آ¯ط·آ© ط·آ§ط¸â€‍ط¸â€¦ط·آ® ط·آ¨ط·آ§ط¸â€‍ط·آµط·آ¨ط·ط›ط¸â€،', 'MRV', 1, '2017-06-03 08:08:00', '2017-06-03 08:08:00'),
(4, 'ط·آ§ط¸â€‍ط¸â€¦ط·آ® ط¸ث† ط·آ´ط·آ±ط·آ§ط¸ظ¹ط¸ظ¹ط¸â€  ط·آ§ط¸â€‍ط¸â€¦ط·آ®', 'Brain & MRA', 1, '2017-06-03 08:08:00', '2017-06-03 08:08:00'),
(5, 'ط·آ§ط¸â€‍ط¸â€¦ط·آ® ط¸ث† ط·آ£ط¸ث†ط·آ±ط·آ¯ط·آ© ط·آ§ط¸â€‍ط¸â€¦ط·آ® ط·آ¨ط·آ§ط¸â€‍ط·آµط·آ¨ط·ط›ط¸â€،', 'Brain & MRV', 1, '2017-06-03 08:08:00', '2017-06-03 08:08:00'),
(6, 'ط·آ´ط·آ±ط·آ§ط¸ظ¹ط¸ظ¹ط¸â€  ط¸ث† ط·آ£ط¸ث†ط·آ±ط·آ¯ط·آ© ط·آ§ط¸â€‍ط¸â€¦ط·آ® ط·آ¨ط·آ§ط¸â€‍ط·آµط·آ¨ط·ط›ط¸â€،', 'MRA & MRV', 1, '2017-06-03 08:08:00', '2017-06-03 08:08:00'),
(7, 'ط·آ§ط¸â€‍ط¸â€¦ط·آ® ط¸ث†ط·آ´ط·آ±ط·آ§ط¸ظ¹ط¸ظ¹ط¸â€  ط·آ§ط¸â€‍ط¸â€¦ط·آ® ط¸ث†ط·آ§ط¸ث†ط·آ±ط·آ¯ط·آ© ط·آ§ط¸â€‍ط¸â€¦ط·آ® ط·آ¨ط·آ§ط¸â€‍ط·آµط·آ¨ط·ط›ط¸â€،', 'Brain & MRA & V', 1, '2017-06-03 08:08:00', '2017-06-03 08:08:00'),
(8, 'ط·آ§ط¸â€‍ط·آ¹ط¸â€¦ط¸ث†ط·آ¯ ط·آ§ط¸â€‍ط¸ظ¾ط¸â€ڑط·آ±ط¸â€° ط·آ¨ط·آ§ط¸â€‍ط¸ئ’ط·آ§ط¸â€¦ط¸â€‍', 'Whole Spine ', 1, '2017-06-03 08:08:00', '2017-06-03 08:08:00'),
(9, 'ط·آ§ط¸â€‍ط·ط›ط·آ¯ط·آ© ط·آ§ط¸â€‍ط¸â€ ط·آ®ط·آ§ط¸â€¦ط¸ظ¹ط·آ© ( ط·آ¨ط·آ§ط¸â€‍ط·آµط·آ¨ط·ط›ط·آ© )', 'Sella Turcica', 1, '2017-06-03 08:08:00', '2017-06-03 08:08:00'),
(10, 'ط·آ§ط¸â€‍ط·آ¹ط¸ظ¹ط¸â€  ( ط·آ¨ط·آ§ط¸â€‍ط·آµط·آ¨ط·ط›ط·آ© )', 'Orbits', 1, '2017-06-03 08:08:00', '2017-06-03 08:08:00'),
(11, 'ط·آ§ط¸â€‍ط·آ£ط·آ°ط¸â€  ( ط·آ¨ط·آ§ط¸â€‍ط·آµط·آ¨ط·ط›ط·آ© )', 'Petrous Bone ( CPA )', 1, '2017-06-03 08:08:00', '2017-06-03 08:08:00'),
(12, 'ط¸â€¦ط¸ظ¾ط·آµط¸â€‍ط¸â€° ط·آ§ط¸â€‍ط¸ظ¾ط¸ئ’', 'Temporomandibular Joints ( TMJ )', 1, '2017-06-03 08:08:00', '2017-06-03 08:08:00'),
(13, 'ط·آ§ط¸â€‍ط·آ¬ط¸ظ¹ط¸ث†ط·آ¨ ط·آ§ط¸â€‍ط·آ£ط¸â€ ط¸ظ¾ط¸ظ¹ط·آ© ( ط·آ¨ط·آ§ط¸â€‍ط·آµط·آ¨ط·ط›ط·آ© )', 'Paranasal Sinuses', 1, '2017-06-03 08:08:00', '2017-06-03 08:08:00'),
(14, 'ط·آ§ط¸â€‍ط·آ¹ط¸â€ ط¸â€ڑ ط¸ث† ط·آ§ط¸â€‍ط·آ­ط¸â€ ط·آ¬ط·آ±ط·آ© ( ط·آ¨ط·آ§ط¸â€‍ط·آµط·آ¨ط·ط›ط·آ© )', 'Neck & Larynx', 1, '2017-06-03 08:08:00', '2017-06-03 08:08:00'),
(15, 'ط·آ§ط¸â€‍ط·آ¨ط¸â€‍ط·آ¹ط¸ث†ط¸â€¦ ط·آ§ط¸â€‍ط·آ£ط¸â€ ط¸ظ¾ط¸â€° ط·آ¨ط·آ§ط¸â€‍ط·آµط·آ¨ط·ط›ط¸â€،', 'Naso Pharynx', 1, '2017-06-03 08:08:00', '2017-06-03 08:08:00'),
(16, 'ط·آ§ط¸â€‍ط¸â€‍ط·آ³ط·آ§ط¸â€ ', 'Tongue', 1, '2017-06-03 08:08:00', '2017-06-03 08:08:00'),
(17, 'ط·آ§ط¸â€‍ط¸ظ¾ط¸â€ڑط·آ±ط·آ§ط·ع¾ ط·آ§ط¸â€‍ط·آ¹ط¸â€ ط¸â€ڑط¸ظ¹ط·آ©', 'Cervical Spine ( CS )', 1, '2017-06-03 08:08:00', '2017-06-03 08:08:00'),
(18, 'ط·آ§ط¸â€‍ط¸ظ¾ط¸â€ڑط·آ±ط·آ§ط·ع¾ ط·آ§ط¸â€‍ط·آ¸ط¸â€،ط·آ±ط¸ظ¹ط·آ©', 'Dorsal Spine ( DS )', 1, '2017-06-03 08:08:00', '2017-06-03 08:08:00'),
(19, 'ط·آ§ط¸â€‍ط¸ظ¾ط¸â€ڑط·آ±ط·آ§ط·ع¾ ط·آ§ط¸â€‍ط¸â€ڑط·آ·ط¸â€ ط¸ظ¹ط·آ© ط·آ§ط¸â€‍ط·آ¸ط¸â€،ط·آ±ط¸ظ¹ط·آ©', 'Dorsolumbar', 1, '2017-06-03 08:08:00', '2017-06-03 08:08:00'),
(20, 'ط·آ§ط¸â€‍ط¸ظ¾ط¸â€ڑط·آ±ط·آ§ط·ع¾ ط·آ§ط¸â€‍ط¸â€ڑط·آ·ط¸â€ ط¸ظ¹ط·آ©', 'Lumbar Spine ( L.S.S )', 1, '2017-06-03 08:08:00', '2017-06-03 08:08:00'),
(21, 'ط·آ§ط¸â€‍ط¸ئ’ط·ع¾ط¸ظ¾', 'Shoulder', 1, '2017-06-03 08:08:01', '2017-06-03 08:08:01'),
(22, 'ط¸â€¦ط¸ظ¾ط·آµط¸â€‍ط¸â€° ط·آ§ط¸â€‍ط·آ­ط¸ث†ط·آ¶', 'Both Hips', 1, '2017-06-03 08:08:01', '2017-06-03 08:08:01'),
(23, 'ط·آ§ط¸â€‍ط¸ئ’ط·آ§ط·آ­ط¸â€‍', 'Ankle', 1, '2017-06-03 08:08:01', '2017-06-03 08:08:01'),
(24, 'ط·آ§ط¸â€‍ط·آ³ط¸â€¦ط·آ§ط¸â€ ط·آ©', 'Calf', 1, '2017-06-03 08:08:01', '2017-06-03 08:08:01'),
(25, 'ط·آ§ط¸â€‍ط¸â€ڑط·آ¯ط¸â€¦', 'Foot', 1, '2017-06-03 08:08:01', '2017-06-03 08:08:01'),
(26, 'ط·آ£ط·آµط·آ§ط·آ¨ط·آ¹ ط·آ§ط¸â€‍ط¸ظ¹ط·آ¯', 'Fingers', 1, '2017-06-03 08:08:01', '2017-06-03 08:08:01'),
(27, 'ط·آ£ط·آµط·آ§ط·آ¨ط·آ¹ ط·آ§ط¸â€‍ط¸â€ڑط·آ¯ط¸â€¦', 'Toes', 1, '2017-06-03 08:08:01', '2017-06-03 08:08:01'),
(28, 'ط·آ§ط¸â€‍ط·آ³ط·آ§ط¸â€ڑ', 'Leg', 1, '2017-06-03 08:08:01', '2017-06-03 08:08:01'),
(29, 'ط·آ¹ط·آ¸ط¸â€¦ط·آ© ط·آ§ط¸â€‍ط¸ظ¾ط·آ®ط·آ°', 'Femur', 1, '2017-06-03 08:08:01', '2017-06-03 08:08:01'),
(30, 'ط·آ§ط¸â€‍ط¸ظ¾ط·آ®ط·آ°', 'Thigh', 1, '2017-06-03 08:08:01', '2017-06-03 08:08:01'),
(31, 'ط·آ§ط¸â€‍ط¸ظ¹ط·آ¯', 'Hand', 1, '2017-06-03 08:08:01', '2017-06-03 08:08:01'),
(32, 'ط·آ§ط¸â€‍ط·آ±ط·آ³ط·ط›', 'Wrist', 1, '2017-06-03 08:08:01', '2017-06-03 08:08:01'),
(33, 'ط·آ§ط¸â€‍ط¸ئ’ط¸ث†ط·آ¹', 'Elbow', 1, '2017-06-03 08:08:01', '2017-06-03 08:08:01'),
(34, 'ط·آ§ط¸â€‍ط·آ³ط·آ§ط·آ¹ط·آ¯', 'Arm Or Forearm', 1, '2017-06-03 08:08:01', '2017-06-03 08:08:01'),
(35, 'ط·آ§ط¸â€‍ط·آ¹ط·آ¶ط·آ¯', 'Humerus', 1, '2017-06-03 08:08:01', '2017-06-03 08:08:01'),
(36, 'ط·آ§ط¸â€‍ط·آ­ط¸ث†ط·آ¶ ( ط·آ¹ط·آ¸ط·آ§ط¸â€¦ )', 'Pelvis ( Bone ) - Both Hips', 1, '2017-06-03 08:08:01', '2017-06-03 08:08:01'),
(37, 'ط·آ§ط¸â€‍ط¸â€ڑط¸â€‍ط·آ¨ ', 'Heart', 1, '2017-06-03 08:08:01', '2017-06-03 08:08:01'),
(38, 'ط·آ§ط¸â€‍ط·آ§ط·آ·ط·آ±ط·آ§ط¸ظ¾ ط·آ§ط¸â€‍ط·آ³ط¸ظ¾ط¸â€‍ط¸ظ¹ط·آ©', 'Angio Lower Limbs', 1, '2017-06-03 08:08:01', '2017-06-03 08:08:01'),
(39, 'ط·آ´ط·آ±ط·آ§ط¸ظ¹ط¸ظ¹ط¸â€  ط·آ§ط¸â€‍ط·آ±ط¸â€ڑط·آ¨ط·آ©', 'Angio Carotid', 1, '2017-06-03 08:08:01', '2017-06-03 08:08:01'),
(40, 'ط·آ´ط·آ±ط·آ§ط¸ظ¹ط¸ظ¹ط¸â€  ط·آ§ط¸â€‍ط¸ئ’ط¸â€‍ط¸â€°', 'Angio Renal', 1, '2017-06-03 08:08:01', '2017-06-03 08:08:01'),
(41, 'ط·آ§ط¸â€‍ط·آ´ط·آ±ط¸ظ¹ط·آ§ط¸â€  ط·آ§ط¸â€‍ط·آ±ط·آ¦ط¸ث†ط¸â€° ', 'Angio Palmonary', 1, '2017-06-03 08:08:01', '2017-06-03 08:08:01'),
(42, 'ط·آ§ط¸â€‍ط·آ·ط¸ظ¹ط¸ظ¾ ط·آ§ط¸â€‍ط¸â€¦ط·آ®ط¸â€°', 'Spectroscopy', 1, '2017-06-03 08:08:01', '2017-06-03 08:08:01'),
(43, 'ط·آ§ط¸â€‍ط·ع¾ط·آµط¸ث†ط¸ظ¹ط·آ± ط·آ¨ط·آ§ط¸â€‍ط·آ±ط¸â€ ط¸ظ¹ط¸â€  ط·آ§ط¸â€‍ط¸â€¦ط·ط›ط¸â€ ط·آ§ط·آ·ط¸ظ¹ط·آ³ط¸ظ¹ ط·آ§ط¸â€‍ط·آ§ط¸â€ ط·ع¾ط·آ´ط·آ§ط·آ±ط¸ظ¹ ط¸â€‍ط¸â€‍ط·آ§ط¸â€‍ط¸ظ¹ط·آ§ط¸ظ¾ ط·آ§ط¸â€‍ط·آ¹ط·آµط·آ¨ط¸ظ¹ط¸â€،', 'Tractography', 1, '2017-06-03 08:08:02', '2017-06-03 08:08:02'),
(44, 'ط·آ§ط¸â€‍ط¸â€ڑط¸â€ ط¸ث†ط·آ§ط·ع¾ ط·آ§ط¸â€‍ط¸â€¦ط·آ±ط·آ§ط·آ±ط¸ظ¹ط·آ©', 'MRCP', 1, '2017-06-03 08:08:02', '2017-06-03 08:08:02'),
(45, 'ط·آ§ط¸â€‍ط¸â€¦ط·آ³ط·آ§ط¸â€‍ط¸ئ’ ط·آ§ط¸â€‍ط·آ¨ط¸ث†ط¸â€‍ط¸ظ¹ط·آ©', 'MRU', 1, '2017-06-03 08:08:02', '2017-06-03 08:08:02'),
(46, 'ط·آ§ط¸â€‍ط·آ¨ط¸â€ ط¸ئ’ط·آ±ط¸ظ¹ط·آ§ط·آ³', 'Pancreas', 1, '2017-06-03 08:08:02', '2017-06-03 08:08:02'),
(47, 'ط·آ§ط¸â€‍ط¸ئ’ط¸â€‍ط¸â€°', 'Kidneys', 1, '2017-06-03 08:08:02', '2017-06-03 08:08:02'),
(48, 'ط·آ§ط¸â€‍ط¸ئ’ط·آ¨ط·آ¯', 'Triphasic Liver', 1, '2017-06-03 08:08:02', '2017-06-03 08:08:02'),
(49, 'ط·آ§ط¸â€‍ط·آ§ط¸â€¦ط·آ¹ط·آ§ط·طŒ ط·آ§ط¸â€‍ط·آ¯ط¸â€ڑط¸ظ¹ط¸â€ڑط·آ©', 'Entrography', 1, '2017-06-03 08:08:02', '2017-06-03 08:08:02'),
(50, 'ط·آ§ط¸â€‍ط·آ«ط·آ¯ط¸â€°', 'Mammography', 1, '2017-06-03 08:08:02', '2017-06-03 08:08:02'),
(51, 'ط·ع¾ط·آ¯ط¸ظ¾ط¸â€ڑ ط·آ³ط·آ§ط·آ¦ط¸â€‍ ط·آ§ط¸â€‍ط¸â€ ط·آ®ط·آ§ط·آ¹ ط·آ§ط¸â€‍ط·آ´ط¸ث†ط¸ئ’ط¸ظ¹', 'Brain CSF Flow', 1, '2017-06-03 08:08:02', '2017-06-03 08:08:02'),
(52, 'ط¸â€ ط·آ¶ط·آ­ ط·آ§ط¸â€‍ط·آ¯ط¸â€¦ ط·آ¨ط·آ§ط¸â€‍ط¸â€¦ط·آ®', 'Perfusion', 1, '2017-06-03 08:08:02', '2017-06-03 08:08:02'),
(53, 'ط¸â€ ط·آ¶ط·آ­ ط·آ§ط¸â€‍ط·آ¯ط¸â€¦ ط·آ¨ط·آ§ط¸â€‍ط¸â€¦ط·آ® ط¸ث† ط·آ§ط¸â€‍ط¸â€¦ط·آ³ط·آ­ ط·آ§ط¸â€‍ط·آ·ط¸ظ¹ط¸ظ¾ط¸ظ¹', 'Perfusion & Spectroscopy', 1, '2017-06-03 08:08:02', '2017-06-03 08:08:02'),
(54, 'ط·آ§ط¸â€‍ط¸â€¦ط·آ³ط·آ­ ط·آ§ط¸â€‍ط·آ·ط¸ظ¹ط¸ظ¾ط¸ظ¹ ط¸ث† ط·آ§ط¸â€‍ط¸â€¦ط·آ®', 'Brain & Spectroscopy', 1, '2017-06-03 08:08:02', '2017-06-03 08:08:02'),
(55, 'ط¸â€ ط·آ¶ط·آ­ ط·آ§ط¸â€‍ط·آ¯ط¸â€¦ ', 'Perfusion &Tractography', 1, '2017-06-03 08:08:02', '2017-06-03 08:08:02'),
(56, 'ط·آ§ط¸â€‍ط·ع¾ط·آµط¸ث†ط¸ظ¹ط·آ± ط·آ¨ط·آ§ط¸â€‍ط·آ±ط¸â€ ط¸ظ¹ط¸â€  ط·آ§ط¸â€‍ط¸â€¦ط·ط›ط¸â€ ط·آ§ط·آ·ط¸ظ¹ط·آ³ط¸ظ¹ ط·آ§ط¸â€‍ط·آ§ط¸â€ ط·ع¾ط·آ´ط·آ§ط·آ±ط¸ظ¹ ط¸â€‍ط¸â€‍ط·آ§ط¸â€‍ط¸ظ¹ط·آ§ط¸ظ¾ ط·آ§ط¸â€‍ط·آ¹ط·آµط¸ظ¹ط·آ¨ط¸â€، ط¸ث† ط·آ§ط¸â€‍ط¸â€¦ط·آ®', 'Brain &Tractography', 1, '2017-06-03 08:08:02', '2017-06-03 08:08:02'),
(57, 'ط·آ§ط¸â€‍ط·ع¾ط·آµط¸ث†ط¸ظ¹ط·آ± ط·آ¨ط·آ§ط¸â€‍ط·آ±ط¸â€ ط¸ظ¹ط¸â€  ط·آ§ط¸â€‍ط¸â€¦ط·ط›ط¸â€ ط·آ§ط·آ·ط¸ظ¹ط·آ³ط¸ظ¹ ط·آ§ط¸â€‍ط·آ§ط¸â€ ط·ع¾ط·آ´ط·آ§ط·آ±ط¸ظ¹ ط¸â€‍ط¸â€‍ط·آ§ط¸â€‍ط¸ظ¹ط·آ§ط¸ظ¾ ط·آ§ط¸â€‍ط·آ¹ط·آµط¸ظ¹ط·آ¨ط¸â€، ط¸ث† ط·آ§ط¸â€‍ط¸â€¦ط·آ³ط·آ­ ط·آ§ط¸â€‍ط·آ·ط¸ظ¹ط¸ظ¾ط¸ظ¹', 'Tractography & Spectroscopy', 1, '2017-06-03 08:08:02', '2017-06-03 08:08:02'),
(58, 'ط¸ظ¾ط·آ­ط·آµ ط·آ§ط¸â€‍ط·آ­ط¸ث†ط·آ¶ ط·آ§ط¸â€‍ط·آ¯ط¸ظ¹ط¸â€ ط·آ§ط¸â€¦ط¸ظ¹ط¸ئ’ط¸ظ¹ ط·آ¨ط·آ§ط¸â€‍ط·آµط·آ¨ط·ط›ط¸â€،', 'Dynamic Pelvis', 1, '2017-06-03 08:08:02', '2017-06-03 08:08:02'),
(59, 'ط·آ­ط·آ²ط¸â€¦ط¸â€، ط¸ظ¾ط·آ­ط·آµ ط·آ§ط¸â€‍ط·آ³ط¸ئ’ط·ع¾ط¸â€، ط·آ§ط¸â€‍ط·آ¯ط¸â€¦ط·آ§ط·ط›ط¸ظ¹ط·آ©', 'Stroke Package', 1, '2017-06-03 08:08:02', '2017-06-03 08:08:02'),
(60, 'ط¸ظ¾ط·آ­ط·آµ ط·آ§ط¸â€‍ط·آ¶ط¸ظ¾ط¸ظ¹ط·آ±ط·آ© ط·آ§ط¸â€‍ط·آ¹ط·آ¶ط·آ¯ط¸ظ¹ط·آ©', 'Brachil Plexus', 1, '2017-06-03 08:08:02', '2017-06-03 08:08:02'),
(61, 'ط¸ظ¾ط·آ­ط·آµ ط·ع¾ط¸â€ڑط·آ§ط·آ·ط·آ¹ ط·آ§ط¸â€‍ط·آ¹ط¸â€ ط¸â€ڑ ط¸â€¦ط·آ¹ ط·آ§ط¸â€‍ط·آ¬ط¸â€¦ط·آ¬ط¸â€¦ط·آ©', 'Cranio c-s', 1, '2017-06-03 08:08:02', '2017-06-03 08:08:02'),
(62, 'ط·آ§ط¸â€‍ط·آ¨ط·آ±ط¸ث†ط·آ³ط·ع¾ط·آ§ط·ع¾ط·آ§', 'Prostate', 1, '2017-06-03 08:08:02', '2017-06-03 08:08:02'),
(63, 'ط¸ظ¾ط·آ­ط·آµ ط·آ§ط¸â€‍ط¸â€¦ط·آ³ط·ع¾ط¸â€ڑط¸ظ¹ط¸â€¦', 'Rectum', 1, '2017-06-03 08:08:02', '2017-06-03 08:08:02'),
(64, 'ط¸ث†ط·آ¹ط·آ§ط·طŒ ط·آ§ط¸â€‍ط·آ®ط·آµط¸ظ¹ط·ع¾ط¸ظ¹ط¸â€ ', 'Scrotum', 1, '2017-06-03 08:08:02', '2017-06-03 08:08:02'),
(65, 'ط¸ظ¾ط·آ­ط·آµ ط·آ§ط¸â€‍ط¸ئ’ط¸â€‍ط¸ظ¹ط·ع¾ط¸ظ¹ط¸â€ ', 'Renogram', 1, '2017-06-03 08:08:02', '2017-06-03 08:08:02'),
(66, 'ط·آ¬ط·آ¯ط·آ§ط·آ± ط·آ§ط¸â€‍ط·آµط·آ¯ط·آ±', 'Chest Wall', 1, '2017-06-03 08:08:02', '2017-06-03 08:08:02'),
(67, 'ط·آ§ط¸â€‍ط·آ­ط¸ث†ط·آ¶', 'Pelvis', 1, '2017-06-03 08:08:03', '2017-06-03 08:08:03'),
(68, 'ط¸â€ ط·آ§ط·آµط¸ث†ط·آ±', 'Fistula', 1, '2017-06-03 08:08:03', '2017-06-03 08:08:03'),
(69, 'ط·آ§ط¸â€‍ط·آ¹ط·آ¸ط¸â€¦ ط·آ§ط¸â€‍ط¸ئ’ط·ع¾ط¸ظ¾ط¸ظ¹', 'Scapula', 1, '2017-06-03 08:08:03', '2017-06-03 08:08:03'),
(70, 'ط¸ظ¾ط·آ­ط·آµ ط¸â€¦ط¸ظ¾ط·آµط¸â€‍ ط·آ§ط¸â€‍ط·آ¹ط·آ¬ط·آ²ط¸ظ¹ ط·آ§ط¸â€‍ط·آ­ط·آ±ط¸â€ڑط¸ظ¾ط¸ظ¹', 'Sacroilic', 1, '2017-06-03 08:08:03', '2017-06-03 08:08:03'),
(71, 'ط·آ¹ط·آ¸ط¸â€¦ ط·آ§ط¸â€‍ط·آ¹ط·آ¶ط·آ¯', 'Humerus', 1, '2017-06-03 08:08:03', '2017-06-03 08:08:03'),
(72, 'ط·آ¹ط·آ¸ط¸â€¦ط·آ© ط·آ§ط¸â€‍ط·آ¹ط·آ¬ط·آ²', 'Sacrum', 1, '2017-06-03 08:08:03', '2017-06-03 08:08:03'),
(73, 'ط¸ظ¾ط·آ­ط·آµ ط¸â€¦ط¸ظ¾ط·آµط¸â€‍ ط·آ§ط¸â€‍ط¸ئ’ط·ع¾ط¸ظ¾ ط·آ¨ط·آ§ط¸â€‍ط·آµط·آ¨ط·ط›ط·آ© ', 'Shoulder Arthrogram', 1, '2017-06-03 08:08:03', '2017-06-03 08:08:03'),
(74, 'ط¸ظ¾ط·آ­ط·آµ ط¸â€¦ط¸ظ¾ط·آµط¸â€‍ ط·آ§ط¸â€‍ط·آ±ط·آ³ط·ط› ط·آ¨ط·آ§ط¸â€‍ط·آµط·آ¨ط·ط›ط·آ©', 'Wrist Arthrogram', 1, '2017-06-03 08:08:03', '2017-06-03 08:08:03'),
(75, 'ط·آ§ط¸â€‍ط·ع¾ط·آµط¸ث†ط¸ظ¹ط·آ± ط·آ§ط¸â€‍ط·آ¹ط·آµط·آ¨ط¸ظ¹ ط·آ§ط¸â€‍ط¸ئ’ط¸â€¦ط¸ظ¹', 'neuro Quant', 1, '2017-06-03 08:08:03', '2017-06-03 08:08:03'),
(76, 'ط·آ³ط¸ظ¹ط¸â€‍ط·آ§ط¸â€  ط·آ³ط·آ§ط·آ¦ط¸â€‍ ط·آ§ط¸â€‍ط¸â€ ط·آ®ط·آ§ط·آ¹ ط·آ§ط¸â€‍ط·آ´ط¸ث†ط¸ئ’ط¸ظ¹ ط¸â€¦ط¸â€  ط·آ§ط¸â€‍ط·آ§ط¸â€ ط¸ظ¾', 'CSF Rhinorrhea', 1, '2017-06-03 08:08:03', '2017-06-03 08:08:03'),
(77, 'ط·آ²ط·آ±ط·آ¹ ط¸â€ڑط¸ث†ط¸â€ڑط·آ¹ط·آ© ط·آ¨ط·آ§ط¸â€‍ط·آ§ط·آ°ط¸â€ ', 'Cochlear Implant', 1, '2017-06-03 08:08:03', '2017-06-03 08:08:03'),
(78, ' ط¸ظ¾ط·آ®ط·آµ ط·آ§ط¸â€‍ط¸ث†ط·آ±ط¸â€¦ ط·آ§ط¸â€‍ط¸ئ’ط¸ث†ط¸â€‍ط¸ظ¹ط·آ³ط·ع¾ط·آ±ط¸ث†ط¸â€‍ط¸ظ¹ ط·آ¨ط·آ§ط¸â€‍ط·آ§ط·آ°ط¸â€ ', 'Cholesteatoma', 1, '2017-06-03 08:08:03', '2017-06-03 08:08:03'),
(79, 'ط·آµط·آ¨ط·ط›ط¸â€، ط·آ§ط¸â€‍ط·آ¬ط·آ§ط·آ¯ط¸ث†ط¸â€‍ط¸ظ¹ط¸â€ ط¸ظ¹ط·آ§ط¸â€¦', 'Contrast Medium ( Gadolinum )', 1, '2017-06-03 08:08:03', '2017-06-03 08:08:03'),
(80, 'ط¸â€¦ط¸â€،ط·آ¯ط¸â€°ط·طŒ ط·آ¹ط·آ§ط¸â€¦ ط·آ±ط¸â€ ط¸ظ¹ط¸â€  ط¸â€¦ط·ط›ط¸â€ ط·آ§ط·آ·ط¸ظ¹ط·آ³ط¸â€°', '', 1, '2017-06-03 08:08:03', '2017-06-03 08:08:03'),
(81, 'ط·آ§ط¸â€‍ط¸â€¦ط·آ®', 'Brain', 2, '2017-06-03 08:08:03', '2017-06-03 08:08:03'),
(82, 'ط·آ´ط·آ±ط·آ§ط¸ظ¹ط¸ظ¹ط¸â€  ط·آ§ط¸â€‍ط¸â€¦ط·آ®', 'MRA', 2, '2017-06-03 08:08:03', '2017-06-03 08:08:03'),
(83, 'ط·آ£ط¸ث†ط·آ±ط·آ¯ط·آ© ط·آ§ط¸â€‍ط¸â€¦ط·آ®', 'MRV ', 2, '2017-06-03 08:08:03', '2017-06-03 08:08:03'),
(84, 'ط·آ§ط¸â€‍ط¸â€¦ط·آ® ط¸ث† ط·آ´ط·آ±ط·آ§ط¸ظ¹ط¸ظ¹ط¸â€  ط·آ§ط¸â€‍ط¸â€¦ط·آ®', 'Brain & MRA ', 2, '2017-06-03 08:08:03', '2017-06-03 08:08:03'),
(85, 'ط·آ§ط¸â€‍ط¸â€¦ط·آ® ط¸ث† ط·آ£ط¸ث†ط·آ±ط·آ¯ط·آ© ط·آ§ط¸â€‍ط¸â€¦ط·آ® ط·آ¨ط·آ§ط¸â€‍ط·آµط·آ¨ط·ط›ط¸â€،', 'Brain&MRV ', 2, '2017-06-03 08:08:03', '2017-06-03 08:08:03'),
(86, 'ط·آ´ط·آ±ط·آ§ط¸ظ¹ط¸ظ¹ط¸â€  ط¸ث† ط·آ£ط¸ث†ط·آ±ط·آ¯ط·آ© ط·آ§ط¸â€‍ط¸â€¦ط·آ® ط·آ¨ط·آ§ط¸â€‍ط·آµط·آ¨ط·ط›ط¸â€،', 'MRA&MRV', 2, '2017-06-03 08:08:03', '2017-06-03 08:08:03'),
(87, 'ط·آ§ط¸â€‍ط¸â€¦ط·آ® ط¸ث†ط·آ´ط·آ±ط·آ§ط¸ظ¹ط¸ظ¹ط¸â€  ط·آ§ط¸â€‍ط¸â€¦ط·آ® ط¸ث†ط·آ§ط¸ث†ط·آ±ط·آ¯ط·آ© ط·آ§ط¸â€‍ط¸â€¦ط·آ®', 'Brain & MRA & V', 2, '2017-06-03 08:08:03', '2017-06-03 08:08:03'),
(88, 'ط·آ§ط¸â€‍ط·آ¹ط¸â€¦ط¸ث†ط·آ¯ ط·آ§ط¸â€‍ط¸ظ¾ط¸â€ڑط·آ±ط¸â€° ط·آ¨ط·آ§ط¸â€‍ط¸ئ’ط·آ§ط¸â€¦ط¸â€‍', 'Whole Spine', 2, '2017-06-03 08:08:03', '2017-06-03 08:08:03'),
(89, 'ط·آ§ط¸â€‍ط·ط›ط·آ¯ط·آ© ط·آ§ط¸â€‍ط¸â€ ط·آ®ط·آ§ط¸â€¦ط¸ظ¹ط·آ© ', 'Sella trucica', 2, '2017-06-03 08:08:03', '2017-06-03 08:08:03'),
(90, 'ط·آ§ط¸â€‍ط·آ¹ط¸ظ¹ط¸â€  ', 'Orbits', 2, '2017-06-03 08:08:03', '2017-06-03 08:08:03'),
(91, 'ط·آ§ط¸â€‍ط¸ظ¾ط¸â€ڑط·آ±ط·آ§ط·ع¾ ط·آ§ط¸â€‍ط·آ¹ط¸â€ ط¸â€ڑط¸ظ¹ط·آ©', 'Cervical Spine', 2, '2017-06-03 08:08:04', '2017-06-03 08:08:04'),
(92, 'ط·آ§ط¸â€‍ط¸ظ¾ط¸â€ڑط·آ±ط·آ§ط·ع¾ ط·آ§ط¸â€‍ط¸â€ڑط·آ·ط¸â€ ط¸ظ¹ط·آ©', 'Lumbar Spine', 2, '2017-06-03 08:08:04', '2017-06-03 08:08:04'),
(93, 'ط¸â€¦ط¸ظ¾ط·آµط¸â€‍ط¸â€° ط·آ§ط¸â€‍ط·آ­ط¸ث†ط·آ¶', 'Both Hips', 2, '2017-06-03 08:08:04', '2017-06-03 08:08:04'),
(94, 'ط·آ§ط¸â€‍ط·آ±ط¸ئ’ط·آ¨ط·آ©', 'Knee ( One Side )', 2, '2017-06-03 08:08:04', '2017-06-03 08:08:04'),
(95, 'ط·آ§ط¸â€‍ط¸ئ’ط·ع¾ط¸ظ¾', 'Shoulder ( One Side )', 2, '2017-06-03 08:08:04', '2017-06-03 08:08:04'),
(96, 'ط·آ§ط¸â€‍ط¸ئ’ط·آ§ط·آ­ط¸â€‍', 'Ankle ( One Side )', 2, '2017-06-03 08:08:04', '2017-06-03 08:08:04'),
(97, 'ط·آ§ط¸â€‍ط·آ³ط·آ§ط¸â€ڑ ط·آ§ط¸ث† ط·آ§ط¸â€‍ط·آ³ط¸â€¦ط·آ§ط¸â€ ط·آ©', 'Leg Or Calf ( One Side )', 2, '2017-06-03 08:08:04', '2017-06-03 08:08:04'),
(98, 'ط·آ§ط¸â€‍ط¸â€ڑط·آ¯ط¸â€¦ ط·آ§ط¸ث† ط·آ§ط·آµط·آ¨ط·آ¹ ط·آ§ط¸â€‍ط¸â€ڑط·آ¯ط¸â€¦', 'foot Or Toes ( One Side )', 2, '2017-06-03 08:08:04', '2017-06-03 08:08:04'),
(99, 'ط·آ§ط¸â€‍ط¸ظ¾ط·آ®ط·آ° ', 'Thigh Or Femur ( One Side )', 2, '2017-06-03 08:08:04', '2017-06-03 08:08:04'),
(100, 'ط·آ§ط¸â€‍ط¸ئ’ط¸ث†ط·آ¹', 'Elbow ( One Side )', 2, '2017-06-03 08:08:04', '2017-06-03 08:08:04'),
(101, 'ط·آ§ط¸â€‍ط·آ³ط·آ§ط·آ¹ط·آ¯', 'Forarm ( One Side )', 2, '2017-06-03 08:08:04', '2017-06-03 08:08:04'),
(102, 'ط·آ§ط¸â€‍ط·آ±ط·آ³ط·ط›', 'Wrist ( One Side )', 2, '2017-06-03 08:08:04', '2017-06-03 08:08:04'),
(103, 'ط·آ§ط¸â€‍ط¸ظ¹ط·آ¯ ط·آ§ط¸ث† ط·آ§ط·آµط·آ¨ط·آ¹ ط·آ§ط¸â€‍ط¸ظ¹ط·آ¯', 'Hand Or Fingers ( One Side )', 2, '2017-06-03 08:08:04', '2017-06-03 08:08:04'),
(104, 'ط·آ§ط·آ³ط·ع¾ط¸ئ’ط¸â€¦ط·آ§ط¸â€‍ ط¸â€¦ط·آ±ط·آ­ط¸â€‍ط·آ© ط¸ث†ط·آ§ط·آ­ط·آ¯ط·آ©', 'ط¸ظ¾ط·آ­ط·آµ ط·آ§ط·آ¶ط·آ§ط¸ظ¾ط¸ظ¹ T2 ', 2, '2017-06-03 08:08:04', '2017-06-03 08:08:04'),
(105, 'ط·آ§ط·آ³ط·ع¾ط·آ®ط·آ¯ط·آ§ط¸â€¦ ط·آµط·آ¨ط·ط›ط·آ© ط¸ظ¾ط¸â€° ط·آ§ط¸â€‍ط¸ظ¾ط·آ­ط·آµ', 'contrast mri', 2, '2017-06-03 08:08:04', '2017-06-03 08:08:04'),
(106, 'ط¸â€¦ط¸â€،ط·آ¯ط¸â€°ط·طŒ ط·آ¹ط·آ§ط¸â€¦ ط·آ±ط¸â€ ط¸ظ¹ط¸â€  ط¸â€¦ط·ط›ط¸â€ ط·آ§ط·آ·ط¸ظ¹ط·آ³ط¸â€° ', ' ', 2, '2017-06-03 08:08:04', '2017-06-03 08:08:04'),
(107, 'ط·آ§ط¸â€‍ط¸â€¦ط·آ® ط·آ¨ط·آ¯ط¸ث†ط¸â€  ط·آµط·آ¨ط·ط›ط·آ© ', 'Brain non contrast', 3, '2017-06-03 08:08:04', '2017-06-03 08:08:04'),
(108, 'ط·آ§ط¸â€‍ط¸â€¦ط·آ® ط·آ¨ط·آ§ط¸â€‍ط·آµط·آ¨ط·ط›ط·آ© ', 'Brain with contrast', 3, '2017-06-03 08:08:04', '2017-06-03 08:08:04'),
(109, 'ط·آ§ط¸â€‍ط·ط›ط·آ¯ط·آ© ط·آ§ط¸â€‍ط¸â€ ط·آ®ط·آ§ط¸â€¦ط¸ظ¹ط·آ©', 'Sella Turcica', 3, '2017-06-03 08:08:04', '2017-06-03 08:08:04'),
(110, 'ط·آ§ط¸â€‍ط·آ¹ط¸ظ¹ط¸â€ ', 'Orbits', 3, '2017-06-03 08:08:04', '2017-06-03 08:08:04'),
(111, 'ط·آ§ط¸â€‍ط·آ¨ط¸â€‍ط·آ¹ط¸ث†ط¸â€¦ ط·آ§ط¸â€‍ط·آ£ط¸â€ ط¸ظ¾ط¸â€°', 'Naso - Pharynx', 3, '2017-06-03 08:08:04', '2017-06-03 08:08:04'),
(112, 'ط·آ«ط¸â€ ط·آ§ط·آ¦ط¸ظ¹ط·آ© ط·آ§ط¸â€‍ط¸â€¦ط·آ±ط·آ§ط·آ­ط¸â€‍ ط·آ¹ط¸â€‍ط¸â€° ط·آ§ط¸â€‍ط·آ±ط¸â€ڑط·آ¨ط·آ©', '2 Phase Neck', 3, '2017-06-03 08:08:04', '2017-06-03 08:08:04'),
(113, 'ط·آ§ط¸â€‍ط·آ¬ط¸ظ¹ط¸ث†ط·آ¨ ط·آ§ط¸â€‍ط·آ£ط¸â€ ط¸ظ¾ط¸ظ¹ط·آ© ط·آ¨ط·آ§ط¸â€‍ط¸ئ’ط·آ§ط¸â€¦ط¸â€‍', 'Paranasal Sinuses ( Axial and Coronal )', 3, '2017-06-03 08:08:04', '2017-06-03 08:08:04'),
(114, 'ط·آ§ط¸â€‍ط·آ¬ط¸ظ¹ط¸ث†ط·آ¨ ط·آ§ط¸â€‍ط·آ£ط¸â€ ط¸ظ¾ط¸ظ¹ط·آ© ط·آ¨ط·آ§ط¸â€‍ط¸ئ’ط·آ§ط¸â€¦ط¸â€‍ ط¸ث†ط·آ¨ط·آ§ط¸â€‍ط·آµط·آ¨ط·ط›ط·آ©', 'Paranasal Sinuses ( Axial and Coronal with contrast)', 3, '2017-06-03 08:08:04', '2017-06-03 08:08:04'),
(115, 'ط·آ§ط¸â€‍ط·آ¬ط¸ظ¹ط¸ث†ط·آ¨ ط·آ§ط¸â€‍ط·آ£ط¸â€ ط¸ظ¾ط¸ظ¹ط·آ© -  ط¸â€¦ط¸â€ڑط·آ§ط·آ·ط·آ¹', 'Paranasal Sinuses ( Coronal Only )', 3, '2017-06-03 08:08:05', '2017-06-03 08:08:05'),
(116, 'ط·آ§ط¸â€‍ط·آ¹ط·آ¸ط¸â€¦ط·آ© ط·آ§ط¸â€‍ط·آµط·آ®ط·آ±ط¸ظ¹ط·آ© ط¸â€¦ط¸â€ڑط·آ§ط·آ·ط·آ¹ ط·آ£ط¸ظ¾ط¸â€ڑط¸ظ¹ط·آ© ط¸ث†ط·آ±ط·آ£ط·آ³ط¸ظ¹ط·آ© ', 'Petrous Bone ( Axial and Coronal )', 3, '2017-06-03 08:08:05', '2017-06-03 08:08:05'),
(117, 'ط¸â€¦ط¸ظ¾ط·آµط¸â€‍ط¸â€° ط·آ§ط¸â€‍ط¸ظ¾ط¸ئ’', 'Temporomandibular Joints ( TMJ )', 3, '2017-06-03 08:08:05', '2017-06-03 08:08:05'),
(118, 'ط·آ§ط¸â€‍ط¸ظ¾ط¸â€ڑط·آ±ط·آ§ط·ع¾ ط·آ§ط¸â€‍ط¸â€ڑط·آ·ط¸â€ ط¸ظ¹ط·آ© ط¸ث†ط·آ§ط¸â€‍ط·آ¹ط·آ¬ط·آ²ط¸ظ¹ط·آ© ط·آ¨ط·آ§ط¸â€‍ط¸ئ’ط·آ§ط¸â€¦ط¸â€‍', 'Lumbosacral Spine ( L1 -S1 )', 3, '2017-06-03 08:08:05', '2017-06-03 08:08:05'),
(119, 'ط¸ظ¾ط¸â€ڑط·آ±ط·آ§ط·ع¾ ط·آ¹ط¸â€ ط¸â€ڑط¸ظ¹ط·آ© ط·آ¨ط·آ¯ط¸ث†ط¸â€  ط·آµط·آ¨ط·ط›ط·آ©', 'Cervical Spine ( Non Contrast )', 3, '2017-06-03 08:08:05', '2017-06-03 08:08:05'),
(120, 'ط¸ظ¾ط¸â€ڑط·آ±ط·آ§ط·ع¾ ط·آ¸ط¸â€،ط·آ±ط¸ظ¹ط·آ© - 6 ط¸ظ¾ط¸â€ڑط·آ±ط·آ§ط·ع¾ ط¸ظ¾ط¸â€ڑط·آ· ط·آ¨ط·آ¯ط¸ث†ط¸â€  ط·آµط·آ¨ط·ط›ط·آ©', 'Dorsal Spine (6 only )', 3, '2017-06-03 08:08:05', '2017-06-03 08:08:05'),
(121, 'ط·آ§ط¸â€‍ط·آµط·آ¯ط·آ±', 'Chest', 3, '2017-06-03 08:08:05', '2017-06-03 08:08:05'),
(122, 'ط·آ§ط¸â€‍ط·آµط·آ¯ط·آ± (ط·آ¨ط·آ¯ط¸ث†ط¸â€  ط·آµط·آ¨ط·ط›ط·آ©)', 'Chest ( Non Contrast )', 3, '2017-06-03 08:08:05', '2017-06-03 08:08:05'),
(123, 'ط¸â€¦ط¸â€ڑط·آ·ط·آ¹ط¸ظ¹ط·آ© ط·آ¹ط¸â€‍ط¸â€° ط·آ§ط¸â€‍ط·آ¨ط·آ·ط¸â€  ط¸ظ¾ط·آ§ط·آ¦ط¸â€ڑط·آ© ط·آ§ط¸â€‍ط·آ¯ط¸â€ڑط·آ©( ط·آ¨ط·آ¯ط¸ث†ط¸â€  ط·آµط·آ¨ط·ط›ط·آ©)', 'Non contrast Abdomen thin cuts', 3, '2017-06-03 08:08:05', '2017-06-03 08:08:05'),
(124, 'ط·آ§ط¸â€‍ط·آ¨ط·آ·ط¸â€  ط¸ث†ط·آ§ط¸â€‍ط·آ­ط¸ث†ط·آ¶', 'Abdomen & Pelvis', 3, '2017-06-03 08:08:05', '2017-06-03 08:08:05'),
(125, 'ط·آ§ط¸â€‍ط·آ¨ط·آ·ط¸â€  ط¸ث† ط·آ§ط¸â€‍ط·آ­ط¸ث†ط·آ¶ ط·آ«ط¸â€‍ط·آ§ط·آ«ط¸ظ¹ط·آ© ط·آ§ط¸â€‍ط¸â€¦ط·آ±ط·آ§ط·آ­ط¸â€‍ (ط·آ§ط¸â€‍ط¸ئ’ط·آ¨ط·آ¯)', 'Abdomen& triphasic', 3, '2017-06-03 08:08:05', '2017-06-03 08:08:05'),
(126, 'ط·آ«ط¸â€‍ط·آ§ط·آ«ط¸ظ¹ط·آ© ط·آ§ط¸â€‍ط¸â€¦ط·آ±ط·آ§ط·آ­ط¸â€‍ ط·آ¹ط¸â€‍ط¸â€° ط·آ§ط¸â€‍ط·آ¨ط·آ·ط¸â€ (ط·آ§ط¸â€‍ط·آ¨ط¸â€ ط¸ئ’ط·آ±ط¸ظ¹ط·آ§ط·آ³)', 'Abdomen& triphasic(pancreas)', 3, '2017-06-03 08:08:05', '2017-06-03 08:08:05'),
(127, ' ط·آ«ط¸â€‍ط·آ§ط·آ«ط¸ظ¹ط·آ© ط·آ§ط¸â€‍ط¸â€¦ط·آ±ط·آ§ط·آ­ط¸â€‍ ط·آ¹ط¸â€‍ط¸â€° ط·آ§ط¸â€‍ط¸ئ’ط·آ¨ط·آ¯ ط¸â€¦ط·آ¹ ط¸â€ڑط¸ظ¹ط·آ§ط·آ³ ط·آ­ط·آ¬ط¸â€¦ ط·آ§ط¸â€‍ط¸ئ’ط·آ¨ط·آ¯', 'Triphasic liver with volume', 3, '2017-06-03 08:08:05', '2017-06-03 08:08:05'),
(128, ' ط·آ«ط¸â€‍ط·آ§ط·آ«ط¸ظ¹ط·آ© ط·آ§ط¸â€‍ط¸â€¦ط·آ±ط·آ§ط·آ­ط¸â€‍ ط·آ¹ط¸â€‍ط¸â€° ط·آ§ط¸â€‍ط¸ئ’ط·آ¨ط·آ¯ ط¸â€¦ط·آ¹ ط·آ§ط¸â€‍ط·آ§ط¸â€¦ط·آ¹ط·آ§ط·طŒ ط·آ§ط¸â€‍ط·آ¯ط¸â€ڑط¸ظ¹ط¸â€ڑط·آ© ط¸ث† ط·آ§ط¸â€‍ط·ط›ط¸â€‍ط¸ظ¹ط·آ¸ط·آ©', 'Triphasic liver with entrocolonography', 3, '2017-06-03 08:08:05', '2017-06-03 08:08:05'),
(129, 'ط·آ§ط¸â€‍ط·آ¨ط·آ·ط¸â€  ط¸ث† ط·آ§ط¸â€‍ط·آ­ط¸ث†ط·آ¶ ط¸ث† ط·آ§ط¸â€‍ط¸â€ ط·آ²ط¸ظ¹ط¸ظ¾ ط·آ§ط¸â€‍ط·آ¯ط·آ§ط·آ®ط¸â€‍ط¸â€°', 'GI Bleeding', 3, '2017-06-03 08:08:05', '2017-06-03 08:08:05'),
(130, 'ط·آ§ط¸â€‍ط·آ¨ط·آ·ط¸â€  ط¸ث†ط·آ§ط¸â€‍ط·آ­ط¸ث†ط·آ¶ ط¸ث† ط·آ§ط¸â€‍ط¸â€ ط·آ§ط·آµط¸ث†ط·آ± ', 'CT Sinogram', 3, '2017-06-03 08:08:05', '2017-06-03 08:08:05'),
(131, 'ط·آ«ط¸â€ ط·آ§ط·آ¦ط¸ظ¹ط·آ© ط·آ§ط¸â€‍ط¸â€¦ط·آ±ط·آ§ط·آ­ط¸â€‍ ط·آ¹ط¸â€‍ط¸â€° ط·آ§ط¸â€‍ط·آ¨ط·آ·ط¸â€ (ط·آ§ط¸â€‍ط·ط›ط·آ¯ط·آ© ط¸ظ¾ط¸ث†ط¸â€ڑ ط·آ§ط¸â€‍ط¸ئ’ط¸â€‍ط¸ث†ط¸ظ¹ط·آ©)', '2 Phase Abdomen (Adrenal)', 3, '2017-06-03 08:08:05', '2017-06-03 08:08:05'),
(132, 'ط¸ظ¾ط·آ­ط·آµ  ط¸â€‍ط¸â€‍ط¸â€¦ط·آ³ط·آ§ط¸â€‍ط¸ئ’ ط·آ§ط¸â€‍ط·آ¨ط¸ث†ط¸â€‍ط¸ظ¹ط·آ© ط·آ¨ط·آ§ط¸â€‍ط·آµط·آ¨ط·ط›ط·آ©', 'CTA Urography Haematurie', 3, '2017-06-03 08:08:05', '2017-06-03 08:08:05'),
(133, 'ط¸ظ¾ط·آ­ط·آµ ط·آ¹ط¸â€‍ط¸â€° ط·آ§ط¸â€‍ط¸â€ڑط¸ث†ط¸â€‍ط¸ث†ط¸â€ ', 'CT Colonography', 3, '2017-06-03 08:08:05', '2017-06-03 08:08:05'),
(134, 'ط¸ظ¾ط·آ­ط·آµ ط·آ¹ط¸â€‍ط¸â€° ط·آ§ط¸â€‍ط·آ§ط¸â€¦ط·آ¹ط·آ§ط·طŒ ط·آ§ط¸â€‍ط·آ¯ط¸â€ڑط¸ظ¹ط¸â€ڑط·آ©', 'CT Enterography', 3, '2017-06-03 08:08:05', '2017-06-03 08:08:05'),
(135, 'ط¸â€¦ط·ع¾ط·آ¹ط·آ¯ط·آ¯ط·آ© ط·آ§ط¸â€‍ط¸â€¦ط¸â€ڑط·آ§ط·آ·ط·آ¹ ط·آ¹ط¸â€‍ط¸â€° ط·آ§ط¸â€‍ط¸ئ’ط¸â€‍ط¸â€° ', 'Multi Phase Kidney', 3, '2017-06-03 08:08:05', '2017-06-03 08:08:05'),
(136, 'ط¸â€ڑط¸ظ¹ط·آ§ط·آ³ ط·آ·ط¸ث†ط¸â€‍ ط·آ§ط¸â€‍ط·آ§ط·آ·ط·آ±ط·آ§ط¸ظ¾ ', 'Scanogram', 3, '2017-06-03 08:08:05', '2017-06-03 08:08:05'),
(137, 'ط¸â€¦ط¸ظ¾ط·آµط¸â€‍ط¸â€° ط·آ§ط¸â€‍ط·آ­ط¸ث†ط·آ¶', 'Both Hips', 3, '2017-06-03 08:08:05', '2017-06-03 08:08:05'),
(138, 'ط·آ±ط¸ئ’ط·آ¨ط·آ©', 'Knee ( One Side )', 3, '2017-06-03 08:08:05', '2017-06-03 08:08:05'),
(139, 'ط¸â€¦ط¸ظ¾ط·آµط¸â€‍ ط·آ§ط¸â€‍ط¸ئ’ط·آ§ط·آ­ط¸â€‍', 'Ankle', 3, '2017-06-03 08:08:06', '2017-06-03 08:08:06'),
(140, 'ط·آ§ط¸â€‍ط¸ئ’ط·ع¾ط¸ظ¾', 'Shoulder ( One Side )', 3, '2017-06-03 08:08:06', '2017-06-03 08:08:06'),
(141, 'ط·آ¹ط·آ¸ط·آ§ط¸â€¦ ط·آ§ط¸â€‍ط·آ£ط·آ·ط·آ±ط·آ§ط¸ظ¾', 'Limbs', 3, '2017-06-03 08:08:06', '2017-06-03 08:08:06'),
(142, 'ط¸ظ¾ط·آ­ط·آµ ط·آ«ط¸â€‍ط·آ§ط·آ«ط¸â€° ط·آ§ط¸â€‍ط·آ§ط·آ¨ط·آ¹ط·آ§ط·آ¯ ط·آ¹ط¸â€‍ط¸â€° ط·آ§ط¸â€‍ط¸ث†ط·آ¬ط¸â€،(ط·آ¨ط·آ¯ط¸ث†ط¸â€  ط·آµط·آ¨ط·ط›ط·آ©)', '3D face( Non Contrast )', 3, '2017-06-03 08:08:06', '2017-06-03 08:08:06'),
(143, 'ط¸ظ¾ط·آ­ط·آµ ط·آ§ط¸â€ ط·آ¬ط¸ظ¹ط¸ث† ط·آ¹ط¸â€‍ط¸â€° ط·آ´ط·آ±ط·آ§ط¸ظ¹ط¸ظ¹ط¸â€  ط·آ§ط¸â€‍ط¸ث†ط·آ¬ط¸â€،ط·آ©(ط·آ¨ط·آ§ط¸â€‍ط·آµط·آ¨ط·ط›ط·آ©)', 'CTA Angio Face(with contrast)', 3, '2017-06-03 08:08:06', '2017-06-03 08:08:06'),
(144, 'ط¸ظ¾ط·آ­ط·آµ ط·آ§ط¸â€ ط·آ¬ط¸ظ¹ط¸ث† ط·آ¹ط¸â€‍ط¸â€° ط·آ§ط¸â€‍ط¸â€¦ط·آ®', 'CTA Angio Brain', 3, '2017-06-03 08:08:06', '2017-06-03 08:08:06'),
(145, 'ط¸ظ¾ط·آ­ط·آµ ط·آ§ط¸â€ ط·آ¬ط¸ظ¹ط¸ث† ط·آ¹ط¸â€‍ط¸â€° ط·آ´ط·آ±ط·آ§ط¸ظ¹ط¸ظ¹ط¸â€  ط·آ§ط¸â€‍ط·آ±ط¸â€ڑط·آ¨ط·آ©', 'CTA Angio carotid', 3, '2017-06-03 08:08:06', '2017-06-03 08:08:06'),
(146, 'ط¸ظ¾ط·آ­ط·آµ ط·آ§ط¸â€ ط·آ¬ط¸ظ¹ط¸ث† ط·آ¹ط¸â€‍ط¸â€° ط·آ§ط¸â€‍ط¸â€¦ط·آ® ط¸ث† ط·آ´ط·آ±ط·آ§ط¸ظ¹ط¸ظ¹ط¸â€  ط·آ§ط¸â€‍ط·آ±ط¸â€ڑط·آ¨ط·آ©', 'CTA Angio Brain and carotid', 3, '2017-06-03 08:08:06', '2017-06-03 08:08:06'),
(147, 'ط¸ظ¾ط·آ­ط·آµ ط·آ§ط¸â€ ط·آ¬ط¸ظ¹ط¸ث† ط·آ¹ط¸â€‍ط¸â€° ط·آ§ط¸â€‍ط·آ§ط¸ث†ط·آ±ط·آ·ط¸â€° ط·آ§ط¸â€‍ط·آ¨ط·آ·ط¸â€ ط¸â€° ط¸ث† ط·آ§ط¸â€‍ط·آµط·آ¯ط·آ±ط¸â€°', 'CTA Angio Aorta Chest & Abdomen', 3, '2017-06-03 08:08:06', '2017-06-03 08:08:06'),
(148, 'ط¸ظ¾ط·آ­ط·آµ ط·آ§ط¸â€ ط·آ¬ط¸ظ¹ط¸ث† ط·آ¹ط¸â€‍ط¸â€° ط·آ§ط¸â€‍ط·آ§ط¸ث†ط·آ±ط·آ·ط¸â€° ط¸ث† ط·آ§ط¸â€‍ط·آ§ط·آ·ط·آ±ط·آ§ط¸ظ¾ ط·آ§ط¸â€‍ط·آ³ط¸ظ¾ط¸â€‍ط¸ظ¹ط·آ©', 'CTA Angio Aorta Chest & Abdomen & Both Lower Limbs', 3, '2017-06-03 08:08:06', '2017-06-03 08:08:06'),
(149, 'ط¸ظ¾ط·آ­ط·آµ ط·آ§ط¸â€ ط·آ¬ط¸ظ¹ط¸ث† ط·آ¹ط¸â€‍ط¸â€° ط·آ§ط¸â€‍ط·آ§ط¸ث†ط·آ±ط·آ·ط¸â€° ط¸ث† ط·آ·ط·آ±ط¸ظ¾ ط·آ¹ط¸â€‍ط¸ث†ط¸â€° ط¸ظ¾ط¸â€ڑط·آ· ', 'CTA Angio Aorta Chest & Abdomen & one upper Limb', 3, '2017-06-03 08:08:06', '2017-06-03 08:08:06'),
(150, 'ط¸ظ¾ط·آ­ط·آµ ط·آ§ط¸â€ ط·آ¬ط¸ظ¹ط¸ث† ط·آ¹ط¸â€‍ط¸â€° ط·آ§ط¸ث†ط·آ±ط·آ¯ط·آ© ط·آ§ط¸â€‍ط·آ§ط·آ·ط·آ±ط·آ§ط¸ظ¾ ط·آ§ط¸â€‍ط·آ³ط¸ظ¾ط¸â€‍ط¸ظ¹ط·آ©', 'CTA Angio Venogram Both Lower Limbs', 3, '2017-06-03 08:08:06', '2017-06-03 08:08:06'),
(151, 'ط¸ظ¾ط·آ­ط·آµ ط·آ§ط¸â€ ط·آ¬ط¸ظ¹ط¸ث† ط·آ¹ط¸â€‍ط¸â€° ط·آ´ط·آ±ط·آ§ط¸ظ¹ط¸ظ¹ط¸â€  ط·آ§ط¸â€‍ط¸â€ڑط¸â€‍ط·آ¨', 'CTA angio Coronary', 3, '2017-06-03 08:08:06', '2017-06-03 08:08:06'),
(152, 'ط·آ§ط¸â€‍ط·آ´ط·آ¹ط·آ¨ ط·آ§ط¸â€‍ط¸â€،ط¸ث†ط·آ§ط·آ¦ط¸ظ¹ط·آ© ط·آ¨ط·آ¯ط¸ث†ط¸â€  ط·آ§ط·آ³ط·ع¾ط·آ®ط·آ¯ط·آ§ط¸â€¦ ط¸â€¦ط¸â€ ط·آ¸ط·آ§ط·آ±', 'CTA Virtual Bronchoscopy', 3, '2017-06-03 08:08:06', '2017-06-03 08:08:06'),
(153, 'ط·آ§ط¸â€‍ط·آ´ط·آ±ط¸ظ¹ط·آ§ط¸â€  ط·آ§ط¸â€‍ط·آ±ط·آ¦ط¸ث†ط¸â€° ', 'CTA Pulmonary', 3, '2017-06-03 08:08:06', '2017-06-03 08:08:06'),
(154, 'ط¸ظ¾ط·آ­ط·آµ ط·آ§ط¸â€ ط·آ¬ط¸ظ¹ط¸ث† ط·آ¹ط¸â€‍ط¸â€° ط·آ´ط·آ±ط·آ§ط¸ظ¹ط¸ظ¹ط¸â€  ط·آ§ط¸â€‍ط¸ئ’ط¸â€‍ط¸â€°', 'CTA Renal', 3, '2017-06-03 08:08:06', '2017-06-03 08:08:06'),
(155, 'ط¸ظ¾ط·آ­ط·آµ ط¸â€¦ط·آ­ط·آ¯ط¸ث†ط·آ¯ + ط·آ¹ط¸ظ¹ط¸â€ ط·آ©', 'Limited CT Study + biopsy', 3, '2017-06-03 08:08:06', '2017-06-03 08:08:06'),
(156, 'ط·آ¹ط¸ظ¹ط¸â€ ط·آ© ط¸â€¦ط·آ­ط·آ¯ط¸ث†ط·آ¯ط·آ© ط·آ¹ط¸â€  ط·آ·ط·آ±ط¸ظ¹ط¸â€ڑ ط·آ§ط¸â€‍ط¸â€¦ط¸ث†ط·آ¬ط·آ§ط·ع¾ ط·آ§ط¸â€‍ط·آµط¸ث†ط·ع¾ط¸ظ¹ط·آ©', 'Biopsy Under US', 3, '2017-06-03 08:08:06', '2017-06-03 08:08:06'),
(157, 'ط·آ¹ط¸ظ¹ط¸â€ ط·آ© ط¸â€¦ط¸â€  ط·آ§ط¸â€‍ط·ط›ط·آ¯ط·آ© ط·آ§ط¸â€‍ط·آ¯ط·آ±ط¸â€ڑط¸ظ¹ط·آ©', 'Thyroid Biopsy Under US', 3, '2017-06-03 08:08:06', '2017-06-03 08:08:06'),
(158, 'ط·آ¹ط¸ظ¹ط¸â€ ط·آ© ط¸â€¦ط·آ­ط·آ¯ط¸ث†ط·آ¯ط·آ© ط¸â€¦ط¸â€  ط·آ§ط¸â€‍ط·آ¹ط·آ¸ط¸â€¦', 'Bone Biopsy', 3, '2017-06-03 08:08:06', '2017-06-03 08:08:06'),
(159, ' ', 'Biopsy with big tale', 3, '2017-06-03 08:08:06', '2017-06-03 08:08:06'),
(160, 'ط·آ¹ط¸ظ¹ط¸â€ ط·آ© ط¸â€¦ط¸â€  ط·آ§ط¸â€‍ط·آµط·آ¯ط·آ± ط·آ¹ط¸â€  ط·آ·ط·آ±ط¸ظ¹ط¸â€ڑ ط·آ§ط¸â€‍ط·آ£ط¸â€ ط·آ¨ط¸ث†ط·آ¨', 'Chest Tube Biopsy', 3, '2017-06-03 08:08:06', '2017-06-03 08:08:06'),
(161, 'ط·ع¾ط¸ظ¾ط·آ±ط¸ظ¹ط·ط› ط·ع¾ط·آ¬ط¸â€¦ط·آ¹ ', 'Drinage', 3, '2017-06-03 08:08:06', '2017-06-03 08:08:06'),
(162, 'ط·آ§ط¸â€‍ط¸ث†ط·آ²ط¸â€  ( 100 ط¸ئ’ط·آ¬ط¸â€¦ ط¸ث†ط·آ§ط¸â€ڑط¸â€‍ ط¸â€¦ط¸â€  110 ط¸ئ’ط·آ¬ط¸â€¦ )', 'Over wight from 100to 110', 3, '2017-06-03 08:08:06', '2017-06-03 08:08:06'),
(163, 'ط·آ§ط¸â€‍ط¸ث†ط·آ²ط¸â€  ( 110 ط¸ئ’ط·آ¬ط¸â€¦ ط¸ث†ط·آ§ط¸â€ڑط¸â€‍ ط¸â€¦ط¸â€  120 ط¸ئ’ط·آ¬ط¸â€¦ )', 'Over wight from 110to 120', 3, '2017-06-03 08:08:06', '2017-06-03 08:08:06'),
(164, 'ط·آ§ط¸â€‍ط¸ث†ط·آ²ط¸â€  ( 120 ط¸ئ’ط·آ¬ط¸â€¦ ط¸ث†ط·آ§ط¸â€ڑط¸â€‍ ط¸â€¦ط¸â€  130 ط¸ئ’ط·آ¬ط¸â€¦ )', 'Over wight from 120to 130', 3, '2017-06-03 08:08:06', '2017-06-03 08:08:06'),
(165, 'ط·آ§ط¸â€‍ط¸ث†ط·آ²ط¸â€  ( 130 ط¸ئ’ط·آ¬ط¸â€¦ ط¸ث†ط·آ§ط¸â€ڑط¸â€‍ ط¸â€¦ط¸â€  140 ط¸ئ’ط·آ¬ط¸â€¦ )', 'Over wight from 130to 140', 3, '2017-06-03 08:08:06', '2017-06-03 08:08:06'),
(166, 'ط·آ§ط¸â€‍ط¸ث†ط·آ²ط¸â€  ( 140 ط¸ئ’ط·آ¬ط¸â€¦ ط¸ث†ط·آ§ط¸â€ڑط¸â€‍ ط¸â€¦ط¸â€  150 ط¸ئ’ط·آ¬ط¸â€¦ )', 'Over wight from 140to 150', 3, '2017-06-03 08:08:07', '2017-06-03 08:08:07'),
(167, 'ط·آ¹ط·آ¸ط·آ§ط¸â€¦ ط·آ§ط¸â€‍ط·آ¬ط¸â€¦ط·آ¬ط¸â€¦ط·آ© ط¸ث†ط·آ¶ط·آ¹ط¸ظ¹ط¸â€ ', 'Skull ( 2 Views )', 4, '2017-06-03 08:08:07', '2017-06-03 08:08:07'),
(168, 'ط·آ§ط¸â€‍ط¸â€ ط·ع¾ط¸ث†ط·طŒ ط·آ§ط¸â€‍ط·آ­ط¸â€‍ط¸â€¦ط¸â€° ط·آ§ط¸â€‍ط·آ£ط¸ظ¹ط¸â€¦ط¸â€  ط¸ث†ط·آ§ط¸â€‍ط·آ£ط¸ظ¹ط·آ³ط·آ±', 'Mastoids ( Both Sides )', 4, '2017-06-03 08:08:07', '2017-06-03 08:08:07'),
(169, 'ط·آ§ط¸â€‍ط·آ¬ط¸ظ¹ط¸ث†ط·آ¨ ط·آ§ط¸â€‍ط·آ£ط¸â€ ط¸ظ¾ط¸ظ¹ط·آ© ط¸ث†ط·آ¶ط·آ¹ط¸ظ¹ط¸â€ ', 'Paranasal Sinuses ( 2 Views )', 4, '2017-06-03 08:08:07', '2017-06-03 08:08:07'),
(170, 'ط¸â€¦ط¸ظ¾ط·آµط¸â€‍ط¸â€° ط·آ§ط¸â€‍ط¸ظ¾ط¸ئ’ ط·آ¬ط·آ§ط¸â€ ط·آ¨ط¸ظ¹ط¸â€ ', 'T.M.J. Bilateral', 4, '2017-06-03 08:08:07', '2017-06-03 08:08:07'),
(171, 'ط·آ§ط¸â€‍ط·آ¨ط¸â€‍ط·آ¹ط¸ث†ط¸â€¦ ط·آ§ط¸â€‍ط·آ£ط¸â€ ط¸ظ¾ط¸â€°', 'Nasopharynx ( 1 View )', 4, '2017-06-03 08:08:07', '2017-06-03 08:08:07'),
(172, 'ط¸ظ¾ط¸â€ڑط·آ±ط·آ§ط·ع¾ ط·آ¹ط¸â€ ط¸â€ڑط¸ظ¹ط·آ© ط·آ£ط·آ±ط·آ¨ط·آ¹ ط·آ£ط¸ث†ط·آ¶ط·آ§ط·آ¹', 'Cervical Spine ( 4 Views )', 4, '2017-06-03 08:08:07', '2017-06-03 08:08:07'),
(173, 'ط¸ظ¾ط¸â€ڑط·آ±ط·آ§ط·ع¾ ط·آ¹ط¸â€ ط¸â€ڑط¸ظ¹ط·آ©  ط¸ث†ط·آ¶ط·آ¹ط¸ظ¹ط¸â€ ', 'Cervical Spine ( 2 Views )', 4, '2017-06-03 08:08:07', '2017-06-03 08:08:07'),
(174, 'ط¸ظ¾ط¸â€ڑط·آ±ط·آ§ط·ع¾ ط¸â€ڑط·آ·ط¸â€ ط¸ظ¹ط·آ© ط¸ث†ط·آ¶ط·آ¹ط¸ظ¹ط¸â€ ', 'Lumbar Spine ( 2 Views )', 4, '2017-06-03 08:08:07', '2017-06-03 08:08:07'),
(175, 'ط¸ظ¾ط¸â€ڑط·آ±ط·آ§ط·ع¾ ط¸â€ڑط·آ·ط¸â€ ط¸ظ¹ط·آ©  ط·آ£ط·آ±ط·آ¨ط·آ¹ ط·آ£ط¸ث†ط·آ¶ط·آ§ط·آ¹', 'Lumbar Spine ( 4 Views )', 4, '2017-06-03 08:08:07', '2017-06-03 08:08:07'),
(176, 'ط¸ث†ط·آ¶ط·آ¹ ط·آ§ط·آ¶ط·آ§ط¸ظ¾ط¸â€° ط·آ¹ط¸â€‍ط¸â€° ط¸ظ¾ط¸â€ڑط·آ±ط·آ© ط¸â€ڑط·آ·ط¸â€ ط¸ظ¹ط·آ© ط·آ§ط¸ث† ط·آ¸ط¸â€،ط·آ±ط¸ظ¹ط·آ©', 'Lumbar Spine Extra View', 4, '2017-06-03 08:08:07', '2017-06-03 08:08:07'),
(177, 'ط¸ظ¾ط¸â€ڑط·آ±ط·آ§ط·ع¾ ط·آ¸ط¸â€،ط·آ±ط¸ظ¹ط·آ©', 'Dorsal Spine', 4, '2017-06-03 08:08:07', '2017-06-03 08:08:07'),
(178, 'ط¸â€¦ط¸ظ¾ط·آµط¸â€‍ ط·آ§ط¸â€‍ط¸ئ’ط·ع¾ط¸ظ¾', 'Shoulder', 4, '2017-06-03 08:08:07', '2017-06-03 08:08:07'),
(179, 'ط¸â€¦ط¸ظ¾ط·آµط¸â€‍ ط·آ§ط¸â€‍ط¸ئ’ط¸ث†ط·آ¹ - ط¸ث†ط·آ¶ط·آ¹ط¸ظ¹ط¸â€ ', 'Elbow Joint ( 2 Views )', 4, '2017-06-03 08:08:07', '2017-06-03 08:08:07'),
(180, 'ط¸â€¦ط¸ظ¾ط·آµط¸â€‍ ط·آ§ط¸â€‍ط·آ±ط·آ³ط·ط› - ط¸ث†ط·آ¶ط·آ¹ط¸ظ¹ط¸â€ ', 'Wrist Joint ( 2 Views )', 4, '2017-06-03 08:08:07', '2017-06-03 08:08:07'),
(181, 'ط·آ§ط¸â€‍ط·آ¹ط·آ¶ط·آ¯ ط·آ£ط¸ث† ط·آ§ط¸â€‍ط·آ³ط·آ§ط·آ¹ط·آ¯ - ط¸ث†ط·آ¶ط·آ¹ط¸ظ¹ط¸â€ ', 'Forearm or Arm ( 2 Views )', 4, '2017-06-03 08:08:07', '2017-06-03 08:08:07'),
(182, 'ط·آ§ط¸â€‍ط¸ظ¹ط·آ¯ - ط¸ث†ط·آ¶ط·آ¹ط¸ظ¹ط¸â€ ', 'Hand ( 2 Views )', 4, '2017-06-03 08:08:07', '2017-06-03 08:08:07'),
(183, 'ط¸ث†ط·آ¶ط·آ¹ ط·آ§ط·آ¶ط·آ§ط¸ظ¾ط¸â€° ', 'Additional  View', 4, '2017-06-03 08:08:07', '2017-06-03 08:08:07'),
(184, 'ط·آ§ط¸â€‍ط·آ­ط¸ث†ط·آ¶ - ط¸ث†ط·آ¶ط·آ¹ ط¸ث†ط·آ§ط·آ­ط·آ¯', 'Pelvis ( 1 View )', 4, '2017-06-03 08:08:07', '2017-06-03 08:08:07'),
(185, 'ط¸â€¦ط¸ظ¾ط·آµط¸â€‍ ط·آ§ط¸â€‍ط¸ظ¾ط·آ®ط·آ° - ط¸ث†ط·آ¶ط·آ¹ط¸ظ¹ط¸â€ ', 'Hip Joint ( 2 Views )', 4, '2017-06-03 08:08:07', '2017-06-03 08:08:07'),
(186, 'ط·آ¹ط·آ¸ط·آ§ط¸â€¦ ط·آ§ط¸â€‍ط¸ظ¾ط·آ®ط·آ° - ط¸ث†ط·آ¶ط·آ¹ط¸ظ¹ط¸â€ ', 'Femur ( 2 Views )', 4, '2017-06-03 08:08:07', '2017-06-03 08:08:07'),
(187, 'ط¸â€¦ط¸ظ¾ط·آµط¸â€‍ ط·آ§ط¸â€‍ط·آ±ط¸ئ’ط·آ¨ط·آ© - ط¸ث†ط·آ¶ط·آ¹ط¸ظ¹ط¸â€ ', 'Knee Joint ( 2 Views )', 4, '2017-06-03 08:08:07', '2017-06-03 08:08:07'),
(188, 'ط·آ¹ط·آ¸ط·آ§ط¸â€¦ ط·آ§ط¸â€‍ط·آ³ط·آ§ط¸â€ڑ - ط¸ث†ط·آ¶ط·آ¹ط¸ظ¹ط¸â€ ', 'Leg ( 2 Views )', 4, '2017-06-03 08:08:08', '2017-06-03 08:08:08'),
(189, 'ط¸â€¦ط¸ظ¾ط·آµط¸â€‍ ط·آ§ط¸â€‍ط¸ئ’ط·آ§ط·آ­ط¸â€‍ - ط¸ث†ط·آ¶ط·آ¹ط¸ظ¹ط¸â€ ', 'Ankle Joint ( 2 Views )', 4, '2017-06-03 08:08:08', '2017-06-03 08:08:08'),
(190, 'ط·آ¹ط·آ¸ط·آ§ط¸â€¦ ط·آ§ط¸â€‍ط¸â€ڑط·آ¯ط¸â€¦ - ط¸ث†ط·آ¶ط·آ¹ط¸ظ¹ط¸â€ ', 'Foot ( 2 Views )', 4, '2017-06-03 08:08:08', '2017-06-03 08:08:08'),
(191, 'ط·آ§ط¸â€‍ط¸ئ’ط·آ¹ط·آ¨ط¸ظ¹ط¸â€ ', 'Both Heels', 4, '2017-06-03 08:08:08', '2017-06-03 08:08:08'),
(192, 'ط¸ث†ط·آ¶ط·آ¹ ط·آ§ط·آ¶ط·آ§ط¸ظ¾ط¸â€°', 'Additional View', 4, '2017-06-03 08:08:08', '2017-06-03 08:08:08'),
(193, 'ط·آ§ط¸â€‍ط·آµط·آ¯ط·آ± ط¸ث†ط·آ§ط¸â€‍ط¸â€ڑط¸â€‍ط·آ¨ ط¸ث†ط·آ¶ط·آ¹ ط¸ث†ط·آ§ط·آ­ط·آ¯', 'Chest & Heart ( 1 View )', 4, '2017-06-03 08:08:08', '2017-06-03 08:08:08'),
(194, 'ط·آ§ط¸â€‍ط·آµط·آ¯ط·آ± ط¸ث†ط·آ§ط¸â€‍ط¸â€ڑط¸â€‍ط·آ¨ ط¸ث†ط·آ¶ط·آ¹ط¸ظ¹ط¸â€ ', 'Chest & Heart ( 2 Views )', 4, '2017-06-03 08:08:08', '2017-06-03 08:08:08'),
(195, 'ط·آ¨ط¸â€‍ط·آ¹ط¸ث†ط¸â€¦ ط·آ£ط¸ث† ط¸â€¦ط·آ±ط·آ¦ ط·آ¨ط·آ§ط¸â€‍ط·آ¨ط·آ§ط·آ±ط¸ظ¹ط¸ث†ط¸â€¦', 'Hypopharynx, Esophagus ( Swallow )', 4, '2017-06-03 08:08:08', '2017-06-03 08:08:08'),
(196, 'ط·آ£ط·آ³ط¸ظ¾ط¸â€‍ ط·آ§ط¸â€‍ط¸â€¦ط·آ±ط·آ¦ ط¸ث†ط·آ§ط¸â€‍ط¸â€¦ط·آ¹ط·آ¯ط·آ© ط¸ث†ط·آ§ط¸â€‍ط·آ§ط·آ«ط¸â€ ط¸â€° ط·آ¹ط·آ´ط·آ±', 'Barium meal', 4, '2017-06-03 08:08:08', '2017-06-03 08:08:08'),
(197, 'ط·آ§ط¸â€‍ط¸â€ڑط¸ث†ط¸â€‍ط¸ث†ط¸â€  ط·آ¨ط·آ§ط¸â€‍ط·آ¨ط·آ§ط·آ±ط¸ظ¹ط¸ث†ط¸â€¦', 'Barium Enema', 4, '2017-06-03 08:08:08', '2017-06-03 08:08:08'),
(198, 'ط¸â€¦ط·آ³ط·آ§ط¸â€‍ط¸ئ’ ط·آ¨ط¸ث†ط¸â€‍ط¸ظ¹ط·آ© ط·آ¹ط·آ§ط·آ¯ط¸ظ¹ط·آ©', 'U.T.', 4, '2017-06-03 08:08:08', '2017-06-03 08:08:08'),
(199, 'ط¸â€¦ط·آ³ط·آ§ط¸â€‍ط¸ئ’ ط·آ¨ط¸ث†ط¸â€‍ط¸ظ¹ط·آ© ط·آ¨ط·آ§ط¸â€‍ط·آµط·آ¨ط·ط›ط·آ©', 'Intravenous Urography ( IVU )', 4, '2017-06-03 08:08:08', '2017-06-03 08:08:08'),
(200, 'ط¸â€ڑط¸â€ ط·آ§ط·آ© ط¸â€¦ط·آ¬ط·آ±ط¸â€° ط·آ§ط¸â€‍ط·آ¨ط¸ث†ط¸â€‍ -  ط·آ¨ط·آ§ط¸â€‍ط·آµط·آ¨ط·ط›ط·آ©', 'Ascending Uretherography', 4, '2017-06-03 08:08:08', '2017-06-03 08:08:08'),
(201, 'ط·آ§ط¸â€‍ط¸â€¦ط·آ«ط·آ§ط¸â€ ط·آ© ط¸ث†ط¸â€ڑط¸â€ ط·آ§ط·آ© ط¸â€¦ط·آ¬ط·آ±ط¸â€° ط·آ§ط¸â€‍ط·آ¨ط¸ث†ط¸â€‍', 'Cysto - Uretherography', 4, '2017-06-03 08:08:08', '2017-06-03 08:08:08'),
(202, 'ط·آ£ط·آ´ط·آ¹ط·آ© ط·آ¨ط·آ§ط¸â€‍ط·آµط·آ¨ط·ط›ط·آ© ط·آ¹ط¸â€‍ط¸â€° ط·آ§ط¸â€‍ط¸â€ ط·آ§ط·آµط¸ث†ط·آ±', 'Sinogram', 4, '2017-06-03 08:08:08', '2017-06-03 08:08:08'),
(203, 'ط·آ§ط·آ´ط·آ¹ط·آ© ط·آ¨ط·آ§ط¸â€‍ط·آµط·آ¨ط·ط›ط·آ© ط·آ¹ط¸â€‍ط¸â€° ط·آ§ط¸â€‍ط·آ±ط·آ­ط¸â€¦ ( ط·آ·ط·آ¨ط¸ظ¹ط·آ¨ط·آ©ط·آ£&ط·آ·ط·آ¨ط¸ظ¹ط·آ¨ ط·آ£ط·آ´ط·آ¹ط·آ© )', 'Hystro-salpingography ( HSG ) (Radiology doctor )', 4, '2017-06-03 08:08:08', '2017-06-03 08:08:08'),
(204, 'ط·آ§ط¸â€‍ط·آ¨ط·آ·ط¸â€ ', 'Abdomen', 5, '2017-06-03 08:08:08', '2017-06-03 08:08:08'),
(205, 'ط·آ§ط¸â€‍ط·آ­ط¸ث†ط·آ¶', 'Pelvis', 5, '2017-06-03 08:08:08', '2017-06-03 08:08:08'),
(206, 'ط·آ§ط¸â€‍ط·آ¨ط·آ·ط¸â€  ط¸ث†ط·آ§ط¸â€‍ط·آ­ط¸ث†ط·آ¶', 'Abdomen & Pelvis', 5, '2017-06-03 08:08:08', '2017-06-03 08:08:08'),
(207, 'ط·آ§ط¸â€‍ط·ط›ط·آ¯ط·آ© ط·آ§ط¸â€‍ط·آ¯ط·آ±ط¸â€ڑط¸ظ¹ط·آ©', 'Thyroid Gland', 5, '2017-06-03 08:08:08', '2017-06-03 08:08:08'),
(208, 'ط·آ§ط¸â€‍ط·آ«ط·آ¯ط¸â€°', 'Breast', 5, '2017-06-03 08:08:08', '2017-06-03 08:08:08'),
(209, 'ط·آ§ط¸â€‍ط¸â€¦ط¸ظ¾ط·آ§ط·آµط¸â€‍ (ط·آ§ط¸â€‍ط·آ±ط¸ئ’ط·آ¨ط·آ© -ط·آ§ط¸â€‍ط¸ئ’ط¸ث†ط·آ¹ -ط·آ§ط¸â€‍ط·آ±ط·آ³ط·ط› ...)', 'Joints', 5, '2017-06-03 08:08:08', '2017-06-03 08:08:08'),
(210, 'ط¸â€¦ط·ع¾ط·آ§ط·آ¨ط·آ¹ط·آ© ط·ع¾ط·آ¨ط¸ث†ط¸ظ¹ط·آ¶', 'Follicular.Scanning', 5, '2017-06-03 08:08:08', '2017-06-03 08:08:08'),
(211, 'ط¸â€¦ط¸ث†ط·آ¬ط·آ§ط·ع¾ ط·آµط¸ث†ط·ع¾ط¸ظ¹ط·آ© ط·آ¹ط¸â€  ط·آ·ط·آ±ط¸ظ¹ط¸â€ڑ ط·آ§ط¸â€‍ط¸â€¦ط¸â€،ط·آ¨ط¸â€‍', 'Trans Vaginal', 5, '2017-06-03 08:08:08', '2017-06-03 08:08:08'),
(212, 'ط¸â€¦ط¸ث†ط·آ¬ط·آ§ط·ع¾ ط·آµط¸ث†ط·ع¾ط¸ظ¹ط·آ© ط·آ¹ط¸â€  ط·آ·ط·آ±ط¸ظ¹ط¸â€ڑ ط·آ§ط¸â€‍ط·آ´ط·آ±ط·آ¬', 'Trans Rictal', 5, '2017-06-03 08:08:09', '2017-06-03 08:08:09'),
(213, 'ط·آ¹ط¸ظ¹ط¸â€ ط·آ© ط·آ¹ط¸â€  ط·آ·ط·آ±ط¸ظ¹ط¸â€ڑ ط·آ§ط¸â€‍ط¸â€¦ط¸ث†ط·آ¬ط·آ§ط·ع¾ ط·آ§ط¸â€‍ط·آµط¸ث†ط·ع¾ط¸ظ¹ط·آ©', 'Biopsy Under US', 5, '2017-06-03 08:08:09', '2017-06-03 08:08:09'),
(214, 'ط·ع¾ط¸ظ¾ط·آ±ط¸ظ¹ط·ط› ط·ع¾ط·آ¬ط¸â€¦ط·آ¹ ط·آ¨ط·آ§ط¸â€‍ط·آµط·آ¯ط·آ± ط·آ§ط¸ث† ط·آ§ط¸â€‍ط·آ¨ط·آ·ط¸â€  ط·آ¨ط·آ§ط·آ³ط·ع¾ط·آ®ط·آ¯ط·آ§ط¸â€¦ ط·آ§ط¸â€‍ط¸â€¦ط¸ث†ط·آ¬ط·آ§ط·ع¾ ط·آ§ط¸â€‍ط·آµط¸ث†ط·ع¾ط¸ظ¹ط·آ©', 'Drainage of thoracic or abdominal collection', 5, '2017-06-03 08:08:09', '2017-06-03 08:08:09'),
(215, 'ط·آ¯ط¸ث†ط·آ¨ط¸â€‍ط·آ± ط¸ث†ط·آ±ط¸ظ¹ط·آ¯ ط¸ئ’ط·آ¨ط·آ¯ط¸â€°', 'Portal Doppler', 6, '2017-06-03 08:08:09', '2017-06-03 08:08:09'),
(216, 'ط·آ¯ط¸ث†ط·آ¨ط¸â€‍ط·آ± ط·آ´ط·آ±ط·آ§ط¸ظ¹ط¸ظ¹ط¸â€  ط·آ§ط¸â€‍ط·آ±ط¸â€ڑط·آ¨ط·آ©', 'Carotid Doppler', 6, '2017-06-03 08:08:09', '2017-06-03 08:08:09'),
(217, 'ط·آ¯ط¸ث†ط·آ¨ط¸â€‍ط·آ± ط·آ´ط·آ±ط·آ§ط¸ظ¹ط¸ظ¹ط¸â€  ط·آ§ط¸â€‍ط¸ئ’ط¸â€‍ط¸â€°', 'Renal Doppler', 6, '2017-06-03 08:08:09', '2017-06-03 08:08:09'),
(218, 'ط·آ¯ط¸ث†ط·آ¨ط¸â€‍ط·آ± ط·آ®ط·آµط¸ظ¹ط·ع¾ط¸ظ¹ط¸â€  ط¸ث† ط¸ئ’ط¸ظ¹ط·آ³ ط·آ§ط¸â€‍ط·آµط¸ظ¾ط¸â€ ', 'Scrotal Doppler', 6, '2017-06-03 08:08:09', '2017-06-03 08:08:09'),
(219, 'ط·آ¯ط¸ث†ط·آ¨ط¸â€‍ط·آ± ط·آ­ط¸â€¦ط¸â€‍', 'Obst Doppler', 6, '2017-06-03 08:08:09', '2017-06-03 08:08:09'),
(220, 'ط·آ±ط·آ¨ط·آ§ط·آ¹ط¸ظ¹ط·آ© ط·آ§ط¸â€‍ط·آ§ط·آ¨ط·آ¹ط·آ§ط·آ¯ ط·آ¹ط¸â€‍ط¸â€° ط·آ§ط¸â€‍ط·آ­ط¸â€¦ط¸â€‍ ', '4D', 6, '2017-06-03 08:08:09', '2017-06-03 08:08:09'),
(221, 'ط·آ¯ط¸ث†ط·آ¨ط¸â€‍ط·آ± ط·آ§ط¸ث†ط·آ±ط·آ¯ط·آ© ط·آ·ط·آ±ط¸ظ¾  ط¸ث†ط·آ§ط·آ­ط·آ¯', 'Venous Doppler for one L L', 6, '2017-06-03 08:08:09', '2017-06-03 08:08:09'),
(222, 'ط·آ¯ط¸ث†ط·آ¨ط¸â€‍ط·آ± ط·آ´ط·آ±ط·آ§ط¸ظ¹ط¸ظ¹ط¸â€  ط·آ·ط·آ±ط¸ظ¾  ط¸ث†ط·آ§ط·آ­ط·آ¯', 'Arterial Doppler for one L L', 6, '2017-06-03 08:08:09', '2017-06-03 08:08:09'),
(223, 'ط·آ¯ط¸ث†ط·آ¨ط¸â€‍ط·آ± ط·آ§ط¸ث†ط·آ±ط·آ¯ط·آ© ط¸ث† ط·آ´ط·آ±ط·آ§ط¸ظ¹ط¸ظ¹ط¸â€  ط·آ·ط·آ±ط¸ظ¾  ط¸ث†ط·آ§ط·آ­ط·آ¯', 'Arterial and Venous Doppler for one L L', 6, '2017-06-03 08:08:09', '2017-06-03 08:08:09'),
(224, 'ط·آ¯ط¸ث†ط·آ¨ط¸â€‍ط·آ± ط·آ§ط¸ث†ط·آ±ط·آ¯ط·آ© ط·آ·ط·آ±ط¸ظ¾ط¸ظ¹ط¸ظ¹ط¸â€ ', 'Venous Doppler for both L L', 6, '2017-06-03 08:08:09', '2017-06-03 08:08:09'),
(225, 'ط·آ¯ط¸ث†ط·آ¨ط¸â€‍ط·آ± ط·آ´ط·آ±ط·آ§ط¸ظ¹ط¸ظ¹ط¸â€  ط·آ·ط·آ±ط¸ظ¾ط¸ظ¹ط¸ظ¹ط¸â€  ', 'Arterial Doppler for Both L L', 6, '2017-06-03 08:08:09', '2017-06-03 08:08:09'),
(226, 'ط·آ¯ط¸ث†ط·آ¨ط¸â€‍ط·آ± ط·آ§ط¸ث†ط·آ±ط·آ¯ط·آ© ط¸ث† ط·آ´ط·آ±ط·آ§ط¸ظ¹ط¸ظ¹ط¸â€  ط·آ·ط·آ±ط¸ظ¾ط¸ظ¹ط¸ظ¹ط¸â€  ', 'Arterial and Venous Doppler for Both L L', 6, '2017-06-03 08:08:09', '2017-06-03 08:08:09'),
(227, 'ط·آ¯ط¸ث†ط·آ¨ط¸â€‍ط·آ± ط·آ§ط¸â€‍ط·آ¹ط·آ¶ط¸ث† ط·آ§ط¸â€‍ط·آ°ط¸ئ’ط·آ±ط¸â€°', 'Penile Doppler', 6, '2017-06-03 08:08:09', '2017-06-03 08:08:09'),
(228, 'ط·آ¯ط¸ث†ط·آ¨ط¸â€‍ط·آ± ط·آ¹ط¸â€‍ط¸â€° ط¸â€¦ط¸ظ¾ط·آµط¸â€‍ ط·آ¹ط·آ¸ط¸â€¦ط¸â€°', 'Joints & Limbs', 6, '2017-06-03 08:08:09', '2017-06-03 08:08:09'),
(229, 'ط·آ¯ط¸ث†ط·آ¨ط¸â€‍ط·آ± ط·آ¹ط¸â€‍ط¸â€° ط·آ§ط¸â€‍ط·آ¹ط·آ¶ط¸â€‍ط·آ§ط·ع¾', 'Muscles', 6, '2017-06-03 08:08:09', '2017-06-03 08:08:09'),
(230, 'ط¸ئ’ط·آ§ط¸â€¦ط¸â€‍ ط·آ§ط¸â€‍ط·آ¬ط·آ³ط¸â€¦', 'Total Body Dexa', 7, '2017-06-03 08:08:09', '2017-06-03 08:08:09'),
(231, 'ط·آ§ط¸â€‍ط·آ«ط·آ¯ط¸ظ¹ط¸ظ¹ط¸â€ ', 'Both Breasts', 8, '2017-06-03 08:08:09', '2017-06-03 08:08:09'),
(232, 'ط·آ«ط·آ¯ط¸â€° ط·آ¬ط·آ§ط¸â€ ط·آ¨ ط¸ث†ط·آ§ط·آ­ط·آ¯', 'Breast (One Side)', 8, '2017-06-03 08:08:09', '2017-06-03 08:08:09'),
(233, 'ط·آ¨ط·آ§ط¸â€ ط¸ث†ط·آ±ط·آ§ط¸â€¦ط·آ§ ط·آ§ط¸â€‍ط·آ§ط·آ³ط¸â€ ط·آ§ط¸â€  ط·آ§ط¸â€‍ط·آ±ط¸â€ڑط¸â€¦ط¸â€°', 'Digital Panorama', 9, '2017-06-03 08:08:09', '2017-06-03 08:08:09'),
(234, 'ط·آ³ط¸ظ¹ط¸ظ¾ط·آ§ط¸â€‍ط¸ث†ط¸â€¦ط·ع¾ط·آ±ط¸â€° ', 'Cephalometry', 9, '2017-06-03 08:08:10', '2017-06-03 08:08:10');

-- --------------------------------------------------------

--
-- Table structure for table `radiology_type_categories`
--

DROP TABLE IF EXISTS `radiology_type_categories`;
CREATE TABLE IF NOT EXISTS `radiology_type_categories` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `en_name` text COLLATE utf8_unicode_ci NOT NULL,
  `ar_name` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `radiology_type_categories`
--

INSERT INTO `radiology_type_categories` (`id`, `en_name`, `ar_name`, `created_at`, `updated_at`) VALUES
(1, 'Closed MRI 1.5 Tesla', 'ط·آ§ط¸â€‍ط·آ±ط¸â€ ط¸ظ¹ط¸â€  ط·آ§ط¸â€‍ط¸â€¦ط·ط›ط¸â€ ط·آ§ط·آ·ط¸ظ¹ط·آ³ط¸â€° ط·آ§ط¸â€‍ط¸â€¦ط·ط›ط¸â€‍ط¸â€ڑ 1.5 ط·ع¾ط·آ³ط¸â€‍ط·آ§', '2017-06-03 08:08:00', '2017-06-03 08:08:00'),
(2, 'MRI Open', 'ط¸ظ¾ط·آ­ط¸ث†ط·آµط·آ§ط·ع¾', '2017-06-03 08:08:03', '2017-06-03 08:08:03'),
(3, 'Multi Slice CT', 'ط·آ§ط¸â€‍ط·آ§ط·آ´ط·آ¹ط·آ© ط·آ§ط¸â€‍ط¸â€¦ط¸â€ڑط·آ·ط·آ¹ط¸ظ¹ط·آ© ط¸â€¦ط·ع¾ط·آ¹ط·آ¯ط·آ¯ط·آ© ط·آ§ط¸â€‍ط¸â€¦ط¸â€ڑط·آ§ط·آ·ط·آ¹', '2017-06-03 08:08:04', '2017-06-03 08:08:04'),
(4, 'X-Ray', 'ط·آ§أ¯آ»آ·ط·آ´ط·آ¹ط·آ© ط·آ§ط¸â€‍ط·آ¹ط·آ§ط·آ¯ط¸ظ¹ط·آ© - ط·آ£ط·آ´ط·آ¹ط·آ© ط·آ£ط¸ئ’ط·آ³', '2017-06-03 08:08:07', '2017-06-03 08:08:07'),
(5, 'Ultra Sound', 'ط·آ§ط¸â€‍ط¸â€¦ط¸ث†ط·آ¬ط·آ§ط·ع¾ ط¸ظ¾ط¸ث†ط¸â€ڑ ط·آ§ط¸â€‍ط·آµط¸ث†ط·ع¾ط¸ظ¹ط·آ©', '2017-06-03 08:08:08', '2017-06-03 08:08:08'),
(6, 'Doppler', 'ط·آ§ط¸â€‍ط¸â€¦ط¸ث†ط·آ¬ط·آ§ط·ع¾ ط¸ظ¾ط¸ث†ط¸â€ڑ ط·آ§ط¸â€‍ط·آµط¸ث†ط·ع¾ط¸ظ¹ط·آ© ط·آ§ط¸â€‍ط¸â€¦ط¸â€‍ط¸ث†ط¸â€ ط·آ©', '2017-06-03 08:08:09', '2017-06-03 08:08:09'),
(7, 'DEXA', 'ط¸â€ڑط¸ظ¹ط·آ§ط·آ³ ط¸ئ’ط·آ«ط·آ§ط¸ظ¾ط·آ© ط·آ§ط¸â€‍ط·آ¹ط·آ¸ط·آ§ط¸â€¦', '2017-06-03 08:08:09', '2017-06-03 08:08:09'),
(8, 'Mamogram + US', 'ط·آ§ط¸â€‍ط¸â€¦ط·آ§ط¸â€¦ط¸ث†ط·آ¬ط·آ±ط·آ§ط¸â€¦', '2017-06-03 08:08:09', '2017-06-03 08:08:09'),
(9, 'Panorama', 'ط·آ§ط¸â€‍ط·آ¨ط·آ§ط¸â€ ط¸ث†ط·آ±ط·آ§ط¸â€¦ط·آ§', '2017-06-03 08:08:09', '2017-06-03 08:08:09');

-- --------------------------------------------------------

--
-- Table structure for table `request_answers`
--

DROP TABLE IF EXISTS `request_answers`;
CREATE TABLE IF NOT EXISTS `request_answers` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `request_id` int(10) UNSIGNED NOT NULL,
  `question_id` int(10) UNSIGNED NOT NULL,
  `answer` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `request_answers_request_id_foreign` (`request_id`),
  KEY `request_answers_question_id_foreign` (`question_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `request_answers`
--

INSERT INTO `request_answers` (`id`, `request_id`, `question_id`, `answer`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, NULL, NULL),
(2, 1, 2, 0, NULL, NULL),
(3, 1, 3, 1, NULL, NULL),
(4, 1, 4, 1, NULL, NULL),
(5, 1, 5, 1, NULL, NULL),
(6, 1, 6, 0, NULL, NULL),
(7, 2, 1, 0, NULL, NULL),
(8, 2, 2, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `response_centers`
--

DROP TABLE IF EXISTS `response_centers`;
CREATE TABLE IF NOT EXISTS `response_centers` (
  `center_id` int(10) UNSIGNED NOT NULL,
  `response_id` int(10) UNSIGNED NOT NULL,
  `arrive_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `response_centers_center_id_foreign` (`center_id`),
  KEY `response_centers_response_id_foreign` (`response_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `response_centers`
--

INSERT INTO `response_centers` (`center_id`, `response_id`, `arrive_datetime`, `created_at`, `updated_at`) VALUES
(1, 92, '2017-12-31 10:59:00', '2017-06-09 08:09:31', '2017-06-09 08:09:31'),
(2, 92, '2017-12-31 10:59:00', '2017-06-09 08:09:31', '2017-06-09 08:09:31'),
(1, 93, '2017-12-31 10:59:00', '2017-06-09 08:14:34', '2017-06-09 08:14:34'),
(2, 93, '2017-12-31 10:59:00', '2017-06-09 08:14:35', '2017-06-09 08:14:35'),
(1, 99, '2017-12-31 10:59:00', '2017-06-09 09:05:04', '2017-06-09 09:05:04'),
(2, 99, '2017-01-31 10:59:00', '2017-06-09 09:05:04', '2017-06-09 09:05:04'),
(1, 101, '2017-12-31 10:59:00', '2017-06-09 19:36:08', '2017-06-09 19:36:08'),
(3, 101, '2017-12-31 10:58:00', '2017-06-09 19:36:08', '2017-06-09 19:36:08'),
(1, 102, '2016-12-31 23:00:00', '2017-06-12 13:33:12', '2017-06-12 13:33:12'),
(2, 103, '2017-12-31 10:59:00', '2017-06-12 13:35:54', '2017-06-12 13:35:54'),
(2, 109, '2017-12-31 10:59:00', '2017-06-12 14:16:32', '2017-06-12 14:16:32'),
(2, 110, '2017-12-31 10:59:00', '2017-06-12 15:52:43', '2017-06-12 15:52:43'),
(2, 111, '2017-12-31 10:59:00', '2017-06-13 09:27:05', '2017-06-13 09:27:05'),
(1, 112, '2017-12-31 10:59:00', '2017-06-13 09:32:09', '2017-06-13 09:32:09'),
(2, 112, '2017-12-31 10:59:00', '2017-06-13 09:32:09', '2017-06-13 09:32:09'),
(2, 120, '2017-12-31 10:59:00', '2017-06-13 09:57:43', '2017-06-13 09:57:43');

-- --------------------------------------------------------

--
-- Table structure for table `response_radiology_items`
--

DROP TABLE IF EXISTS `response_radiology_items`;
CREATE TABLE IF NOT EXISTS `response_radiology_items` (
  `type_id` int(10) UNSIGNED NOT NULL,
  `response_id` int(10) UNSIGNED NOT NULL,
  `price` decimal(8,2) NOT NULL DEFAULT '0.00',
  `definition` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `preparation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `response_radiology_items_type_id_foreign` (`type_id`),
  KEY `response_radiology_items_response_id_foreign` (`response_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `response_radiology_items`
--

INSERT INTO `response_radiology_items` (`type_id`, `response_id`, `price`, `definition`, `preparation`, `notes`, `created_at`, `updated_at`) VALUES
(179, 38, '99999.00', '', '', '', '2017-06-08 21:10:02', '2017-06-08 21:10:02'),
(179, 39, '99999.00', '', '', '', '2017-06-08 21:21:18', '2017-06-08 21:21:18'),
(179, 39, '99999.00', '', '', '', '2017-06-08 21:21:18', '2017-06-08 21:21:18'),
(179, 40, '99999.00', '', '', '', '2017-06-08 21:21:43', '2017-06-08 21:21:43'),
(46, 46, '0.00', NULL, NULL, NULL, '2017-06-09 00:22:28', '2017-06-09 00:22:28'),
(121, 46, '0.00', NULL, NULL, NULL, '2017-06-09 00:22:28', '2017-06-09 00:22:28'),
(48, 48, '0.00', NULL, NULL, NULL, '2017-06-09 00:28:34', '2017-06-09 00:28:34'),
(121, 48, '0.00', NULL, NULL, NULL, '2017-06-09 00:28:34', '2017-06-09 00:28:34'),
(49, 49, '0.00', NULL, NULL, NULL, '2017-06-09 00:30:51', '2017-06-09 00:30:51'),
(121, 49, '0.00', NULL, NULL, NULL, '2017-06-09 00:30:51', '2017-06-09 00:30:51'),
(209, 84, '0.00', NULL, NULL, NULL, '2017-06-09 07:47:53', '2017-06-09 07:47:53'),
(209, 85, '9844.00', '9494949', '9494949', '9494949', '2017-06-09 07:48:45', '2017-06-09 07:48:45'),
(209, 86, '9844.00', '9494949', '9494949', '9494949', '2017-06-09 07:49:21', '2017-06-09 07:49:21'),
(209, 88, '9844.00', '9494949', '9494949', '49494949', '2017-06-09 07:52:01', '2017-06-09 07:52:01'),
(209, 89, '9844.00', '9494949', '9494949', '49494949', '2017-06-09 07:53:44', '2017-06-09 07:53:44'),
(209, 90, '9844.00', '9494949', '9494949', '49494949', '2017-06-09 08:08:46', '2017-06-09 08:08:46'),
(209, 91, '9844.00', '9494949', '9494949', '49494949', '2017-06-09 08:09:10', '2017-06-09 08:09:10'),
(209, 92, '9844.00', '9494949', '9494949', '49494949', '2017-06-09 08:09:31', '2017-06-09 08:09:31'),
(209, 93, '9844.00', '9494949', '9494949', '49494949', '2017-06-09 08:14:34', '2017-06-09 08:14:34'),
(218, 94, '1000.00', 'go', 'to', 'hell', '2017-06-09 08:26:08', '2017-06-09 08:26:08'),
(114, 99, '4444.00', 'ijijij', 'llkllk', 'lklklklk', '2017-06-09 09:05:04', '2017-06-09 09:05:04'),
(116, 101, '222.00', 'sdsdsd', 'sddsdsd', 'sdsdsd', '2017-06-09 19:36:08', '2017-06-09 19:36:08'),
(210, 102, '5678.00', '6889jjkjkjkj', 'kjkjkjkjkj', 'kjkjkjkjkkj', '2017-06-12 13:33:11', '2017-06-12 13:33:11'),
(121, 103, '789.00', 'hoka', 'hoka', 'hoka boka', '2017-06-12 13:35:54', '2017-06-12 13:35:54'),
(173, 109, '4444.00', '', '', '', '2017-06-12 14:16:32', '2017-06-12 14:16:32'),
(225, 110, '890.00', '', '', '', '2017-06-12 15:52:43', '2017-06-12 15:52:43'),
(179, 111, '44.00', '', '', '', '2017-06-13 09:27:05', '2017-06-13 09:27:05'),
(223, 112, '1234.00', '', '', '', '2017-06-13 09:32:09', '2017-06-13 09:32:09'),
(176, 120, '0.00', '', '', '', '2017-06-13 09:57:43', '2017-06-13 09:57:43');

-- --------------------------------------------------------

--
-- Table structure for table `search_sort_by`
--

DROP TABLE IF EXISTS `search_sort_by`;
CREATE TABLE IF NOT EXISTS `search_sort_by` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `title_ar` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `search_sort_by`
--

INSERT INTO `search_sort_by` (`id`, `title`, `title_ar`) VALUES
(1, 'Experience', ''),
(3, 'nearest', '');

-- --------------------------------------------------------

--
-- Table structure for table `specialities`
--

DROP TABLE IF EXISTS `specialities`;
CREATE TABLE IF NOT EXISTS `specialities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title_en` varchar(45) DEFAULT NULL,
  `title_ar` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `specialities`
--

INSERT INTO `specialities` (`id`, `title_en`, `title_ar`) VALUES
(1, 'internal medicin', NULL),
(2, 'cardiology', NULL),
(3, 'ophthalmology ', NULL),
(4, 'urology ', NULL),
(5, 'oncology ', NULL),
(6, 'nuclearMedicine ', NULL),
(7, 'pediatrics ', NULL),
(8, 'general surgery', NULL),
(9, 'GI surgery', NULL),
(10, 'nuro surgery', NULL),
(11, 'neurology ', NULL),
(12, 'psychology ', NULL),
(13, 'orthopidics ', NULL),
(14, 'hand surgery', NULL),
(15, 'knee surgery', NULL),
(16, 'hip surgery ', NULL),
(17, 'radiology ', NULL),
(18, 'pain therapy', NULL),
(19, 'nutritionist ', NULL),
(20, 'physical medicine doctor ', NULL),
(21, 'physioTherapy ', NULL),
(22, 'dermatology ', NULL),
(23, 'ENT ', NULL),
(24, 'emergency doctor', NULL),
(25, 'diabetes ', NULL),
(26, 'obstetrics and gynecology', NULL),
(27, 'plastic surgery ', NULL),
(28, 'chest ', NULL),
(29, 'General ', NULL),
(30, 'dental ', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `statusmessages`
--

DROP TABLE IF EXISTS `statusmessages`;
CREATE TABLE IF NOT EXISTS `statusmessages` (
  `sid` int(4) NOT NULL AUTO_INCREMENT COMMENT 'Status id',
  `statusNumber` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 - success, 1 - error',
  `statusMessage` varchar(400) CHARACTER SET utf8 NOT NULL COMMENT 'brief status message',
  PRIMARY KEY (`sid`)
) ENGINE=InnoDB AUTO_INCREMENT=407 DEFAULT CHARSET=latin1 COMMENT='status messages for the appl response';

--
-- Dumping data for table `statusmessages`
--

INSERT INTO `statusmessages` (`sid`, `statusNumber`, `statusMessage`) VALUES
(1, 1, 'Patient not logged in'),
(2, 1, 'Email Already Reigstered'),
(406, 1, 'Patient not logged in');

-- --------------------------------------------------------

--
-- Table structure for table `temp_phones`
--

DROP TABLE IF EXISTS `temp_phones`;
CREATE TABLE IF NOT EXISTS `temp_phones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `patient_id` int(11) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `urls`
--

DROP TABLE IF EXISTS `urls`;
CREATE TABLE IF NOT EXISTS `urls` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `href` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url_group_id` int(10) UNSIGNED NOT NULL,
  `order` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `urls_url_group_id_foreign` (`url_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `urls`
--

INSERT INTO `urls` (`id`, `name`, `href`, `url_group_id`, `order`, `created_at`, `updated_at`) VALUES
(3, 'List Requests', 'patientrequests', 4, 0, '2017-06-03 11:37:09', '2017-06-03 11:37:09'),
(4, 'List Users', 'user', 5, 0, '2017-06-03 11:39:18', '2017-06-03 11:39:18'),
(5, 'Create User', 'user/create', 5, 1, '2017-06-03 11:39:18', '2017-06-03 11:39:18'),
(9, 'List Responses', 'centerresponses', 12, 1, '2017-06-03 11:39:18', '2017-06-03 11:39:18');

-- --------------------------------------------------------

--
-- Table structure for table `url_groups`
--

DROP TABLE IF EXISTS `url_groups`;
CREATE TABLE IF NOT EXISTS `url_groups` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `href` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '#',
  `order` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `url_groups`
--

INSERT INTO `url_groups` (`id`, `name`, `href`, `order`, `created_at`, `updated_at`) VALUES
(4, 'Requests', '#', 1, '2017-06-03 11:37:09', '2017-06-03 11:37:09'),
(5, 'Users', '#', 0, '2017-06-03 11:39:18', '2017-06-03 11:39:18'),
(12, 'Responses', '#', 2, '2017-06-12 11:15:06', '2017-06-12 11:15:08');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_type_id` int(10) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_user_type_id_foreign` (`user_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `user_type_id`, `username`) VALUES
(3, 'admin', NULL, '$2y$10$YDKRykKXsqhkmK.XfSENk.2FRnzbG85JD6gXOmBkriDZJvbjo6XiW', 'o1Klr9LOqoSns5mBggOKjp5EX9tqasHp9sF8Ep9sArfUsRTQjGSzi2ep5YpI', '2017-06-03 08:35:09', '2017-06-05 05:10:23', 1, 'admin'),
(4, 'Mohamed', 'mohamedtalaatzaki@gmail.com', '$2y$10$TRaJji1YLSC80v/vx1tpV.9IlzqmBNqJ2AvFG6tQshbdP0PTMzkV6', NULL, '2017-06-11 07:53:05', '2017-06-11 07:53:05', 1, 'mohamedtalaatzaki'),
(5, 'Mohamed', 'mohamed.talaat@sanaTechnology.com', '$2y$10$9Bv93ZqWGs.juCMXGtXaPuXf1xPsYGlczcOdenrts9qHSyrnLtHHW', NULL, '2017-06-11 08:12:30', '2017-06-11 08:12:30', 5, 'Mtalaat');

-- --------------------------------------------------------

--
-- Table structure for table `user_types`
--

DROP TABLE IF EXISTS `user_types`;
CREATE TABLE IF NOT EXISTS `user_types` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_types_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_types`
--

INSERT INTO `user_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'administrator', '2017-06-03 08:35:09', '2017-06-03 08:35:09'),
(5, 'accountant', '2017-06-03 08:37:24', '2017-06-03 08:37:24'),
(6, 'employee', '2017-06-03 08:37:24', '2017-06-03 08:37:24'),
(7, 'Doctors', '2017-06-11 12:30:32', '2017-06-11 12:30:32');

-- --------------------------------------------------------

--
-- Table structure for table `user_type_url`
--

DROP TABLE IF EXISTS `user_type_url`;
CREATE TABLE IF NOT EXISTS `user_type_url` (
  `user_type_id` int(10) UNSIGNED NOT NULL,
  `url_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `user_type_url_url_id_foreign` (`url_id`),
  KEY `user_type_url_user_type_id_foreign` (`user_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_type_url`
--

INSERT INTO `user_type_url` (`user_type_id`, `url_id`, `created_at`, `updated_at`) VALUES
(1, 3, NULL, NULL),
(1, 4, NULL, NULL),
(1, 5, NULL, NULL);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_patient`
--
DROP VIEW IF EXISTS `v_patient`;
CREATE TABLE IF NOT EXISTS `v_patient` (
`PatientCode` int(11)
,`PatientName` varchar(45)
,`PatientGender` varchar(45)
,`email` varchar(45)
,`Phones` text
,`Addresses` text
,`MapAddresses` text
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_patientanswers`
--
DROP VIEW IF EXISTS `v_patientanswers`;
CREATE TABLE IF NOT EXISTS `v_patientanswers` (
`RequestNo` int(10) unsigned
,`EnQusestion` varchar(255)
,`QuestionAnswerBool` tinyint(1)
,`QuestionAnswerStr` varchar(3)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_patientrequest`
--
DROP VIEW IF EXISTS `v_patientrequest`;
CREATE TABLE IF NOT EXISTS `v_patientrequest` (
`PatientCode` int(11)
,`PatientName` varchar(45)
,`PatientGender` varchar(45)
,`email` varchar(45)
,`Phones` text
,`Addresses` text
,`MapAddresses` text
,`Age` int(10) unsigned
,`Weight` varchar(13)
,`is_at_home` tinyint(1)
,`Location` varchar(9)
,`Stable` varchar(3)
,`RequestDate` varchar(19)
,`Uploads` text
,`RequestNo` int(10) unsigned
,`status` char(2)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_patientrequestuploads`
--
DROP VIEW IF EXISTS `v_patientrequestuploads`;
CREATE TABLE IF NOT EXISTS `v_patientrequestuploads` (
`request_id` int(10) unsigned
,`PrescriptionImages` text
);

-- --------------------------------------------------------

--
-- Structure for view `v_patient`
--
DROP TABLE IF EXISTS `v_patient`;

CREATE VIEW `v_patient`  AS  select `pd`.`id` AS `PatientCode`,`pd`.`name` AS `PatientName`,`pd`.`gender` AS `PatientGender`,`pd`.`email` AS `email`,group_concat(trim(concat(`pp`.`country_code`,'-',`pp`.`phone`)) separator ',') AS `Phones`,group_concat(trim(`pa`.`address`) separator ',') AS `Addresses`,group_concat(concat('(',`pa`.`latitude`,',',`pa`.`longitude`,')') separator ',') AS `MapAddresses` from ((`patient` `pd` left join `patient_phone` `pp` on((`pd`.`id` = `pp`.`patient_id`))) left join `patient_addresses` `pa` on((`pd`.`id` = `pa`.`patient_id`))) group by `pd`.`id`,`pd`.`name`,`pd`.`gender`,`pd`.`email` ;

-- --------------------------------------------------------

--
-- Structure for view `v_patientanswers`
--
DROP TABLE IF EXISTS `v_patientanswers`;

CREATE VIEW `v_patientanswers`  AS  select `a`.`request_id` AS `RequestNo`,`q`.`en_name` AS `EnQusestion`,`a`.`answer` AS `QuestionAnswerBool`,(case when (`a`.`answer` = 0) then 'No' else 'Yes' end) AS `QuestionAnswerStr` from (`request_answers` `a` left join `radiology_questions` `q` on((`a`.`question_id` = `q`.`id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `v_patientrequest`
--
DROP TABLE IF EXISTS `v_patientrequest`;

CREATE VIEW `v_patientrequest`  AS  select `p`.`PatientCode` AS `PatientCode`,`p`.`PatientName` AS `PatientName`,`p`.`PatientGender` AS `PatientGender`,`p`.`email` AS `email`,`p`.`Phones` AS `Phones`,`p`.`Addresses` AS `Addresses`,`p`.`MapAddresses` AS `MapAddresses`,`r`.`patient_age` AS `Age`,concat(`r`.`patient_weight`,' KG') AS `Weight`,`r`.`is_at_home` AS `is_at_home`,(case when (`r`.`is_at_home` = 0) then 'At Center' else 'At Home' end) AS `Location`,(case when (`r`.`is_able_to_be_stable` = 0) then 'No' else 'Yes' end) AS `Stable`,date_format(`r`.`created_at`,'%d-%m-%Y %h:%i %p') AS `RequestDate`,`u`.`PrescriptionImages` AS `Uploads`,`r`.`id` AS `RequestNo`,`r`.`status` AS `status` from ((`patient_requests` `r` left join `v_patient` `p` on((`r`.`patient_id` = `p`.`PatientCode`))) left join `v_patientrequestuploads` `u` on((`r`.`id` = `u`.`request_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `v_patientrequestuploads`
--
DROP TABLE IF EXISTS `v_patientrequestuploads`;

CREATE VIEW `v_patientrequestuploads`  AS  select `patient_uploads`.`request_id` AS `request_id`,group_concat(trim(concat(`patient_uploads`.`image_url`)) separator ',') AS `PrescriptionImages` from `patient_uploads` group by `patient_uploads`.`request_id` ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `center_phones`
--
ALTER TABLE `center_phones`
  ADD CONSTRAINT `center_phones_center_id_foreign` FOREIGN KEY (`center_id`) REFERENCES `centers` (`id`);

--
-- Constraints for table `center_responses`
--
ALTER TABLE `center_responses`
  ADD CONSTRAINT `center_responses_request_id_foreign` FOREIGN KEY (`request_id`) REFERENCES `patient_requests` (`id`);

--
-- Constraints for table `patient_requests`
--
ALTER TABLE `patient_requests`
  ADD CONSTRAINT `patient_requests_patient_address_id_foreign` FOREIGN KEY (`patient_address_id`) REFERENCES `patient_addresses` (`id`),
  ADD CONSTRAINT `patient_requests_patient_id_foreign` FOREIGN KEY (`patient_id`) REFERENCES `patient` (`id`);

--
-- Constraints for table `patient_uploads`
--
ALTER TABLE `patient_uploads`
  ADD CONSTRAINT `patient_uploads_request_id_foreign` FOREIGN KEY (`request_id`) REFERENCES `patient_requests` (`id`);

--
-- Constraints for table `radiology_types`
--
ALTER TABLE `radiology_types`
  ADD CONSTRAINT `radiology_types_type_group_id_foreign` FOREIGN KEY (`type_group_id`) REFERENCES `radiology_type_categories` (`id`);

--
-- Constraints for table `request_answers`
--
ALTER TABLE `request_answers`
  ADD CONSTRAINT `request_answers_question_id_foreign` FOREIGN KEY (`question_id`) REFERENCES `radiology_questions` (`id`),
  ADD CONSTRAINT `request_answers_request_id_foreign` FOREIGN KEY (`request_id`) REFERENCES `patient_requests` (`id`);

--
-- Constraints for table `response_centers`
--
ALTER TABLE `response_centers`
  ADD CONSTRAINT `response_centers_center_id_foreign` FOREIGN KEY (`center_id`) REFERENCES `centers` (`id`),
  ADD CONSTRAINT `response_centers_response_id_foreign` FOREIGN KEY (`response_id`) REFERENCES `center_responses` (`id`);

--
-- Constraints for table `response_radiology_items`
--
ALTER TABLE `response_radiology_items`
  ADD CONSTRAINT `response_radiology_items_response_id_foreign` FOREIGN KEY (`response_id`) REFERENCES `center_responses` (`id`),
  ADD CONSTRAINT `response_radiology_items_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `radiology_types` (`id`);

--
-- Constraints for table `urls`
--
ALTER TABLE `urls`
  ADD CONSTRAINT `urls_url_group_id_foreign` FOREIGN KEY (`url_group_id`) REFERENCES `url_groups` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_user_type_id_foreign` FOREIGN KEY (`user_type_id`) REFERENCES `user_types` (`id`);

--
-- Constraints for table `user_type_url`
--
ALTER TABLE `user_type_url`
  ADD CONSTRAINT `user_type_url_url_id_foreign` FOREIGN KEY (`url_id`) REFERENCES `urls` (`id`),
  ADD CONSTRAINT `user_type_url_user_type_id_foreign` FOREIGN KEY (`user_type_id`) REFERENCES `user_types` (`id`);

